import os
from setuptools import find_packages, setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='stem-qe-common',
    packages=find_packages(),
    version=os.environ.get('BUILD_VERSION', '1.0.0'),
    author='Stem Inc.',
    url='https://bitbucket.org/stemedge/qe/src',
    description='Stem QE Source',
    long_description='',
    license='Stem Inc.',
    install_requires=required,
    dependency_links=[],
    include_package_data=True,
    classifiers=[
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Operating System :: MacOS :: MacOS X",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ]
)

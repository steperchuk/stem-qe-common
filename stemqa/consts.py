# controls if responses to REST APIs should be logged
LOG_RESPONSE = False

# web
DEFAULT_USERNAME = 'irina.elent'
DEFAULT_PASSWORD = 'Password1'
SUPERUSER_USERNAME = 'stem_superuser'

# dbhelper
DB_ACCESS_RETRY_CNT = 10
DB_ACCESS_DELAY_SEC = 5

AWS_REGION = 'us-west-2'
AWS_ACCESS_KEY_ID_QE_READ_ONLY = 'AKIAJZDER5L6PECKREHQ'
AWS_SECRET_ACCESS_KEY_QE_READ_ONLY = 'f2jlNb8Bci985Y7tmBah/ks2VzQuRUMj4hAl0DGP'
AWS_DEFAULT_BUCKET = 'stemqa-sampledata'

# historical data on s3
S3_FOLDER_MONITOR_DATA_15_MIN = "s3://stemqa-sampledata/csv_import/meter_logs/realtime/monitor_data_15_minute"
S3_WEATHER_DATA_FILE = "csv_import/forecast/realtime/weather_data/weather_data.json"
S3_FULL_WEATHER_DATA_FILE = "csv_import/forecast/realtime/weather_data/weather_data_prod_full.json"
S3_SIMULATION_DATA_FILE = 'csv_import/meter_logs/master/monitor_data_15_minute/simulation_15min_interval_yearly.csv'

# RMQ message count
MQ_MSG_BILLING_START = 5  # Number of messages OE sends to RMQ if it is running day before billing cycle starts.
MQ_MSG_NOT_START = 3  # Number of messages OE sends to RMQ if it is running at any other day.
# 6 is the default messages count for solution with power snap messages:
# update_multi_setpoints, sync_setpoint, set_tariff_data,
# set_battery_curves,apply_max_power_snap_offset, apply_max_power_snap_threshold
MQ_MSG_POWER_SNAP = 6

# weather
TYPE_HISTORICAL = 'historical'
TYPE_FORECAST = 'forecast'
REQUEST_RETRIES_COUNT = 5

SECONDS_IN_DAY = 86400
HOURS_IN_DAY = 24
SECONDS_IN_HOUR = 3600
ISO8601_TIME_FORMAT = "%Y-%m-%dT%H:%M:%S+00:00"
ISO_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
TIME_SHORT = "%Y-%m-%d"

DEFAULT_LATITUDE = 37.599003
DEFAULT_LONGITUDE = -122.382907

# tariffs
PGE_E19_TARIFF_ALT_ID = 81959

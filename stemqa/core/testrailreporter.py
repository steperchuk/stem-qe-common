# pylint: disable=E1121
import datetime
import os

from retry import retry
from testrail import TestRail
from testrail.helper import TestRailError, ServiceUnavailableError, TooManyRequestsError

from stemqa.config.settings import settings
from stemqa.config.logconfig import get_logger

logger = get_logger(__name__)


class TestRailReporter(object):
    """
    Reports results of test execution for automated tests to TestRail
    """

    automation_id_to_case_lookup = {}
    case_id_to_test_lookup = {}
    testrail = None

    @retry((TestRailError, TooManyRequestsError, ServiceUnavailableError), tries=5, delay=60, backoff=2)
    def __init__(self, enabled, plan_id):
        """
        :param enabled: if enabled or not
        :param plan_id: test plan id
        """
        if not enabled or not plan_id:
            logger.debug('TestRail reporting is disabled. Either Test Plan Id is '
                         'not specified or Reporting is not enabled')
            return

        if not os.environ.get('TESTRAIL_USER_EMAIL') or not os.environ.get('TESTRAIL_USER_KEY'):
            raise SystemError(
                'TestRail reporting requires TESTRAIL_USER_EMAIL and TESTRAIL_USER_KEY environment variables')

        os.environ['TESTRAIL_URL'] = settings['testrail']['url']
        project_id = int(settings['testrail']['project-id'])
        self.testrail = TestRail(project_id=project_id)

        self.populate_caches(plan_id)

    @retry((TestRailError, TooManyRequestsError, ServiceUnavailableError), tries=5, delay=60, backoff=2)
    def report(self, report):
        """
        Reports results to TestRail
        :param report: pytest test report object
        :return:
        """
        if not self.testrail:
            return

        tr_outcome = None
        nodeid = report.nodeid
        when = report.when
        outcome = report.outcome
        data = {
            'nodeid': nodeid,
            'automation_id': nodeid[nodeid.rfind('::') + 2:],
            'duration': report.duration,
            'when': when,
            'exception': report.longrepr
        }

        if when == 'setup':
            if outcome in ['failed', 'error']:
                tr_outcome = 'failed'
            elif outcome == 'skipped':
                tr_outcome = 'skipped'
            elif outcome != 'passed':
                logger.warn('Unexpected outcome status for setup phase: %s', outcome)
        elif when == 'call':
            if outcome in ['failed', 'passed', 'skipped']:
                tr_outcome = outcome
            else:
                logger.warn('Unexpected outcome status for call phase: %s', outcome)
        elif when == 'teardown':
            if outcome == 'failed':
                tr_outcome = 'failed'
            elif outcome not in ['passed', 'skipped']:
                logger.warn('Unexpected outcome status for teardown phase: %s', outcome)
        else:
            raise SystemError('Unknown phase for pytest test execution: ' + when)

        if tr_outcome:
            data['outcome'] = tr_outcome
            try:
                self._report(data)
            except Exception:
                logger.exception('Ran into exception while reporting results to TestRail')

    def _report(self, data):
        """
        Report to TestRail
        :param data: map
        :return:
        """
        automation_id = data['automation_id']
        tr = self.testrail
        case_id = self.automation_id_to_case_lookup.get(automation_id)
        if not case_id:
            logger.error('Automation Id \'%s\' not found in TestRail Test Plan. '
                         'Check TestRail configuration.', automation_id)
            return

        result = tr.result()
        result.test = self.case_id_to_test_lookup[case_id]
        result.status = tr.status(data['outcome'])
        duration = int(data['duration'])
        duration = duration if duration > 0 else 1
        result._content['elapsed'] = str(duration) + 's'
        result.comment = self.prepare_comment(data, duration)
        tr.add(result)

    @staticmethod
    def prepare_comment(data, duration):
        """
        Creates a comment for TestRail test result
        :param data: map
        :param duration: duration of test (used to create kibana url
        :return:
        """
        env_name = str(settings.get('env-name', {}))
        kibana_host = settings['kibana']['url']
        comment = 'Test Id: `%s`\n' % data['nodeid']
        if os.getenv('BUILD_URL'):
            comment += 'Build Url: `%s`\n' % os.getenv('BUILD_URL')
        if env_name:
            comment += 'Target Env: `%s`\n' % env_name
        if os.getenv('NODE_NAME'):
            comment += 'Jenkins Node: `%s`\n' % os.getenv('NODE_NAME')
        if os.getenv('WORKSPACE'):
            comment += 'Jenkins Workspace: `%s`\n' % os.getenv('WORKSPACE')

        endtime = datetime.datetime.utcnow()
        starttime = endtime - datetime.timedelta(seconds=(duration + 60))  # end time - duration - 1minute
        endtime = endtime + datetime.timedelta(seconds=30)  # now + 30secs

        starttime_str = starttime.strftime('%Y-%m-%dT%H:%M:%S.000Z')
        endtime_str = endtime.strftime('%Y-%m-%dT%H:%M:%S.000Z')

        nothing_here_to_see = 'https://vignette.wikia.nocookie.net/forgeofempires/images/' \
                              '9/9c/Nothing_To_See_Here.jpg/revision/latest?cb=20180123200310'
        kibana_url = "[`Apps`]({}/_plugin/kibana/app/kibana#/discover?" \
                     "_g=(refreshInterval:(display:Off,pause:!f,value:0)," \
                     "time:(from:'{}',mode:absolute,to:'{}'))" \
                     "&_a=(columns:!(kubernetes.pod_name,log,kubernetes.namespace_name)," \
                     "index:'logstash-*',interval:auto," \
                     "query:(query_string:(analyze_wildcard:!t," \
                     "query:'kubernetes.namespace_name:%22{}%22%20AND%20log:*ERROR*'))," \
                     "sort:!('@timestamp',desc)))".format(kibana_host,
                                                          starttime_str,
                                                          endtime_str,
                                                          env_name)

        test_logs = '[`Test`](%s)' % nothing_here_to_see
        if os.getenv('BUILD_URL'):
            test_logs = "[`Test`]({}artifact/)".format(os.getenv('BUILD_URL'))
        comment += 'Logs: %s / %s\n' % (kibana_url, test_logs)

        exception = data['exception']
        if exception:
            when = data['when']
            comment += '\nTest failed with following error%s:\n\n%s' \
                       % (' during ' + when if when != 'call' else '',
                          '\n'.join(['\t\t' + line for line in str(exception).split('\n')]))

        logger.info('Comment:')
        logger.info(type(comment))
        logger.info(comment)
        logger.info('Comment:')
        return comment

    @retry((TestRailError, TooManyRequestsError, ServiceUnavailableError), tries=5, delay=60, backoff=2)
    def populate_caches(self, plan_id):
        """
        Populates caches needed while reporting on results
        :param plan_id:
        :return:
        """
        plan = self.testrail.plan(plan_id)
        if not plan:
            raise LookupError('Unable to find plan with id %s' % plan_id)
        entries = plan.entries

        suites = set()
        tests = []
        for entry in entries:
            for run in entry.runs:
                # using _content so that we dont pull complete suite just to get suite_id
                suites.add(run._content.get('suite_id'))
                tests.extend(self.testrail.tests(run))
        logger.info('Collected a total of %d tests in test plan', len(tests))

        assert len(suites) == 1, 'Unsupported configuration. More than one suites (%s) found in Test Plan...' % suites
        suite = self.testrail.suite(next(iter(suites)))

        cases = self.testrail.cases(suite)
        if not cases:
            logger.error('No cases returned for suite with id %s', suite.id)
        for case in cases:
            if case.type.name != 'Automated':
                continue
            automation_id = case.__getattr__('automation_id')
            if automation_id:
                self.automation_id_to_case_lookup[automation_id] = case.id
            else:
                logger.error('automationId not found for case %s titled %s', case.id, case.title)
        logger.info('Populated automation_id_to_case_lookup cache with %d keys',
                    len(self.automation_id_to_case_lookup))

        self.case_id_to_test_lookup = {test._content['case_id']: test for test in tests}
        logger.info('Populated case_id_to_test_lookup cache with %d keys',
                    len(self.case_id_to_test_lookup))

"""
Used to get test data from YAML files for data-driven tests
"""
import logging

import yaml

logger = logging.getLogger(__name__)


def load(*resources):
    """
    Loads the YAML file into test data objects. Each test data object is expected to have an 'id'. E.g.
    ------------------------
    ...
    ...
    - id: test1
      foo: bar
    - id: test2
      foo: abc
    ...
    ...
    ------------------------
    The result is two lists; test id list (tid_list) and test data list (td_list). These can then be passed
    into the data-driven test as using annotion as shown below:

    @pytest.mark.parameterize("testdata", td_list, ids=tid_list)
    def test_dd(testdata):
        pass

    :param resources: relative paths to one ore more YAML file
    :return: two lists; test id list and test data list.
    """
    tdlist = []
    tidlist = []
    for resource in resources:
        with open(resource, 'r') as stream:
            try:
                test_data_list = yaml.load(stream)
                for test_data in test_data_list:
                    tdlist.append(test_data)
                    tidlist.append(test_data['id'])
            except yaml.YAMLError:
                logger.exception('')
    return tidlist, tdlist

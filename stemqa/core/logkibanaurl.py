import datetime

from stemqa.config.settings import settings
from stemqa.config.logconfig import get_logger

logger = get_logger(__name__)


def log(report):
    """
    Logs the generated url to kibana logs if there is a test failure. Only for k8s environments.
    :param report:
    :return:
    """

    if report.outcome not in ['failed', 'error']:
        return

    endtime = datetime.datetime.utcnow()
    starttime = endtime - datetime.timedelta(seconds=(report.duration + 60))  # end time - duration - 1minute
    endtime = endtime + datetime.timedelta(seconds=30)  # now + 30secs

    starttime_str = starttime.strftime('%Y-%m-%dT%H:%M:%S.000Z')
    endtime_str = endtime.strftime('%Y-%m-%dT%H:%M:%S.000Z')

    env_name = settings['env-name']
    kibana_host = settings['kibana']['url']
    kibana_url = "{}/_plugin/kibana/app/kibana#/discover?" \
                 "_g=(refreshInterval:(display:Off,pause:!f,value:0)," \
                 "time:(from:'{}',mode:absolute,to:'{}'))" \
                 "&_a=(columns:!(kubernetes.pod_name,log,kubernetes.namespace_name)," \
                 "index:'logstash-*',interval:auto," \
                 "query:(query_string:(analyze_wildcard:!t," \
                 "query:'kubernetes.namespace_name:%22{}%22%20AND%20log:*ERROR*'))," \
                 "sort:!('@timestamp',desc))".format(kibana_host, starttime_str, endtime_str, env_name)

    logger.error('Kibana Logs: %s', kibana_url)

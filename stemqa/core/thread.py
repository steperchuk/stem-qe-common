"""
Extends the thread class so that if an exception occurs during the execution, we can
save it and then raise it in the join method. This way, we don't ignore exception that
occur in threads.
"""

import queue
import sys
import threading
import traceback


class ExcThread(threading.Thread):

    def __init__(self, target=None, name=None, args=()):
        threading.Thread.__init__(self, target=target, name=name, args=args)
        self.__status_queue = queue.Queue()
        self.exc_info = None

    def run(self):
        try:
            threading.Thread.run(self)
            self.__status_queue.put(None)
        except Exception:
            # print out at the point of error
            traceback.print_exc(file=sys.stderr)
            self.__status_queue.put(sys.exc_info())

    def join(self, timeout=None):
        threading.Thread.join(self, timeout)
        self.exc_info = self.__status_queue.get()
        if self.exc_info is None:
            return
        else:
            raise self.exc_info[0](
                "Caught exception while running thread: '{}'".format(self.exc_info[1])).with_traceback(self.exc_info[2])

    def get_exc_info(self):
        return self.exc_info

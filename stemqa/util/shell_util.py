import json
import logging
import os
import subprocess
import sys
import threading
import time

import yaml

__author__ = 'nullin'

logger = logging.getLogger(__name__)


class ShellUtilError(Exception):
    """
    Generic error raised by the functions in this module
    """

    def __init__(self, message):
        super(ShellUtilError, self).__init__(message)
        self.message = message

    def __str__(self):
        return self.message


def run_process(cmd, show_output=False, fail_on_error=True, timeout=3600):
    """
    Runs a shell command
    :param cmd: any shell command
    :param show_output: if True, show the output of running the command
    :param fail_on_error: in case of error, fail if True
    :param timeout: timeout for the command, in seconds
    :return:
    """
    level = logging.INFO if show_output else logging.DEBUG
    logger.log(level, 'Running command: %s', cmd)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, shell=True)
    append_str = '\tproc[{}]> '.format(threading.currentThread().name)
    output = ''
    if show_output:
        logger.info('-' * 10 + 'Output START' + '-' * 10)

    keep_running = True
    start_time = time.time()
    while keep_running:
        if time.time() > start_time + timeout:
            raise ShellUtilError('Timed out after waiting for %ssecs for command to run to completion.' % timeout)
        if p.poll() is not None:
            keep_running = False
        read = p.stdout.readline().decode('utf-8')
        while read:
            if time.time() > start_time + timeout:
                raise ShellUtilError('Timed out after waiting for %ssecs for command to run to completion.' % timeout)
            output += read
            if show_output:
                print((append_str + read[:-1]))
                sys.stdout.flush()
            read = p.stdout.readline().decode('utf-8')

    if show_output:
        logger.info('-' * 10 + 'Output END' + '-' * 10)

    if fail_on_error and p.returncode != 0:
        __print_error(show_output, p.returncode, output)
        raise ShellUtilError("CLI command '{}' failed with exit code: {}".format(cmd, str(p.returncode)))

    return output, p.returncode


def __print_error(show_output, returncode, output):
    """
    If show output is False and exit code is non-zero, this method prints the last 100 lines (or less)
    of the process output
    :param show_output: should output be printed
    :param returncode: return code for process execution
    :param output: output from process execution
    :return:
    """
    if not show_output and returncode != 0:
        append_str = '\tproc[{}]> '.format(threading.currentThread().name)
        # print last 100 lines of output to show error
        logger.info('---------------- OUTPUT START -----------------')
        logger.info('... (truncated)')
        logger.info('...')
        for line in output.splitlines()[-100:]:
            print((append_str + line))
            sys.stdout.flush()
        logger.info('---------------- OUTPUT END -----------------')


def __print_buffer(buf, print_last=False):
    """
    Helper method to print out the buffer. Splits the buffer into lines and prints out top n-1 lines
    followed by the last line, as needed.
    :param buf: buffer
    :param print_last: if false, prints out top n-1 lines, if true, also prints out the last line
    :return:
    """
    buf_arr = buf.split('\n')
    append_str = '\tssh[{}]> '.format(threading.currentThread().name)
    for line in buf_arr[:-1]:
        print((append_str + line))
        sys.stdout.flush()
    if print_last and buf_arr[-1]:
        print((append_str + buf_arr[-1]))
        sys.stdout.flush()
    return buf_arr[-1]


# TODO: following methods need to be cleaned up and moved into their own util file
def kubectl(environment, cmd, json_output=True, show_output=False, fail_on_error=True, timeout=3600):
    kube_config = '/root/.kube/config'
    if os.environ.get('STEM_CLI_CONFIG'):
        stem_cli_config = os.environ['STEM_CLI_CONFIG']
        cfg = yaml.safe_load(open(os.path.expanduser(stem_cli_config)))
        kube_config = cfg['kube.config'] if cfg.get('kube.config') else kube_config

    output, rc = run_process('kubectl --kubeconfig=%s -n=%s %s %s'
                             % (kube_config, environment, '-o=json' if json_output else '', cmd),
                             show_output=show_output, fail_on_error=fail_on_error, timeout=timeout)
    if json_output:
        output = json.loads(output)
    return output, rc


def get_pod_starts_with(name, env, func):
    resp, _ = kubectl(env, 'get pod --selector=app=%s' % func)
    for item in resp['items']:
        item_name = item['metadata']['name']
        if name in item_name:
            return item_name
    return None


def get_pod(env, func):
    resp, _ = kubectl(env, 'get pod --selector=app=%s' % func)
    return resp['items'][0]['metadata']['name']

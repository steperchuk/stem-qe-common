import json
import logging
from datetime import datetime, timedelta
from time import sleep

import redis

from stemqa.config.settings import settings
from stemqa.rabbitmq.publisherrmq import PublisherRmq

logger = logging.getLogger(__name__)

CACHE_OPERATIONS_TIMEOUT = 8
CACHE_CONNECTION_PROPERTIES = dict(host=settings['redis']['host'],
                                   port=settings['redis']['port-6379'],
                                   decode_responses=True)


def send_bms_telemetry_message(bms_hostname,
                               current_capacity,
                               timestamp=datetime.utcnow(),
                               available_energy_capacity=None,
                               current_charge=1,
                               delta_t=1,
                               delta_i=0
                               ):
    """
    Send BMS telemetry state message data to cache through message bus (ver. PS 1.x by default)
    :param bms_hostname: Battery Management System host name
    :param current_capacity: Available power, in kW
    :param timestamp: datetime Timestamp of bms telemetry message (utcnow by default)
    :param available_energy_capacity: Available energy in storage, in Wh i.e. SOC * SOH * Nameplate Capacity (if PS 3.x)
    :param current_charge: State Of Charge, as a fraction (1 - fully charged, 0 - discharged)
    :param delta_t: Nominal difference between successive samples
    :param delta_i: Indicates if there was a change in the battery system's charge for the prior time
     period measured in watt-hours. Positive - charge / Negative - discharge
    """
    logger.info("Send BMS telemetry message from site host: %s", bms_hostname)
    timestamp = timestamp.replace(second=0, microsecond=0).isoformat()
    payload = {
        "arr": [
            {
                "id": "pgx.hq.{}.bms.1.telemetry".format(bms_hostname),
                "data": [
                    {
                        "t": "%s" % timestamp,
                        "delta_t": delta_t,
                        "current_charge": current_charge,
                        "current_capacity": current_capacity,
                        "delta_i": delta_i
                    }
                ]
            }
        ]
    }
    if available_energy_capacity is not None:
        payload['arr'][0]['data'][0]['available_energy_capacity'] = available_energy_capacity
    PublisherRmq(settings['rmq']) \
        .publish_message(exchange='pgx',
                         routing_key='pgx.hq.{}.bms.1.telemetry'.format(bms_hostname),
                         payload=payload)


# Cache control methods: To aware that proper bms state stored from RabbitMQ to cache and to have ability to clean it
# in JEDI-13850 - was designed to consume last bms state from redis-cache for OE grid service
def get_bms_telemetry_cache(bms_hostname):
    """
    Retrieve BMS telemetry state from cache
    :param bms_hostname: Battery Management System (BMS) host name
    :return <dict> | {} - if value absent in cache
    """
    logger.info('Get bms telemetry state of [%s] from Redis cache', bms_hostname)
    cache = redis.Redis(**CACHE_CONNECTION_PROPERTIES)
    bms_telemetry_record = cache.hgetall('BMS_TELEMETRY:{}.bms.1'.format(bms_hostname))
    logger.debug('Redis values for bms host (%s):\n%s', bms_hostname, json.dumps(bms_telemetry_record, indent=2))
    return bms_telemetry_record


def remove_bms_telemetry_cache(bms_hostname):
    """
    Remove BMS telemetry state from cache
    :param bms_hostname: Battery Management System (BMS) host name
    """
    logger.info('Remove bms telemetry state of [%s] from Redis cache', bms_hostname)
    cache = redis.Redis(**CACHE_CONNECTION_PROPERTIES)
    cache.delete('BMS_TELEMETRY:{}.bms.1'.format(bms_hostname))


def wait_for_message_state(bms_hostname,
                           is_presence_in_cache_expected=True,
                           timeout=CACHE_OPERATIONS_TIMEOUT):
    """
    Wait for massage state in cache. If is_presence_in_cache_expected =True and and message will stored/appear in cache
    or if is_presence_in_cache_expected =False and and message removed from cache - wait operation will finished.
    else - will runs till timeout
    :param bms_hostname:  Battery Management System (BMS) host name
    :param is_presence_in_cache_expected: True - waiter expect that message will appear in cache
                                          False - waiter expect that message will deleted from cache
    :param timeout: time out period to quit from waiter, second
    """
    cache = redis.Redis(**CACHE_CONNECTION_PROPERTIES)
    start_time = datetime.now()
    while True:
        message = cache.hgetall('BMS_TELEMETRY:{}.bms.1'.format(bms_hostname))
        elapsed_time = timedelta.total_seconds(datetime.now() - start_time)
        if message and is_presence_in_cache_expected:
            logger.debug('Message {} cache as expected after {} seconds'.format(
                'saved to' if is_presence_in_cache_expected else 'removed from', elapsed_time))
            break
        elif elapsed_time > timeout:
            logger.error('Message {} cache after time out {} seconds'.format(
                'not in' if is_presence_in_cache_expected else 'not removed from', timeout))
            break
        sleep(0.01)  # to 100 ms wait reduce network/cpu load


def send_bms_message_with_cache_control(bms_hostname,
                                        current_capacity,
                                        timestamp=datetime.utcnow(),
                                        available_energy_capacity=None,
                                        current_charge=1,
                                        delta_t=1,
                                        delta_i=0,
                                        is_message_expected_in_cache=True):
    """
    Send BMS telemetry state message data to cache through message bus (ver. PS 1.x by default),
    and clean cache value for host  before and retrieve cache state after sending the message
    :param bms_hostname: Battery Management System host name
    :param current_capacity: Available power, in kW
    :param timestamp: datetime Timestamp of bms telemetry message (utcnow by default)
    :param available_energy_capacity: Available energy in storage, in Wh i.e. SOC * SOH * Nameplate Capacity (if PS 3.x)
    :param current_charge: State Of Charge, as a fraction (1 - fully charged, 0 - discharged)
    :param delta_t: Nominal difference between successive samples
    :param delta_i: Indicates if there was a change in the battery system's charge for the prior time
     period measured in watt-hours. Positive - charge / Negative - discharge
    :param is_message_expected_in_cache: expected condition of message presence in cache
    :returns <dict> | {} - if value absent in cache
    """
    remove_bms_telemetry_cache(bms_hostname)
    wait_for_message_state(bms_hostname=bms_hostname,
                           is_presence_in_cache_expected=False)
    send_bms_telemetry_message(bms_hostname=bms_hostname,
                               current_capacity=current_capacity,
                               timestamp=timestamp,
                               available_energy_capacity=available_energy_capacity,
                               current_charge=current_charge,
                               delta_t=delta_t,
                               delta_i=delta_i
                               )
    wait_for_message_state(bms_hostname=bms_hostname,
                           is_presence_in_cache_expected=is_message_expected_in_cache)
    return get_bms_telemetry_cache(bms_hostname)

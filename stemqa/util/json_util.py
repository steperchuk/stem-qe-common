import json
import logging
import numbers
from copy import deepcopy
from os import path
from pprint import pformat

from deepdiff import DeepDiff
from jsonschema import validate

from stemqa.util.s3_util import S3Util

logger = logging.getLogger(__name__)


def print_bill(json_data):
    """
    prints out bill and tariff details in readable format from api call to PowerCalcsClient.get_bills_for_location()
    :param json_data: PowerCalcsClient.get_bills_for_location() response
    :return:
    """
    data = json_data['data']

    # prints out the tariff info used to calc bills
    logger.debug("TARIFF INFO:")
    # tariff info
    tariff_info = data['tariff_info']
    logger.debug('Tariff Display Name: %s Tariff Id: %s Utility Display Name: %s Utility Id: %s',
                 tariff_info['tariff_display_name'],
                 tariff_info['tariff_id'],
                 tariff_info['utility_display_name'],
                 tariff_info['utility_id'])
    # tariff rate details
    tsi_details_demand_list = [("DEMAND", tariff_info['tsi_details'].get('DEMAND', {})),
                               ("ENERGY", tariff_info['tsi_details'].get('ENERGY', {}))]
    for name, tsi_details_demand in tsi_details_demand_list:
        logger.debug('\t %s', name)
        for key in list(tsi_details_demand.keys()):
            details = tsi_details_demand[key]

            logger.debug('\t\tTOU ID: %s Tariff Item: %s', key, details['tariff_item_display_name'])
            period_specific_data = details['period_specific_data']
            for period in period_specific_data:
                if period.get('message') is None:
                    for rate_period in period['rate_period_breakdown']:
                        logger.debug('\t\tDays applicable: %s Tariff start date: %s Pro-rate start: '
                                     '%s Pro-rate end: %s TOU price per unit %s', rate_period['days_applicable'],
                                     rate_period['tariff_effective_date'],
                                     rate_period['proration_period_date_start'],
                                     rate_period['proration_period_date_end'],
                                     rate_period['tou_price_per_unit'])

    # prints out the info for each of the billing cycles
    results = data.get('results')
    for result in results:
        logger.debug('BILL CHARGES:')
        charges = result['DATA_ACCESS']['charges']

        meta = result['meta']
        logger.debug('Start date: %s End date: %s Num of days: %s Data source: %s', meta.get('start_date'),
                     meta.get('end_date'), meta.get('num_days'), meta.get('data_source'))

        charge_list = [("DEMAND", charges['UTILITY'].get('DEMAND', {})),
                       ("ENERGY", charges['UTILITY'].get('ENERGY', {}))]
        for name, charge in charge_list:
            logger.debug('\t %s', name)
            logger.debug('\t\tTotal used: %s Total cost: %s',
                         charge.get('total', {}).get('result', {}).get('aggregation_result'),
                         charge.get('total', {}).get('result', {}).get('price_total'))
            for tsi in list(charge.get('tsi', {}).keys()):
                result = charge['tsi'][tsi]['result']
                logger.debug('\t\tTOU ID: %s TOU agg result: %s TOU price total: %s',
                             tsi, result['tou_aggregation_result'], result['tou_price_total'])


def get_utility_by_name(json_data, name):
    data = json_data['data']
    obj = [utility for utility in data if utility.get('name_vendor') == name]
    return obj[0] if obj else None


def verify_json(json_resp, s3_prefix_dir, tcase):
    """
    Verifies expected vs actual results with deep diff for json resp returned from API call
    If response and expect values is not equivalent, write out both actual and expected results to resources directory
    :param json_resp:
    :param s3_prefix_dir:
    :param tcase:
    :return:
    """
    s3_resource = '%s/%s.json' % (s3_prefix_dir, tcase)
    s3util = S3Util()
    obj = s3util.get_s3_object(s3_resource)
    expected_json = json.loads(obj['Body'].read().decode('utf-8'))
    ddiff = DeepDiff(expected_json, json_resp, ignore_order=True)
    assert ddiff == {}, 'Json comparison failed. Differences: %s' % ddiff


class JsonDataValidator:
    """
    JSON data validation tool that helps to validate json data structure by it json schema map
    in other case helps to made deep compare actual dictionary value with it baseline that stored in baseline json file.
    <compare> method helps to create baseline automatically if it does not exist
    able to check numeric values with given tolerance
    """

    def __init__(self, assets_dir):
        """
        :param assets_dir: working dir where validation tool will look for schema and beseline files
        """
        self.working_dir = assets_dir

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def verify(self, actual_data, schema_file_name):
        """
        Verify actual data structure by it json schema
        :param actual_data:
        :param schema_file_name:
        :return: Rise Exception in case actual data does not match to schema
        """
        schema_file = path.join(self.working_dir, '%s.%s' % (schema_file_name, 'json'))
        schema = json.loads(open(schema_file).read())
        logger.debug('Verify that actual data match to json schema from file: %s', schema_file)
        validate(actual_data, schema)

    def compare(self, actual_data, baseline_name, properties_to_skip=None, update_baseline=False, tolerance=0):
        """
        Deep compare actual data with it baseline
        :param actual_data: dictionary  - actual value data that will be compared with baseline
        :param baseline_name: baseline file name (without extension)
        :param properties_to_skip: list of property names in json that should be ignored while compare
        :param update_baseline: whether to rewrite baseline with new data (False by default)
        :param tolerance: tolerance value in percent to compare numeric values in dictionary (default=0 %)
        :return: raise Assert fail in case actual value and baseline have differences
        """
        actual = json.loads(json.dumps(actual_data))
        baseline_file_name = path.join(self.working_dir, '%s.%s' % (baseline_name, 'json'))
        is_baseline = path.isfile(baseline_file_name)
        file_mode = 'w+' if not is_baseline or update_baseline else 'r'
        with open(baseline_file_name, file_mode) as baseline_file:
            if not is_baseline or update_baseline:
                logger.debug('Baseline data will be stored to file: %s', baseline_file_name)
                json.dump(actual, baseline_file, indent=2)
                raise Exception('Please make sure if stored baseline match to expected result for your test case. '
                                'Add/commit all baselines in to repository: %s' % baseline_file_name)
            expected_data = json.loads(baseline_file.read())
            logger.debug('Compare expected data with it baseline from file: %s', baseline_file_name)
            result_ddiff = DeepDiff(expected_data, actual, ignore_order=True)
            if result_ddiff:
                ddiff = deepcopy(result_ddiff)
                if 'values_changed' in ddiff:
                    for value_diff_key in ddiff['values_changed'].keys():
                        if tolerance > 0:
                            # validate numeric values with tolerance
                            new_value = ddiff['values_changed'][value_diff_key]['new_value']
                            old_value = ddiff['values_changed'][value_diff_key]['old_value']
                            if isinstance(old_value, numbers.Number):
                                diff = (abs(new_value - old_value) / old_value) * 100
                                if diff > tolerance:
                                    result_ddiff['values_changed'][value_diff_key]['diff'] = '{0:0.2f}%'.format(diff)
                                else:
                                    del result_ddiff['values_changed'][value_diff_key]
                        if properties_to_skip:
                            # remove diffs that should be ignored while validation
                            for property_to_skip in properties_to_skip:
                                if property_to_skip in value_diff_key:
                                    del result_ddiff['values_changed'][value_diff_key]
            # in case change list empty remove change list
            if 'values_changed' in result_ddiff and not result_ddiff['values_changed'].keys():
                del result_ddiff['values_changed']

            assert result_ddiff == {}, 'Json comparison failed. Differences in:\n %s' % pformat(result_ddiff)

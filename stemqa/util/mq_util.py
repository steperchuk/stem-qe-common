import datetime as dt
import logging
import time

from kombu import Connection, Exchange, Queue, Producer

from stemqa.config.settings import settings

logger = logging.getLogger(__name__)

"""
Utilities to work with RabbitMQ
"""


def mq_create(conn, params):
    """
    Create queue if doesn't exist and bind it to exchange
    If queue exists it will be cleaned
    :param conn: connection to the RMQ (obj)
    :param params: {'exchange': <exchange_name>,
                    'queue': <queue_name>,
                    'type': 'topic',
                    'routing_key': <routing_key>,
                    'durable': True/False,
        '           declare': True/False}
    :return: queue (obj)
    """

    exchange = Exchange(params['exchange'], type=params['type'], durable=params['durable'])
    queue = Queue(params['queue'], exchange=exchange, routing_key=params['routing_key'], channel=conn.channel())
    queue.maybe_bind(conn)
    if params['declare']:
        logger.info('Declaring queue %s, binded to %s.', params['queue'], params['exchange'])
        queue.declare()

    # Clean-up RMQ before test starts
    queue.purge()
    if queue:
        logger.debug("Queue %s was created", params['queue'])

    return queue


def mq_read(queue):
    """
    Reads message from MQ
    :param queue: queue (obj)
    :return: payload of the message
    """
    msg = queue.get()

    if msg:
        return msg.payload
    return None


def mq_write(conn, params):
    """
    Sends message to the queue
    :param conn: connection to the RMQ (obj)
    :param params: {'exchange': <exchange_name>,
                    'queue': <queue_name>,
                    'routing_key': <routing_key>,
                    'type': 'topic',
                    'durable': True/False,
                    'message': <payload>}
    :return: True/False in case of success/failure
    """

    exchange = Exchange(params['exchange'], type=params['type'], durable=params['durable'])
    producer = Producer(exchange=exchange, channel=conn.channel(), routing_key=params['routing_key'])

    logger.info('Sending message to %s.', params['exchange'])
    try:
        producer.publish(params['message'])
        return True
    except Exception as e:
        logger.error("Send message to queue cause an error %s.", e)
        return False


def mq_connection():
    """
    Gets connection to RMQ
    :return: connection (obj)
    """
    mq_url = 'amqp://%s:%s@%s:%s' % (settings['rmq']['username'],
                                     settings['rmq']['password'],
                                     settings['rmq']['host'],
                                     settings['rmq']['rabbit-port'])

    conn = Connection(mq_url, accept=['json'])

    return conn


def mq_delete(queue):
    """
    Deleting queue
    :param queue:
    :return:
    """
    queue.purge()
    queue.delete()


def wait_for_msgs(queue, msg_count=0, timeout=300, pause=5):
    """
    Waits for messages in queue
    :param queue: queue (obj)
    :param msg_count: number of expected messages, if 0 (default) method returns as many messages as it got
    :param timeout: waiting time in seconds (default - 5 minutes)
    :param pause: pause to sleep between queue checks (default - 5 seconds)
    :return: list of messages from queue
    """
    wait_time = dt.datetime.now() + dt.timedelta(seconds=timeout)
    mq_messages = []
    while dt.datetime.now() < wait_time:
        my_msgs = mq_read(queue)
        if not my_msgs:
            time.sleep(pause)
            continue

        mq_messages.append(my_msgs)
        if msg_count == 0 or msg_count == len(mq_messages):
            return mq_messages

    if msg_count == 0:
        logger.error('Failed to receive any messages within specified timeout of %dsecs', timeout)
    else:
        logger.error('Received only %d messages when %d messages were expected', len(mq_messages), msg_count)
        logger.error('Received messages: %s', mq_messages)

    raise RuntimeError("Number of messages in queue differ from expected.")

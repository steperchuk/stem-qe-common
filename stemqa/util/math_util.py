def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """
    Compares two values and checks if their difference is within a specified range
    :param a:
    :param b:
    :param rel_tol:
    :param abs_tol:
    :return:
    """
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

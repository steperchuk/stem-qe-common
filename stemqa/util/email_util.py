import base64
import email
import imaplib
import logging
import smtplib

logger = logging.getLogger(__name__)


def delete_emails(email_server, to):
    """
    Connects to gs test email acccount and deletes emails
    :param email_server:
    :param to:
    :return:
    """
    # delete all emails in the gs test account after test
    box = imaplib.IMAP4_SSL(email_server, 993)

    auth_file = open('resources/gridservice/mail-auth_test_account.txt', 'rb')
    password = base64.b64decode(auth_file.read())
    box.login(to, password.decode())
    box.select('Inbox')
    _, data = box.search(None, 'ALL')
    for num in data[0].split():
        _, data = box.fetch(num, '(RFC822)')
        msg = email.message_from_string(data[0][1].decode())
        decode_subject = email.header.decode_header(msg['Subject'])[0]
        subject = str(decode_subject[0])
        decode_date = email.header.decode_header(msg['Date'])[0]
        date = str(decode_date[0])
        logger.debug('Deleting mail Subject: %s Date: %s', subject, date)
        box.store(num, '+FLAGS', '\\Deleted')
    box.close()
    box.logout()


def send_email(email_server, from_address, to_address, msg):
    """
    Send msg from specified address to specified address through specified email server
    :param email_server:
    :param from_address:
    :param to_address:
    :param msg:
    :return:
    """
    logger.debug("Sending email to: %s from: %s", msg['To'], msg['From'])
    logger.debug("Subject: %s", msg['Subject'])

    auth_file = open('resources/gridservice/mail-auth.txt', 'rb')
    from_pwd = base64.b64decode(auth_file.read())
    smtp = smtplib.SMTP(timeout=30)
    smtp.set_debuglevel(True)
    smtp.connect(email_server, smtplib.SMTP_PORT)
    smtp.ehlo()
    if smtp.has_extn('STARTTLS'):
        smtp.starttls()
        smtp.ehlo()  # re-identify ourselves over TLS connection
    smtp.login(from_address, from_pwd.decode())
    smtp.sendmail(from_address, to_address, msg.as_string())
    smtp.quit()

import datetime as dt
import logging
import time

import isodate
import pause
import pytz
# plugin that gives you assume function for collecting errors instead of asserting
# https://github.com/astraw38/pytest-assume
from pytest import assume  # pylint: disable=E0611
from pytz import UTC
from requests import HTTPError

from stemqa.rest.powerqa import PowerQAClient
from stemqa.util.openadr_util import VEN_NAME
from stemqa.util.openadr_util import calculate_event_start_time
from stemqa.util.openadr_util import event_testing
from stemqa.util.openadr_util import openadr_create_events
from stemqa.util.openadr_util import openadr_default_test_data
from stemqa.util.openadr_util import openadr_test_info
from stemqa.util.openadr_util import openadr_test_setup
from stemqa.util.openadr_util import openadr_test_startup
from stemqa.util.openadr_util import scheduled_callback_loop
from stemqa.util.openadr_util import setup_emulators
from stemqa.util.openadr_util import teardown_emulators
from stemqa.util.s4_util import S4Util
from stemqa.vpp.helper import dt_utcnow, iso8601_utc_datetime, iso8601_utcnow, json_dumps, round_time_up, \
    WEB_URL, STR_FORMAT, get_program_info
from stemqa.vpp.openadr.common import POLL_FREQ, reset_ven_registration, \
    parse_report_update, DAILY_FORECASTS_PREFIX, HOURLY_FORECASTS_PREFIX, filter_forecasts_by_resources, \
    delete_forecast_objects, get_forecast_object_names, EXTERNAL_REFERENCE_ID
from stemqa.vpp.openadr.report import create_dummy_forecasts, anti_deadlock_sleep, created_forecasts, \
    verify_forecast_files
from stemqa.vpp.openadr.report_verification import verify_current_status_reports, verify_charge_curves, \
    verify_nameplate_reports, verify_forecast_reports, verify_report_count, verify_report_schema, \
    get_forecast_intervals
from stemqa.vpp.openadr.vtn_api import vtnc
from stemqa.vpp.resource import set_tariffs_schedule_end_date, import_resource_history_data, import_emulator_settings
from stemqa.vpp.telemetry import telemetry_data

logger = logging.getLogger(__name__)

s4c = S4Util()

SYSTEM_REPORT_LAG = 3


def calculate_start_time_based_on_telemetry(testdata, minute_rounded):
    resources = testdata['resources']
    data = []
    checks = 0
    while len(data) < len(resources) and checks < 5:
        test_start_dt = round_time_up(dt.datetime.now(tz=UTC), 60)
        test_start_dt_wait = test_start_dt + dt.timedelta(seconds=15)
        logger.debug('Waiting until %s to check telemetry', test_start_dt_wait.strftime(STR_FORMAT))
        pause.until(test_start_dt_wait)
        data = telemetry_data(resources, test_start_dt - dt.timedelta(minutes=1), test_start_dt)
        logger.debug('TELEMETRY DATA: %s', data)
        checks += 1

    if checks >= 5:
        raise Exception('No telemetry found in expected time frame.')
    if minute_rounded:
        return round_time_up(test_start_dt + dt.timedelta(seconds=1), 60)
    return dt.datetime.now(tz=UTC)


def calculate_report_start_time(testdata, minute_rounded):
    lag = POLL_FREQ + SYSTEM_REPORT_LAG + testdata.get('report_start_delta', 0)
    test_start_dt = dt.datetime.now(tz=UTC) + dt.timedelta(seconds=lag)
    if minute_rounded:
        test_start_dt = round_time_up(test_start_dt, 60)
    test_start = test_start_dt.strftime(STR_FORMAT)
    logger.debug('First possible report test start is at: %s', test_start)
    return test_start_dt


def generic_report_test_setup(testdata,
                              custom_report_test,
                              default_report_config=None,
                              wait_for_reports=True,
                              add_missing_testdata=True,
                              rounded_start=False,
                              custom_event_test=None,
                              custom_interval_test=None,
                              custom_telemetry_test=None,
                              **kwargs):
    openadr_test_startup(testdata, add_missing_testdata, **kwargs)
    events_coupling = {}

    if testdata.get('adjust_start_based_on_telemetry_downsampling'):
        base_start = calculate_start_time_based_on_telemetry(testdata, rounded_start)
    elif testdata.get('events'):
        base_start = calculate_event_start_time(testdata, minute_rounded=rounded_start)
    else:
        base_start = calculate_report_start_time(testdata, rounded_start)

    # logger.debug('Base time before rounding to odd minutes: %s', base_start.strftime(STR_FORMAT))
    # if base_start.minute % 2 == 0:
    #    #base_start = round_time_up(base_start, minutes=2)
    #    base_start = base_start + dt.timedelta(minutes=1)
    # logger.debug('Base time after rounding to odd minutes: %s', base_start.strftime(STR_FORMAT))

    # resetting ven registration should reset delivered and received energy values
    # but it also cancels all events so we have to use it before we create them
    if base_start.second != 0:
        rounded_reset_time = round_time_up(base_start, minutes=1)
    else:
        rounded_reset_time = base_start
    logger.debug('Resetting ven registartion time to: %s', rounded_reset_time.strftime(STR_FORMAT))
    reset_ven_registration(testdata['external_reference_id'], rounded_reset_time)

    if testdata.get('events'):
        logger.debug('Creating events...')
        dispatch_lag_base_start = base_start - dt.timedelta(seconds=testdata['dispatch_lag'])
        base_event_start, events_coupling = openadr_create_events(testdata, start_time=dispatch_lag_base_start)
        assert dispatch_lag_base_start == base_event_start

    logger.debug('Base event start time is at: %s', base_start.strftime(STR_FORMAT))
    system_report_lag = testdata.get('system_report_lag') or SYSTEM_REPORT_LAG
    wait_seconds = testdata.get('wait_for_reports_seconds', 0)
    wait_round_to = testdata.get('wait_for_reports_round_to')
    wait_for_reports_dt = None
    if wait_seconds or wait_round_to:
        wait_for_reports_dt = base_start + dt.timedelta(seconds=wait_seconds)
        if wait_round_to:
            wait_for_reports_dt = round_time_up(wait_for_reports_dt, wait_round_to)

    if not testdata.get('report_requests') and wait_for_reports_dt:
        if custom_telemetry_test:
            scheduled_callback_loop(custom_telemetry_test, wait_for_reports_dt, 60, 60,
                                    testdata=testdata)
        logger.debug(' **  Waiting until: %s', wait_for_reports_dt)
        pause.until(wait_for_reports_dt + system_report_lag)

    report_config = {
        'report_interval_start': iso8601_utc_datetime(base_start)
    }
    if default_report_config:
        report_config.update(default_report_config)
    report_requests, ven_response = vtnc.create_report_requests(testdata['ven_name'], testdata['report_requests'],
                                                                report_config, wait_for_ven_response=True)

    report_created_dt = dt_utcnow()
    # ###################  TESTING ######################
    logger.debug('TESTING REPORTS...')
    logger.debug(80 * '*')
    for rep_req_data in report_requests:
        rep_req = rep_req_data['response']
        request_id = rep_req['report_request_id']
        logger.debug('\nreport request: ' + request_id + '\n' + 80 * '-')
        logger.debug('\nrep_req_data: %s', json_dumps(rep_req_data))
        ven_name = rep_req['ven']
        cancel_report = False
        if wait_for_reports:
            if wait_for_reports_dt:
                wait_for_reports = False
                cancel_report = True
                wait_dt = wait_for_reports_dt
            else:
                reporting_duration = isodate.parse_duration(rep_req['report_interval_duration'])
                report_back_duration = isodate.parse_duration(rep_req['report_back_duration'])
                if report_back_duration and not reporting_duration:
                    assume(False, 'This report runs forever(use `wait_for_reports_*`!:\n%s' %
                           json_dumps(rep_req_data))
                    continue
                report_start_dt = isodate.parse_datetime(rep_req['report_interval_start'])
                wait_dt = report_start_dt + reporting_duration
            wait_dt = wait_dt + dt.timedelta(seconds=system_report_lag)
            logger.debug('Waiting for report updates [request: %s]',
                         request_id)
            # telemetry testing -> callback every minute
            if custom_telemetry_test:
                scheduled_callback_loop(custom_telemetry_test, wait_dt, 60, 60, testdata=testdata)
            logger.debug('*** Waiting until %s', wait_dt.strftime(STR_FORMAT))
            pause.until(wait_dt)

        end_dt = None
        if cancel_report and testdata.get('cancel_reports_after_wait', True):
            vtnc.cancel_report_requests(ven_name, [request_id])
            end_dt = dt_utcnow()

        try:
            report_updates = vtnc.get_report_updates(ven_name, request_id)
        except HTTPError as e:
            assume(False,
                   ('Report request `%s` is missing from report updates:\n'
                    'ERROR_MSG: %s\nREQUEST_DATA:\n%s\n') %
                   (request_id, e, json_dumps(rep_req_data)))
            continue

        if report_updates:
            logger.debug('Report updates for report_request_id `%s`', request_id)
        else:
            logger.debug('There are no report updates for report_request_id: `%s`', request_id)

        for ridx, report_update in enumerate(report_updates):
            created_dt_i = report_update.get('created_date_time')
            assume(created_dt_i)
            if report_update['intervals']:
                report_update_p = json_dumps(parse_report_update(report_update))
            else:
                report_update_p = json_dumps(report_update)
            logger.debug('Report update [idx: %s, created: %s]:\n%s', ridx, created_dt_i, report_update_p)

        custom_report_test(testdata, rep_req_data, report_updates,
                           events=events_coupling,
                           report_created_dt=report_created_dt,
                           test_start_dt=base_start,
                           end_dt=end_dt)
        logger.debug('Ended testing report request: %s', request_id)
        logger.debug(80 * '-')

    logger.debug('TESTING EVENTS...')
    if events_coupling and (custom_event_test or custom_interval_test or custom_telemetry_test):
        event_testing(events_coupling, testdata,
                      custom_event_test=custom_event_test,
                      custom_interval_test=custom_interval_test,
                      custom_telemetry_test=custom_telemetry_test,
                      postpone_assert_error=True)

    logger.debug('ven report creation response:\n%s', json_dumps(ven_response))


def generic_report_test_setup_with_teardown(testdata,
                                            custom_report_test,
                                            default_report_config=None,
                                            wait_for_reports=True,
                                            add_missing_testdata=True,
                                            custom_event_test=False,
                                            **kwargs):
    """Generic report test function.

       Creates openADR events and reports from testdata.Than waits for test time specified
       or until events and reports finish. After that report updates for every report
       are tested with custom report testing function provided.
    """
    try:
        generic_report_test_setup(testdata,
                                  custom_report_test,
                                  default_report_config,
                                  wait_for_reports,
                                  add_missing_testdata,
                                  custom_event_test=custom_event_test,
                                  **kwargs)
    finally:
        if kwargs.get('SETUP_EMULATORS') and kwargs.get('TEARDOWN_EMULATORS', True):
            teardown_emulators(
                testdata['resources'],
                kwargs.get('RECONFIGURE_EMULATORS', False),
                kwargs.get('RESET_EMULATORS', False))
        if kwargs.get('CANCEL_REPORT_REQUESTS', True):
            vtnc.cancel_all_pending_reports(testdata.get('ven_name', VEN_NAME))


# TODO merge changes with new refactored code
def create_forecast_objects(external_reference_id=None,
                            program_timezone=None,
                            program_name=None,
                            resources=None,
                            days=1, hours=2,
                            anti_deadlock_seconds=30,
                            verify_forecast_data=True,
                            forecast_timeout=120,
                            dummy_forecasts=False,
                            round_down_local_start_dt=False,
                            check_current_capacity_intervals=False,
                            SEND_NO_START_DT=False,
                            forecast_power_cmd=None,
                            **kwargs):
    if SEND_NO_START_DT:
        assert days <= 1 and hours <= 1, 'you can only create and verify next forecast with this settings'
    logger.debug('Creating forecast files...')
    program_tz = pytz.timezone(program_timezone)
    logger.debug('PROGRAM TIMEZONE: %s', program_tz)
    UTC_DT = isodate.parse_datetime(kwargs.get('start_datetime', iso8601_utcnow()))
    if kwargs.get('start_offset_seconds'):
        UTC_DT += dt.timedelta(seconds=kwargs['start_offset_seconds'])
    if kwargs.get('start_time'):
        utc_time = isodate.parse_time(kwargs.get('start_time'))
        UTC_DT = dt.datetime.combine(UTC_DT.date(), utc_time)
    LOCAL_DT = UTC_DT.astimezone(program_tz)
    # local time of resource - all our openADR programs are in PST for now
    logger.debug('UTC_TIME: %s', UTC_DT)
    logger.debug('PROGRAM_TIME: %s', LOCAL_DT)
    daily_dts_start = []
    daily_dts_start_str = []
    daily_dts = []
    daily_dts_str = []
    hourly_dts_start = []
    hourly_dts_start_str = []
    hourly_dts = []
    hourly_dts_str = []

    if round_down_local_start_dt:
        dt_start_daily_naive = dt.datetime(
            year=UTC_DT.year,
            month=UTC_DT.month,
            day=UTC_DT.day)
        dt_start_daily = program_tz.localize(dt_start_daily_naive)
        dt_start_hourly_naive = dt.datetime(
            year=LOCAL_DT.year,
            month=LOCAL_DT.month,
            day=LOCAL_DT.day,
            hour=LOCAL_DT.hour)
        dt_start_hourly = program_tz.localize(dt_start_hourly_naive)
    else:
        dt_start_daily = LOCAL_DT
        dt_start_hourly = LOCAL_DT

    for day_ix in range(0, days):
        dt_local = dt_start_daily + dt.timedelta(days=day_ix)
        dt_local_str = dt_local.strftime(STR_FORMAT)
        dt_utc = dt_local.astimezone(UTC)
        dt_utc_str = dt_utc.strftime(STR_FORMAT)
        daily_dts_start.append(dt_utc)
        daily_dts_start_str.append(dt_utc_str)
        dt_local_gen = round_time_up(dt_local, days=1)
        dt_local_gen_str = dt_local_gen.strftime(STR_FORMAT)
        # NOTE daily s3 object path was changed with JEDI-11792 to have local instead of utc timestamp
        # dt_utc_gen = dt_local_gen.astimezone(UTC)
        # dt_utc_gen_str = dt_utc_gen.strftime(STR_FORMAT)
        daily_dts.append(dt_local_gen)
        daily_dts_str.append(dt_local_gen_str)
        logger.debug('>> daily forecast info <<')
        logger.debug(' -> dt_local: %s      (used when calling creation of dailies)', dt_local_str)
        logger.debug(' -> dt_local_gen: %s  (daily local_dt expected to get created)', dt_local_gen_str)
        logger.debug('------------------------------------------------------------')

    for ix in range(0, hours):
        dt_local = dt_start_hourly + dt.timedelta(hours=ix)
        dt_local_str = dt_local.strftime(STR_FORMAT)
        dt_utc = dt_local.astimezone(UTC)
        dt_utc_str = dt_utc.strftime(STR_FORMAT)
        hourly_dts_start.append(dt_utc)
        hourly_dts_start_str.append(dt_utc_str)
        dt_local_gen = round_time_up(dt_local, hours=1)
        # dt_local_generated = round_time_up(dt_local + dt.timedelta(microseconds=1), hours=1)
        dt_local_gen_str = dt_local_gen.strftime(STR_FORMAT)
        dt_utc_gen = dt_local_gen.astimezone(UTC)
        dt_utc_gen_str = dt_utc_gen.strftime(STR_FORMAT)
        hourly_dts.append(dt_utc_gen)
        hourly_dts_str.append(dt_utc_gen_str)
        logger.debug('>> hourly forecast info <<')
        logger.debug(' -> dt_local: %s      (used to calculate utc_dt)', dt_local_str)
        logger.debug(' -> dt_utc: %s        (used when calling creation of hourlies)', dt_utc_str)
        logger.debug(' -> dt_local_gen: %s  (hourly local_dt expected to get created)', dt_local_gen_str)
        logger.debug(' -> dt_utc_gen: %s    (hourly utc_dt expected to get created)', dt_utc_gen_str)
        logger.debug('------------------------------------------------------------')

    # NOTE daily s3 file object path was changed with JEDI-11792
    # daily_iso_dts = [isodate.datetime_isoformat(udt) for udt in daily_dts]
    daily_iso_dts = [udt.strftime(STR_FORMAT) for udt in daily_dts]
    hourly_iso_dts = [isodate.datetime_isoformat(udt) for udt in hourly_dts]

    if dummy_forecasts:
        create_dummy_forecasts(external_reference_id, program_name, resources,
                               daily_iso_dts, hourly_iso_dts, power_cmd=forecast_power_cmd)
    else:
        with PowerQAClient(WEB_URL) as client:
            for dt_utc_str in daily_dts_start_str:
                if SEND_NO_START_DT:
                    dt_utc_str = None
                client.all_vpp_daily_forecasts(program_name, start_dt_utc=dt_utc_str)
                anti_deadlock_sleep(anti_deadlock_seconds)

            for dt_utc_str in hourly_dts_start_str:
                if SEND_NO_START_DT:
                    dt_utc_str = None
                client.all_vpp_hourly_forecasts(program_name, start_dt_utc=dt_utc_str)
                anti_deadlock_sleep(anti_deadlock_seconds)

    waited = 0
    wait_step_seconds = 5
    daily_forecasts, hourly_forecasts = created_forecasts(external_reference_id, resources)
    logger.debug('Forecast completion timeout: %ss', forecast_timeout)
    while (len(daily_forecasts) != days * len(resources)
           or len(hourly_forecasts) != hours * len(resources)) and waited <= forecast_timeout:
        logger.debug('Waiting %ss for until checking again if all forecasts were created',
                     wait_step_seconds)
        time.sleep(wait_step_seconds)
        waited += wait_step_seconds
        daily_forecasts, hourly_forecasts = created_forecasts(external_reference_id, resources)

    resource_locations = [res['target_location'] for res in resources]
    logger.debug('locations: %s', json_dumps(resource_locations))
    logger.debug('local dates sent in request for daily forecast: %s',
                 json_dumps(daily_dts_str))
    logger.debug('daily dates expected: %s', json_dumps(daily_iso_dts))
    logger.debug('local dates sent in request for hourly forecast: %s',
                 json_dumps(hourly_dts_str))
    logger.debug('hourly dates expected: %s', json_dumps(hourly_iso_dts))
    info = """
    INITIAL LOCAL DATETIME USED FOR CREATION: %s\n
    INITIAL UTC DATETIME USED FOR CREATION: %s\n
    RESOURCES: %s\n
    DAILY -> COUNT OF PROGRAM(all resources) DAILIES REQUESTED: %s\n
    DAILY FORECASTS CREATED: %s\n
    DAILY REQUEST DATES: %s\n
    DAILY EXPECTED DATES: %s\n
    HOURLY -> COUNT OF PROGRAM(all resources) DAILIES REQUESTED: %s\n
    HOURLY FORECASTS CREATED: %s\n
    HOURLY REQUEST DATES: %s\n
    HOURLY EXPECTED DATES: %s\n'
           """ % (LOCAL_DT.strftime(STR_FORMAT),
                  UTC_DT.strftime(STR_FORMAT),
                  json_dumps(resources),
                  days,
                  json_dumps(daily_forecasts),
                  json_dumps(daily_dts_str),
                  json_dumps(daily_iso_dts),
                  hours,
                  json_dumps(hourly_forecasts),
                  json_dumps(hourly_dts_str),
                  json_dumps(hourly_iso_dts),
                  )

    assert len(daily_forecasts) == days * len(resources), \
        'Number of daily forecasts for this program does not match: \n%s' % info
    assert len(hourly_forecasts) == hours * len(resources), \
        'Number of hourly forecasts for this program does not match: \n%s' % info

    for res_loc in resource_locations:
        for iso_dt in daily_iso_dts:
            expected_name = '%s/%s/%s' % (DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id),
                                          iso_dt, res_loc)
            logger.debug('Expected forecast name: %s', expected_name)
            assert expected_name in daily_forecasts, \
                'Daily forecast file not found: %s' % expected_name
        for iso_dt in hourly_iso_dts:
            expected_name = '%s/%s/%s' % (HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id),
                                          iso_dt, res_loc)
            logger.debug('Expected forecast name: %s', expected_name)
            assert expected_name in hourly_forecasts, \
                'Hourly forecast file not found: %s' % expected_name

    if verify_forecast_data:
        logger.debug('Verifying forecast data of all forecast...')
        verify_forecast_files(resources, external_reference_id, program_name)

    if check_current_capacity_intervals:
        # Making sure we have forecast data available for current and next hour
        now = dt_utcnow()
        current_forecast_intervals = get_forecast_intervals(now, external_reference_id, resources)
        assert len(current_forecast_intervals) == len(resources)
        next_hour = now + dt.timedelta(hours=1)
        forecast_intervals_for_next_hour = get_forecast_intervals(next_hour, external_reference_id, resources)
        assert len(forecast_intervals_for_next_hour) == len(resources)


def forecast_reports_test_setup(days=2, hours=2, import_telemetry=True, use_cached=False, **kwargs):
    logger.debug('forecast_reports_test_setup -> kwargs: %s', kwargs)
    testsetup = kwargs.copy()

    # TODO check that program timezone matches timezone on programs resources
    openadr_default_test_data(testsetup)
    testsetup['days'] = days
    testsetup['hours'] = hours
    testsetup['use_cached'] = use_cached
    testsetup.setdefault('CLEAR_VEN', False)
    testsetup.setdefault('RESTART_VEN', False)
    testsetup.setdefault('SETUP_EXTERNAL_REFERENCE', True)

    resources = testsetup.get('resources')
    testsetup.setdefault('SETUP_EMULATORS', True if resources else False)
    testsetup.setdefault('SETUP_EMULATORS_DATA_ONLY', True if resources else False)

    logger.debug('forecast_reports_test_setup settings: %s', json_dumps(testsetup))
    if testsetup.get('test_info'):
        openadr_test_info(testsetup)
    openadr_test_setup(**testsetup)
    if testsetup.get('SETUP_EMULATORS'):
        setup_emulators(resources,
                        testsetup.get('RECONFIGURE_EMULATORS', False),
                        testsetup.get('RESET_EMULATORS', False),
                        testsetup.get('DELETE_TELEMETRY', True))
    elif testsetup.get('SETUP_EMULATORS_DATA_ONLY'):
        for resource in resources:
            import_emulator_settings(resource)

    if import_telemetry:
        for resource in resources:
            import_resource_history_data(resource)
    cached_reports = filter_forecasts_by_resources(get_forecast_object_names(), resources)
    cached = len(cached_reports) == (days + hours) * len(resources)
    if (not cached or not use_cached) and (hours or days):
        # TODO do we still need this tariff setting?
        set_tariffs_schedule_end_date(807, '2031-12-31 23:59:00')
        delete_forecast_objects()
        create_forecast_objects(**testsetup)
    elif not use_cached and not (hours or days):
        delete_forecast_objects()


class DummyReportRequest(object):
    def __init__(self, response, config, report_created_dt):
        self.report_request = response
        self.report_request_id = response['report_request_id']
        self.report_request_created_dt = report_created_dt
        self.data = config

    def toJSONshort(self):
        return 'DummyReportRequest(report_request_id=%s)' % self.report_request_id

    def __str__(self):
        return self.toJSONshort()

    def toJSON(self):
        return {'__class__': 'DummyReportRequest',
                'report_request': self.report_request,
                'report_request_id': self.report_request_id,
                'report_request_created_dt': str(self.report_request_created_dt),
                'data': self.data
                }


def wrap_data_for_new_code(testdata, rep_req_data, report_created_dt):
    program = {'name': testdata['program_name'],
               'resources': testdata.get('resources', []),
               'external_reference_id': testdata.get('external_reference_id') or EXTERNAL_REFERENCE_ID
               }
    program['info'] = get_program_info(program['name'])
    logger.debug('Program info: %s', json_dumps(program))
    report_verification = {'relative_tolerance': testdata.get('relative_tolerance_reports', 0.05),
                           'absolute_tolerance_kw': testdata.get('absolute_tolerance_reports', 2),
                           'expected_reports': testdata.get('expected_reports'),
                           'expected_count': testdata.get('expected_count'),
                           'forecasts_present': testdata.get('forecasts_present', True),
                           'max_expected_count': testdata.get('max_expected_count', testdata.get('expected_count'))}
    report_object = DummyReportRequest(rep_req_data['response'], rep_req_data['config'], report_created_dt)
    return program, report_verification, report_object


def basic_report_verification(testdata, rep_req_data, report_updates, report_created_dt, **kwargs):
    logger.debug('basic_report_verification -> kwargs: %s', kwargs)
    end_dt = kwargs.get('end_dt') or dt_utcnow() - dt.timedelta(seconds=SYSTEM_REPORT_LAG)
    _, report_verification, report_object = wrap_data_for_new_code(testdata, rep_req_data, report_created_dt)
    verify_report_count(report_object, report_updates, report_verification, end_dt)
    if testdata.get('verify_payloads', True):
        verify_report_schema(report_object, report_updates, report_verification)


def verify_current_status_reports_old(testdata, rep_req_data, report_updates, events,
                                      report_created_dt, test_start_dt, **kwargs):
    logger.debug('verify_current_status_reports -> extra kwargs: %s', kwargs)
    logger.debug('verify_current_status_reports -> events: %s', events)
    basic_report_verification(testdata, rep_req_data, report_updates, report_created_dt, **kwargs)
    program, report_verification, report_object = wrap_data_for_new_code(testdata, rep_req_data, report_created_dt)
    verify_current_status_reports(program, report_object, report_updates, report_verification, test_start_dt)


def verify_forecast_reports_old(testdata, rep_req_data, report_updates, events,
                                report_created_dt, **kwargs):
    logger.debug('verify_forecast_reports -> kwargs: %s', kwargs)
    logger.debug('verify_forecast_reports -> events: %s', events)
    basic_report_verification(testdata, rep_req_data, report_updates, report_created_dt, **kwargs)
    program, report_verification, report_object = wrap_data_for_new_code(testdata, rep_req_data, report_created_dt)
    if not testdata.get('verify_payloads', True):
        return
    verify_forecast_reports(program, report_object, report_updates, report_verification)


def verify_charge_curves_old(testdata, rep_req_data, report_updates, report_created_dt, **kwargs):
    logger.debug('verify_charge_curves -> kwargs: %s', kwargs)
    basic_report_verification(testdata, rep_req_data, report_updates, report_created_dt, **kwargs)
    program, report_verification, report_object = wrap_data_for_new_code(testdata, rep_req_data, report_created_dt)
    verify_charge_curves(program, report_object, report_updates, report_verification)


def verify_nameplate_reports_old(testdata, rep_req_data, report_updates, report_created_dt, **kwargs):
    logger.debug('verify_nameplate_reports -> kwargs: %s', kwargs)
    basic_report_verification(testdata, rep_req_data, report_updates, report_created_dt, **kwargs)
    program, report_verification, report_object = wrap_data_for_new_code(testdata, rep_req_data, report_created_dt)
    verify_nameplate_reports(program, report_object, report_updates, report_verification)

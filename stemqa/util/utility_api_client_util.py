import logging

from stemqa.config.settings import settings
from stemqa.tools.utilityapiclient import UtilityApiClient

WEB_URL = settings['stem-web-app']['url']

logger = logging.getLogger(__name__)


def get_utilityapi_interval_data_boundaries(vendor_auth, vendor_site_id):
    """
    Returns start, end of interval data stored in utilityAPI
    :param vendor_auth:
    :param vendor_site_id:
    :return:
    """
    response = UtilityApiClient.get_meters(vendor_auth)
    meters = response.json()['meters']['meters']
    expected_meter = [meter for meter in meters if meter['uid'] == vendor_site_id][0]
    intervals = expected_meter['interval_coverage']
    start_first_interval = intervals[0][0]
    parts = start_first_interval.split('.')
    start_interval = str(parts[0])
    end_last_interval = intervals[-1][1]
    parts = end_last_interval.split('.')
    end_interval = str(parts[0])
    logger.debug('UtilityAPI interval data start: %s end: %s', start_interval, end_interval)
    return start_interval, end_interval


def get_intervals_for_meter(meter_uid, start=None, end=None):
    """
    Returns list of all readings for the specified meter
    :param meter_uid:
    :param start:
    :param end:
    :return:
    """
    response = UtilityApiClient.get_interval_data(meter_uid, start=start, end=end)
    content = response.json()
    intervals = content['intervals']
    all_readings = []
    for interval in intervals:
        all_readings.extend(interval['readings'])
    all_readings.sort(key=lambda item: item['start'], reverse=False)
    return all_readings

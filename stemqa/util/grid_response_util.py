import datetime as dt
import logging
import time

from pytz import UTC

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DB_METER_LOGS, DB_GRID_SERVICES, DB_OPTIMIZATION
from stemqa.rest.gridresponse import GridResponseClient
from stemqa.util import math_util
from stemqa.util.emulator_util import configure_simple_emulator, setup_emulator_container

logger = logging.getLogger(__name__)

timezone = 'UTC'
str_format = "%Y-%m-%d %H:%M:%S"
dcm_inhibit_property = '{"START": {"TYPE": "RELATIVE_TIME", "ANCHOR": "DEPLOYMENT_START", "VALUE": {"DELTA": %s}}, ' \
                       '"END": {"TYPE": "RELATIVE_TIME",  "ANCHOR": "DEPLOYMENT_START", "VALUE": {"DELTA": 0}} }'

dispatch_buffer_property = '{"TYPE": "TIME_BEFORE_DISPATCH", "VALUE": ["MINUTE", %s ] }'


def get_program_id(client, program_name):
    """
    Returns the program id for the specified program
    :param client:
    :param program_name:
    :return:
    """
    programs = client.get_all_programs()
    program = next((prog for prog in programs if prog['name'] == program_name), None)
    if program:
        return program['program_id']
    else:
        raise RuntimeError('No program with name %s found' % program_name)


def get_event_logs(client, gridservice_event_id, program_id, attributes=None):
    """
    Queries event logs and prints out event start/end and list of events and their types and status
    :param client:
    :param gridservice_event_id:
    :param program_id:
    :param attributes:
    :return:
    """
    if attributes is None:
        attributes = ['timestamp']

    data = client.query_event_logs(program_id, gridservice_event_id)
    events = {}
    if data:
        if 'event_detail' in data:
            event_details = data['event_detail']
            logger.debug('Amount requested(kW): %s Duration(sec): %s Start: %s End: %s UTC',
                         event_details['capacity'], event_details['duration'],
                         event_details['start_time'], event_details['end_time'])
        if 'event_log' in data:
            for event in data['event_log']:
                logger.debug('Dispatch_type: %s Timestamp: %s Resource: %s Status: %s',
                             event['dispatch_type'],
                             event['timestamp'],
                             event['resource']['name'],
                             event['status'])
                dispatch_type = str(event['dispatch_type'])
                status = str(event['status'])
                d = {}
                for attribute in attributes:
                    attribute_value = str(event[attribute])
                    d[attribute] = attribute_value
                if dispatch_type not in events:
                    events[dispatch_type] = {}
                events[dispatch_type][status] = d
    return events


def get_interval_delta_for_monitor(dbhelper, monitor_id, start_date, end_date):
    """
    Returns the aggregate kW delta for the given monitor over the given time range from the
    meter_logs.monitor_data_minute table
       - Iterate from start_date to end_date in meter_logs.monitor_data_minute table in 1 minute intervals and
       determines max_kw for time interval
       - Aggregates max_kw measurement
    :param dbhelper:
    :param monitor_id:
    :param start_date:
    :param end_date:
    :return: total_kw: aggregate kW for specified interval
             interval_count: number of intervals sampled
    """

    if end_date <= start_date:
        raise RuntimeError('End date must be later than start date')

    total_kw = 0
    interval_count = 0
    interval = 60
    str_format = "%Y-%m-%d %H:%M:%S"

    # Calculate the kW for the given monitor over a 1 minute period
    interval_start_date = start_date
    interval_end_date = (dt.datetime.strptime(interval_start_date, str_format)
                         + dt.timedelta(seconds=interval)).strftime(str_format)
    while interval_start_date < end_date:
        sql = "SELECT (SUM(kw_total_sum / sample_count) * 0.0167) total_kwh, MAX(kw_total_sum / sample_count) max_kw " \
              "FROM meter_logs.monitor_data_minute WHERE monitor_id = '%s' AND sample_start_datetime " \
              "BETWEEN '%s' and '%s';" \
              % (monitor_id, interval_start_date, interval_end_date)
        result = dbhelper.query(DB_METER_LOGS, sql)

        # keep adding up the results for the intervals
        interval_kw = result[0]['max_kw']
        log = 'Interval: %s - %s max_kw: %s' % (interval_start_date, interval_end_date, interval_kw)
        logger.debug(log)
        if interval_kw is not None:
            total_kw = total_kw + interval_kw
            interval_count = interval_count + 1

        # go to the next interval time frame
        interval_start_date = (dt.datetime.strptime(interval_start_date, str_format)
                               + dt.timedelta(seconds=interval)).strftime(str_format)
        interval_end_date = (dt.datetime.strptime(interval_start_date, str_format)
                             + dt.timedelta(seconds=interval)).strftime(str_format)
    return total_kw, interval_count


def get_battery_output(dbhelper, start_dt, end_dt, monitor_id_list, location_code_list, str_format):
    """
    Returns a list of tuples(total_kw/start/end) from 1 minute meter log table for interval, monitor_id specified
    """
    battery_output = []
    while start_dt < end_dt:
        start = start_dt.strftime(str_format)
        end = (start_dt + dt.timedelta(seconds=60)).strftime(str_format)
        for monitor_id, location_code in zip(monitor_id_list, location_code_list):
            meter_log_values = {'start': start, 'end': end, 'location_code': location_code}
            for monitor in ['%s-site' % monitor_id, '%s-main' % monitor_id, '%s-inverter' % monitor_id]:
                total_kw, _ = get_interval_delta_for_monitor(dbhelper, monitor, start, end)

                # get second index of dash (emulator-9-site, emulator-10-site)
                import re
                indices = [s.start() for s in re.finditer('-', monitor)]
                index = indices[1]

                meter_log_values[monitor[index + 1:]] = total_kw
            battery_output.append(meter_log_values)
        start_dt = start_dt + dt.timedelta(seconds=60)

    logger.debug("\nmonitor_data_minute table results:")
    for interval in battery_output:
        logger.debug('%s -> %s, %s site: %s main: %s inverter: %s',
                     interval['start'],
                     interval['end'],
                     interval['location_code'],
                     interval['site'],
                     interval['main'],
                     interval['inverter'])

    return battery_output


def round_time(time_now=None, round_to=60):
    """
    Rounds time to next nearest interval in seconds for meter log 1 minute table
        60: next minute
        60*30: next half hour
    :param time_now:
    :param round_to:
    :return:
    """
    if time_now is None:
        time_now = dt.datetime.now()
    seconds = (time_now.replace(tzinfo=None) - time_now.min).seconds
    rounding = (seconds + round_to / 2) // round_to * round_to
    test_start_dt = time_now + dt.timedelta(0, rounding - seconds, -time_now.microsecond)
    if test_start_dt.minute == time_now.minute:
        test_start_dt = test_start_dt + dt.timedelta(minutes=1)
    return test_start_dt


def get_resource_dispatch_transaction_data(dbhelper, gridservice_event_id, location_codes, str_format):
    """
    Gets dispatch values for all location codes for given grid service event
    """
    locations = ""
    for location_code in location_codes[:-1]:
        locations = locations + "'" + location_code + "',"
    locations = locations + "'" + location_codes[-1] + "'"
    logger.debug('Using locations: %s', locations)

    sql = "SELECT rdt.gridservice_event_id, " \
          "rdt.resource_id, " \
          "rdt.dispatch_type_id, " \
          "rdt.last_status_id, " \
          "rdt.requested_power, " \
          "rdt.dispatch_start_datetime, " \
          "rdt.dispatch_end_datetime, " \
          "r.location_code " \
          "FROM  grid_services.resource_dispatch_transaction rdt, grid_services.resource r " \
          "WHERE rdt.gridservice_event_id = %s " \
          "AND r.location_code in (%s) " \
          "AND rdt.dispatch_type_id = 4 " \
          "AND r.resource_id = rdt.resource_id order by dispatch_start_datetime;" % (gridservice_event_id, locations)
    result = dbhelper.query(DB_GRID_SERVICES, sql)

    resource_dispatch_requests = []
    for line in result:
        resource_dispatch_requests.append({'start': line['dispatch_start_datetime'].strftime(str_format),
                                           'location_code': line['location_code'],
                                           'requested_power': line['requested_power']})

    if resource_dispatch_requests:
        logger.debug("\nFound correct gs event dispatch results:")
        for line in resource_dispatch_requests:
            logger.debug('%s, location: %s requested power: %s',
                         line['start'],
                         line['location_code'],
                         line['requested_power'])
    else:
        sql = "SELECT rdt.gridservice_event_id, " \
              "rdt.resource_id, " \
              "rdt.dispatch_type_id, " \
              "rdt.last_status_id, " \
              "rdt.requested_power, " \
              "rdt.dispatch_start_datetime, " \
              "rdt.dispatch_end_datetime, " \
              "r.location_code, " \
              "rdts.name " \
              "FROM  grid_services.resource_dispatch_transaction rdt, grid_services.resource r, " \
              "grid_services.resource_dispatch_transaction_status rdts " \
              "WHERE rdt.gridservice_event_id = %s " \
              "AND r.location_code in (%s) " \
              "AND rdt.dispatch_type_id = 4 " \
              "AND rdt.last_status_id = rdts.status_id " \
              "AND r.resource_id = rdt.resource_id order by dispatch_start_datetime;" % \
              (gridservice_event_id, locations)
        result = dbhelper.query(DB_GRID_SERVICES, sql)
        error_resource_dispatch_requests = []
        for line in result:
            error_resource_dispatch_requests.append({'start': line['dispatch_start_datetime'].strftime(str_format),
                                                     'location_code': line['location_code'],
                                                     'requested_power': line['requested_power'],
                                                     'last_status_id': line['last_status_id'],
                                                     'name': line['name']})
        logger.error("\nFound erroneous gs event dispatch results:")
        for line in error_resource_dispatch_requests:
            logger.error('%s, location: %s requested power: %s dispatch_type_id: %s last status: %s',
                         line['start'],
                         line['location_code'],
                         line['requested_power'],
                         line['name'],
                         line['last_status_id'])

    if resource_dispatch_requests:
        logger.debug("\nFound correct gs event dispatch results:")
        for line in resource_dispatch_requests:
            logger.debug('%s, location: %s requested power: %s',
                         line['start'],
                         line['location_code'],
                         line['requested_power'])
    else:
        sql = "SELECT rdt.gridservice_event_id, " \
              "rdt.resource_id, " \
              "rdt.dispatch_type_id, " \
              "rdt.last_status_id, " \
              "rdt.requested_power, " \
              "rdt.dispatch_start_datetime, " \
              "rdt.dispatch_end_datetime, " \
              "r.location_code, " \
              "rdts.name " \
              "FROM  grid_services.resource_dispatch_transaction rdt, grid_services.resource r, " \
              "grid_services.resource_dispatch_transaction_status rdts " \
              "WHERE rdt.gridservice_event_id = %s " \
              "AND r.location_code in (%s) " \
              "AND rdt.dispatch_type_id = 4 " \
              "AND rdt.last_status_id = rdts.status_id " \
              "AND r.resource_id = rdt.resource_id order by dispatch_start_datetime;" % \
              (gridservice_event_id, locations)
        result = dbhelper.query(DB_GRID_SERVICES, sql)
        error_resource_dispatch_requests = []
        for line in result:
            error_resource_dispatch_requests.append({'start': line['dispatch_start_datetime'].strftime(str_format),
                                                     'location_code': line['location_code'],
                                                     'requested_power': line['requested_power'],
                                                     'last_status_id': line['last_status_id'],
                                                     'name': line['name']})
        logger.error("\nFound erroneous gs event dispatch results:")
        for line in error_resource_dispatch_requests:
            logger.error('%s, location: %s requested power: %s dispatch_type_id: %s last status: %s',
                         line['start'],
                         line['location_code'],
                         line['requested_power'],
                         line['name'],
                         line['last_status_id'])

    assert resource_dispatch_requests, 'No resource dispatch results found in expected state, ' \
                                       'see logs above for dispatches and dispatch states that were fetched from db!'
    return resource_dispatch_requests


def cancel_grid_service_event(program_id, gridservice_event_id, settings_url):
    """
    Cancels specified grid service event and waits up to 90 seconds til it is fully in CANCELLED state
    :param program_id:
    :param gridservice_event_id:
    :param settings_url:
    :return:
    """
    if program_id is not None and gridservice_event_id is not None:
        with GridResponseClient(settings_url) as client:
            events = client.event_list(program_id)
            if list(events['events'].keys()):
                client.cancel_dispatch_event(program_id, gridservice_event_id)
                data = client.event_list(program_id)
                cancelled_event = data['events'][str(gridservice_event_id)]
                wait_start = dt.datetime.now()
                while cancelled_event['status'] != 'CANCELLED' and \
                        dt.datetime.now() < (wait_start + dt.timedelta(seconds=90)):
                    time.sleep(15)
                    data = client.event_list(program_id)
                    cancelled_event = data['events'][str(gridservice_event_id)]
    else:
        logger.debug('No event cancelled!')


def calc_diffs(battery_output, resource_dispatch_results, str_format, testdata, gs_interval=5, rel_tol=0.05):
    """
    Compares the battery output from 1 minute meter log tables for resources in gs event to the requested
    amount from gs event
    :param battery_output:
    :param resource_dispatch_results:
    :param str_format:
    :param testdata:
    :param gs_interval: dispatcher interval
    :return:
    """
    total_intervals_count = len(battery_output)
    error_intervals_rel = []
    error_intervals_abs = []
    error_intervals_abs_and_rel = []
    intervals = []

    # iterate through each gs dispatch
    for line in resource_dispatch_results:
        gs_dispatch_start = line['start']
        gs_dispatch_start_dt = dt.datetime.strptime(gs_dispatch_start, str_format)
        gs_dispatch_end = (gs_dispatch_start_dt + dt.timedelta(minutes=gs_interval)).strftime(str_format)
        gs_dispatch_power = line['requested_power']
        gs_dispatch_resource = line['location_code']

        # iterate through 1 minute meter log data
        for interval in battery_output:
            if gs_dispatch_resource == interval['location_code']:
                if interval['start'] >= gs_dispatch_start and interval['end'] <= gs_dispatch_end:
                    if gs_dispatch_power == 0.0 and interval['inverter'] == 0.0:
                        val = 0.0
                    elif gs_dispatch_power == 0.0 and interval['inverter'] != 0.0:
                        gs_dispatch_power = 0.001
                        val = abs(gs_dispatch_power - interval['inverter']) / gs_dispatch_power * 100
                    else:
                        val = abs(gs_dispatch_power - interval['inverter']) / gs_dispatch_power * 100

                    interval_data = {'resource': gs_dispatch_resource, 'gs_start': gs_dispatch_start,
                                     'gs_power': gs_dispatch_power, 'interval_start': interval['start'],
                                     'interval_end': interval['end'], 'interval_site': interval['site'],
                                     'interval_main': interval['main'], 'interval_inverter': interval['inverter'],
                                     'percent_diff': val}

                    # verify we are within relative(5%) and absolute(5kw) tolerances
                    # between actual battery output and requested amount during gs dispatch
                    a = math_util.isclose(interval['inverter'], gs_dispatch_power, rel_tol=0.05)
                    b = math_util.isclose(interval['inverter'], gs_dispatch_power, abs_tol=5)
                    if not a:
                        error_intervals_rel.append(interval_data)
                    if not b:
                        error_intervals_abs.append(interval_data)
                    if not a and not b:
                        error_intervals_abs_and_rel.append(interval_data)

                    intervals.append(interval_data)

    logger.debug('**** INTERVALS')
    if not intervals:
        logger.debug('Printing battery_output START')
        for interval in battery_output:
            logger.debug(interval)
        logger.debug('Printing battery_output END')

        logger.debug('Printing resource_dispatch_results START')
        for interval in resource_dispatch_results:
            logger.debug(interval)
        logger.debug('Printing resource_dispatch_results END')
    assert intervals, 'No intervals found!'

    for i in intervals:
        _print_row(i)

    errors = []
    if error_intervals_abs_and_rel:
        for error_interval in error_intervals_abs_and_rel:
            if error_interval['interval_start'] != error_interval['gs_start']:
                errors.append(error_interval)

    # print out readable results
    logger.debug('**** RESULTS')
    if error_intervals_rel:
        logger.debug('Errors found in relative comparison (greater than 0.05% relative diff)')
        logger.debug('Error intervals: %s, Total intervals: %s', len(error_intervals_rel), total_intervals_count)
        for i in error_intervals_rel:
            _print_row(i)
    if error_intervals_abs:
        logger.debug('Errors found in absolute comparison (greater than 5kW absolute diff)')
        logger.debug('Error intervals: %s, Total intervals: %s', len(error_intervals_abs), total_intervals_count)
        for i in error_intervals_abs:
            _print_row(i)
    if error_intervals_abs_and_rel:
        logger.debug('Errors found in both relative and absolute comparison')
        logger.debug('Error intervals: %s, Total intervals: %s',
                     len(error_intervals_abs_and_rel), total_intervals_count)
        for i in error_intervals_abs_and_rel:
            _print_row(i)
    if errors:
        logger.debug('Errors found in both relative and absolute comparison not the first interval of gs dispatch')
        logger.debug('Error intervals: %s, Total intervals: %s', len(errors), total_intervals_count)
        for i in errors:
            _print_row(i)

    requested_kw_sum = 0
    actual_kw_sum = 0
    actual_site_kw_sum = 0
    for interval in intervals:
        requested_kw_sum = requested_kw_sum + interval['gs_power']
        actual_kw_sum = actual_kw_sum + interval['interval_inverter']
        actual_site_kw_sum = actual_site_kw_sum + interval['interval_site']

    actual_site_kw_average = actual_site_kw_sum / len(intervals)
    logger.debug('Avg site kw %s', actual_site_kw_average)
    logger.debug('Total grid amount requested: %s ', requested_kw_sum)
    logger.debug('Total grid amount requested over time period: %s ', requested_kw_sum / len(intervals))
    logger.debug('Actual total amount discharged: %s', actual_kw_sum)
    logger.debug('Actual total amount discharged over time period: %s ', actual_kw_sum / len(intervals))
    expected_output = testdata['requested_capacity_kw'] * 60 * len(intervals) / 60
    logger.debug('Expected output %s (kwh)', expected_output)
    # meter_requested_kw = mathutil.isclose(actual_kw_sum, testdata['total_requested_kwh'], rel_tol=0.10)
    # grid_requested_kw = mathutil.isclose(requested_kw_sum, testdata['total_requested_kwh'], rel_tol=0.10)
    # assert meter_requested_kw and grid_requested_kw, 'gs requested kw and actual requested kw should be w/in ' \
    #                                                 '10 percent of %s' % testdata['total_requested_kwh']
    assert math_util.isclose(actual_kw_sum, requested_kw_sum, rel_tol=rel_tol), \
        'gs requested kw and actual requested kw should be w/in %s percent of one another' % rel_tol


def _print_row(i):
    interval_data_str = 'Location: %s GS: %s GS(kw): %s Interval: %s-%s Site: %s Main: %s ' \
                        'Inverter(kw): %s Percent_diff: %s' \
                        % (i['resource'],
                           i['gs_start'],
                           i['gs_power'],
                           i['interval_start'],
                           i['interval_end'],
                           i['interval_site'],
                           i['interval_main'],
                           i['interval_inverter'],
                           i['percent_diff'])
    logger.debug(interval_data_str)


def get_battery_soc(dbhelper, interval_start_dt, interval_end_dt, host_name, charge_index):
    """
    Returns a list of SOC from 'Powerstore_bms_minute' table for interval, host specified
    """
    sql = "SELECT hostname, msg_timestamp_start_datetime, %s " \
          "FROM optimization.powerstore_bms_minute " \
          "WHERE hostname LIKE '%s' " \
          "AND msg_timestamp_start_datetime BETWEEN '%s' AND '%s' " \
          "ORDER by msg_timestamp_start_datetime;" \
          % (charge_index, host_name, interval_start_dt, interval_end_dt)

    return dbhelper.query_with_retry(DB_OPTIMIZATION, sql, retry_count=3, retry_time=30)


def setup_emulator_resources(dbhelper, testdata):
    """
    Iterates through list of resources defined in test data and creates and starts emulators
    :param dbhelper:
    :param testdata:
    :return:
    """
    for resource in testdata['resources']:
        # setup emulator container
        batterysoc = None
        setpoint = None
        if 'batterysoc' in resource:
            batterysoc = resource['batterysoc']
        if 'setpoint' in resource:
            setpoint = resource['setpoint']

        setup_emulator_container(emulator_name=resource['emulator_hostname'])

        # configure emulator to replay telemetry
        configure_simple_emulator(dbhelper,
                                  target_location=resource['target_location'],
                                  emulator_hostname=resource['emulator_hostname'],
                                  source_location='qa-QA_CLIENT_00_SAMPLE_001-1-M1',
                                  sample_start_date='2017-01-11 18:30:00.5500',
                                  sample_end_date='2017-01-11 21:29:59.6329',
                                  batterysoc=batterysoc,
                                  setpoint=setpoint)


def wait_for_event_and_verify(client, expected_events_list, timeout):
    """
    Wait for gs event to be created and verify its contents
    :param client:
    :param expected_events_list:
    :param timeout:
    :return:
    """
    for expected_event in expected_events_list:
        program_id = get_program_id(client, expected_event['program'])
        # wait for integration service to pick up the email to create the gs event, periodically poll for new
        # scheduled gs events
        scheduled_events = wait_for_event(client, program_id, timeout)
        # verify one gs event is scheduled
        assert len(scheduled_events) == 1, 'GS event not schedule within %s min...' % timeout
        assert scheduled_events[0]['status'] == 'SCHEDULED'
        assert scheduled_events[0]['deployment_start'] == expected_event['utc_start']
        assert scheduled_events[0]['deployment_end'] == expected_event['utc_end']


def wait_for_event(client, program_id, timeout):
    """
    Wait for expected event or timeout, returns scheduled events found for program, if any
    :param client:
    :param program_id:
    :param timeout:
    :return:
    """
    wait_start = dt.datetime.now()
    scheduled_events = []
    while not scheduled_events and dt.datetime.now() < wait_start + dt.timedelta(seconds=timeout):
        data = client.event_list(program_id)
        events = data['events']
        if events:
            for event in list(events.values()):
                if event['status'] == 'SCHEDULED':
                    logger.debug('Found a new scheduled event!')
                    scheduled_events.append(event)
                else:
                    logger.debug('Found event status: %s', event['status'])
        if not scheduled_events:
            logger.debug('No scheduled events found, sleeping for 15sec...')
            time.sleep(15)
    return scheduled_events


def create_event(client, dbhelper, event_end, event_start, monitor_id_list, program_id, requested_capacity_kw):
    """
    Wait for interval data to populate the powerstore_bms_minute, monitor_data_minute tables for the resources in the
    program before initiating event
    :param client:
    :param dbhelper:
    :param event_end:
    :param event_start:
    :param monitor_id_list:
    :param program_id:
    :param requested_capacity_kw:
    :return:
    """
    for monitor_id in monitor_id_list:
        # build up some data in optimization.powerstore_bms_minute table before schedule of event
        sql = "SELECT count(*) FROM optimization.powerstore_bms_minute where hostname = '%s';" % monitor_id
        _wait_for_count(dbhelper, sql, DB_OPTIMIZATION, 10, 15)
        # build up some data in monitor_data_minute table before schedule of event
        sql = "SELECT count(*) FROM meter_logs.monitor_data_minute where monitor_id = '%s-site';" % monitor_id
        _wait_for_count(dbhelper, sql, DB_METER_LOGS, 10, 15)

    # create an event
    data = client.create_dispatch_event(program_id, event_start, event_end,
                                        requested_kw=requested_capacity_kw, timezone=timezone)
    return data


def _wait_for_count(dbhelper, sql, db, retry_count, sleep_time):
    """
    Executes sql query waiting for the count of intervals to exist
    :param dbhelper:
    :param sql:
    :param db:
    :return:
    """
    count = 0
    retry = 0
    while count == 0 and retry < retry_count:
        result = dbhelper.query(db, sql)
        if result:
            key = list(result[0].keys())[0]
            count = result[0][key]
            logger.debug("Sql result for column '%s' is %s", key, count)
        logger.debug('Sleeping %s sec', sleep_time)
        time.sleep(sleep_time)
        retry = retry + 1

    if not count:
        raise ValueError('Failed while waiting for interval data to populate tables. count(*) is still 0.')


def get_monitor_ids_and_location_codes(testdata):
    # get the monitor_id and location_code for each resource
    monitor_id_list = []
    location_codes_list = []
    for resource in testdata['resources']:
        monitor_id_list.append(resource['emulator_hostname'])
        location_codes_list.append(resource['target_location'])
    return location_codes_list, monitor_id_list


def get_filtered_program_events(event_id_list, program_name, status_list=None):
    """
    Returns all events from grid_services.grid_service_event_received that match search status
    This method is used in openadr tests.
    :param event_id_list:
    :param program_name:
    :param status_list: if status_list is None, return all events for the program
                            where event_id == prog_event['reference_id']
                        if status_list is not None, return all events for the program
                            where status_id in status_list
    :return:
    """
    grid_events = []
    with GridResponseClient(settings['stem-web-app']['url']) as client:
        program_id = get_program_id(client, program_name)
        prog_events = client.event_list(program_id)['events']
        for event_id in event_id_list:
            for prog_event in list(prog_events.values()):
                if status_list:
                    if prog_event['status'] in status_list \
                            and event_id in prog_event['reference_id'] \
                            and prog_event not in grid_events:
                        grid_events.append(prog_event)
                else:
                    if event_id in prog_event['reference_id'] and prog_event not in grid_events:
                        grid_events.append(prog_event)

    return grid_events


def wait_for_telemetry_for_resources(dbhelper, monitor_id_list, location_codes):
    """
    Wait for interval data to populate the powerstore_bms_minute, monitor_data_minute tables for the resources in the
    program before initiating event
    :param dbhelper:
    :param monitor_id_list:
    :param location_codes:
    :return:
    """
    for monitor_id in monitor_id_list:
        # build up some data in monitor_data_minute table before schedule of event
        sql = "SELECT count(*) FROM meter_logs.monitor_data_minute where monitor_id = '%s';" % monitor_id
        _wait_for_count(dbhelper, sql, DB_METER_LOGS, 14, 15)
    for location_code in location_codes:
        # build up some data in optimization.powerstore_bms_minute table before schedule of event
        sql = "SELECT count(*) FROM optimization.powerstore_bms_minute where hostname = '%s';" % location_code.lower()
        _wait_for_count(dbhelper, sql, DB_OPTIMIZATION, 14, 15)


def create_event_new_emu(dbhelper, client, inverters, locations, program_id, testdata):
    """
    Wait for telemetry and then start gs event
    :param dbhelper:
    :param client:
    :param inverters:
    :param locations:
    :param program_id:
    :param testdata:
    :return:
    """
    wait_for_telemetry_for_resources(dbhelper, inverters, locations)

    # calculate start and end times for gs event, create event
    # NOTE: we should probably wait til telemetry starts before calculating gs event start/end
    # (now need to wait up to 2min due to telemetry server processing for it to appear in DB)
    event_start_dt = \
        round_time(dt.datetime.now(tz=UTC)) + dt.timedelta(seconds=testdata['event_start_time'])
    event_end_dt = event_start_dt + dt.timedelta(seconds=testdata['event_duration'])
    event_start = event_start_dt.strftime(str_format)
    event_end = event_end_dt.strftime(str_format)
    logger.debug('Event: start: %s end: %s', event_start, event_end)
    data = client.create_dispatch_event(program_id, event_start, event_end,
                                        requested_kw=testdata['requested_capacity_kw'], timezone=timezone)
    assert not data['execution_errors'], 'Execution errors found %s' % data['execution_errors']
    gridservice_event_id = data['gridservice_event_id'][0]
    logger.debug("New grid service event with id %s created ", gridservice_event_id)
    assert data['execution_success'], 'Execution of gs event failed'
    return event_start_dt, gridservice_event_id


def get_monitors_as_list(dbhelper, locations):
    """
    For a set of locations, returns inverter, site, meter monitor ids
    :param dbhelper:
    :param locations:
    :return:
    """
    inverters = []
    sites = []
    meters = []
    for location in locations:
        sql = "SELECT location_code, monitor_id FROM business.location_monitor where location_code like '%s%%'" \
              % location
        results = dbhelper.query(None, sql)
        for result in results:
            if result['location_code'].endswith('_INVERTER'):
                inverters.append(result['monitor_id'])
            if result['location_code'].endswith('_MAIN'):
                meters.append(result['monitor_id'])
            if result['location_code'].endswith('_SITE'):
                sites.append(result['monitor_id'])
    return inverters, sites, meters

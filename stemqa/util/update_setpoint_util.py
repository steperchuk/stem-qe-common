import socket
import sys
import threading

from kombu import Connection, Exchange, Queue, Consumer, Producer

from stemqa.config import logconfig

logger = logconfig.get_logger(__name__)


class MyConsumer(threading.Thread):
    """
    This class is used to update the setpoint on the emulator and receive the reply
    Class needs to be generalized further when we understand how more commands are sent down to powerstore to update
    it via the 'pgx_cmd_relay' exchange.
    """

    def __init__(self, rabbit_url, reply_queue_name, listen_timeout=60):
        threading.Thread.__init__(self)
        self.conn = Connection(rabbit_url)
        self.channel = self.conn.channel()
        consumer_exchange = Exchange('pgx', type='topic', durable=False)
        self.reply_queue = Queue(reply_queue_name, exchange=consumer_exchange,
                                 routing_key=reply_queue_name,
                                 channel=self.conn.channel(), exclusive=True)
        self.listen_timeout = listen_timeout

    def process_message(self, body):
        logger.debug('Reply: %s', body)
        self._reply = body

    def run(self):
        consumer = Consumer(self.conn, queues=self.reply_queue, callbacks=[self.process_message])
        consumer.consume()
        try:
            self.conn.drain_events(timeout=60)
        except socket.timeout:
            logger.warn('Failed to receive expected message(s) before %s sec timeout', self.listen_timeout)
            sys.exit(1)
        finally:
            self.conn.close()


class MyProducer(object):

    def __init__(self, rabbit_url, emulator_name):
        producer_connection = Connection(rabbit_url)
        producer_exchange = Exchange('pgx_cmd_relay', type='topic', durable=False)
        self.producer = Producer(exchange=producer_exchange,
                                 channel=producer_connection.channel(),
                                 routing_key='%s_cmd' % emulator_name)

    def execute(self, reply_queue_name, setpoint):
        msg = '{"reply_exchange": "pgx", "args": { "new_setpoint":%s }, ' \
              '"command": "update_setpoint", "reply_queue": "%s"}' \
              % (setpoint, reply_queue_name)
        logger.debug('Sending message... %s', msg)
        self.producer.publish(msg)


def update_setpoint(settings, setpoint, emulator_name):
    rabbit_url = 'amqp://%s:%s@%s:%s' % (settings['rmq']['username'],
                                         settings['rmq']['password'],
                                         settings['rmq']['host'],
                                         settings['rmq']['rabbit-port'])

    # setup consumer connection, channel and start listening on the specified reply queue
    # consumer waits for response msgs for specified services or timeout(default = 60sec)
    reply_queue_name = 'routable.replies.iterate_command_init_reply_queue_%s' % emulator_name
    c = MyConsumer(rabbit_url, reply_queue_name)
    c.start()
    # # setup producer to send command
    p = MyProducer(rabbit_url, emulator_name)
    p.execute(reply_queue_name, setpoint)
    c.join()
    return c._reply

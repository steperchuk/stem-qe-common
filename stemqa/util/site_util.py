import logging

from stemqa.helpers.db_helper import DbHelper, DB_FORECAST, DB_TOPOLOGY

logger = logging.getLogger(__name__)


def get_zip(test_site):
    """
    Get zip code for target location
    :param test_site: Test site location code
    :return: postal code for test site (str)
    """
    dbhelper = DbHelper.default_instance()
    zip_sql = "SELECT postal_code FROM topology.address " \
              "WHERE address_id IN (" \
              "SELECT address_id FROM topology.location_address " \
              "WHERE location_code = '%s');" % test_site
    response = dbhelper.query(DB_TOPOLOGY, zip_sql)

    zip_code = db_respond(response, 'postal_code')

    return str(zip_code)


def get_psid(test_site):
    """
    Get PowerStore ID by target location
    :param test_site: Test site location code
    :return: PowerStore ID (str). ID = 'ERROR', if record doesn't exist.
    """
    dbhelper = DbHelper.default_instance()
    ps_sql = "SELECT powerstore_id FROM forecast.powerstore WHERE name='%s';" % test_site
    response = dbhelper.query(DB_FORECAST, ps_sql)

    ps_id = db_respond(response, 'powerstore_id')

    return str(ps_id)


def get_tariff_id(test_site):
    """
    Get tariff ID by location_name
    :param test_site: Test site location name (str)
    :return: tariff id (str)
    """
    dbhelper = DbHelper.default_instance()
    tariff_sql = "SELECT tariff_schedule_id FROM topology.location_tariff_info WHERE location_code = '%s';" \
                 % test_site
    response = dbhelper.query(DB_TOPOLOGY, tariff_sql)

    tariff_id = db_respond(response, 'tariff_schedule_id')

    return str(tariff_id)


def get_host_name(ps_id):
    """
    Get hostname by PowerStore ID
    :param ps_id: PowerStore ID (str)
    :return: hostname (str)
    """
    dbhelper = DbHelper.default_instance()
    hostname_sql = "SELECT hostname FROM forecast.powerstore WHERE powerstore_id = '%s';" % ps_id

    response = dbhelper.query(DB_TOPOLOGY, hostname_sql)

    host_name = db_respond(response, 'hostname')

    return str(host_name)


def get_site_tz(site_name):
    """
    Get site time zone from the database
    :param site_name: location code for site
    :return: timezone
    """
    dbhelper = DbHelper.default_instance()
    timezone_sql = "SELECT timezone FROM topology.location WHERE location_code = '%s';" % site_name

    response = dbhelper.query(DB_TOPOLOGY, timezone_sql)

    site_tz = db_respond(response, 'timezone')

    return str(site_tz)


def get_oe_version(site_name):
    """
    Get OE version for test site from forecast.powerstore
    :param site_name: test site name
    :return: oe version
    """
    dbhelper = DbHelper.default_instance()
    oe_sql = "SELECT oe_version FROM forecast.powerstore where name = '%s';" % site_name

    response = dbhelper.query(DB_FORECAST, oe_sql)

    oe_version = db_respond(response, 'oe_version')

    return str(oe_version)


def get_asset_id(location_code):
    """
    Get asset id by location code
    :param location_code: location code
    :return: asset id
    """
    dbhelper = DbHelper.default_instance()
    asset_sql = "SELECT asset_id FROM topology.location_asset " \
                "WHERE location_code = '{0}_SITE' OR location_code = '{0}-site';" \
        .format(location_code)

    response = dbhelper.query(DB_TOPOLOGY, asset_sql)

    asset_id = db_respond(response, 'asset_id')

    return str(asset_id)


def get_topology_id(asset_id, emul=False):
    """
    Get topology id based on asset id and emulator/non-emulator site
    :param asset_id: Asset ID
    :param emul: If test site works with emilator should be 'True'
    :return: PowerStore name
    """

    dbhelper = DbHelper.default_instance()
    topology_sql = "SELECT topology_id, hostname FROM forecast.topology WHERE hostname like '%%%s%%';" % asset_id

    response = dbhelper.query(DB_FORECAST, topology_sql)

    # Selection based on current naming convention:
    # - there could be two topologies for asset id: with and without emulator
    # - topology with emulator has "INVERTER" in hostname

    for each in response:
        if emul:
            if 'INVERTER' in each['hostname']:
                return each['topology_id']
        else:
            if 'INVERTER' not in each['hostname']:
                return each['topology_id']

    raise RuntimeError("Cannot retrieve topology id.")


def get_ps_name(topology_id):
    """
    Get PowerStore name based on topology id
    :param topology_id: topology id
    :return: PowerStore name (str)
    """
    dbhelper = DbHelper.default_instance()
    ps_sql = "SELECT name FROM forecast.powerstore WHERE topology_id = '%s';" % topology_id

    response = dbhelper.query(DB_FORECAST, ps_sql)

    ps_name = db_respond(response, 'name')

    return str(ps_name)


def get_reading_id(location_code):
    """
    Get meter reading table schedule id for site, based on location code
    :param location_code:
    :return: reading_id
    """
    dbhelper = DbHelper.default_instance()
    reading_sql = "SELECT meter_reading_schedule_table_id FROM topology.location_tariff_info " \
                  "WHERE location_code = '%s';" % location_code

    response = dbhelper.query(DB_TOPOLOGY, reading_sql)

    reading_id = db_respond(response, 'meter_reading_schedule_table_id')

    return str(reading_id)


def site_info(test_site, emulator=False):
    """
    Collect information for test site
    :param test_site: test site location code
    :param emulator: If test runs with emulator, should be set to 'True'. By default - 'False'
    :return: my_site - dictionary with site parameters
    """

    my_site = {'location_code': str(test_site),
               'reading_id': str(get_reading_id(test_site)),
               'zip_location': str(get_zip(test_site)),
               'asset_id': str(get_asset_id(test_site)),
               'timezone': str(get_site_tz(test_site)),
               'tariff_id': str(get_tariff_id(test_site)),
               'topology_id': '',
               'ps_name': '',
               'ps_id': '',
               'hostname': '',
               'oe_version': ''}

    if my_site['asset_id'] != 'None':
        my_site['topology_id'] = str(get_topology_id(my_site['asset_id'], emulator))
        my_site['ps_name'] = str(get_ps_name(my_site['topology_id']))
        my_site['ps_id'] = str(get_psid(my_site['ps_name']))
        my_site['hostname'] = str(get_host_name(my_site['ps_id']))
        my_site['oe_version'] = str(get_oe_version(my_site['ps_name']))

    return my_site


def db_respond(response, field_name, record=0):
    """
    Format response from SELECT
    :param response: Database response
    :param field_name: Name of the field in response
    :param record: number in collection: 0 for first, -1 for last
    :return:
    """

    if response:
        my_value = response[record][field_name]
    else:
        logger.error("No records found in database.")
        my_value = None

    return my_value


def get_coordinates(site_name):
    """
    Return latitude and longitude of target location
    :param: site_name: site name
    :return: latitude and longitude
    """
    dbhelper = DbHelper.default_instance()
    address_sql = "SELECT address_id FROM topology.location_address where location_code = '%s';" % site_name
    response = dbhelper.query(DB_TOPOLOGY, address_sql)
    address_id = db_respond(response, 'address_id')
    coordinated_sql = "SELECT latitude, longitude FROM topology.address where address_id = '%s';" % address_id
    response = dbhelper.query(DB_TOPOLOGY, coordinated_sql)
    latitude = db_respond(response, 'latitude')
    longitude = db_respond(response, 'longitude')

    return latitude, longitude


def get_sites_collection(location_code, location_group_type_id=1):
    """
    Return list of sites based on location code and location_group_type_id
    :param location_code: (str) location_code
    :param location_group_type_id: int
    :return: list of locations
    """
    test_sites = list()
    dbhelper = DbHelper.default_instance()
    sites_sql = "SELECT location_code FROM topology.location " \
                "WHERE location_group_type_id = '{type_id}' AND location_code like '%{location}%';". \
        format(type_id=location_group_type_id, location=location_code)
    response = dbhelper.query(DB_TOPOLOGY, sites_sql)

    for item in response:
        test_sites.append(item['location_code'])
    return test_sites


def update_site_timezone(time_zone, location_code):
    """
    Updates site timezone in topology.location
    :param time_zone: str
    :param location_code: str
    :return:
    """
    dbhelper = DbHelper.default_instance()
    update_timezone_sql = "UPDATE topology.location SET timezone = '{time_zone}' " \
                          "WHERE location_code like '%%{location}%%';". \
        format(time_zone=time_zone, location=location_code)
    dbhelper.query(DB_TOPOLOGY, update_timezone_sql)


def get_topology_location(location_code):
    """
    Get all data from topology.location table for specific location code
    :param location_code: str
    :return: query result
    """
    dbhelper = DbHelper.default_instance()
    ps_sql = "SELECT * FROM topology.location WHERE location_code = '%s';" % location_code
    response = dbhelper.query(DB_FORECAST, ps_sql)
    return response


def get_topology_location_attribute(location_code):
    """
    Get all data from topology.location_attribute table for specific location code
    :param location_code: str
    :return: query result
    """
    dbhelper = DbHelper.default_instance()
    ps_sql = "SELECT * FROM topology.location_attribute WHERE location_code = '%s';" % location_code
    response = dbhelper.query(DB_FORECAST, ps_sql)
    return response

import datetime as dt
from time import sleep

import pytz

from stemqa.consts import SECONDS_IN_DAY, ISO8601_TIME_FORMAT
from stemqa.config import logconfig

logger = logconfig.get_logger(__name__)


def calculate_days_offset(end_time, start_time, offset):
    """
    Calculates days offset for specific dates which is used for meter and weather historical log data creation
    :param end_time: date for which offset should be applied
    :param start_time: log file start time
    :param offset: offset value in days
    :return: day offset int value
    """
    days_offset = 0
    if offset is not None:
        delta = end_time - start_time
        days_offset = int(delta.total_seconds() / SECONDS_IN_DAY) - offset
    logger.debug("Dates in logs will be updated for {d} days.".format(d=offset))
    return days_offset


def convert_to_iso_date(date_time):
    return dt.datetime.strftime(date_time.replace(second=0, microsecond=0), ISO8601_TIME_FORMAT)


def choose_midnight_timezone():
    """
    Returns midnight time zone
    :return: time_zone
    """
    for time_zone in pytz.common_timezones:
        tz = pytz.timezone(time_zone)

        # Get current local time for timezone
        right_now_dt = dt.datetime.now(tz).time()

        # Exit from loop when find zone, with local time between 11pm and midnight
        if right_now_dt.hour == 23:
            return time_zone


def wait_for_next_hour(date_time):
    """
    Sleep while date time min 0 or less then 55
    Used for test_oe_orchestration
    :param date_time:
    :return: utcnow
    """
    if date_time.minute < 1 or date_time.minute > 55:
        while date_time.minute < 1 or date_time.minute > 55:
            sleep(10)
            logger.info("Sleeping 10 sec while awaiting next hour starts.")
            return dt.datetime.utcnow()
    return date_time

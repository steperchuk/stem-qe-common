import time

from stemqa.config import logconfig
from stemqa.helpers.db_helper import DbHelper, DB_PORTAL
from stemqa.tools.genabilityclient import get_tariff_versions

logger = logconfig.get_logger(__name__)


def get_revisions_count_by_id(mtid):
    """
    Query genability for the available revisions count
    :param mtid:
    :return: count
    """
    response = get_tariff_versions(mtid)
    return response.json()['count']


def wait_for_tariff_revisions_by_id(mtid, timeout=800):
    """
    Query genability for the revisions available and wait for all revisions to be imported to DB
    :param mtid:
    :param timeout:
    :return:
    """
    versions = get_revisions_count_by_id(mtid)

    dbHelper = DbHelper.default_instance()
    sql_versions = "SELECT count(*) count FROM portal.trf_tariff_sched_version " \
                   "WHERE tariff_sched_id = (SELECT id FROM portal.trf_tariff_sched WHERE alt_id = '%s')" % mtid
    time_end = time.time() + timeout
    version_count = 0
    while version_count != versions and time.time() < time_end:
        result = dbHelper.query(DB_PORTAL, sql_versions)
        version_count = result[0]['count']
        if version_count == versions:
            logger.debug('All %s versions found', version_count)
            return True
        logger.debug('Sleeping for 10seconds, %s versions found', version_count)
        time.sleep(10)
    return False

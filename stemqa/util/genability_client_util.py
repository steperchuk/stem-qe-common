import logging

from stemqa.tools.genabilityclient import get_tariffs_by_utility, get_genability_utilites, search_for_tariffs

logger = logging.getLogger(__name__)

"""
Utility methods used in conjunction with Genability API calls
"""


def get_genability_utility_list():
    """
    Utility method to get list of all utilities for genability
    :return:
    """
    response = get_genability_utilites(0, 1)

    num_total_utilities = response.json()['count']

    utility_list = []

    num_utilites_per_request = 100

    num_indexes = int(round(num_total_utilities / num_utilites_per_request)) + 1

    for index in range(0, num_indexes):
        response = get_genability_utilites(index * 100, 100)
        results = response.json()['results']
        for result in results:
            utility_list.append(result)

    return utility_list


def get_utility_tariffs(genability_utility_id):
    """
    Utility method to get list of tariffs for specified utility
    :param genability_utility_id:
    :return:
    """
    response = get_tariffs_by_utility(genability_utility_id=genability_utility_id, page_count=1)

    num_total_tariffs = response.json()['count']

    tariff_list = []

    num_tariff_per_request = 100

    num_indexes = int(round(num_total_tariffs / num_tariff_per_request)) + 1

    for index in range(0, num_indexes):
        response = get_tariffs_by_utility(genability_utility_id=genability_utility_id,
                                          page_start=index * 100,
                                          page_count=100)
        results = response.json()['results']
        for result in results:
            tariff_list.append(result)
            # logger.debug('Tariff name: %s master tariff id: %s' % (result['tariffName'], result['masterTariffId']))

    return tariff_list


def get_utilities():
    """
    Utility method to get list of utilities
    :param genability_utility_id:
    :return:
    """
    response = get_genability_utilites()

    num_total_tariffs = response.json()['count']

    utility_list = []

    num_tariff_per_request = 100

    num_indexes = int(round(num_total_tariffs / num_tariff_per_request)) + 1

    for index in range(0, num_indexes):
        response = get_genability_utilites(page_start=index * 100, page_count=100)
        results = response.json()['results']
        for result in results:
            utility_list.append(result)

    return utility_list


def get_genability_utility(utility_name):
    """
    Utility method to get genability utility
    :param utility_name:
    :return:
    """
    utility_list = get_genability_utility_list()
    utility = next((utility for utility in iter(utility_list) if utility_name in utility['name']), None)
    if utility:
        return utility
    else:
        raise RuntimeError('No utility with name %s found' % utility_name)


def get_genability_tariff_versions_list_by_name(tariff_list, tariff_name):
    """
    Utility method to get genability utility
    :param utility_name:
    :return:
    """
    tariff_versions_list = [tariff for tariff in tariff_list if tariff_name == tariff['tariffName']]
    return tariff_versions_list


def get_genability_tariff_versions_list_by_tariffCode(tariff_list, tariff_code):
    """
    Utility method to get list of tariff versions based on tariff code
    :param utility_name:
    :return:
    """
    tariff_versions_list = [tariff for tariff in tariff_list if tariff_code == tariff['tariffCode']]
    return tariff_versions_list


def get_genability_tariff_versions_list_by_masterTariffId(tariff_list, master_tariff_id):
    """
    Utility method to get list of tariff versions based on master tariff id
    :param utility_name:
    :return:
    """
    tariff_versions_list = [tariff for tariff in tariff_list if master_tariff_id == tariff['masterTariffId']]
    return tariff_versions_list


def get_tariffs_for_utility(gen_util_id):
    """
    Returns a map of tariffs
        - key: masterTariffId
        - value: tuple (tariffName, list of versions)
    :param gen_util_id:
    :return:
    """
    tariff_list = get_utility_tariffs(gen_util_id)
    tariff_version_dict = {}
    for tariff in tariff_list:
        if tariff['customerClass'] != 'RESIDENTIAL':
            if tariff['masterTariffId'] not in list(tariff_version_dict.keys()):
                tariff_version_dict[tariff['masterTariffId']] = {'tariffName': tariff['tariffName'],
                                                                 'tariffCode': tariff['tariffCode'],
                                                                 'versions': [tariff['tariffId']]}
            else:
                versions = tariff_version_dict[tariff['masterTariffId']]['versions']
                versions.append(tariff['tariffId'])
                tariff_version_dict[tariff['masterTariffId']] = {'tariffName': tariff['tariffName'],
                                                                 'tariffCode': tariff['tariffCode'],
                                                                 'versions': versions}
    return tariff_version_dict


def search_for_tariff_with_exact_code_match(search=None, searchOn=None):
    data = search_for_tariffs(search=search, searchOn=searchOn).json()
    tariff_list = data['results']
    tariff_list_result = [tariff for tariff in tariff_list if tariff[searchOn] == search]
    return tariff_list_result

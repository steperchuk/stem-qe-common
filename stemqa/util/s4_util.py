import logging

from minio import Minio
from minio.error import ResponseError

from stemqa.config.settings import settings

logger = logging.getLogger(__name__)


class S4Util(object):
    def __init__(self,
                 endpoint='%s:%s' % (settings['s4server']['host'],
                                     settings['s4server']['app-port']),
                 access_key=settings['s4server']['access-key'],
                 secret_key=settings['s4server']['secret-key']):
        self._client = Minio(endpoint,
                             access_key=access_key,
                             secret_key=secret_key,
                             secure=False)
        logger.debug('Initialized s4 client with access key: %s', access_key)

    def get_all_bucket_names(self):
        return [bucket.name for bucket in self._client.list_buckets()]

    def remove_objects(self, bucket_name, object_names):
        try:
            del_errors = []
            for del_err in self._client.remove_objects(bucket_name, object_names):
                del_errors.append(del_err)
                logger.debug("Deletion Error: %s", del_err)
            if del_errors:
                raise Exception('S4 deletion errors: %s' % del_errors)
        except ResponseError as err:
            raise err

    def list_objects(self, bucket_name, prefix, recursive=False):
        return self._client.list_objects(bucket_name, prefix, recursive)

    def list_objects_names(self, bucket_name, prefix, recursive=False):
        objects = self.list_objects(bucket_name, prefix, recursive)
        object_names = [obj.object_name for obj in objects]
        return object_names

    def get_object(self, bucket_name, object_name):
        return self._client.get_object(bucket_name, object_name)

    def put_object(self, bucket_name, object_name, data, size, content_type='text/plain'):
        return self._client.put_object(bucket_name, object_name, data, size, content_type)

import logging

import boto3
import botocore

from stemqa import consts

logger = logging.getLogger(__name__)


class S3Util(object):
    """
    this class is responsible for accessing s3 objects
    we use the default aws user 'qe_s3_read_only' and the default 'stemqa-sampledata' bucket
    """

    def __init__(self, aws_access_key_id=consts.AWS_ACCESS_KEY_ID_QE_READ_ONLY,
                 aws_secret_access_key=consts.AWS_SECRET_ACCESS_KEY_QE_READ_ONLY):
        self._client = boto3.client(service_name='s3', aws_access_key_id=aws_access_key_id,
                                    aws_secret_access_key=aws_secret_access_key)
        logger.debug('Initialized s3 client with aws_access_key %s', aws_access_key_id)

    def get_s3_object(self, s3_resource, bucket_name=consts.AWS_DEFAULT_BUCKET):
        try:
            return self._client.get_object(Bucket=bucket_name, Key=s3_resource)
        except botocore.exceptions.ClientError as ex:
            logger.error('Failed to get S3 resource %s under bucket %s', s3_resource, bucket_name)
            raise ex

    def download_s3_file(self, s3_file, local_output_file, bucket_name=consts.AWS_DEFAULT_BUCKET):
        try:
            self._client.download_file(bucket_name, s3_file, local_output_file)
        except botocore.exceptions.ClientError as ex:
            logger.error('Failed to download S3 resource %s under bucket %s', s3_file, bucket_name)
            raise ex

    def get_s3_file_contents(self, s3_file, bucket_name=consts.AWS_DEFAULT_BUCKET):
        try:
            obj = self._client.get_object(Bucket=bucket_name, Key=s3_file)
            return obj['Body'].read().decode()
        except botocore.exceptions.ClientError as ex:
            logger.error('Failed to download S3 resource %s under bucket %s', s3_file, bucket_name)
            raise ex

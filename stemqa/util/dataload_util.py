import json
import logging

import requests

from stemqa.config.settings import settings

logger = logging.getLogger(__name__)


def load_data(filepath, options, upload=False):
    """
    Loads CSV data into database using the sql executor web app running inside mysql local container
    :param filepath: path to csv file
    :param options: user specified optins for the data load
    :param upload: if the file needs to be uploaded
    :return:
    """
    logger.debug('Importing data from %s into db using options: %s', filepath, options)
    files = None
    data = json.dumps({"files": [filepath], "options": options})  # data var is type str
    if upload:
        files = {filepath: (filepath, open(filepath, 'r'), 'text/csv', {'Expires': '0'})}
        data = {"data": data}  # data var is now dict
    hostname = settings['mysql']['host']
    port = settings['mysql']['upload-port']

    response = requests.post(url='http://%s:%s/import' % (hostname, port),
                             files=files,
                             data=data)
    assert response.json()['status'] == 'SUCCESS', str(response.json())
    logger.debug('Imported!')

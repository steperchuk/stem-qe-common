import json
import os
import tempfile
from csv import reader
from datetime import datetime as dt
from itertools import zip_longest

from stemqa.config.settings import settings
from stemqa.util import math_util
from stemqa.util.s3_util import S3Util
from stemqa.config.logconfig import get_logger

WEB_URL = settings['stem-web-app']['url']

logger = get_logger(__name__)


#########################
# utility methods
#########################

def obtain_expected_results_file(expected_results_filename, s3_expected_filename):
    """
    Obtain the expected results file from local, if present, or s3
    :param expected_results_filename:
    :param s3_expected_filename:
    :return: expected_results_file_object
    """

    try:
        LOCAL_SUFFIX = ".local"
        expected_results_filename_local = expected_results_filename + LOCAL_SUFFIX

        logger.debug('Check for existence of local (S3 override) expected file %s ',
                     expected_results_filename_local)
        with open(expected_results_filename_local, 'r') as expected_results_file:
            from shutil import copyfile

            logger.debug('Copy file %s to %s', expected_results_filename_local, expected_results_file.name)
            copyfile(expected_results_filename_local, expected_results_filename)
            logger.info('Test will use local (S3 override) expected results file')
    except Exception:
        logger.debug('Local file not present, fall back to S3')
        with open(expected_results_filename, 'w') as expected_results_file:
            logger.debug('Exporting expected results to %s ', expected_results_file.name)
            s3util = S3Util()
            s3_file = s3_expected_filename
            s3util.download_s3_file(s3_file=s3_file, local_output_file=expected_results_file.name)
            logger.debug('Saving s3 file %s to %s', s3_file, expected_results_file.name)

    # confirm contents of expected file
    assert expected_results_file, 'Expected results file should not be null! ' \
                                  'Neither local, nor S3 file, available: (%s, S3:%s)' \
                                  % (expected_results_filename_local, s3_expected_filename)

    return expected_results_file


def obtain_actual_results_file(content, actual_results_filename, reformat_json=True):
    """
    Write the result content to the actual results file
    :param content
    :param actual_results_filename
    :param reformat_json
    :return: actual_results_file_object
    """

    assert content, 'Actual result content should not be null!'

    # write actual results to file
    with open(actual_results_filename, 'w') as actual_results_file:
        logger.debug('Exporting actual results to %s ', actual_results_file.name)
        if reformat_json:
            content_to_write = json.dumps(json.loads(content), indent=1)
        else:
            content_to_write = content
        actual_results_file.write(content_to_write)
        actual_results_file.flush()
        os.fsync(actual_results_file.fileno())

    # confirm contents of actual
    assert actual_results_file, 'Actual result file should not be null!'

    return actual_results_file


#########################
# test methods
#########################

def download_and_verify_chart_data(expected_results_filename, s3_expected_filename, actual_results_filename, client,
                                   optimization_scenario_id, site_location_code, resource_hash, **kwargs):
    """
    Downloads chart data and verifies it
    :param expected_results_filename:
    :param s3_expected_filename:
    :param actual_results_filename:
    :param client:
    :param optimization_scenario_id:
    :param site_location_code:
    :param resource_hash:
    :return:
    """
    content = \
        client.get_scenario_analysis_chart_data(optimization_scenario_id, site_location_code, resource_hash, **kwargs)

    verify_chart_data(actual_results_filename, content, expected_results_filename, s3_expected_filename)


def verify_chart_data(actual_results_filename, content, expected_results_filename, s3_expected_filename):
    """
    Saves chart data or verifies its content against s3 gold copy
    :param actual_results_filename:
    :param content:
    :param expected_results_filename:
    :param s3_expected_filename:
    :return:
    """
    test = True
    if test:
        # use local file, or download and save expected results from s3
        expected_results_file = obtain_expected_results_file(expected_results_filename, s3_expected_filename)

        actual_results_file = obtain_actual_results_file(content, actual_results_filename)

        logger.debug('Comparing actual %s vs expected %s ', actual_results_file.name, expected_results_file.name)
        with open(expected_results_file.name, 'r') as expected_json_file:
            expected_results = json.load(expected_json_file)

        with open(actual_results_file.name, 'r') as actual_json_file:
            actual_results = json.load(actual_json_file)

        for section in ['BMS', 'CHARGE', 'DISCHARGE', 'MONITOR', 'SET_POINT']:
            for actual, expect in zip_longest(actual_results['data'][section],
                                              expected_results['data'][section]):
                actual_date = actual[0]
                actual_val = actual[1]
                expected_date = expect[0]
                expected_val = expect[1]
                assert actual_date == expected_date
                err_msg = 'Error in %s for date %s act %s exp %s' \
                          % (section, expected_date, actual_val, expected_val)
                if actual_val is None:
                    assert actual_val == expected_val, err_msg
                else:
                    assert math_util.isclose(actual_val, expected_val, rel_tol=0.02), err_msg
        # if the test passes, delete the file, otherwise leave the actual results for debugging...
        try:
            os.remove(actual_results_filename)
        except OSError as e:
            logger.error('Failed to delete file: %s Code: %s Error: %s', e.filename, e.errno, e.strerror)
        try:
            os.remove(expected_results_filename)
        except OSError as e:
            logger.error('Failed to delete file: %s Code: %s Error: %s', e.filename, e.errno, e.strerror)
    else:
        logger.debug('Exporting results to %s ', expected_results_filename)
        with open(expected_results_filename, 'w') as result_file:
            result_file.write(json.dumps(json.loads(content), indent=1))


def download_and_verify_chart_data_as_csv(expected_results_filename,
                                          s3_expected_filename,
                                          actual_results_filename,
                                          client,
                                          optimization_scenario_id,
                                          site_location_code,
                                          resource_hash,
                                          load_definition_code, **kwargs):
    """
    Downloads chart data and verifies it
    :param expected_results_filename:
    :param s3_expected_filename:
    :param actual_results_filename:
    :param client:
    :param optimization_scenario_id:
    :param site_location_code:
    :param resource_hash:
    :return:
    """
    logger.debug('Comparing actual vs expected chart_data_as_csv for load_definition_code: %s ' % load_definition_code)
    content = client.get_scenario_analysis_chart_data_as_csv(optimization_scenario_id,
                                                             site_location_code,
                                                             resource_hash,
                                                             load_definition_code,
                                                             **kwargs)

    _verify_chart_data_as_csv(actual_results_filename, content, expected_results_filename, s3_expected_filename)


def _verify_chart_data_as_csv(actual_results_filename, content, expected_results_filename, s3_expected_filename):
    """
    Saves chart data or verifies its content against s3 gold copy
    :param actual_results_filename:
    :param content:
    :param expected_results_filename:
    :param s3_expected_filename:
    :return:
    """
    test = True
    if test:
        # use local file, or download and save expected results from s3
        expected_results_file = obtain_expected_results_file(expected_results_filename, s3_expected_filename)

        actual_results_file = obtain_actual_results_file(content, actual_results_filename, reformat_json=False)

        logger.debug('Comparing actual %s vs expected %s ',
                     actual_results_file.name, expected_results_file.name)

        with open(expected_results_file.name, 'r') as expected_json_file:
            expected_results = expected_json_file.read().splitlines()

        with open(actual_results_file.name, 'r') as actual_json_file:
            actual_results = actual_json_file.read().splitlines()

        for actual, expected in zip_longest(actual_results, expected_results):

            actual_list = actual.split(',')
            expected_list = expected.split(',')

            assert len(actual_list) == len(expected_list), \
                "number of elements does not match; actual: %s, expected: %s" % (actual_list, expected_list)

            for i, item in enumerate(actual_list):
                try:
                    actual_list_i_float = float(actual_list[i])
                except ValueError:
                    actual_list_i_float = 0

                try:
                    expected_list_i_float = float(expected_list[i])
                except ValueError:
                    expected_list_i_float = 0

                assert actual_list[i] == expected_list[i] or math_util.isclose(actual_list_i_float,
                                                                               expected_list_i_float,
                                                                               rel_tol=0.02), \
                    "Element[%s] does not match! actual:%s, expected: %s ; actual:%s, expected:%s" \
                    % (i, actual_list[i], expected_list[i], actual_list, expected_list)

        # if the test passes, delete the file, otherwise leave the actual results for debugging...
        try:
            os.remove(actual_results_filename)
        except OSError as e:
            logger.error('Failed to delete file: %s Code: %s Error: %s', e.filename, e.errno, e.strerror)
        try:
            os.remove(expected_results_filename)
        except OSError as e:
            logger.error('Failed to delete file: %s Code: %s Error: %s', e.filename, e.errno, e.strerror)

    else:
        logger.debug('Exporting results to %s ', expected_results_filename)
        with open(expected_results_filename, 'w') as result_file:
            result_file.write(content)


def download_and_verify_analysis_csv(expected_results_filename_analysis_csv, num_analyis_result_file_rows,
                                     client, optimization_scenario_id, site_location_code, resource_hash):
    """
    Simulates clicking on 'Download Analysis CSV' link to download csv and verifies csv contents
    :param expected_results_filename_analysis_csv:
    :param num_analyis_result_file_rows:
    :param client:
    :param optimization_scenario_id:
    :param site_location_code:
    :param resource_hash:
    :return:
    """
    content = client.get_analysis_csv_results(optimization_scenario_id, resource_hash, site_location_code)

    verify_analysis_csv(content, expected_results_filename_analysis_csv, num_analyis_result_file_rows)


def verify_analysis_csv(content, expected_results_filename_analysis_csv, num_analyis_result_file_rows):
    """
    Writes out content returned from /get_analysis_csv_results to temp file
    Compares temp file contents to expected results
    :param content:
    :param expected_results_filename_analysis_csv:
    :param num_analyis_result_file_rows:
    :return:
    """
    expected_result_csv = expected_results_filename_analysis_csv
    test = True
    if test:
        error_list_for_csv = []
        with tempfile.NamedTemporaryFile('w') as temp:
            temp.write(content)
            temp.flush()
            logger.debug('Writing temp file to %s', temp.name)

            actual_keys = None
            expected_keys = None
            # open the actual and expected results csv
            with open(temp.name, 'r') as actual_result, open(expected_result_csv, 'r') as expected_result:
                actual_reader = reader(actual_result)
                expected_reader = reader(expected_result)
                logger.debug('Comparing temp file %s to expected %s', temp.name, expected_result_csv)

                # iterate line by line through each file until we find the line with the resource_type
                # the following line represents the keys for the results
                for expected_values in expected_reader:
                    if expected_values and expected_values[0] == 'STORAGE ONLY':
                        expected_keys = next(expected_reader)
                        break

                for actual_values in actual_reader:
                    if actual_values and actual_values[0] == 'STORAGE ONLY':
                        actual_keys = next(actual_reader)
                        break

                if not actual_keys:
                    logger.error("Failed to find the row with 'STORAGE ONLY' in the analysis csv download")
                    print_file(temp.name)

                # assert that the keys are the same  between files
                assert set(actual_keys) == set(expected_keys), 'Actual and expected keys differ'

                # for each set of results, compare the values
                non_numeric_fields = ['Month', 'Season', 'Analysis Period Start', 'Analysis Period End',
                                      'Storage Cycles Per Period', 'TOU Period']
                for index in range(0, num_analyis_result_file_rows):

                    # get the values for the set
                    actual_values = next(actual_reader)
                    expected_values = next(expected_reader)

                    # create a map with the keys and values, compare the values if they exist
                    actual_map = dict(list(zip(actual_keys, actual_values)))
                    expected_map = dict(list(zip(expected_keys, expected_values)))
                    for key in expected_keys:
                        a = actual_map.get(key)
                        e = expected_map.get(key)
                        csv_key = '%s: actual: %s expected: %s' % (key, a, e)
                        logger.debug('Verify: %s', csv_key)

                        # for non-numeric fields, compare string values
                        if key in non_numeric_fields:
                            if a != e:
                                error_list_for_csv.append('Key %s actual: %s expected: %s' % (csv_key, a, e))
                        else:
                            if a != '' and e != '':
                                if not math_util.isclose(float(a), float(e), rel_tol=0.05):
                                    diff = (abs(float(a) - float(e)) / float(e)) * 100
                                    error_list_for_csv.append('Key %s diff: %s' % (csv_key, str(diff)))
                            else:
                                if a != e:
                                    error_list_for_csv.append('Key %s' % (csv_key))

        # Assert results of csv download of dst scenario
        logger.warning('Errors found in csv: %s', len(error_list_for_csv))
        assert not error_list_for_csv, '\n'.join(error_list_for_csv)

    else:
        logger.debug('Exporting results to %s ', expected_results_filename_analysis_csv)
        with open(expected_results_filename_analysis_csv, 'w') as result_file:
            result_file.write(content)
            result_file.flush()


def download_and_verify_summary_csv(client, optimization_scenario_id, testdata):
    """
    Downloads summary csv and verifies it
    :param client:
    :param optimization_scenario_id:
    :param testdata:
    :return:
    """
    response = client.download_csv(location_code=testdata['location_code'],
                                   scenario_id=optimization_scenario_id)
    _verify_summary_csv_results(testdata, response.text())


def _verify_summary_csv_results(testdata, response_content):
    """
    Saves summary csv or verifies it against gold copy
    :param testdata:
    :return:
    """
    expected_result_csv = testdata['result_file']
    test = True
    if test:
        error_list_for_csv = []
        with tempfile.NamedTemporaryFile('w') as temp:
            temp.write(response_content)
            temp.flush()
            logger.debug('Writing temp file to %s', temp.name)

            actual_keys = None
            expected_keys = None
            # open the actual and expected results csv
            with open(temp.name, 'r') as actual_result, open(expected_result_csv, 'r') as expected_result:
                actual_reader = reader(actual_result)
                expected_reader = reader(expected_result)
                logger.debug('Comparing temp file %s to expected %s', temp.name, expected_result_csv)

                # iterate line by line through each file until we find the line with the resource_type
                # the following line represents the keys for the results
                for expected_values in expected_reader:
                    if expected_values and expected_values[0] == testdata['resource_type']:
                        expected_keys = next(expected_reader)
                        break

                for actual_values in actual_reader:
                    if actual_values and actual_values[0] == testdata['resource_type']:
                        actual_keys = next(actual_reader)
                        break

                if not actual_keys:
                    logger.error('Failed to find the results with correct resource type.')
                    print_file(temp.name)
                    assert False, "Failed to find the results with correct resource type. Check test logs."

                # assert that the keys are the same  between files
                assert set(actual_keys) == set(expected_keys), 'Actual and expected keys differ'

                # for each set of results, compare the values
                for index in range(0, len(testdata['results'])):

                    # get the values for the set
                    actual_values = next(actual_reader)
                    expected_values = next(expected_reader)

                    # create a map with the keys and values, compare the values if they exist
                    actual_map = dict(list(zip(actual_keys, actual_values)))
                    expected_map = dict(list(zip(expected_keys, expected_values)))
                    for key in expected_keys:
                        a = actual_map.get(key)
                        e = expected_map.get(key)
                        csv_key = '%s: %s actual: %s expected: %s' \
                                  % (testdata['results'][index]['capacity'], key, a, e)
                        logger.debug('Verify: %s', csv_key)
                        if a != '' and e != '':
                            if not math_util.isclose(float(a), float(e), rel_tol=0.05):
                                diff = (abs(float(a) - float(e)) / float(e)) * 100
                                error_list_for_csv.append('Key %s diff: %s' % (csv_key, str(diff)))
                        else:
                            if a != e:
                                error_list_for_csv.append('Key %s' % (csv_key))

        # Assert results of csv download of dst scenario
        logger.warning('Errors found in csv: %s', len(error_list_for_csv))
        assert not error_list_for_csv, '\n'.join(error_list_for_csv)
    else:
        logger.debug('Exporting results to %s', expected_result_csv)
        with open(expected_result_csv, 'w') as result_file:
            result_file.write(response_content)
            result_file.flush()


def download_and_verify_scenario_summary_results(client, optimization_scenario_id, testdata):
    """
    Downloads summary results and verifies it
    :param client:
    :param optimization_scenario_id:
    :param testdata:
    :return:
    """
    data = client.get_scenario_summary_results(optimization_scenario_id,
                                               testdata['location_code'], return_projected=True)
    verify_scenario_summary_results(data, testdata)


def verify_scenario_summary_results(data, testdata):
    """
    Verify summary results
    :param data:
    :param testdata:
    :return:
    """
    error_list_for_summary_values = []

    scenario_summary = data['scenario_summary'][testdata['resource_type']]
    for actual_scenario, expected_scenario in zip(scenario_summary, testdata['results']):
        scenario_system = actual_scenario['stem_system_details']
        resource_kw = scenario_system['resource_kw']
        resource_kwh = scenario_system['resource_kwh']
        capacity = "%s/%s" % (resource_kw, resource_kwh)
        logger.debug('Verifying capacity: %s', capacity)
        assert capacity == expected_scenario['capacity'], 'Actual and expected capacity differ!'
        summary_results = actual_scenario['summary_results']
        for idx, result in enumerate(summary_results):
            result_year = str(result['year'])
            if testdata.get('apply_solar'):
                savings_types = ['SOLAR_INTERVAL', 'SOLAR_INTERVAL_INCREMENTAL', 'SOLAR_ONLY', 'UTILITY']
            else:
                savings_types = ['UTILITY']
            for savings in savings_types:
                actual_values = {}
                for level in ['low', 'expected', 'potential']:
                    actual_value = summary_results[idx][savings][level]['demand_price_total_delta']
                    expected_value = expected_scenario['result'][result_year][savings][level]
                    key = 'Capacity: %s Savings Type: %s Level: %s Year: %s Actual: %s Expected: %s' \
                          % (capacity, savings, level, result_year, actual_value, expected_value)
                    logger.debug('Verify: %s', key)
                    if not math_util.isclose(actual_value, expected_value, rel_tol=0.05):
                        diff = (abs(actual_value - expected_value) / expected_value) * 100
                        error_list_for_summary_values.append('%s diff: %s' % (key, diff))
                    if result_year == '1' and level == 'expected':
                        num_months = data['scenario_inputs']['num_months']
                        actual_price = None
                        if savings == 'SOLAR_INTERVAL_INCREMENTAL':
                            actual_price = abs(actual_value / num_months / resource_kw)
                        if savings == 'UTILITY' and 'SOLAR_INTERVAL_INCREMENTAL' not in savings_types:
                            actual_price = abs(actual_value / num_months / resource_kw)
                        if actual_price is not None:
                            expected_price = expected_scenario['result']['price/kw']
                            key = 'Price/kw(year 1) Capacity: %s Savings Type: %s Level: %s Actual: %s Expected: %s' \
                                  % (capacity, savings, level, actual_price, expected_price)
                            logger.debug('Verify %s', key)
                            if not math_util.isclose(actual_price, expected_price, rel_tol=0.05):
                                diff = (abs(actual_price - expected_price) / expected_price) * 100
                                error_list_for_summary_values.append('%s diff: %s' % (key, diff))
                    actual_values[level] = actual_value
                # logger.debug("       '%s'  : { %s: { low : %.2f, expected : %.2f, potential : %.2f } }"
                #              % (result_year, savings,
                #                 actual_values['low'],
                #                 actual_values['expected'],
                #                 actual_values['potential']))

    # Assert results from summary api call
    logger.warning('Errors found in call to get_scenario_summary_results: %s',
                   len(error_list_for_summary_values))
    assert not error_list_for_summary_values, '\n'.join(error_list_for_summary_values)


def pollresult_and_verify_csv_and_summary(client, dbhelper, testdata):
    """
    Polls for optimization scenario
    Once finished, verify the scenario summary csv and the scenario summary results json used to populate UI
    :param client:
    :param dbhelper:
    :param testdata:
    :return:
    """
    optimization_scenario_id = wait_for_scenario_to_finish(client, dbhelper,
                                                           testdata['location_code'],
                                                           testdata['analysis_timeout'])
    download_and_verify_summary_csv(client, optimization_scenario_id, testdata)

    download_and_verify_scenario_summary_results(client, optimization_scenario_id, testdata)

    return optimization_scenario_id


def pollresult(client, dbhelper, testdata):
    """
    Polls for scenario to finish for given location, returns optimization scenario id when completed
    :param client:
    :param dbhelper:
    :param testdata:
    :return:
    """
    optimization_scenario_id = wait_for_scenario_to_finish(client,
                                                           dbhelper,
                                                           testdata['location_code'],
                                                           testdata['analysis_timeout'])
    return optimization_scenario_id


def wait_for_scenario_to_finish(client, dbhelper, location_code, analysis_timeout):
    """
    Waits for scenario_id to be inserted into db then poll for scenario to finish
    Returns scenario id upon finish
    :param client:
    :param dbhelper:
    :param testdata:
    :return:
    """
    start_time = dt.utcnow()
    logger.debug("Starting analyze job at '%s' ", start_time)
    # wait for insert of location_code in optimization job table...
    query = "select * from optimization_scenario where location_code_primary = '%s';"
    sql = query % location_code
    dbhelper.query_with_retry('analytics', sql)
    logger.debug("Polling for analyze job for location '%s' to finish", location_code)
    response = client.get_submitted_scenario(location_code, analysis_timeout)
    optimization_scenario_id = response.data()[0].get('optimization_scenario_id')
    end_time = dt.utcnow()
    analysis_time = (end_time - start_time).total_seconds()
    logger.debug("Location %s scenario %s took %s seconds",
                 location_code, optimization_scenario_id, analysis_time)
    return optimization_scenario_id


def print_file(filename):
    """
    Print contents of the specified file
    :param filename:
    :return:
    """
    with open(filename, 'r') as f:
        logger.info('%s contents: \n%s', filename, f.read())

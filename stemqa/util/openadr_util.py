import datetime as dt
import json
import logging
import os
import time
import traceback

import isodate
import pause
import pytz
# plugin that enables you to collect errors/suspicious values instead of asserting
# https://github.com/astraw38/pytest-assume
from pytest import assume  # pylint: disable=E0611
from pytz import UTC
from requests import ConnectionError
from retry import retry

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_OPTIMIZATION, DB_FORECAST
from stemqa.util import shell_util
from stemqa.util.emulator_util import configure_simple_emulator
from stemqa.util.emulator_util import setup_emulator_container
from stemqa.util.emulator_util import stop_and_delete_emulator
from stemqa.util.grid_response_util import get_filtered_program_events
from stemqa.util.grid_response_util import get_program_id, GridResponseClient
from stemqa.util.shell_util import ShellUtilError
from stemqa.vpp.container import run_docker_command, get_pods_info
from stemqa.vpp.helper import dt_utcnow, iso8601_utc_datetime, json_dumps, round_time_up, ExcThread, STR_FORMAT, \
    get_program_info
from stemqa.vpp.openadr.common import MODIFY_SETPOINT_TABLE, POLL_FREQ
from stemqa.vpp.openadr.event import event_time_info_old, event_cleanup
from stemqa.vpp.openadr.event_verification import fetch_event_test_data_old, verify_event_data_new
from stemqa.vpp.openadr.test_setup import setup_external_reference_for_program, set_discharge_filter
from stemqa.vpp.openadr.vtn_api import vtnc, vtn_sim, VENS
from stemqa.vpp.resource import clear_offset_schedule, set_max_demand_settings, setup_aggregation_resources
from stemqa.vpp.telemetry import last_telemetry_data, telemetry_data

logger = logging.getLogger(__name__)

local_env = os.environ.get('LOCAL_ENV', False)
k8s_env_name = settings['env-name'] if not local_env else None

VEN_NAME = 'STEM_TEST_VEN'
VEN_CONTAINER = 'oadr-ven'
VTN_CONTAINER = 'vtn-sim'
S4_CONTAINER = 's4server'
PROGRAM_NAME = 'OpenADR Austin SHINES'
PROGRAM_ID = 89
EXTERNAL_REFERENCE_ID = settings['vtn']['vtn-id']
DISPATCH_LAG = 6
SYSTEM_LAG = 0
VEN_K8_FUNC = 'stem-ven-austin-shines'
VTN_K8_FUNC = 'vtn-sim'
S4_K8_FUNC = 'stem-s4-server'


def get_pod_name(pod_func, fail_on_error=False):
    k8cmd = 'get pods -l app=%s' % pod_func
    pods_i, _ = shell_util.kubectl(k8s_env_name, k8cmd, json_output=False, fail_on_error=fail_on_error)
    for pod_i in pods_i.splitlines()[1:]:
        pod_info = pod_i.split()
        pod_name = pod_info[0]
        if pod_info[2].upper() == 'Running'.upper():
            return pod_name
        elif pod_info[2].upper().startswith('INIT'):
            return pod_name


def setup_emulators(resources,
                    reconfigure_emulators=False,
                    reset_emulators=False,
                    delete_telemetry=True,
                    modify_setpoint_table=MODIFY_SETPOINT_TABLE):
    """
    Restarting and reconfiguring emulators.
    modify_setpoint_table: Sets powerstore_setpoint_change with resource setpoint so that proper calculations of
                           available charge power can be used by current status reports and event allocation.
                           Both get the data from -> `stem-lib/stem-gridservice/stem_gs/services/topology_utils.py`
                           This table is supposed to be populated in production.
    """

    @retry(tries=3, delay=2)
    def setup_resource(resource):
        if not local_env:
            # if previous tests fail to clean specific emulators, we have to clean them up.
            # especially useful when stopping/restarting tests with local test runner.
            pod_name = get_pod_name(resource['emulator_hostname'])
            logger.debug('Pod name: %s', pod_name)
            if pod_name:
                try:
                    stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                             location_code=resource['target_location'],
                                             delete_db_entries=False)
                except ShellUtilError as e:
                    logger.warning('Emulator deletion failed: %s', e)
                    pods_info = get_pods_info(resource['emulator_hostname'])
                    logger.debug('PODS INFO: \n%s', json.dumps(pods_info, indent=2))
                    raise e

                pod_name = get_pod_name(resource['emulator_hostname'])
                waiting_for_pod_to_delete = 0
                while pod_name and waiting_for_pod_to_delete < 60:
                    logger.warning('Waiting for emulator pod to delete. REUSE OF EMULATOR AND FAILED CLEANUP!!!')
                    waiting_for_pod_to_delete += 4
                    time.sleep(4)
                    pod_name = get_pod_name(resource['emulator_hostname'])

                if pod_name:
                    raise RuntimeError('Emulator Pod %s failed to delete in 60secs' % pod_name)

        else:
            cmd_ps = 'docker ps -a -q -f name=%s' % resource['emulator_hostname']
            response, _ = run_docker_command(cmd_ps)
            tag = response.strip()
            if tag:
                cmd_rm = 'docker rm -f %s' % tag
                run_docker_command(cmd_rm)

        try:
            setup_emulator_resource(resource,
                                    configure_only=False,
                                    delete_telemetry=delete_telemetry,
                                    modify_setpoint_table=modify_setpoint_table)
        except ShellUtilError as e:
            if not local_env:
                # simple retry logic in case emulator setup is fragile
                logger.warning('Emulator setup failed: %s', e)
                pods_info = get_pods_info(resource['emulator_hostname'])
                logger.debug('EMULATOR %s PODS INFO: \n%s', resource['emulator_hostname'],
                             json.dumps(pods_info, indent=2))
            try:
                stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                         location_code=resource['target_location'],
                                         delete_db_entries=False)
            except ShellUtilError as e:
                logger.warning('Emulator deletion failed: %s', e)

            time.sleep(10)
            setup_emulator_resource(resource,
                                    configure_only=False,
                                    delete_telemetry=delete_telemetry,
                                    modify_setpoint_table=modify_setpoint_table)
        finally:
            if not local_env:
                pod = get_pod_name(resource['emulator_hostname'])
                logger.debug('EMULATOR %s POD: \n%s', resource['emulator_hostname'], pod)

    if len(resources) > 1:
        # setup will be faster if we run each emulator setup in thread.
        threads = []
        for res in resources:
            th = ExcThread(target=setup_resource, args=[res],
                           name='setup-%s' % res['emulator_hostname'])
            threads.append(th)
            th.start()

        for thr in threads:
            thr.join(timeout=1200)
            if thr.isAlive():
                raise Exception('Emulator setup is taking more than 20 minutes')
    else:
        setup_resource(resources[0])


def teardown_emulators(resources, reconfigure_emulators=False, reset_emulators=False):
    if not (reconfigure_emulators or reset_emulators):
        stop_emulators(resources, fail_on_error=False)
    else:
        for resource in resources:
            if reconfigure_emulators:
                stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                         location_code=resource['target_location'],
                                         delete_db_entries=False)
            elif reset_emulators:
                input_args = {
                    'host': settings['rmq']['host'],
                    'username': settings['rmq']['username'],
                    'password': settings['rmq']['password'],
                    'amqp_port': resource['emulator_rabbit_port']
                }
                clear_offset_schedule(**input_args)


def stop_emulators(resources, stop_telemetry=True, fail_on_error=True):
    """
    stop all emulator containers used for test

    On AWS and your local env, things behaved very similarly in that you could call docker commands.
    On K8, there is no notion of docker images. You need to use the kubectl interface to manage pods and to stop them.
       Calling docker stop <container> doesn't work on K8.
    """
    # stop emulator container
    for resource in resources:
        if not local_env:
            pod = get_pod_name(resource['emulator_hostname'])
            if not pod:
                continue
            try:
                stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                         location_code=resource['target_location'],
                                         delete_db_entries=False)
            except ShellUtilError as err:
                pods_info = get_pods_info(resource['emulator_hostname'])
                logger.debug('EMULATOR %s PODS INFO: \n%s', resource['emulator_hostname'],
                             json.dumps(pods_info, indent=2))
                last_telemetry_data([resource])
                if not fail_on_error:
                    logger.error('Deleting emulator %s pod FAILED:\n%s',
                                 resource['emulator_hostname'], err)
                else:
                    raise
        else:
            cmd_ps = 'docker ps -a -q -f name=%s' % resource['emulator_hostname']
            response, _ = run_docker_command(cmd_ps)
            tag = response.strip()
            if tag:
                if stop_telemetry:
                    stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                             location_code=resource['target_location'],
                                             delete_db_entries=False)
                else:
                    cmd_rm = 'docker rm -f %s' % tag
                    run_docker_command(cmd_rm)


def setup_emulator_resource(resource, configure_only=False, delete_telemetry=True,
                            modify_setpoint_table=MODIFY_SETPOINT_TABLE):
    """
    modify_setpoint_table: Sets powerstore_setpoint_change with resource setpoint so that proper calculations of
                           available charge power can be used by current status reports and event allocation.
                           Both get the data from -> `stem-lib/stem-gridservice/stem_gs/services/topology_utils.py`
                           This table is supposed to be populated in production.
    """
    dbhelper = DbHelper.default_instance()
    emulator_hostname = resource['emulator_hostname']
    if delete_telemetry:
        sql_to_execute = """
        SET @EMU = '%s';
        DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-site');
        DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-inverter');
        DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-main');
        DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-site');
        DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-inverter');
        DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-main');
        DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-site');
        DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-inverter');
        DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-main');
        DELETE FROM optimization.powerstore_bms where hostname = @EMU;
        DELETE FROM optimization.powerstore_bms_minute where hostname = @EMU;
        DELETE FROM optimization.powerstore_bms_15_minute where hostname = @EMU;
        """
        sql = sql_to_execute % (emulator_hostname)
        dbhelper.query(DB_FORECAST, sql)

    batterysoc = resource['batterysoc']
    setpoint = resource['setpoint']

    source_location = resource.get('source_location') or 'qa-QA_CLIENT_00_SAMPLE_001-1-M1'
    sample_start_date = resource.get('sample_start_date') or '2018-01-27 08:00:00.0000'
    sample_end_date = resource.get('sample_end_date') or '2018-01-27 09:00:00.5000'

    if not configure_only:
        setup_emulator_container(emulator_name=emulator_hostname)

    # configure emulator to replay telemetry
    configure_simple_emulator(dbhelper,
                              target_location=resource['target_location'],
                              emulator_hostname=emulator_hostname,
                              source_location=source_location,
                              sample_start_date=sample_start_date,
                              sample_end_date=sample_end_date,
                              batterysoc=batterysoc,
                              setpoint=setpoint)
    set_max_demand_settings(resource)

    sql = "Delete from powerstore_setpoint_change where hostname='{hostname}';".format(hostname=emulator_hostname)
    dbhelper.query(DB_OPTIMIZATION, sql)

    if modify_setpoint_table:
        logger.debug('Setting powerstore_setpoint_change table.')
        sql = '''
        Insert into powerstore_setpoint_change
        (id, component_id, hostname, optimization_id, msg_timestamp_change_datetime, set_point)
        values (NULL, '{hostname}-site_load_averager', '{hostname}', '{hostname}-optimization',
        '2018-05-08 16:00:00', {setpoint});
        '''.format(hostname=emulator_hostname, setpoint=setpoint)
        dbhelper.query(DB_OPTIMIZATION, sql)


def verify_event_data(event, testdata, battery_output, resource_dispatch_results, event_idx=None):
    oadr_event = event['oadr_event']
    # grid_events = event['grid_events']
    grid_events = get_filtered_program_events([oadr_event['event_id']], testdata['program_name'])
    expected_amount_scheduled_per_interval = testdata.get('expected_amount_scheduled_per_interval')
    if expected_amount_scheduled_per_interval and \
            any([isinstance(val[0], list) for val in list(expected_amount_scheduled_per_interval.values())]):
        assert all([isinstance(val[0], list) for val in list(expected_amount_scheduled_per_interval.values())])
        expected_amount_scheduled_per_interval = \
            {key: val[event_idx] for key, val in list(expected_amount_scheduled_per_interval.items())}

    total_expected_amount_scheduled_per_interval = testdata.get('total_expected_amount_scheduled_per_interval')
    if total_expected_amount_scheduled_per_interval and \
            isinstance(total_expected_amount_scheduled_per_interval[0], list):
        total_expected_amount_scheduled_per_interval = total_expected_amount_scheduled_per_interval[event_idx]

    expected_grid_event_status_per_interval = testdata.get('expected_grid_event_status_per_interval')
    if expected_grid_event_status_per_interval and isinstance(expected_grid_event_status_per_interval[0], list):
        expected_grid_event_status_per_interval = expected_grid_event_status_per_interval[event_idx]

    verification = {'relative_tolerance': testdata.get('relative_tolerance') or 0.05,
                    'absolute_tolerance_kw': testdata.get('absolute_tolerance') or 5,
                    'expected_amount_scheduled_per_interval': expected_amount_scheduled_per_interval,
                    'total_expected_amount_scheduled_per_interval': total_expected_amount_scheduled_per_interval,
                    'expected_grid_event_status_per_interval': expected_grid_event_status_per_interval,
                    'expect_dispatches': testdata.get('expect_dispatches', True),
                    'battery_output': battery_output,
                    'resource_dispatch_results': resource_dispatch_results}
    resources = testdata['resources']
    return verify_event_data_new(oadr_event, grid_events, verification, resources,
                                 dispatch_lag=testdata.get('dispatch_lag', DISPATCH_LAG))


def calculate_event_start_time(testdata, minute_rounded=True):
    # NOTE dispatch_lag is the difference between grid service event start and actual
    #     time when schedules are dispatched to resource
    # NOTE system_lag is the extra time system needs to process event and send it to resource.
    #     It depends from setup to setup. More emulators there are running,
    #     slower machine where docker runs, more containers in testing compose setup... bigger system lag.
    system_lag = testdata.get('system_lag', SYSTEM_LAG)
    dispatch_lag = testdata.get('dispatch_lag', DISPATCH_LAG)
    lag = dispatch_lag + system_lag + POLL_FREQ
    test_start_dt = dt.datetime.now(tz=UTC) + dt.timedelta(seconds=lag)
    if minute_rounded:
        test_start_dt = round_time_up(test_start_dt, 60)
    test_start = test_start_dt.strftime(STR_FORMAT)
    logger.debug('First possible event start is at: %s', test_start)
    test_start_dt -= dt.timedelta(seconds=dispatch_lag)
    test_start = test_start_dt.strftime(STR_FORMAT)
    logger.debug('Reduced for few second because of dispatch lag: %s', test_start)
    return test_start_dt


def create_test_events(base_event_start, testdata):
    events = []
    for event in testdata['events']:
        assert bool(event.get('interval_duration')) != bool(event.get('interval_seconds')), \
            'you can only use one of this settings to set interval length'
        if 'interval_duration' in event:
            interval_td = isodate.parse_duration(event['interval_duration'])
            event['interval_seconds'] = int(interval_td.total_seconds())
            del event['interval_duration']
        assert event['interval_seconds'] % 60 == 0
        assert event['charge_payloads']
        event_kwargs = event.copy()
        event_kwargs['ven_name'] = testdata['ven_name']
        if 'event_start_time' not in event:
            event_kwargs['event_start_time'] = iso8601_utc_datetime(base_event_start)
        logger.debug('openADR /events POST data:\n%s', json_dumps(event_kwargs))
        creation_response = vtnc.create_event(**event_kwargs)
        logger.debug('Creation response:\n%s', json_dumps(creation_response))
        events.append(creation_response['data'])
    logger.debug('Created openADR events:\n%s', json_dumps(events))
    return events


def couple_oadr_with_grid_events(program_id, oadr_events, max_wait_time=80):
    wait_interval_seconds = 5
    wait_time = 0
    all_events_found = False
    prog_events = None
    events_coupling = {}
    with GridResponseClient(settings['stem-web-app']['url']) as client:
        while not all_events_found and wait_time <= max_wait_time:
            logger.debug('Waiting few seconds for events to get created')
            time.sleep(wait_interval_seconds)
            wait_time += wait_interval_seconds

            events_coupling = {}
            all_events_found = True
            prog_events = client.event_list(program_id)['events']
            logger.debug('program_events: %s', json_dumps(prog_events))
            for oadr_event in oadr_events:
                event_id = oadr_event['event_id']
                events_coupling[event_id] = {'oadr_event': oadr_event, 'grid_events': []}
                for prog_event in list(prog_events.values()):
                    reference_id = prog_event['reference_id']
                    if reference_id and event_id in reference_id:
                        events_coupling[event_id]['grid_events'].append(prog_event)
                actual_intervals = [e for e in oadr_event['charge_payloads'] if
                                    e is not None and str(e).upper() != 'NONE']
                logger.debug('Intervals: %s', actual_intervals)
                if len(actual_intervals) != len(events_coupling[event_id]['grid_events']):
                    all_events_found = False
    assert all_events_found
    logger.debug('Events for program %s:\n%s', program_id, json_dumps(events_coupling))
    return events_coupling


def openadr_test_setup(ven_name=VEN_NAME,
                       program_name=PROGRAM_NAME,
                       external_reference_id=EXTERNAL_REFERENCE_ID,
                       program_id=PROGRAM_ID,
                       SETUP_EXTERNAL_REFERENCE=True,
                       CLEAN_EVENTS=False,
                       CANCEL_REPORT_REQUESTS=False,
                       RESTART_VTN=False,
                       RESTART_VEN=False,
                       CLEAR_VEN=False,
                       SETUP_RESOURCES=False,
                       **kwargs):
    if SETUP_EXTERNAL_REFERENCE:
        setup_external_reference_for_program(program_name, external_reference_id)

    if SETUP_RESOURCES and program_name == PROGRAM_NAME:
        resources = [
            "91, 1, 1, 100.00, 100.00, 0.00, 0.00, 0.00, 0.00"
        ]
        setup_aggregation_resources(11454, resources)

    if kwargs.get('prevent_discharge_time_frame'):
        # set_discharge_filter(program_id, '(datetime.time(0,0),datetime.time(23,59))', '[0,1,2,3,4,5,6]')
        start_delta_seconds, end_delta_seconds = kwargs.get('prevent_discharge_time_frame')
        program_timezone_str = get_program_info(program_name)['timezone']
        prog_tz = pytz.timezone(program_timezone_str)
        current_dt = dt.datetime.now(tz=prog_tz)
        start_time = (current_dt + dt.timedelta(seconds=start_delta_seconds)).time()
        end_time = (current_dt + dt.timedelta(seconds=end_delta_seconds)).time()
        set_discharge_filter(program_id,
                             '(%s,%s)' % (repr(start_time), repr(end_time)),
                             '[0,1,2,3,4,5,6]')
    else:
        # deleting all discharge filters for test program(if they exist from previous tests)
        set_discharge_filter(program_id, None, None)

    try:
        if CLEAN_EVENTS:
            event_cleanup(program_id)
            vtnc.delete_events(ven_name)
        if CANCEL_REPORT_REQUESTS:
            vtnc.cancel_all_pending_reports(ven_name)
        if CLEAR_VEN:
            vtnc.clear_ven(ven_name)
        if RESTART_VTN:
            vtn_sim.restart()
        if RESTART_VEN:
            wait = not kwargs.get('vtn_sim_optional', False)
            VENS[VEN_NAME].restart(wait_for_registration=wait)
    except ConnectionError as e:
        logger.error('VTN simulator not running. Connection error. %s', e)
        if not kwargs.get('vtn_sim_optional'):
            raise


def openadr_default_test_data(testdata):
    testdata.setdefault('ven_name', VEN_NAME)
    testdata.setdefault('program_name', PROGRAM_NAME)
    testdata.setdefault('external_reference_id', EXTERNAL_REFERENCE_ID)
    testdata.setdefault('dispatch_lag', DISPATCH_LAG)
    testdata.setdefault('system_lag', SYSTEM_LAG)
    with GridResponseClient(settings['stem-web-app']['url']) as client:
        program_id = get_program_id(client, testdata['program_name'])
        testdata.setdefault('program_id', program_id)
        assert isinstance(testdata['program_id'], int)


def openadr_test_info(testdata):
    logger.debug("Test case: %s", testdata['id'])
    logger.debug('Ven name: %s', testdata['ven_name'])
    logger.debug('External reference ID: %s', testdata['external_reference_id'])
    logger.debug('Program: %s (id=%s):\n%s',
                 testdata['program_name'],
                 testdata['program_id'],
                 json_dumps(get_program_info(testdata['program_name'])))


def openadr_test_startup(testdata, add_missing_testdata, **kwargs):
    if add_missing_testdata:
        openadr_default_test_data(testdata)
    openadr_test_info(testdata)
    setup_config = {}
    setup_config.update(kwargs)
    setup_config.update(testdata)
    logger.debug(json_dumps(setup_config))
    openadr_test_setup(**setup_config)

    if kwargs.get('SETUP_EMULATORS'):
        setup_emulators(testdata['resources'],
                        kwargs.get('RECONFIGURE_EMULATORS', False),
                        kwargs.get('RESET_EMULATORS', False),
                        kwargs.get('DELETE_TELEMETRY', True))


def openadr_create_events(testdata, start_time=None, round_start_time=True):
    if not start_time:
        start_time = calculate_event_start_time(testdata, round_start_time)

    logger.debug('Start time is at: %s', start_time.strftime(STR_FORMAT))
    oadr_events = create_test_events(start_time, testdata)
    events_coupling = couple_oadr_with_grid_events(testdata['program_id'],
                                                   oadr_events)
    return start_time, events_coupling


def scheduled_callback_loop(callback, end_dt, repeat_seconds, round_to_seconds, *args, **kwargs):
    repeat_td = dt.timedelta(seconds=repeat_seconds)
    next_callback_dt = round_time_up(dt_utcnow(), seconds=round_to_seconds)
    while next_callback_dt <= end_dt:
        logger.debug('  *    Waiting until next callback call: %s',
                     next_callback_dt.strftime(STR_FORMAT))
        pause.until(next_callback_dt)
        callback(next_callback_dt, *args, **kwargs)
        next_callback_dt = next_callback_dt + repeat_td
        next_callback_dt = round_time_up(next_callback_dt, seconds=round_to_seconds)


def event_testing(events, testdata,
                  custom_event_test=None,
                  custom_interval_test=None,
                  custom_telemetry_test=None,
                  postpone_assert_error=False,
                  **kwargs):
    logger.debug('TESTING EVENTS...')
    logger.debug('extra kwargs: %s', kwargs)
    logger.debug(40 * '=')
    sorted_events = sorted(list(events.items()), key=lambda x: x[1]['oadr_event']['event_start_delta'])
    for idx, (event_id, event_data) in enumerate(sorted_events):
        try:
            oadr_event = event_data['oadr_event']
            grid_events = event_data['grid_events']
            start_dt, end_dt, _ = event_time_info_old(testdata, oadr_event, grid_events)
            telemetry_wait_td = dt.timedelta(seconds=10)
            event_wait_dt = end_dt + telemetry_wait_td
            logger.debug('***Event starts at: %s', start_dt.strftime(STR_FORMAT))
            logger.debug('***Event will end: %s', end_dt.strftime(STR_FORMAT))
            logger.debug('***Event test will start at: %s',
                         event_wait_dt.strftime(STR_FORMAT))
            logger.debug(40 * '-')

            interval_td = isodate.parse_duration(oadr_event['interval_duration'])
            for interval_idx, interval in enumerate(oadr_event['charge_payloads']):
                interval_start_dt = start_dt + (interval_td * (interval_idx))
                interval_end_dt = start_dt + (interval_td * (interval_idx + 1))
                interval = oadr_event['charge_payloads'][interval_idx]
                logger.debug(' **Event interval [idx: %s value: %s]',
                             interval_idx, interval)
                logger.debug(' **  Starts at: %s  ends: %s',
                             interval_start_dt.strftime(STR_FORMAT),
                             interval_end_dt.strftime(STR_FORMAT))
                if custom_telemetry_test:
                    scheduled_callback_loop(custom_telemetry_test, interval_end_dt, 60, 60,
                                            testdata=testdata)
                logger.debug(' **  Waiting until interval ends: %s', interval_end_dt)
                pause.until(interval_end_dt + telemetry_wait_td)
                if custom_interval_test:
                    logger.debug(' **  Testing interval:')
                    custom_interval_test(testdata, oadr_event, interval,
                                         interval_idx, interval_start_dt, interval_end_dt)
            if custom_event_test:
                pause.until(event_wait_dt)
                # get the interval output for all the monitors and
                # all resource dispatches for this event
                battery, dispatch = fetch_event_test_data_old(testdata, oadr_event, grid_events,
                                                              postpone_assert_error=False)
                # running custom event testing function with all avaliable event data
                custom_event_test(testdata=testdata,
                                  event=event_data,
                                  battery_output=battery,
                                  resource_dispatch_results=dispatch,
                                  event_idx=idx)
        except AssertionError as e:
            if not postpone_assert_error:
                raise
            assume(False, 'Event test failed: %s\n%s\nEVENT:%s\n' %
                   (e, traceback.format_exc(), json_dumps(event_data)))
        logger.debug('Ended testing event: %s', event_id)
        logger.debug(40 * '-')


def generic_event_test_setup(testdata,
                             custom_event_test,
                             add_missing_testdata=True,
                             rounded_start=True,
                             **kwargs):
    openadr_test_startup(testdata, add_missing_testdata, **kwargs)
    start_time, events_coupling = openadr_create_events(testdata, round_start_time=rounded_start)
    kwargs['test_start_time'] = start_time
    try:
        event_testing(events_coupling, testdata, custom_event_test, **kwargs)
    finally:
        telemetry_data(testdata['resources'])


def generic_event_test_setup_with_teardown(testdata,
                                           custom_event_test,
                                           add_missing_testdata=True,
                                           **kwargs):
    """Generic event test function.

       Creates openADR events from testdata and sequentially waits for event to finish
       and tests it with provided event testing function provided. This function receives
       event data(event, battery output, dispatch data) that is relevant for event that just
       finished.
    """
    try:
        generic_event_test_setup(testdata,
                                 custom_event_test,
                                 add_missing_testdata,
                                 **kwargs)
    finally:
        if kwargs.get('SETUP_EMULATORS') and kwargs.get('TEARDOWN_EMULATORS', True):
            teardown_emulators(
                testdata['resources'],
                kwargs.get('RECONFIGURE_EMULATORS', False),
                kwargs.get('RESET_EMULATORS', False))

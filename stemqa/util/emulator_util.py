import logging
import os
import random
import shutil
import string
import time

import edge_api_client.api_client as api_client
from retry import retry
from waiting import wait

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_METER_LOGS
from stemqa.helpers.edge_environment_helper import EdgeEnvironment
from stemqa.rest.powerconfig import PowerConfigClient
from stemqa.util import shell_util
from stemqa.util.shell_util import ShellUtilError

logger = logging.getLogger(__name__)


def configure_simple_emulator(dbhelper, target_location, emulator_hostname, source_location,
                              sample_start_date=None, sample_end_date=None, batterysoc=None, setpoint=None):
    """
    Configure emulator to replay existing source_location meter data through emulator to target_location
    Optional params for start and end date will replay meter data within specified time period,
        syntax: '2016-01-01 07:00:00.0000'
    """
    logger.debug("Configuring %s using source location: %s target location: %s ",
                 emulator_hostname, source_location, target_location)
    k8s_env_name = settings['env-name']
    with PowerConfigClient(settings['stem-web-app']['url']) as client:
        client.deploy_emulator_to_host(target_location, emulator_hostname, batterysoc=batterysoc, setpoint=setpoint)
        client.update_powerstore_configuration(sample_data_monitor_id=source_location,
                                               powerstore_hostname=emulator_hostname,
                                               sample_start_date=sample_start_date,
                                               sample_end_date=sample_end_date,
                                               real_time_speed=True)
    _start_telemetry(emulator_hostname, k8s_env_name, dbhelper)


def _start_telemetry(emulator_hostname, k8s_env_name, dbhelper):
    """
    Starts telemetry with cmd: 'start_ext_site powerstore_runtime'
    :param emulator_hostname:
    :param k8s_env_name:
    :return:
    """

    pod_name = shell_util.get_pod_starts_with(emulator_hostname, k8s_env_name, emulator_hostname)
    if not pod_name:
        raise RuntimeError('Unable to find a pod for %s' % emulator_hostname)

    start_tel_cmd = 'exec %s -t -- /bin/sh -c "/opt/stem/bin/start_ext_site powerstore_runtime"' % pod_name
    output, rc = shell_util.kubectl(k8s_env_name, start_tel_cmd, show_output=True, json_output=False,
                                    fail_on_error=False, timeout=300)

    if '{"reply":"ok","from":"config_init"}' not in output:
        raise RuntimeError('Expected string \'{"reply":"ok","from":"config_init"}\' not '
                           'found in start_ext_site command output. Exit Code: %s' % rc)
    elif rc:
        logger.warning('Found expected message in emulator output even though the '
                       'command returned exit code %s' % rc)

    # wait for up to a minute for telemetry to actually start before moving on to test...
    logger.debug('Waiting for telemetry to start...')
    sql = "SELECT count(*) value FROM meter_logs.monitor_data where monitor_id = '%s-inverter';" % emulator_hostname
    result = dbhelper.query(DB_METER_LOGS, sql)
    logger.debug('Telemetry points: %s', result[0]['value'])

    time_end = time.time() + 60
    while result[0]['value'] == 0 and time.time() < time_end:
        logger.debug('Sleeping 5sec before recheck...')
        time.sleep(5)
        result = dbhelper.query(DB_METER_LOGS, sql)
        logger.debug('Telemetry points: %s', result[0]['value'])

    if result[0]['value'] == 0:
        raise ValueError('Failed to find any telemetry started for %s after waiting 1 minute!' % emulator_hostname)


def stop_and_delete_emulator(emulator_hostname, location_code, delete_db_entries=True):
    """
    Stops and deletes emulator container
    :param emulator_hostname:
    :param location_code:
    :param delete_db_entries:
    :return:
    """
    teardown_emulator_container(emulator_hostname)

    if delete_db_entries:
        reset_emulator_db_entries(emulator_hostname, location_code)


def reset_emulator_db_entries(emulator_hostname, location_code):
    """
    Upon creation and startup of emulator, table rows are added or updated throughout schema
    The following script reverts updates or removes rows
    This should allow the same emulator/location_code to be executed repeatedly w.out requiring db restart
    :param emulator_hostname:
    :param location_code:
    :return:
    """
    dbhelper = DbHelper.default_instance()

    # In the following sql, not removing from location_monitor as some tests depend on
    # location_code being present in the table
    # DELETE FROM business.location_monitor where location_code like CONCAT(@LOCATION_CODE,'%%');

    sql_to_execute = """
    SET @EMU = '%s';
    SET @LOCATION_CODE = '%s';
    SET @EMU_OVERRIDE = CONCAT(@LOCATION_CODE,'.',@EMU,'_override');
    SET @ASSET_ID = CONCAT('UNINIT_EMUL_', @LOCATION_CODE);

    DELETE FROM forecast.configuration where name = @EMU_OVERRIDE;
    UPDATE forecast.powerstore SET name=@LOCATION_CODE, hostname=CONCAT(@ASSET_ID, '_INVERTER')
           WHERE `hostname`=@EMU;
    DELETE FROM forecast.topology where hostname = @EMU;
    DELETE FROM forecast.topology_component where hostname = @EMU;

    UPDATE topology.location_asset SET asset_id = @ASSET_ID WHERE `location_code` = CONCAT(@LOCATION_CODE,'_SITE');
    UPDATE topology.location_asset SET asset_id = @ASSET_ID WHERE `location_code` = CONCAT(@LOCATION_CODE,'_MAIN');
    UPDATE topology.location_asset SET asset_id = @ASSET_ID WHERE `location_code` = CONCAT(@LOCATION_CODE,'_INVERTER');

    DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-site');
    DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-inverter');
    DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-main');
    DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-site');
    DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-inverter');
    DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-main');
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-site');
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-inverter');
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-main');

    DELETE FROM optimization.powerstore_bms where hostname = @EMU;
    DELETE FROM optimization.powerstore_bms_minute where hostname = @EMU;
    DELETE FROM optimization.powerstore_bms_15_minute where hostname = @EMU;
    DELETE FROM optimization.powerstore_setpoint_change where hostname = @EMU;
    """
    sql = sql_to_execute % (emulator_hostname, location_code)
    dbhelper.query(None, sql)


def setup_emulator_container(emulator_name):
    """
    Setup emulator container on the fly
    1. Takes emulator docker compose yml and replaces vars
    2. Executes docker-compose with new emulator yml file and waits for 7 expected queues to start
    :param emulator_name:
    :return:
    """
    version = settings['artifacts'].get('stem-emulator', None)
    if not version:
        raise RuntimeError('Unable to find version for stem-emulator artifact to use.')

    k8s_env_name = settings['env-name']
    # running into issues with helm del, so added a del here in case test is rerun
    shell_util.run_process('stem-cli deploy-tasks uninstall-chart --env-name %s --name stem-emulator-%s'
                           % (k8s_env_name, emulator_name), fail_on_error=False)  # don't care if it doesnt exist
    # wait for emulator pod to be removed altogether
    wait(lambda: shell_util.kubectl(k8s_env_name, 'get pods | grep %s' % emulator_name,
                                    json_output=False, fail_on_error=False)[0].strip() == '',
         timeout_seconds=120, sleep_seconds=5)
    # install new emulator
    vault_addr = settings['vault']['addr']
    vault_header = settings['vault']['header']
    vault_role = settings['vault']['role']
    vault_version = settings['vault']['version']
    shell_util.run_process('stem-cli deploy-tasks install-chart '
                           '--release-name-suffix %s '
                           '--env-name %s '
                           '--type stem-emulator '
                           '--version %s '
                           '--set deploy.emulatorName=%s '
                           '--set k8s.pdbEnabled=true '
                           '--set k8s.execEnv=compose '
                           '--set vault.address=%s '
                           '--set vault.header=%s '
                           '--set vault.role=%s '
                           '--set vault.version=%s ' %
                           (emulator_name, k8s_env_name, version, emulator_name, vault_addr,
                            vault_header, vault_role, vault_version), show_output=True)
    shell_util.run_process('stem-cli deploy-tasks wait-for-environment --env-name %s' % k8s_env_name,
                           show_output=True)
    _wait_for_emulator_queues(k8s_env_name, emulator_name)


def _wait_for_emulator_queues(k8s_env_name, emulator_hostname):
    """
    Waits for emulator queues(7) to show up
    Pulled from env-reset code
    """
    pod_name = shell_util.get_pod_starts_with(emulator_hostname, k8s_env_name, emulator_hostname)
    if not pod_name:
        raise RuntimeError('Unable to find a pod for %s' % emulator_hostname)

    # wait for rabbitmq startup
    logger.info('Waiting for rabbitmq in emulator container to start...')
    cmd = 'rabbitmqctl wait /var/run/rabbitmq/pid'
    cmd = 'exec %s -- /bin/sh -c "%s"' % (pod_name, cmd)
    time.sleep(10)  # give rabbitmq a chance to start
    shell_util.kubectl(k8s_env_name, cmd, json_output=False, show_output=True)

    # will assume defaults when it comes to configuration
    expected_queue_count = '7'
    logger.info('Waiting for %s queues related to emulator', expected_queue_count)
    cmd = 'rabbitmqctl list_queues | grep %s | wc -l' % emulator_hostname
    cmd = 'exec %s -- /bin/sh -c "%s"' % (pod_name, cmd)

    timeout = 120  # secs
    start_time = time.time()
    found_match = False
    output = None
    while not found_match and time.time() < start_time + timeout:
        try:
            output, _ = shell_util.kubectl(k8s_env_name, cmd, json_output=False, show_output=True, timeout=90)
        except ShellUtilError:
            # possible that this cmd fails because rmq is not yet started
            output = ''
        # 'Data frame sent\n7' comes from queue messages when emulator started locally,
        # so to have ability debug emulator tests additional condition was added
        if expected_queue_count == output.strip() or 'Data frame sent\n7' in output:
            found_match = True
        logger.debug('Found match: %s', found_match)
        if not found_match:
            logger.debug('Sleeping 10secs before trying again...')
            time.sleep(5)

    if not found_match:
        # need to teardown the emulator b/c we may retry test with same release
        teardown_emulator_container(emulator_hostname)
        raise RuntimeError('Unable to find the right number of queues. Expected %s, Found %s'
                           % (expected_queue_count, output))
    logger.info('Finished waiting for %s queues related to emulator', expected_queue_count)


def teardown_emulator_container(emulator_hostname):
    """
    Stops emulator telemetry
    :param emulator_hostname:
    :return:
    """
    logger.debug('Stopping telemetry on %s', emulator_hostname)
    k8s_env_name = settings['env-name']
    # running into issues with helm del, so added retry here

    logger.info('Searching for emulator named %s', emulator_hostname)
    o = shell_util.kubectl(k8s_env_name, 'get pods | grep %s' % emulator_hostname,
                           json_output=False, fail_on_error=False)[0].strip()
    if o:
        logger.info('Tearing down Emulator %s.', emulator_hostname)
        shell_util.run_process('stem-cli deploy-tasks uninstall-chart --env-name %s --name stem-emulator-%s'
                               % (k8s_env_name, emulator_hostname),
                               show_output=True, fail_on_error=True)
        logger.info('Emulator %s stopped/removed.', emulator_hostname)
    else:
        logger.info('Emulator %s not found. Probably not installed in first place.', emulator_hostname)


# =============================================
# Code below this line is for the new emulator
# NOTE: remove all code above once all tests are migrated over to use new emulator
# =============================================


@retry((RuntimeError), delay=10, tries=3)
def collect_logs(edge_env):
    """
    Try to collect logs for emulator
    :param edge_env:
    :return:
    """
    try:
        edge_env.save_logs(exec_monit_stop=False)
    except Exception:
        logger.exception('Unable to zip up logs for emulator %s' % edge_env.ns_name)
        raise RuntimeError('Unable to zip up logs for emulator %s' % edge_env.ns_name)


@retry((RuntimeError), delay=15, tries=3)
def delete_emulator_namespace(namespace):
    """
    Try to delete namespace name
    :param namespace:
    :return:
    """
    try:
        logger.debug("Tearing down edge environment %s", namespace)
        shell_util.run_process('stem-cli deploy-tasks delete-environment --env-name %s' % namespace)
        shutil.rmtree('%s-chart' % namespace)
    except Exception:
        logger.exception('Unable to delete emulator namespace %s' % namespace)
        raise RuntimeError('Unable to delete emulator namespace %s' % namespace)


def remove_csv_telemetry_for_loc(location, dbhelper):
    """
    Removes: monitor_data(1hz, 1min, 15min)
             powerstore_bms(1hz, 1min, 15min)
             powerstore_setpoint_change
    :param location:
    :param dbhelper:
    :return:
    """
    logger.debug('About to remove db telemetry for %s', location)
    sql_to_execute = """
    DELETE FROM meter_logs.monitor_data where monitor_id like '{location}%%';
    DELETE FROM meter_logs.monitor_data_minute where monitor_id like '{location}%%';
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id like '{location}%%';
    DELETE FROM optimization.powerstore_bms where hostname = '{location}';
    DELETE FROM optimization.powerstore_bms_minute where hostname = '{location}';
    DELETE FROM optimization.powerstore_bms_15_minute where hostname = '{location}';
    DELETE FROM optimization.powerstore_setpoint_change where hostname = '{location}';
    """
    loc_low = location.lower()
    sql = sql_to_execute.format(location=loc_low)
    dbhelper.query(None, sql)


def upload_interval_data(edge_env, meter_network_host, load_data_path):
    """
    Imports interval file to meter container for new emulator
    :param edge_env:
    :param meter_network_host:
    :param load_data_path:
    :return:
    """
    load_data_path = '%s/%s' % ('resources/edge/load_data', load_data_path)
    logger.debug('Sending file: {load_data_path} to namespace: {ns} meter: {meter_network_host}'
                 .format(load_data_path=load_data_path, ns=edge_env.namespace, meter_network_host=meter_network_host))
    edge_env.client.meter_sims[meter_network_host].set_csv(csv_file=load_data_path, immediate=True)


def wait_for_emulator_svc_startup(edge_env, timeout=30):
    """
    Wait for all microservices to come up by pinging their API status endpt.
    Returns individual state for each of the 8 edge-bp microservices
    Timeout if all microservices don't reach steady state within default 30sec
    :param edge_env:
    :param timeout:
    :return:
    """
    logger.debug('Begin polling for services to come up...')
    client = edge_env.client
    start_time = time.time()
    ms_list = [(client.supervisor, 'supervisor'),
               (client.rtc, 'rtc'),
               (client.cloud_relay_agent, 'cloud_relay_agent'),
               (client.scripting_agent, 'scripting_agent'),
               (client.quicksilver, 'quicksilver'),
               (client.setpoint_controller, 'setpoint_controller'),
               (client.solution_manager, 'solution_manager'),
               (client.power_offset_controller, 'power_offset_controller')]
    found_match = False
    while not found_match and time.time() < start_time + timeout:
        # re-initialize map so we get latest status
        ms_status_map = {}
        # iterate through edge services and check if they are up
        for ms in ms_list:
            try:
                result = ms[0].get_state("version")
                ms_status_map[ms[1]] = result
            except Exception as e:
                logger.error("Failed to get state for %s with exception %s", ms[1], str(e))
        logger.debug(ms_status_map)
        if len(list(ms_status_map)) == len(ms_list):
            found_match = True
        time.sleep(5)
        logger.debug('Sleeping 5sec before retrying status check...')

    if not found_match:
        edge_env.teardown()
        raise RuntimeError('All microservices did not startup w/in %s seconds \n%s' % (timeout, ms_status_map))
    logger.info('Finished waiting for microservices for %s', edge_env.namespace)


def update_current_tariff_data_tou(client, period_key, max_utility_demand_kw,
                                   max_stem_demand_kw, setpoint_kw, solution_setpoint_kw):
    """
    Sets setpoint data for specified TOU
    :param client:
    :param period_key:
    :param max_utility_demand_kw:
    :param max_stem_demand_kw:
    :param setpoint_kw:
    :param solution_setpoint_kw:
    :return:
    """
    logger.debug('Updating tou data for %s ...' % period_key)
    setpoint_data = api_client.SetpointData(timezone='US/Pacific')
    setpoint_data.add_setpoint_entry(period_key=period_key,
                                     max_utility_demand_kw=max_utility_demand_kw,
                                     max_stem_demand_kw=max_stem_demand_kw,
                                     setpoint_kw=setpoint_kw,
                                     solution_setpoint_kw=solution_setpoint_kw)
    client.solution_manager.set_setpoint_data(setpoint_data)


def set_rtc_setpoint(client, setpoint_kw):
    return client.rtc.set_setpoint_kw(setpoint_kw)


def set_ess_soc(client, ess_name, soc):
    return client.ess_sims[ess_name].set_bms_soc(soc)


def _create_config(resource, resource_dir):
    """
    Creates config files for running emulator
    - config.json: describes meter, ess, site objects
    - test.options: describes config for rabbitmq
    - config dir
    :param resource:
    :param resource_dir:
    :return:
    """
    config = '%s/config.json' % resource_dir
    template = 'resources/edge/templates/%s' % resource['template']
    os.system(r"sed -e 's/\@@SETPOINT@@/%s/g;s/\@@HOSTNAME@@/%s/g;' <%s > %s" % (resource['setpoint'],
                                                                                 resource['hostname'],
                                                                                 template, config))
    test_opts = '%s/test.options' % resource_dir
    template = 'resources/edge/templates/test.options'
    os.system(r"sed -e 's/\@@CONFIG@@/%s/g;' <%s > %s" % (resource_dir.replace('/', r'\/'), template, test_opts))
    if not os.path.exists('%s/config' % resource_dir):
        os.makedirs('%s/config' % resource_dir)


def create_emu_configs(testdata):
    """
    Setup resource(s) for a given test
    Folder structure:
     - test_case_id  (truncated to 8 chars)
         - emu_1
            - config files
         - emu_2
            - config files
         - emu_n
            - config files
    For each resource
    - use templates to generate config files for each resource
    - start emulator(edge_env) resource
    - upload telemetry load file
    - update Off-Peak tariff tou settings
    - update RTC setpoint
    :param testdata:
    :return:
    """
    # all config files will be created under 'resources/edge'
    test_dir = "resources/edge"
    edge_env_list = []
    for resource in testdata['resources']:
        edge_env = create_emu_config(resource, test_dir)
        edge_env_list.append((resource, edge_env))
    return edge_env_list


def create_emu_config(resource, test_dir):
    """
    For each resource
    - use templates to generate config files for each resource
    - initialize edge_env name and return it
    NOTE: the emulator isn't started in this step, it's first initialized here
    and then started in a second step- in case the emulator startup fails(in K8 env),
    we will have the emulator configuration so we can delete it.
    :param resource:
    :param test_dir:
    :return: edge_env which will be used to configure and start the new emulator
    """
    if 'emulator' in resource:
        emu_name = resource['emulator']
    elif 'emulator_hostname' in resource:
        emu_name = resource['emulator_hostname']
    else:
        logger.debug('Emulator does not have name, providing random one...')
        emu_name = 'emu-%s' % ''.join(random.choice(list(set(string.hexdigits.lower()))) for _ in range(5))
    resource_dir = '%s/%s' % (test_dir, emu_name)
    logger.debug('Creating emulator resource config: %s', resource_dir)
    if not os.path.exists(resource_dir):
        os.makedirs(resource_dir)

    _create_config(resource, resource_dir)

    edge_env = EdgeEnvironment(emu_name, resource_dir=test_dir)
    return edge_env


def setup_env(edge_env, resource):
    """
    Use the test params to setup and configure the emulator
    - wait for all edge-bp microservices to startup
    - upload telemetry for replay if applicable
    - update tariff data if applicable
    - update setpoint if applicable
    - update SOC if applicable
    :param edge_env:
    :param resource:
    :return:
    """
    edge_env.setup()
    edge_client = edge_env.client
    wait_for_emulator_svc_startup(edge_env, timeout=60)
    if 'meters' in resource:
        for meter in resource['meters']:
            if 'load_data' in meter:
                upload_interval_data(edge_env, meter['name'], meter['load_data'])
    if 'current_tariff_data' in resource:
        update_current_tariff_data_tou(edge_client, 'Off Peak', **resource['current_tariff_data'])
    if 'setpoint' in resource:
        set_rtc_setpoint(edge_client, resource['setpoint'])
    if 'ess' in resource:
        for ess in resource['ess']:
            if 'soc' in ess:
                set_ess_soc(edge_client, ess['ess_name'], ess['soc'])

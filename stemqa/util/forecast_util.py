import datetime
import json

from stemqa.helpers.db_helper import DbHelper, DB_FORECAST, DB_METER_LOGS
from stemqa.config import logconfig

logger = logconfig.get_logger(__name__)

# Default forecast algorithm db record values
DEFAULT_ALGORITHMS_COUNT = 1
DEFAULT_TYPES_COUNT = 3
DEFAULT_TRAINING_START_DATE = '2016-02-01 01:00:00'
DEFAULT_SCORE = 99
DEFAULT_PARAMETERS = '{"min_samples_split": 2, "n_estimators": 100, "min_samples_leaf": 6}'
DEFAULT_FEATURES = '["temperature", "tod_index", "dow_index"]'

# Expected data which represents ExtraTrees forecast algorithm in db
EXTRA_TREES = dict(id=2,
                   name='ExtraTrees',
                   parameter_grid='{"n_estimators": [100, 150, 200]}',
                   valid_end_datetime=None,
                   valid_start_datetime=datetime.datetime(2013, 1, 1, 0, 0))

# Expected data which represents GradientBoosting forecast algorithm in db
GRADIENT_BOOSTING = dict(id=5,
                         name='GradientBoosting',
                         parameter_grid='[{"n_estimators": [100], "loss": ["quantile"], "max_depth": [3],'
                                        ' "alpha": [0.6, 0.7, 0.8, 0.9]}, {"n_estimators": [100], "loss": ["ls"],'
                                        ' "max_depth": [3]}]',
                         valid_end_datetime=None,
                         valid_start_datetime=datetime.datetime(2018, 9, 1, 0, 0))

# Expected forecast type record in db
FORECAST_TYPE = dict(
    forecast_type_id=2,
    forecast_max_hours=24,
    forecast_frequency_hours=24,
    forecast_variable='power',
    optimization_target=None,
    function_name='',
    resolution_seconds=900,
    feature_sets='["temperature", {"hdd": {"baseline": 65, "method": "min"}}, {"cdd": {"baseline": 72, "method": '
                 '"max"}}, "holiday", "tod_index", "hour_index", "dow_index", "dow_in_month_index", "day_index", '
                 '"week_index", "month_index", "quarter_index", {"encode": {"cols_to_enc": ["month_index", '
                 '"quarter_index"]}}]',
    training_ndays='[30, 60, 90, 180, 365]',
    forecast_metrics='["r2_score", {"under_tou_score": {"peak_delta": 1.5, "weighted_avg": false}}]',
    tie_breaker_metric='{"under_tou_score": {"peak_delta": 1.5, "weighted_avg": false}}',
    scoring_variable='{"tariff_charges":{"unit":"unit_kw"}}',
    sample_weights=None,
    cross_validation_days=365,
    update_freq_nhours=720,
    default_algorithm_id=5,
    default_training_ndays=30,
    default_features='["temperature", {"hdd": {"baseline": 65, "method": "min"}}, {"cdd": {"baseline": 72, "method": '
                     '"max"}}, "holiday", "tod_index", "hour_index", "dow_index", "dow_in_month_index", "day_index", '
                     '"week_index", "month_index", "quarter_index", {"encode": {"cols_to_enc": ["month_index", '
                     '"quarter_index"]}}]',
    default_sample_weight=None,
    valid_start_datetime=datetime.datetime(2018, 9, 20, 17, 12, 44),
    valid_end_datetime=None,
    name=None)


def get_forecast_algorithm_db_record(forecast_type_id, location_code):
    """
    Returns algorithm records from forecast.forecast table for specific site location and forecast type
    :param forecast_type_id: type id of algorithm
    :param location_code: site location code
    :return: list of records
    """
    dbhelper = DbHelper.default_instance()
    logger.debug("Getting algorithm records from forecast.forecast table where "
                 "forecast_type_id = {type_id} and site location = {location}."
                 .format(type_id=forecast_type_id, location=location_code))

    sql_query = "SELECT * FROM forecast.forecast WHERE location_code = \"{0}\"" \
                " and forecast_type_id = \"{1}\" order by start_date;" \
        .format(location_code, forecast_type_id)
    response = dbhelper.query(DB_FORECAST, sql_query)
    return response


def set_site_algorithm(location_code, forecast_type_id, forecast_start_date,
                       forecast_end_date, forecast_training_days, forecast_algorithm_id,
                       forecast_computed_date, features=DEFAULT_FEATURES):
    """
    Creates forecast record in forecast.forecast table with specified set of parameters
    which represents columns in db table
    :param features: list of features
    :param location_code:
    :param forecast_type_id:
    :param forecast_start_date: date/time
    :param forecast_end_date: date/time
    :param forecast_training_days:
    :param forecast_algorithm_id:
    :param forecast_computed_date: date/time
    :return: query result
    """
    dbhelper = DbHelper.default_instance()
    logger.debug("Algorithm {algorithm} is not set for {location} location and would be updated."
                 .format(algorithm=forecast_algorithm_id, location=location_code))

    sql_query = "INSERT INTO forecast.forecast (" \
                "`location_code`," \
                "`forecast_type_id`," \
                "`start_date`," \
                "`end_date`," \
                "`training_ndays`," \
                "`algorithm_id`," \
                "`training_start`," \
                "`features`," \
                "`score`," \
                "`parameters`," \
                "`computed_date`)" \
                " VALUES ('{code}', " \
                "'{type_id}', " \
                "'{start_date}', " \
                "'{end_date}', " \
                "'{training_days}', " \
                "'{algorithm_id}', " \
                "'{training_start}', " \
                "'{features}', " \
                "'{score}', " \
                "'{parameters}', " \
                "'{computed_date}');" \
        .format(code=location_code,
                type_id=forecast_type_id,
                start_date=forecast_start_date,
                end_date=forecast_end_date,
                training_days=forecast_training_days,
                algorithm_id=forecast_algorithm_id,
                training_start=DEFAULT_TRAINING_START_DATE,
                features=features,
                score=DEFAULT_SCORE,
                parameters=DEFAULT_PARAMETERS,
                computed_date=forecast_computed_date, )

    dbhelper.query(DB_FORECAST, sql_query)
    logger.debug("Algorithm record created in db for {location} location.".format(location=location_code))


def clear_site_algorithm(location_code):
    """
    Deletes site algorithm record from forecast.forecast table for specific site location code
    :param location_code:
    :return: query result
    """
    dbhelper = DbHelper.default_instance()
    logger.debug("Deleting algorithm data for {location} location.".format(location=location_code))

    sql_query = "DELETE FROM forecast.forecast WHERE location_code like '{location}%';". \
        format(location=location_code)
    dbhelper.query(DB_METER_LOGS, sql_query)


def get_valid_algorithms():
    """
    Retrieves list of not expired forecast algorithms from forecast.algorithms table
    :return: query result
    """
    dbhelper = DbHelper.default_instance()
    logger.debug("Getting not expired algorithms from forecast.algorithms table.")
    sql_query = "SELECT * FROM forecast.algorithms WHERE valid_end_datetime is Null;"
    response = dbhelper.query(DB_FORECAST, sql_query)
    return response


def get_forecast_type():
    """
    Retrieves list of not expired forecast algorithms types from forecast.forecast_type table
    :return: query result
    """
    dbhelper = DbHelper.default_instance()
    logger.debug("Getting forecast type data from forecast.forecast_type table.")
    sql_query = "SELECT * FROM forecast.forecast_type WHERE valid_end_datetime is Null;"
    response = dbhelper.query(DB_FORECAST, sql_query)
    return response


def update_training_start_date(location_code, start_date):
    """
    Updates forecast algorithms training_start value for specific site location
    :param location_code:
    :param start_date: date/time
    :return:
    """
    dbhelper = DbHelper.default_instance()
    logger.debug('Setting training_start = {0} for location_code = {1}'.format(start_date, location_code))
    sql_query = "UPDATE forecast.forecast SET training_start = '{start_date}' WHERE location_code like '{location}%';" \
        .format(start_date=start_date, location=location_code)
    response = dbhelper.query(DB_FORECAST, sql_query)
    return response


def set_freq_hours(freq_hours):
    """
    Updates forecast update_freq_hours value for forecast types
    :param freq_hours:
    :return:
    """
    dbhelper = DbHelper.default_instance()
    logger.debug('Setting update_freq_nhours = {0} days'.format(freq_hours / 24))
    sql_query = "UPDATE forecast.forecast_type SET update_freq_nhours = '{hours}';".format(hours=freq_hours)
    response = dbhelper.query(DB_FORECAST, sql_query)
    return response


def get_algorithm_name(algorithm_id):
    """
    Retrieves algorithm name by id
    :param algorithm_id:
    :return: string
    """
    dbhelper = DbHelper.default_instance()
    logger.debug('Getting algorithm name by id')
    sql_query = "SELECT name FROM forecast.algorithms where id = {id};".format(id=algorithm_id)
    response = dbhelper.query(DB_FORECAST, sql_query)
    return response[0]['name']


def compare_forecast(expected_forecast, actual_forecast, accuracy=3):
    """
    Compares forecast json data with expected data read from file
    :param accuracy: allowed percentage of difference between values
    :param expected_forecast: file path to the json with expected forecast
    :param actual_forecast: actual forecast dictionary
    :return: list or errors
    """
    key = 'predicted_power'

    if 'response' in actual_forecast:
        return ["Forecast failed with: {log}".format(log=actual_forecast['response'])]

    if 'predicted_solar' in actual_forecast:
        key = 'predicted_solar'
    actual_prediction = actual_forecast[key]
    forecast_errors = actual_forecast['err']

    with open(expected_forecast, 'r') as file:
        expected_data = json.load(file)
        expected_prediction = expected_data[key]
        expected_errors = expected_data['err']

        prediction_diff = compare_values(expected_prediction, actual_prediction, accuracy)
        errors_diff = compare_values(expected_errors, forecast_errors, accuracy)
        comparison_errors = prediction_diff + errors_diff

    return comparison_errors


def compare_values(expected, actual, accuracy):
    """
    Compares values in two dictionaries and returns list of differences
    :param accuracy: allowed percentage of difference between values
    :param expected: dictionary with expected values
    :param actual: dictionary with actual values
    :return: list of differences
    """
    accuracy = 1 + float(accuracy) / 100
    comparison_errors = list()
    for key in actual:
        if actual[key] != expected[key]:
            # incrementing value to predict division by zero
            difference = abs(max(actual[key] + 1, expected[key] + 1) / min(actual[key] + 1, expected[key] + 1))

            if difference > accuracy:
                comparison_errors.append("Value for {key} differs. Expected: {expected}. Actual: {actual}."
                                         " Diff is: {diff} %".format(key=key,
                                                                     expected=expected[key],
                                                                     actual=actual[key],
                                                                     diff=(difference * 100) - 100))
    return comparison_errors

import datetime as dt
import json
import logging

import dateutil.parser
import pytz

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_TOPOLOGY, DB_OPERATIONS
from stemqa.rest.powerdata import PowerDataClient, STATUS_TYPE_PENDING, STATUS_TYPE_PROCESSED
from stemqa.rest.powerqa import PowerQAClient

logger = logging.getLogger(__name__)

WEB_URL = settings['stem-web-app']['url']
str_format = "%Y-%m-%d %H:%M:%S %Z"


def create_and_fetch_data(client, vendor, location_code_site, vendor_site_id,
                          req_start_datetime=None, req_end_datetime=None):
    response = client.create_streams(vendor, str(location_code_site), str(vendor_site_id),
                                     req_start_datetime=req_start_datetime,
                                     req_end_datetime=req_end_datetime)
    content = response.json()
    stream_id = content['stream_id']
    logger.debug('New location code created for external data import: %s', stream_id)
    logger.debug('Fetching interval data for location code: %s', stream_id)
    response = client.fetch_stream(stream_id)
    task_id = response.json()['task']
    # wait on the task responsible for fetching, staging, importing the data
    client2 = PowerQAClient(WEB_URL, client=client)
    client2.wait_for_task_completion(task_id, 300)
    return stream_id


def remove_all_existing_streams(stream_id=None):
    if stream_id:
        with PowerDataClient(WEB_URL) as client:
            client.delete_stream(stream_id)
    else:
        sql = "SELECT distinct location_code " \
              "FROM topology.location_attribute " \
              "WHERE location_code like '%UTILITYAPIQA_UTILITY%';"
        dbhelper = DbHelper.default_instance()
        results = dbhelper.query(DB_TOPOLOGY, sql)
        for result in results:
            stream_id = result['location_code']
            sql = "SELECT location_code_site from operations.third_party_vendor_operations_apercu " \
                  "WHERE location_code = '%s';" % stream_id
            result2 = dbhelper.query(DB_OPERATIONS, sql)
            location_code_site = result2[0]['location_code_site']
            logger.debug('Deleting stream(%s) metadata and data for(%s)', stream_id, location_code_site)
            with PowerDataClient(WEB_URL) as client:
                response = client.delete_stream(stream_id)
                assert response.response.status_code == 204, 'Stream metadata delete returned error code %s' \
                                                             % response.response.status_code
                response = client.delete_s3_stream_data(STATUS_TYPE_PROCESSED, location_code_site=location_code_site)
                assert response.response.status_code == 204, 'Stream data(processed) delete returned error code %s' \
                                                             % response.response.status_code
                response = client.delete_s3_stream_data(STATUS_TYPE_PENDING, location_code_site=location_code_site)
                assert response.response.status_code == 204, 'Stream data(pending) delete returned error code %s' \
                                                             % response.response.status_code
    import time
    time.sleep(5)


def verify_intervals(stem_interval_list, utilityapi_interval_list, testid):
    """
    For each interval in stem list, utilityapi list, compare: start and end time and kw value
    :param stem_interval_list:
    :param utilityapi_interval_list:
    :return:
    """
    logger.debug('Verifying interval data...')
    errors = []
    for stem_interval, utilityapi_interval in zip(stem_interval_list, utilityapi_interval_list):
        utilityapi_interval_value = utilityapi_interval['kwh']
        stem_interval_value = stem_interval['kwh']

        timezone = pytz.UTC
        utilityapi_interval_start = dateutil.parser.parse(utilityapi_interval['start']).astimezone(timezone)
        utilityapi_interval_end = dateutil.parser.parse(utilityapi_interval['end']).astimezone(timezone)
        stem_interval_start = dateutil.parser.parse(stem_interval['start']).astimezone(timezone)
        stem_interval_end = dateutil.parser.parse(stem_interval['end']).astimezone(timezone)

        assert_value = True

        if (utilityapi_interval_start + dt.timedelta(seconds=1)) != stem_interval_start:
            assert_value = False
        if (utilityapi_interval_end + dt.timedelta(seconds=1)) != stem_interval_end:
            assert_value = False
        if utilityapi_interval_value != stem_interval_value:
            assert_value = False

        if not assert_value:
            item = '\nSTEM-        value: %s start: %s end: %s ' \
                   '\nUTILITY_API- value: %s start: %s end: %s' % \
                   (stem_interval_value, stem_interval_start.strftime(str_format),
                    stem_interval_end.strftime(str_format),
                    utilityapi_interval_value, utilityapi_interval_start.strftime(str_format),
                    utilityapi_interval_end.strftime(str_format))
            errors.append(item)
    if errors:
        logger.error('Errors between utilityAPI and stem intervals:')
        for error in errors:
            logger.debug(error)
        logger.error('Printing interval files...')
        # print interval files for debugging failed test...
        with open('resources/dataconnect/stem_%s.csv' % testid, 'w') as outfile:
            json.dump(stem_interval_list, outfile, indent=2)
        with open('resources/dataconnect/utilityapi_%s.csv' % testid, 'w') as outfile:
            json.dump(utilityapi_interval_list, outfile, indent=2)
        assert False

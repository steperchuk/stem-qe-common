import datetime as dt
import json
import logging

import pytz
from kombu import Connection, Exchange, Queue, Consumer
from kombu.pools import producers
from pytz import UTC

from stemqa.config.settings import settings
from stemqa.util import shell_util

logger = logging.getLogger(__name__)

"""
Utility methods to modify values as you would do through powerstore db page
"""

RETRY_POLICY = {
    'interval_start': 0,  # First retry immediately,
    'interval_step': 4,  # then increase by 4s for every retry.
    'interval_max': 20,  # but don't exceed 20s between retries.
    'max_retries': 4,  # give up after 4 tries.
}


def set_tariff_data(max_utility_demand,
                    setpoint,
                    solution_setpoint,
                    max_stem_demand,
                    amqp_port,
                    emulator,
                    host='localhost',
                    username='powermonitor',
                    password='4hN7k4Uf',
                    retry=True):
    """
    Sets the tariff data.
    See powerstore db page- https://qa-app.stem.com/powerstore/db/emulator-1/live/chart?interval=
        Solution Manager -> current_tariff_data
        This message is received by PowerOffsetController(POC), SolutionManager(SM), SetPointController3(SPC).
        See logs in emulator at /var/log/rabbitmq/powerstore/dts.log
        The values set in this message are used in various filter calculations in power controller runtime.py
        By default the emulator uses the 'Off Peak' period
        This message should be sent to the port on the emulator container
    :param max_utility_demand: maximum utility demand re-calculated every UMI interval
    :param setpoint: setpoint for powerstore
    :param solution_setpoint: setpoint for powerstore
    :param max_stem_demand: maximum stem demand re-calculated every UMI interval
    :param amqp_port:
    :param emulator:
    :param host:
    :param username:
    :param password:
    :return:
    """
    amqp_port = get_emulator_k8port(emulator)

    msg = \
        '''
        {
            "arr": [{
                "id": "pgx.%s.%s.tm.tariff_manager.telemetry",
                "data": [{
                    "tariff_data": {
                        "Off Peak": {
                            "max_utility_demand": %s,
                            "setpoint": %s,
                            "solution_setpoint": %s,
                            "max_stem_demand": %s
                        },
                        "Partial Peak": {
                            "max_utility_demand": 0,
                            "setpoint": 0,
                            "solution_setpoint": 0,
                            "max_stem_demand": 0
                        },
                        "Peak": {
                            "max_utility_demand": 0,
                            "setpoint": 0,
                            "solution_setpoint": 0,
                            "max_stem_demand": 0
                        }
                    },
                    "current_period": "Off Peak",
                    "t": "%s"
                }]
            }],
            "ver": 1.0
        }
        '''
    timestamp_dt = dt.datetime.now(tz=UTC)
    timestamp_dt_str = timestamp_dt.strftime("%Y-%m-%dT%H:%M:%S.%f")
    msg_to_send = msg % (emulator, emulator,
                         max_utility_demand, setpoint, solution_setpoint, max_stem_demand,
                         timestamp_dt_str)
    amqp_url = 'amqp://%s:%s@%s:%s/' % (username,
                                        password,
                                        host,
                                        amqp_port)
    logger.debug('Sending msg on: url: %s, exchange: pgx, routing key: %s',
                 amqp_url, 'command.%s' % emulator)
    logger.debug('Payload:\n%s', msg_to_send)
    connection = Connection(amqp_url)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(msg_to_send, exchange='pgx', routing_key='command.%s' % emulator,
                         retry=retry, retry_policy=RETRY_POLICY)


def set_tariff_data_value(key,
                          value,
                          emulator,
                          amqp_port=settings['rmq']['rabbit-port'],
                          tou='Off Peak',
                          host='localhost',
                          username='powermonitor',
                          password='4hN7k4Uf',
                          retry=True):
    """
    Updates SolutionManager(SM) with single value
    This update is sent to the PowerOffsetController(POC) at every UMI interval.
    This message should be sent to the port on the compose-supervisor container
    :param key: key attribute in tariff data to update
    :param value: value for key attribute in tariff data to update
    :param amqp_port: default compose-supervisor port
    :param emulator:
    :param tou:
    :param host:
    :param username:
    :param password:
    :param retry:
    :return:
    """

    msg = \
        '''
        {
            "reply_exchange": "pgx",
            "args": { "value": { "%s" : {"%s" : %s}}},
            "command": "apply_setpoints_update",
            "reply_queue": "routable.replies.iterate_command_init_reply_queue_%s"
        }
        '''
    amqp_url = 'amqp://%s:%s@%s:%s/' % (username,
                                        password,
                                        host,
                                        amqp_port)
    logger.debug('Sending msg on: url: %s, exchange: %s, routing key: %s',
                 amqp_url, 'pgx_cmd_relay', '%s_cmd' % emulator)
    msg_to_send = msg % (tou, key, value, emulator)
    logger.debug('Payload:\n%s', msg_to_send)
    connection = Connection(amqp_url)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(msg_to_send, exchange='pgx_cmd_relay', routing_key='%s_cmd' % emulator,
                         retry=retry, retry_policy=RETRY_POLICY)


def get_emulator_state(amqp_port,
                       emulator,
                       host='localhost',
                       username='powermonitor',
                       password='4hN7k4Uf',
                       retry=True):
    """
    Trying to get state of emulator. WIP.
    """

    amqp_port = get_emulator_k8port(emulator)
    amqp_url = 'amqp://%s:%s@%s:%s/' % (username,
                                        password,
                                        host,
                                        amqp_port)
    conn = Connection(amqp_url)
    reply_queue = "routable.replies.iterate_command_reply_queue_%s" % emulator
    exchange = Exchange("pgx", type="topic", durable=False)
    queue = Queue(name='TEST', exchange=exchange, routing_key=reply_queue)
    queue.maybe_bind(conn)
    queue.declare()
    msg = \
        '''
        {
            "reply_exchange": "pgx",
            "args": {},
            "command": "get_component_state",
            "reply_queue": "%s"
        }
        '''
    logger.debug('Sending msg on: url: %s, exchange: %s, routing key: %s',
                 amqp_url, 'pgx_cmd_relay', '%s_cmd' % emulator)
    msg_to_send = msg % reply_queue
    logger.debug('Payload:\n%s', msg_to_send)
    with producers[conn].acquire(block=True) as producer:
        producer.publish(msg_to_send, exchange='pgx_cmd_relay', routing_key='%s_cmd' % emulator,
                         retry=retry, retry_policy=RETRY_POLICY)
    data = []

    def process_message(body, message):
        logger.debug("The body is %s", body)
        logger.debug("The message %s", message)
        message.ack()
        data.append({'body': body, 'msg': message})

    with Consumer(conn, queues=queue, callbacks=[process_message]):
        conn.drain_events(timeout=5)

    return data


def get_emulator_k8port(emulator):
    """
    For k8, we need to figure out the dynamically allocated port
    Query k8 emulator svc to get amqp-port
    :param emulator:
    :return:
    """
    logger.debug('Getting NodePort for AMQP port for emulator %s', emulator)
    k8s_env_name = settings['env-name']
    output, _ = shell_util.kubectl(k8s_env_name, 'get svc %s' % emulator, json_output=True, fail_on_error=True)
    spec = output['spec']
    amqp_port_list = [port for port in spec['ports'] if port['name'] == 'amqp-port']
    amqp_port = amqp_port_list[0]['nodePort']
    logger.debug('In k8 env, using port %s for emulator %s', amqp_port, emulator)
    return amqp_port


def set_tariff_data_optimization_mode(optimization_mode,
                                      resource_timezone,
                                      amqp_port,
                                      emulator,
                                      host='localhost',
                                      username='powermonitor',
                                      password='4hN7k4Uf',
                                      retry=True):
    """
    Sets the tariff data settings in Solution Manager.
    See powerstore db page- https://qa-app.stem.com/powerstore/db/emulator-XX/live/chart?interval=
                            (emulator-XX should be changed with emulator you are testing with)
        Solution Manager -> tariff_data
        This message is received by PowerOffsetController(POC), SolutionManager(SM), SetPointController3(SPC).
        See logs in emulator at /var/log/rabbitmq/powerstore/dts.log
        The values set in this message are used in various filter calculations in power controller runtime.py
        By default the emulator uses the 'Off Peak' period
    This message should be sent to the port on the compose-supervisor container
    :param optimization_mode: 1 - resets tariff data at midnight, 3 - tariff data stays
                             (should be configured with powerstore config but emulator is not using it properly)
    :param amqp_port:
    :param emulator:
    :param host:
    :param username:
    :param password:
    :return:
    """

    msg = \
        '''
        {
            "reply_exchange": "pgx",
            "command": "set_tariff_data",
            "reply_queue": "routable.replies.iterate_command_init_reply_queue_%s",
            "args": {
                "value": {
                    "tariff-schedule": {
                        "%s": "Weekday"
                    },
                    "billing-cycle-dates": [
                        "%s",
                        "%s"
                    ],
                    "optimization-mode": %s,
                    "effective-date": {
                        "start_date": "%s",
                        "end_date": "%s"
                    },
                    "powerstore-enable": true,
                    "schedule-data": {
                        "Weekend": [
                            {
                                "time_periods": [
                                    [
                                        1,
                                        1440
                                    ]
                                ],
                                "period": "Off Peak"
                            }
                        ],
                        "Weekday": [
                            {
                                "time_periods": [
                                    [
                                        1,
                                        1440
                                    ]
                                ],
                                "period": "Off Peak"
                            }
                        ]
                    }
                }
            }
        }
        '''
    amqp_url = 'amqp://%s:%s@%s:%s/' % (username,
                                        password,
                                        host,
                                        amqp_port)
    logger.debug('Sending msg on: url: %s, exchange: %s, routing key: %s',
                 amqp_url, 'pgx_cmd_relay', '%s_cmd' % emulator)

    resource_tz = pytz.timezone(resource_timezone)
    resource_dt = dt.datetime.now(tz=resource_tz)
    resource_date = resource_dt.strftime("%m/%d/%Y")
    resource_date_after_month = (resource_dt + dt.timedelta(days=31)).strftime("%m/%d/%Y")

    msg_to_send = msg % (emulator,
                         resource_date,
                         resource_date,
                         resource_date_after_month,
                         optimization_mode,
                         resource_date,
                         resource_date_after_month)
    logger.debug('Payload:\n%s', msg_to_send)
    connection = Connection(amqp_url)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(msg_to_send, exchange='pgx_cmd_relay', routing_key='%s_cmd' % emulator,
                         retry=retry, retry_policy=RETRY_POLICY)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        handlers=[logging.StreamHandler()])
    _host = "localhost"
    _port = "5906"
    _emulator = "emulator-106"
    state = get_emulator_state(amqp_port=_port,
                               emulator=_emulator,
                               host=_host)
    logger.debug('Emulator state: %s', json.dumps(state))

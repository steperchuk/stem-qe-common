from stemqa.helpers.db_helper import DbHelper, DB_ANALYTICS, DB_METER_LOGS, DB_OPERATIONS, DB_OPTIMIZATION
from stemqa.util import dataload_util
from stemqa.config import logconfig

OPTIMIZE_BASE = 's3://stemqa-sampledata/csv_import/optimization/realtime'

OPERATIONS_BASE = 's3://stemqa-sampledata/csv_import/operations/realtime'

METER_LOGS_BASE = 's3://stemqa-sampledata/csv_import/meter_logs/realtime'

ANALYTICS_BASE = 's3://stemqa-sampledata/csv_import/analytics/realtime'

logger = logconfig.get_logger(__name__)

setup_data_ui_gen1 = [
    {'s3_base': ANALYTICS_BASE,
     'db': DB_ANALYTICS,
     'files': [
         {'table': 'location_cached_bill', 'file': 'location_cached_bill_ui_gen1.csv'},
         {'table': 'location_cached_bill_delta', 'file': 'location_cached_bill_delta_ui_gen1.csv'},
         {'table': 'location_tariff_calculation', 'file': 'location_tariff_calculation_ui_gen1.csv'},
         {'table': 'location_tariff_calculation_apercu', 'file': 'location_tariff_calculation_apercu_ui_gen1.csv'}]
     },
    {'s3_base': METER_LOGS_BASE,
     'db': DB_METER_LOGS,
     'files': [
         {'table': 'monitor_data', 'file': 'monitor_data/monitor_data_ui_gen1.csv'},
         {'table': 'monitor_data_minute', 'file': 'monitor_data_minute/monitor_data_minute_ui_gen1.csv'},
         {'table': 'monitor_data_15_minute', 'file': 'monitor_data_15_minute/monitor_data_15_minute_m1_ui_gen1.csv'},
         {'table': 'monitor_data_15_minute',
          'file': 'monitor_data_15_minute/monitor_data_15_minute-inverter_ui_gen1.csv'},
         {'table': 'monitor_data_15_minute', 'file': 'monitor_data_15_minute/monitor_data_15_minute-site_ui_gen1.csv'}]
     },
    {'s3_base': OPERATIONS_BASE,
     'db': DB_OPERATIONS,
     'files': [
         {'table': 'asset_operational_attribute', 'file': 'asset_operational_attribute_ui_gen1.csv'}]
     },
    {'s3_base': OPTIMIZE_BASE,
     'db': DB_OPTIMIZATION,
     'files': [
         {'table': 'powerstore_bms_minute', 'file': 'powerstore_bms_minute_ui_gen1.csv'},
         {'table': 'powerstore_bms_15_minute', 'file': 'powerstore_bms_15_minute_ui_gen1.csv'}]
     }]

setup_data_ui_gen2 = [
    {'s3_base': ANALYTICS_BASE,
     'db': DB_ANALYTICS,
     'files': [
         {'table': 'location_tariff_calculation',
          'file': 'location_tariff_calculation_ui_gen2_2018-01-01_2018-02-12_tariff34.csv'},
         {'table': 'location_tariff_calculation',
          'file': 'location_tariff_calculation_ui_gen2_2018-02-13_2018-03-02_tariff420.csv'},
         {'table': 'location_tariff_calculation_apercu', 'file': 'location_tariff_calculation_apercu_ui_gen2.csv'}]
     },
    {'s3_base': METER_LOGS_BASE,
     'db': DB_METER_LOGS,
     'files': [
         {'table': 'monitor_data_15_minute', 'file': 'monitor_data_15_minute/monitor_data_15_minute_m1_ui_gen2.csv'},
         {'table': 'monitor_data_15_minute',
          'file': 'monitor_data_15_minute/monitor_data_15_minute-inverter_ui_gen2.csv'},
         {'table': 'monitor_data_15_minute',
          'file': 'monitor_data_15_minute/monitor_data_15_minute-site_ui_gen2.csv'}]
     }]

setup_data_ui_client1 = [
    {'s3_base': ANALYTICS_BASE,
     'db': DB_ANALYTICS,
     'files': [
         {'table': 'location_cached_bill', 'file': 'location_cached_bill_ui_client1.csv'},
         {'table': 'location_cached_bill_delta', 'file': 'location_cached_bill_delta_ui_client1.csv'},
         {'table': 'location_tariff_calculation', 'file': 'location_tariff_calculation_ui_client1.csv'},
         {'table': 'location_tariff_calculation_apercu', 'file': 'location_tariff_calculation_apercu_ui_client1.csv'}]
     },
    {'s3_base': METER_LOGS_BASE,
     'db': DB_METER_LOGS,
     'files': [
         {'table': 'monitor_data_15_minute', 'file': 'monitor_data_15_minute/monitor_data_15_minute_m1_ui_client1.csv'},
         {'table': 'monitor_data_15_minute',
          'file': 'monitor_data_15_minute/monitor_data_15_minute-inverter_ui_client1.csv'},
         {'table': 'monitor_data_15_minute',
          'file': 'monitor_data_15_minute/monitor_data_15_minute-site_ui_client1.csv'}]
     },
    {'s3_base': OPERATIONS_BASE,
     'db': DB_OPERATIONS,
     'files': [
         {'table': 'asset_operational_attribute', 'file': 'asset_operational_attribute_ui_client1.csv'}]
     },
    {'s3_base': OPTIMIZE_BASE,
     'db': DB_OPTIMIZATION,
     'files': [
         {'table': 'powerstore_bms_minute', 'file': 'powerstore_bms_minute_ui_client1.csv'},
         {'table': 'powerstore_bms_15_minute', 'file': 'powerstore_bms_15_minute_ui_client1.csv'},
         {'table': 'powerstore_setpoint_change', 'file': 'powerstore_setpoint_change_ui_client1.csv'}]
     }]


def clear_and_setup_data(location_code, setup_data_map):
    """
    Clear out rows in specified tables and then populate with source data.
    Any rows for 'UI_GEN1' client in the following tables are removed and populated with source data.

    Load up setup data
    -  DB_ANALYTICS: ['location_cached_bill',
                      'location_cached_bill_delta',
                      'location_tariff_calculation',
                      'location_tariff_calculation_apercu']
    - DB_METER_LOGS: ['monitor_data',
                      'monitor_data_minute',
                      'monitor_data_15_minute']
    - DB_OPTIMIZATION: ['powerstore_bms_minute',
                        'powerstore_bms_15_minute']
    - DB_OPERATIONS: ['asset_operational_attribute']

    :return:
    """
    clear_data(location_code)

    # load data to all tables
    for entry in setup_data_map:
        for _file in entry['files']:
            options = {
                "database": entry['db'],
                "table": _file['table'],
            }
            s3_file = '%s/%s' % (entry['s3_base'], _file['file'])
            logger.debug('Importing %s', s3_file)
            dataload_util.load_data(s3_file, options, False)


def clear_data(location_code):
    """
    Clears data from tables for specified location code
    :param location_code:
    :return:
    """
    dbhelper = DbHelper.default_instance()
    tables_to_clear = ['location_cached_bill', 'location_cached_bill_delta', 'location_tariff_calculation',
                       'location_tariff_calculation_apercu']
    for table in tables_to_clear:
        sql = "DELETE FROM %s WHERE location_code like '%%%s%%'" % (table, location_code)
        dbhelper.query(DB_ANALYTICS, sql)
    tables_to_clear = ['monitor_data', 'monitor_data_minute', 'monitor_data_15_minute']
    for table in tables_to_clear:
        sql = "DELETE FROM %s WHERE monitor_id like '%%%s%%'" % (table, location_code)
        dbhelper.query(DB_METER_LOGS, sql)
    tables_to_clear = ['powerstore_bms_minute', 'powerstore_bms_15_minute']
    for table in tables_to_clear:
        sql = "DELETE FROM %s WHERE component_id like '%%%s%%'" % (table, location_code)
        dbhelper.query(DB_OPTIMIZATION, sql)
    sql = "DELETE FROM %s WHERE asset_id like '%%%s%%'" % ('asset_operational_attribute', location_code)
    dbhelper.query(DB_OPERATIONS, sql)

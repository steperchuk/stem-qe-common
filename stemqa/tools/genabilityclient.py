import json
import logging
from pprint import pformat

import requests

from stemqa.rest.qarestexception import QaRestException

logger = logging.getLogger(__name__)

GENABILITY_BETA_BASE_TARIFF_URL = "https://api.genability.com/rest/beta/tariffs"
GENABILITY_PUBLIC_BASE_URL = "https://api.genability.com/rest/public"
GENABILITY_USER = 'bffceba3-a01f-4404-86b3-3df14cccec6e'
GENABILITY_PASSWORD = '22a7e6d7-0ef3-4b14-9302-8d869c383ef8'


def add_private_tariff(json_filename):
    """
    Add a private tariff to genability with beta url
    :param json_filename:
    :return:
    """
    headers = {'content-type': 'application/json'}
    contents = open(json_filename, 'r').read()
    logger.info("invoking url(%s) with tariff from %s", GENABILITY_BETA_BASE_TARIFF_URL, json_filename)
    resp = requests.post(url=GENABILITY_BETA_BASE_TARIFF_URL,
                         auth=(GENABILITY_USER, GENABILITY_PASSWORD), data=contents, headers=headers)
    return _handle_response(resp)


def delete_private_tariff(tariff_id):
    """
    Delete a private tariff from genability with beta url
    :param tariff_id:
    :return:
    """
    url = (GENABILITY_BETA_BASE_TARIFF_URL + "/%s") % (tariff_id)
    logger.info("invoking url(%s)", url)
    resp = requests.delete(url=url, auth=(GENABILITY_USER, GENABILITY_PASSWORD))
    return _handle_response(resp)


def update_existing_private_tariff(json_filename):
    """
    Updates private tariff with beta url
    :param json_filename:
    :return:
    """
    headers = {'content-type': 'application/json'}
    contents = open(json_filename, 'r').read()
    logger.info("invoking url(%s) \ndata (%s)", GENABILITY_BETA_BASE_TARIFF_URL, pformat(contents))
    resp = requests.put(url=GENABILITY_BETA_BASE_TARIFF_URL,
                        auth=(GENABILITY_USER, GENABILITY_PASSWORD), data=contents, headers=headers)
    return _handle_response(resp)


def get_tariffs_by_utility(genability_utility_id, page_start=0, page_count=25):
    """
    Returns tariffs for specified genability utility
    :param page_start:
    :param page_count:
    :param genability_utility_id:
    :return:
    """
    url = "%s/%s" % (GENABILITY_PUBLIC_BASE_URL, 'tariffs')
    data = {
        "lseId": genability_utility_id,
        "pageStart": page_start,
        "pageCount": page_count
    }
    logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
    resp = requests.get(url=url,
                        auth=(GENABILITY_USER, GENABILITY_PASSWORD), params=data)
    return _handle_response(resp)


def get_tariff(tariff_id, master_tariff_id=None):
    """
    Returns specified tariff
    :param tariff_id:
    :return:
    """
    url = "%s/%s/%s" % (GENABILITY_PUBLIC_BASE_URL, 'tariffs', tariff_id)
    data = {"populateRates": True, "lookupVariableRates": True}
    if master_tariff_id:
        data['masterTariffId'] = master_tariff_id
    logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
    resp = requests.get(url=url,
                        auth=(GENABILITY_USER, GENABILITY_PASSWORD), params=data)
    return _handle_response(resp)


def search_for_tariffs(search=None, searchOn=None):
    url = "%s/%s" % (GENABILITY_PUBLIC_BASE_URL, 'tariffs')
    data = {}
    if search:
        data['search'] = search
        data['searchOn'] = searchOn
        data['pageCount'] = 100
    logger.info("invoking url(%s) \nparams (%s)", url, pformat(data))
    resp = requests.get(url=url,
                        auth=(GENABILITY_USER, GENABILITY_PASSWORD), params=data)
    return _handle_response(resp)


def get_genability_utilites(page_start=0, page_count=25, search=None):
    """
    Returns list of genability utilities
    :param page_start:
    :param page_count:
    :return:
    """
    url = "%s/%s" % (GENABILITY_PUBLIC_BASE_URL, 'lses')
    data = {"pageStart": page_start, "pageCount": page_count, "fields": "ext"}
    if search:
        data['search'] = search
    logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
    resp = requests.get(url=url,
                        auth=(GENABILITY_USER, GENABILITY_PASSWORD), params=data)
    return _handle_response(resp)


def get_genability_utility_seasons(lseId):
    """
    Returns list of genability utility seasons
    :param lseId:
    :return:
    """
    url = "%s/%s" % (GENABILITY_PUBLIC_BASE_URL, 'seasons')
    data = {"lseId": lseId}
    logger.info("invoking url(%s) \nparams (%s)", url, pformat(data))
    resp = requests.get(url=url,
                        auth=(GENABILITY_USER, GENABILITY_PASSWORD), params=data)
    return _handle_response(resp)


def get_tariff_versions(master_tariff_id):
    url = "%s/%s" % (GENABILITY_PUBLIC_BASE_URL, 'tariffs')
    params = {}
    params['masterTariffId'] = master_tariff_id
    params['pageCount'] = 99
    params['sortOn'] = 'effectiveDate'
    params['sortOrder'] = 'DESC'
    params['populateProperties'] = True
    logger.info("invoking url(%s) \nparams (%s)", url, pformat(params))
    resp = requests.get(url=url, auth=(GENABILITY_USER, GENABILITY_PASSWORD), params=params)
    return _handle_response(resp)


def _handle_response(resp):
    """
    Handles genability respose, throws exception if HTTP response isn't 200 or
    if HTTP response is 200 and status is not success
    :param response:
    :return:
    """
    json_resp = resp.json()
    if resp.status_code != 200:
        raise QaRestException('Exception calling genability. Status code %i returned' % resp.status_code)
    if json_resp.get('status') != 'success':
        raise QaRestException('Exception calling genability. Application error returned: %s'
                              % json_resp.get('results'))
    return resp


if __name__ == '__main__':
    # gell all versions for all pg and e tariffs
    # GenabilityClient.get_genability_versions()

    # add a private tariff
    add_private_tariff('/work/git/stem-qa-automation/resources/tariff_import/original_tariff.json')

    # Update the tariff with another version of same tariff with same 'add_private_tariff()' call as above
    # need to remove all tariffId, tariffRateId, tariffRateBandId elements in json(Genability requirement)
    # should include masterTariffId from the original tariff
    response = \
        add_private_tariff('/work/git/stem-qa-automation/resources/tariff_import/original_tariff_w_updated_rates.json')
    result = response.json()['results'][0]
    logger.debug(json.dumps(result, indent=4))

    # update exsiting private tariff, this DOES NOT add another version but updates an existing tariff
    # response = GenabilityClient().update_existing_private_tariff('/work/tariff_import_work/10_tariff_to_update1.json')

    # delete tariff
    delete_private_tariff(tariff_id=1111111)

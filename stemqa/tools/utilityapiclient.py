import logging
from pprint import pformat

import requests

logger = logging.getLogger(__name__)

api_token = 'b54e3afdb96347529fae2180d91b2e91'
utility_api_auth = 'https://utilityapi.com/api/v2/authorizations'
utility_api_intervals = 'https://utilityapi.com/api/v2/intervals'


# TODO: why is this a class?
class UtilityApiClient(object):
    """
    Client to interact with utilityAPI
    """

    @staticmethod
    def get_accounts():
        url = utility_api_auth
        data = {'access_token': api_token}
        logger.info("invoking url(%s) with \n%s", url, pformat(data))
        return requests.get(url, params=data, verify=False)

    @staticmethod
    def get_meters(authorization_uid):
        url = "%s/%s" % (utility_api_auth, authorization_uid)
        data = {'access_token': api_token,
                'include': 'meters'}
        logger.info("invoking url(%s) with \n%s", url, pformat(data))
        return requests.get(url, params=data, verify=False)

    @staticmethod
    def get_meter_data(meter_uid):
        url = utility_api_intervals
        data = {'access_token': api_token,
                'meters': meter_uid,
                'order': 'earliest_first'}
        logger.info("invoking url(%s) with \n%s", url, pformat(data))
        return requests.get(url, params=data, verify=False)

    @staticmethod
    def get_interval_data(meter_uid, start=None, end=None):
        url = utility_api_intervals
        data = {'access_token': api_token,
                'meters': meter_uid}
        if start and end:
            data['start'] = start
            data['end'] = end
        logger.info("invoking url(%s) with \n%s", url, pformat(data))
        response = requests.get(url, params=data, verify=False)
        return response

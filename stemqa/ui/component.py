from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from stemqa.ui.pages.page import DEFAULT_TIMEOUT


class Component(object):
    """
    Base class for various components that show up on the pages. Either a top level element or it's locator
    should be specified when creating an instance
    """

    def __init__(self, driver, top_element=None, top_element_locator=None):
        self.driver = driver
        self.top_element = top_element
        self.top_element_locator = top_element_locator
        if not self.top_element and not self.top_element_locator:
            raise RuntimeError('Have to specify at least one; element and/or element locator')

        if not self.top_element:
            self.top_element = self.wait_for_element(self.top_element_locator)
        self.wait_until_loaded()

    def wait_until_loaded(self):
        """
        derived classes should implement this method if there are elements that need to be waiting on before
        declaring this component ready for use
        :return:
        """
        pass

    def wait_for_element(self, locator, duration=DEFAULT_TIMEOUT):
        """
        Waits for an element to be present on the component
        :param locator:
        :param duration:
        :return:
        """
        return WebDriverWait(self.driver, duration).until(
            EC.presence_of_element_located(locator)
        )

    def wait_for_elements(self, locator, duration=DEFAULT_TIMEOUT):
        """
        Waits for all elements identified by the locator to be present on the page
        :param locator:
        :param duration:
        :return:
        """
        return WebDriverWait(self.driver, duration).until(
            EC.presence_of_all_elements_located(locator)
        )

    def wait_for(self, condition, duration=DEFAULT_TIMEOUT):
        """
        Waits for the specified function to return a value x such that bool(x) resolves to True
        :param condition:
        :param duration:
        :return:
        """
        return WebDriverWait(self.driver, duration).until(
            condition()
        )

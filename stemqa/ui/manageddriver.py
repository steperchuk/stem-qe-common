import logging

from stemqa import consts
from stemqa.config.settings import settings
from stemqa.rest.stembase import StemClient
from stemqa.ui.pages.page import Page

WEB_URL = settings['stem-web-app']['url']
logger = logging.getLogger(__name__)


class ManagedDriver(object):
    """
    Wrapper around the webdriver. Implements methods so that we can use it with the "with" statement
    and avoid explicit login/logout every time.
    """

    def __init__(self, driver, url=None, page=Page, username=consts.DEFAULT_USERNAME, password=consts.DEFAULT_PASSWORD,
                 client=None):
        """
        Returns the page after logging in and navigating the specified URL
        :param driver: web driver
        :param url: (optional) relative URL to navigate to. Is appended to WEB_URL.
        :param page: page object type
        :param username: username for login
        :param password: password for login
        :param client: An instance of StemClient that is already logged in. If specified, username/password are ignored
        """
        self.driver = driver
        self.managed_client = False
        self.client = client
        self.page = page
        if client:
            self.url = client.url
            self.username = client.username
            self.password = client.password
            self.cookies = client.session.cookies
        else:
            self.url = url
            self.username = username
            self.password = password

    def __enter__(self):
        return self.get_page()

    def get_page(self):
        if not self.client:
            self.client = StemClient(WEB_URL, username=self.username)
            self.client.login()
            self.managed_client = True
        self.cookies = self.client.session.cookies
        self.driver.get(WEB_URL)
        self.driver.add_cookie({'name': 'sessionid', 'value': self.cookies['sessionid']})
        self.driver.add_cookie({'name': 'nodejs_jwt', 'value': self.cookies['nodejs_jwt']})
        navigate_to = WEB_URL
        if self.url:
            navigate_to += self.url if self.url.startswith('/') else '/' + self.url
        self.driver.get(navigate_to)
        return self.page(self.driver)

    def __exit__(self, *args):
        self.close()

    def close(self):
        try:
            if self.managed_client:
                self.client.logout()
        except Exception:
            logger.exception('Failed logging out as user %s', self.username)

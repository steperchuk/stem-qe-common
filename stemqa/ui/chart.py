import logging

from selenium.webdriver.common.by import By

from stemqa.ui.component import Component

logger = logging.getLogger(__name__)


class Chart(Component):
    """
    Represents chart panel on many pages for represent graphical data
    """

    DEFAULT_EL_LOCATOR = (By.CSS_SELECTOR, '.highcharts-container')

    def __init__(self, driver):
        super(Chart, self).__init__(driver, None, self.DEFAULT_EL_LOCATOR)
        self.wait_until_loaded()

    def wait_until_loaded(self):
        """
        Wait for basic elements of the chart to load
        :return:
        """
        super(Chart, self).wait_until_loaded()

    def press_legend_button_for(self, name):
        """
        Press on legend toggle button with target name to hide or shown it curve on chart
        :param name: name of curve on chart
        :return:
        """
        element_locator = '//div[@class="chart-legend-button"][contains(text(), "{}")]'.format(name)
        legend_button = self.wait_for_element((By.XPATH, element_locator), 30)
        legend_button.click()

    def press_overlay_button(self):
        """
        Press on overlay button to maximize or minimize chart view
        :return:
        """
        overlay_button = self.wait_for_element((By.CSS_SELECTOR, 'div.chart-overlay-button'), 30)
        overlay_button.click()

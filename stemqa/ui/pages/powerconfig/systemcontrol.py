import logging

from selenium.webdriver.common.by import By

from stemqa.ui.pages.page import Page

logger = logging.getLogger(__name__)


class SystemControlPage(Page):
    """
    Represents the System Control Page
    """

    URL_SUFFIX_TEMPLATE = '/powerconfig/system_control/%s'

    def wait_until_loaded(self):
        self.wait_for_element((By.CSS_SELECTOR, 'div.form-control-container'))

    def connection_status(self):
        status = self.driver.find_element_by_css_selector('div#device_status.bold')
        return status.text

    def status(self):
        status = self.driver.find_element_by_css_selector('div#optimization_status.bold')
        return status.text

    def restart_smi(self):
        self.driver.find_element_by_css_selector('input.button.gray[value="Restart SMI"]').click()

    def list_systems(self):
        self.driver.find_element_by_css_selector('input.button.gray[value="List Systems"]').click()

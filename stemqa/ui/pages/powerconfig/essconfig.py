import logging
import time

from selenium.webdriver.common.by import By
from waiting import wait

from stemqa.ui.component import Component
from stemqa.ui.pages.page import Page

SEARCH_INPUT = (By.CSS_SELECTOR, 'input.form-control')

logger = logging.getLogger(__name__)


class EssConfigPage(Page):
    """
    Represents the Ess Config Page
    """

    URL_SUFFIX_TEMPLATE = '/powerconfig/ess_config/?locationCode=%s'

    def wait_until_loaded(self):
        self.wait_for_element((By.CSS_SELECTOR, 'div.ess-list--list-container div.ess-card-loading--container'))
        self.wait_for_element((By.CSS_SELECTOR, 'div.ess-chart__chart-area div.range-selector'))
        self.wait_for_element((By.CSS_SELECTOR, 'div.ess-shortcut--shortcut-container div.dot-indicator--dot'))

    def get_loaded_ess_card_count(self):
        """
        :return: the count of all the loaded ESS cards
        """
        els = self.driver.find_elements_by_css_selector('%s div.ess-card--content' % EssCard.ESS_CARD)
        return len(els)

    def get_loading_ess_card_count(self):
        """
        :return: the count of all the ESS cards not yet loaded
        """
        els = self.driver.find_elements_by_css_selector(EssCard.ESS_CARD_LOADING)
        return len(els)

    def get_ess_card_count(self):
        """
        :return: the count of all ESS cards
        """
        return self.get_loaded_ess_card_count() + self.get_loading_ess_card_count()

    def get_loaded_ess_cards(self):
        """
        Gets the loaded ESS cards
        :return:
        """
        elems = self.wait_for_elements((By.CSS_SELECTOR, EssCard.ESS_CARD))
        result = {}
        for el in elems:
            result[el.find_element_by_css_selector('span.ess-card--header-label').text] \
                = EssCard(self.driver,
                          top_element_locator=(By.CSS_SELECTOR, 'div.%s' % el.get_attribute('class').replace(' ', '.')))
        return result

    def get_loaded_ess_phase_sections(self):
        """
        Gets the loaded Phase Sections
        :return:
        """
        elems = self.driver.find_elements_by_css_selector('div.ess-list--phase-section')
        result = {}
        for el in elems:
            result[el.find_element_by_css_selector('span.ess-list--phase-title').text] \
                = EssPhaseSection(self.driver, top_element=el)
        return result

    def get_ess_card(self, ess_id):
        """
        Gets a loaded ESS Card
        :param ess_id:
        :return:
        """
        return EssCard(self.driver, top_element_locator=(By.CSS_SELECTOR, 'div.ess-card__%s' % ess_id))

    def is_ess_card_valid(self, ess_id):
        self.wait_for_element((By.XPATH, '//span[contains(text(), "%s") '
                                         'and contains(@class, "ess-card--header-label")]' % ess_id), 5)
        self.wait_for_element((By.XPATH, '//div[contains(text(), "--") '
                                         'and contains(@class, "ess-card--header-label")]'), 5)
        return len(self.driver.find_elements_by_css_selector('div.ess-card--content')) > 0

    def is_details_toggle_on(self):
        """
        :return: true, if the battery details toggle is enabled, false otherwise.
        """
        attr = self.driver.find_element_by_css_selector('label[for="show_details"] div.stem-toggle__box') \
            .get_attribute('class')
        return 'stem-toggle__box--checked' in attr

    def open_details(self):
        """
        Clicks the checkbox for displaying ess details, if they details aren't already visible
        :return:
        """
        logger.info('Opening Details...')
        if self.is_details_toggle_on():
            logger.info('Details are already visible...')
            return

        self.wait_for_element((By.CSS_SELECTOR, 'label[for="show_details"] div.stem-toggle__box')).click()

        ess_cards = list(self.get_loaded_ess_cards().values())
        [wait(lambda: ess_card.is_details_visible(), timeout_seconds=10) for ess_card in ess_cards]

    def is_log_messages_toggle_on(self):
        """
        Checks if the checkbox for log messages is selected or not
        :param self:
        :return:
        """
        attr = self.driver.find_element_by_css_selector('label[for="log_messages"] div.stem-toggle__box') \
            .get_attribute('class')
        return 'stem-toggle__box--checked' in attr

    def toggle_log_messages_on(self):
        """
        Toggles on the log messages checkbox
        :param self:
        :return:
        """
        logger.info('Toggle on log messages...')
        if self.is_log_messages_toggle_on():
            logger.info('Messages are already being logged...')
            return

        selector = 'label[for="log_messages"] div.stem-toggle__box'
        self.wait_for_element((By.CSS_SELECTOR, selector)).click()
        self.wait_for_element((By.CSS_SELECTOR, '%s.stem-toggle__box--checked' % selector))

    def close_details(self):
        """
        If ess details are visible, clicks checkbox to hide the details
        :param self:
        :return:
        """
        logger.info('Closing Details...')
        if not self.is_details_toggle_on():
            logger.info('Details are already hidden...')
            return

        self.driver.find_element_by_css_selector('label[for="show_details"] div.stem-toggle__box').click()
        self.wait_for_element((By.XPATH, '//div[text() = "CONFIGURATION DETAILS" '
                                         'and contains(@class, "ess-card-row--label")]'), 1)

    def get_last_message_timestamp(self):
        """
        Gets the 'Last Message' timestamp
        :param self:
        :return:
        """
        return self.driver.find_element_by_css_selector('div.site-summary__msg-info span:nth-of-type(2)') \
            .get_attribute('textContent')

    def get_current_power(self):
        """
        Gets the 'CURRENT POWER' stats bar value
        :param self:
        :return:
        """
        el = self.driver.find_element_by_xpath('//div[text() = "CURRENT POWER" '
                                               'and contains(@class, "stats-bar-item__item-name")]')
        el = el.find_element_by_xpath('..')
        return el.find_element_by_css_selector('.stats-bar-item__item-number').text


class EssCard(Component):
    ESS_CARD = 'div.ess-card'
    ESS_CARD_LOADING = 'div.ess-card-loading'

    def get_value(self, item_label):
        """
        Gets the values from the ESS card for the specified property. Note that this doesn't
        work for the alarms, warnings and inverter status messages.
        :param self:
        :param item_label: property name as it appears in the UI.
        :return:
        """
        el = self.top_element.find_element_by_xpath('.//div[text() = "%s"]/..' % item_label)
        return el.find_element_by_css_selector('div.ess-card-item--value').text

    def get_inverter_enabled(self):
        """
        Gets the header element from the ESS card.
        :param self:
        :return:
        """
        el = self.top_element.find_element_by_css_selector('div.dot-indicator--dot')
        rendered_class_name = el.get_attribute('class')
        return 'dot-indicator--dot__success' in rendered_class_name

    def get_status_messages(self, status_message_header, is_error):
        """
        Gets the different types of status messages
        :param self:
        :param status_message_header: matches the header of the value(s) to test against
        :param is_error: if true, returns error messages (marked as red)
        :return: array of strings. Empty array if no messages are found
        """
        css_selector = ':not(.ess-card--status-message__danger)'
        if is_error:
            css_selector = '.ess-card--status-message__danger'

        el = self.top_element.find_element_by_xpath('.//div[contains(text(),"%s")]' % status_message_header)
        el = el.find_element_by_xpath('..')
        return [el.text for el in el.find_elements_by_css_selector(
            'div.ess-card-item--value div.ess-card--status-message%s' % css_selector)]

    def get_table_details(self, row_id, item_label, table_class):
        """
        Gets  details from various tables (from a given row)
        :param self:
        :param row_id: id of row (typically shown in first column)
        :param item_label: column name for the property
        :param table_class: table-specific substring for css selector
        :return: string representing the value
        """
        headers = self.top_element.find_elements_by_css_selector('table.%s-table thead th' % table_class)
        column_idx = [header.text for header in headers].index(item_label)
        if column_idx > -1:
            column_idx += 1

        return self.top_element.find_element_by_css_selector(
            'tr.%s-table--table-row__%s td:nth-of-type(%s)' % (table_class, row_id, column_idx)).text

    def get_sidebar_item_value(self, item_label, sidebar_class):
        """
        Gets property values displayed in sidebar after a row has been selected.
        :param self:
        :param item_label: property name
        :param sidebar_class: sidebar-specific substring for css selector
        :return: the desired item value element, if found
        """
        # Determine location of item value
        header_el = self.top_element \
            .find_element_by_xpath('.//div[contains(@class, "%s-sidebar--item-label") and text() = "%s"]'
                                   % (sidebar_class, item_label))
        row_el = header_el.find_element_by_xpath('..')
        item_label_els = row_el.find_elements_by_css_selector('div.%s-sidebar--item-label' % sidebar_class)
        item_index_in_row = item_label_els.index(header_el)

        value_el = row_el.find_elements_by_css_selector(
            'div.%s-sidebar--item-value' % sidebar_class)[item_index_in_row]
        if not value_el:
            raise RuntimeError('Unable to find item labelled %s' % item_label)
        return value_el

    def get_sidebar_details(self, item_label, sidebar_class, is_error):
        """
        Gets property values displayed in sidebar after a row has been selected.
        :param self:
        :param item_label: property name
        :param sidebar_class: sidebar-specific substring for css selector
        :param is_error: if true, returns error values (marked as red)
        :return: property value
        """
        error_class = '%s-sidebar--item-value__error' % sidebar_class
        value_el = self.get_sidebar_item_value(item_label, sidebar_class)

        # Determine if element is valid
        error_el_found = value_el.get_attribute('class').find(error_class) > -1
        if is_error != error_el_found:
            raise RuntimeError('Item labelled %s was found, but %s error class'
                               % (item_label, 'did not have' if is_error else 'have'))

        return value_el.text

    def get_sidebar_messages(self, item_label, sidebar_class, is_error):
        """
        Gets list of message values displayed in sidebar after a row has been selected.
        :param self:
        :param item_label: property name
        :param sidebar_class: sidebar-specific substring for css selector
        :param is_error: if true, returns error values (marked as red)
        :return: array of strings. Empty array if no messages are found
        """
        css_selector = ':not(.%s-sidebar--item-message__error)' % sidebar_class
        if is_error:
            css_selector = '.%s-sidebar--item-message__error' % sidebar_class
        value_el = self.get_sidebar_item_value(item_label, sidebar_class)

        message_els = value_el.find_elements_by_css_selector(
            '.%s-sidebar--item-message%s' % (sidebar_class, css_selector))
        return [el.text for el in message_els]

    def get_sidebar_error_messages(self, error_msg_header, sidebar_class):
        """
        Gets list of message values displayed in sidebar after a row has been selected.
        :param self:
        :param error_msg_header: property name
        :param sidebar_class: sidebar-specific substring for css selector
        :return: array of strings. Empty array if no messages are found
        """
        header_el = self.top_element.find_element_by_css_selector(
            'div.%s-sidebar--error-message-label=%s' % (sidebar_class, error_msg_header))
        message_els = header_el.find_element_by_xpath('..').find_elements_by_css_selector(
            'div.%s-sidebar--error-message-value' % sidebar_class)
        return [el.text for el in message_els]

    def sidebar_has_error_messages(self, sidebar_class):
        """
        Check if a given sidebar has error messages
        :param self:
        :param sidebar_class: sidebar-specific substring for css selector
        :return: whether error messages were found or not
        """
        error_message_els = self.top_element.find_elements_by_css_selector(
            'div.%s-sidebar--error-message' % sidebar_class)
        return len(error_message_els) > 0

    def is_details_visible(self):
        """
        true if the configuration details section is visible, false otherwise
        :param self:
        :return:
        """
        return len(self.top_element.find_elements_by_xpath('.//div[text() = "CONFIGURATION DETAILS" '
                                                           'and contains(@class, "ess-card-row--label")]')) > 0

    def is_callout_visible(self, table_row_id):
        """
        :param self:
        :param table_row_id: table row id
        :return: true, if a callout is visible for this row, false otherwise.
        """
        el = self.top_element.find_element_by_xpath('.//span[contains(text(),"%s")]' % table_row_id) \
            .find_element_by_xpath('..')
        return len(el.find_elements_by_css_selector('div.stem-callout')) > 0

    def is_sidebar_visible(self, sidebar_class):
        """
        :param self:
        :param sidebar_class:
        :return: true, if the sidebar is visible, false otherwise
        """
        sidebar_root_class = self.top_element.find_element_by_css_selector('div.%s-sidebar' % sidebar_class) \
            .find_element_by_xpath('..').get_attribute('class')
        return 'stem-sidebar__open' in sidebar_root_class

    def open_sidebar(self, sidebar_class, row_id):
        """
        Opens the sidebar, if not already open. Also, pauses for 225ms to wait for the sidebar to
        transition into its final state
        :param self:
        :param sidebar_class: sidebar-specific substring for css selector
        :param row_id: rack id
        :return:
        """
        if self.is_sidebar_visible(sidebar_class) \
                and self.driver.find_element_by_css_selector('div.lithium-ion-string-sidebar--header') \
                .text.startswith(row_id):
            return

        self.top_element.find_element_by_css_selector(
            'tr.%s-table--table-row__%s td:nth-of-type(1) span' % (sidebar_class, row_id)).click()
        time.sleep(0.225)
        self.wait_for_element((By.CSS_SELECTOR, 'div.stem-sidebar__open'))

    def close_sidebar(self, sidebar_class, row_id):
        """
        Closes the sidebar, if it was open.
        """
        if self.is_sidebar_visible(sidebar_class) \
                and self.driver.find_element_by_css_selector('div.lithium-ion-string-sidebar--header') \
                .text.startswith(row_id):
            return

        self.top_element.find_element_by_css_selector('tr.%s-table--table-row__%s td:nth-of-type(1) span'
                                                      % (sidebar_class, row_id)).click()
        wait(lambda: not self.is_sidebar_visible(sidebar_class), 1)


class EssPhaseSection(Component):

    def get_phase_voltage(self):
        """
        Gets the phase voltage
        :return:
        """
        return self.top_element.find_element_by_css_selector(
            'span.ess-list--phase-info-value.ess-list--phase-voltage-value').text.strip()

    def get_blade_count(self):
        """
        Gets the phase blade count
        :return:
        """
        return self.top_element.find_element_by_css_selector(
            'span.ess-list--phase-info-value.ess-list--phase-count-value').text.strip()

    def get_blade_labels(self):
        """
        Gets an array of labels for all the blades in a phase
        :param self:
        :return:
        """
        els = self.top_element.find_elements_by_css_selector('span.ess-card--header-label')
        return [el.text.strip() for el in els]

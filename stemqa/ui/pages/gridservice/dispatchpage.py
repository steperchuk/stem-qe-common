import logging

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.select import Select

from stemqa.ui.component import Component
from stemqa.ui.modal import Modal
from stemqa.ui.pages.page import Page

logger = logging.getLogger(__name__)


class GridServiceDispatchPage(Page):
    """
    Represents the Grid Service Dispatch Scheduler UI page
    """

    URL_SUFFIX = '/gridresponse/scheduler/'

    PROGRAM_DROPDOWN = (By.CSS_SELECTOR, 'div.program-selection-toolbar__container select')
    PROGRAM_SELECT_MSG = (By.CSS_SELECTOR, 'div.well')
    DISPATCH_NOW = (By.CSS_SELECTOR, 'div.stem-button--secondary:not(.loading)')
    DISPATCH_NOW_DISABLED = (By.CSS_SELECTOR, 'div.stem-button--secondary.disabled:not(.loading)')
    SCHEDULE_EVENT = (By.CSS_SELECTOR, 'div.stem-button--primary:not(.loading)')
    SCHEDULE_EVENT_DISABLED = (By.CSS_SELECTOR, 'div.stem-button--primary.disabled:not(.loading)')

    ERROR_NO_DISPATCH_ALLOWED = (By.CSS_SELECTOR, 'div.row > div.col-xs-10 > span')
    ERROR_TEXT = "Unable to create new event:ValueError('Neither DISPATCH_NOW nor FUTURE_DISPATCH " \
                 "found in attribute program. Web dispatch is not allowed.',)"

    def __init__(self, driver):
        self.dropdown = None
        super(GridServiceDispatchPage, self).__init__(driver)

    def wait_until_loaded(self):
        self.dropdown = Select(self.wait_for_element(self.PROGRAM_DROPDOWN, 10))
        self.wait_for_element(self.PROGRAM_SELECT_MSG, 10)

    def get_programs(self):
        """
        Returns gs programs in page
        :param self:
        :return: array of strings. Empty array if no messages are found
        """
        return self.dropdown.options

    def select_program(self, selection):
        """
        Selects specified program and waits for all 'loading' spinners to be resolved
        :param self:
        :param selection: selection program name
        :return:
        """
        self.dropdown.select_by_visible_text(selection)
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, 'div.stem-button--secondary img.stem-button__spinner')), 15)
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, 'div.locations-section__map-stats img.loading-spinner.rotation-spinner')), 15)
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, 'div.fleet-stats__data--box div img.loading-spinner.rotation-spinner')), 15)

    def open_schedule_event_panel(self):
        """
        Clicks on 'Schedule Event' button and waits for panel to load
        :return:
        """
        self.wait_for_element(self.SCHEDULE_EVENT, duration=20).click()
        return ScheduleEventPanel(self.driver)

    def open_dispatch_now_panel(self):
        """
        Clicks on 'Dispatch Now' button and waits for panel to load
        :return:
        """
        self.wait_for_element(self.DISPATCH_NOW, duration=20).click()
        return DispatchNowPanel(self.driver)

    def is_dispatch_now_disabled(self):
        """
        Checks if 'Dispatch Now' button enabled
        :return:
        """
        attribute = self.wait_for_element(self.DISPATCH_NOW).get_attribute('class')
        return 'disabled' in attribute

    def is_schedule_event_disabled(self):
        """
        Checks if 'Schedule Event' button enabled
        :return:
        """
        attribute = self.wait_for_element(self.SCHEDULE_EVENT).get_attribute('class')
        return 'disabled' in attribute

    def get_tooltip(self, selection):
        """
        Hovers over selected button and returns tooltip
        :param self:
        :param selection: button label
        :return: tooltip msg
        """
        button_locator = None
        if selection == 'Dispatch Now':
            button_locator = self.DISPATCH_NOW_DISABLED
        elif selection == 'Schedule Event':
            button_locator = self.SCHEDULE_EVENT_DISABLED

        assert button_locator, 'Invalid selection: %s' % selection

        button = self.wait_for_element(button_locator)
        ActionChains(self.driver).move_to_element(button).perform()
        self.wait_for_element((By.CSS_SELECTOR, '.intercom-app'), 10)
        text = self.wait_for_element((By.CSS_SELECTOR, 'div.stem-popover__content-inner')).text
        self.driver.find_element_by_css_selector('h2.page-header').click()
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, 'div.stem-popover__content-inner')), 10)
        return text

    def get_scheduled_events(self):
        """
        Returns schedules events on page
        :return:
        """
        scheduled_events_el = self.wait_for_element((By.CSS_SELECTOR, '.scheduler-page__content'))
        return scheduled_events_el.find_elements_by_css_selector(
            '.events-section .event-card')

    def wait_for_events(self, els_len):
        """
        Waits for specified number of events to be present
        :param self:
        :param els_len: number of events expected
        :return:
        """
        # wait for spinners to go away in fleet stats container on create events page refresh
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, '.loading-spinner-container')), 15)
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, '.loading-spinner')), 15)
        self.wait_for_element(
            (By.CSS_SELECTOR, 'div.scheduler-page__content .events-section'
                              ' div.event-card:nth-of-type(%s)' % els_len))

    def wait_for_error(self, locator):
        elem = self.wait_for_element(locator, 30)
        return elem.text


class ConfirmationModal(Modal):
    SUBMIT = (By.CSS_SELECTOR, '.stem-modal.stem-modal__footer .stem-button.stem-button--secondary.stem-text-heavy')
    CANCEL = (By.CSS_SELECTOR, '.stem-modal.stem-modal__footer .stem-button.stem-button--tertiary.stem-text-heavy')
    FIRST_EVENT = (By.CSS_SELECTOR, '.events-section .event-card:first-of-type')
    NEW_EVENT = (By.CSS_SELECTOR, 'div.app-content-grow div.row div.col-xs-10')

    def wait_until_loaded(self):
        """
        Waits for some elements to be present and visible in the browser.
        :param self:
        :return:
        """
        super(ConfirmationModal, self).wait_until_loaded()
        self.wait_for_element(self.CANCEL)
        self.wait_for_element(self.SUBMIT)

    def submit(self, expect_error=False):
        """
        Clicks submit button and waits for modal to disappear and the event to be processed into 'New' state
        :param self:
        :return:
        """
        self.wait_for_element(self.SUBMIT).click()
        self.wait_for_close()

        if not expect_error:
            self.wait_for_element(self.NEW_EVENT, 10)
            self.wait_for_element((By.XPATH, '//*[contains(text(), "New event submitted successfully")]'))

            # wait for event processing on main page to finish
            self.wait_for_element(self.FIRST_EVENT, 10)


class ScheduleEventPanel(Component):
    SCHEDULE_EVENT_PANEL = (By.ID, 'scheduleEvent')
    START_DROPDOWN = (By.CSS_SELECTOR, '.event-form__threeColumn div:nth-of-type(1) .stem-select .select-area select')
    END_FIELD = (By.CSS_SELECTOR, '.event-form__threeColumn div:nth-of-type(3)')
    DURATION_DROPDOWN = (By.CSS_SELECTOR,
                         '.event-form__paddedMiddle div:nth-of-type(6) .stem-select .select-area select')
    CANCEL = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Cancel"]')

    def __init__(self, driver):
        super(ScheduleEventPanel, self).__init__(driver, top_element_locator=self.SCHEDULE_EVENT_PANEL)

    def wait_until_loaded(self):
        self.wait_for_element((By.CSS_SELECTOR, '.event-form__buttons .stem-button--primary'))
        self.wait_for_element((By.CSS_SELECTOR, '.event-form__buttons .stem-button--tertiary'))

    def get_available_selections(self):
        """
        Returns available selections for event scheduling
        :param self:
        :return: array of strings. Empty array if no messages are found
        """
        return [el.text for el in self.driver.find_elements_by_css_selector('div.stem-radio.stem-radio__container')]

    def has_selection(self, selection):
        """
        Checks whether specified selection is available
        :param selection: selection to check
        """
        return any([True for txt in self.get_available_selections() if selection in txt])

    def get_start_time_dropdown(self):
        """
        Returns starting time dropdown
        :param self:
        :return:
        """
        return Select(self.wait_for_element(self.START_DROPDOWN, 40))

    def get_end_field(self):
        """
        Returns ending time dropdown
        :param self:
        :return:
        """
        return Select(self.wait_for_element(self.END_FIELD))

    def get_duration_dropdown(self):
        """
        Returns duration dropdown
        :param self:
        :return:
        """
        return Select(self.wait_for_element(self.DURATION_DROPDOWN, 60))

    def get_start_time_options(self):
        """
        Returns available starting times
        :param self:
        :return: Array of strings. Empty array if no messages are found
        """
        return [option.text for option in self.get_start_time_dropdown().options]

    def is_schedule_event_enabled(self):
        """
        Checks whether 'Schedule Event' button is enabled
        :param self:
        :return:
        """
        attribute = self.wait_for_element(
            (By.CSS_SELECTOR, '.event-form__buttons .stem-button--primary')).get_attribute('class')
        return 'disabled' not in attribute

    def schedule_event(self, day, duration, start, expect_error=False):
        """
        Schedules event- sets day, sets duration and then sets the start time
        :param self:
        :param day:
        :param duration:
        :param start:
        :return:
        """
        # configure event
        self.set_day_for_event(day)
        self.get_duration_dropdown().select_by_visible_text(duration)
        self.get_start_time_dropdown().select_by_visible_text(start)

        self.click_schedule_event_button(expect_error)

        return self

    def click_schedule_event_button(self, expect_error=False):
        """
        Clicks 'Schedule Event' button and waits for panel to disappear and 'loading' spinners to disappear
        :param self:
        :return:
        """
        self.wait_for_element((By.CSS_SELECTOR, '.event-form__buttons .stem-button--primary')).click()
        self.wait_for(lambda: ec.invisibility_of_element_located(self.SCHEDULE_EVENT_PANEL))

        # wait for the confirmation modal to appear and then click submit
        modal = ConfirmationModal(self.driver)
        modal.submit(expect_error)

    def click_cancel_button(self):
        """
        Clicks 'Cancel' button and waits for panel to disappear and 'loading' spinners to disappear
        :param self:
        :return:
        """
        self.wait_for_element((By.CSS_SELECTOR, '.event-form__buttons .stem-button--tertiary')).click()
        self.wait_for(lambda: ec.invisibility_of_element_located(self.SCHEDULE_EVENT_PANEL))
        return GridServiceDispatchPage(self.driver)

    def set_day_for_event(self, day):
        """
        Sets day for event
        :param self:
        :param day:
        :return:
        """
        day = day.lower()
        self.wait_for_element((By.CSS_SELECTOR, 'label[for="%s"]' % day)).click()

    def has_duration_warning(self):
        """
        Checks whether duration warning exists
        :param self:
        :return:
        """
        msg = "Duration: Unable to request duration at this time. " \
              "Available duration is based on currently available energy and scheduling constraints."
        duration_warning = './/div[contains(@class, "event-form__unable") and text() = "%s"]' % msg
        self.wait_for(lambda: ec.visibility_of_element_located((By.XPATH, duration_warning)), 20)
        return len(self.driver.find_elements_by_xpath(duration_warning)) > 0

    def wait_for_capacity_msg(self):
        child = self.wait_for_element((By.CSS_SELECTOR, 'label.event-form__section-title'))
        parent = child.find_element_by_xpath('..').find_element_by_xpath('//p')
        return parent.text


class DispatchNowPanel(Component):
    DISPATCH_NOW_PANEL = (By.ID, 'dispatchNow')
    DURATION_SELECT = (By.ID, 'duration-select')
    CANCEL = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Cancel"]')
    DISPATCH_NOW = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Dispatch Now"]')
    CAPACITY_MESSAGE = (By.XPATH,
                        '//*[contains(text(), "Capacity will be controlled automatically throughout the event")]')
    DURATION_DROPDOWN = (By.CSS_SELECTOR,
                         'form .event-form__paddedMiddle .stem-select .select-area select')

    def __init__(self, driver):
        super(DispatchNowPanel, self).__init__(driver, top_element_locator=self.DISPATCH_NOW_PANEL)

    def wait_until_loaded(self):
        self.wait_for_element(self.DURATION_SELECT)
        self.wait_for_element(self.CANCEL)
        self.wait_for_element(self.DISPATCH_NOW)

    def click_cancel_button(self):
        """
        Clicks 'Cancel' button and waits for panel to disappear and 'loading' spinners to disappear
        :param self:
        :return:
        """
        self.wait_for_element((By.CSS_SELECTOR, '.event-form__buttons .stem-button--tertiary')).click()
        self.wait_for(lambda: ec.invisibility_of_element_located(self.DISPATCH_NOW_PANEL))
        return GridServiceDispatchPage(self.driver)

    def wait_for_capacity_msg(self):
        child = self.wait_for_element((By.CSS_SELECTOR, 'label.event-form__section-title'))
        parent = child.find_element_by_xpath('..').find_element_by_xpath('//p')
        return parent.text

    def get_duration_dropdown(self):
        """
        Returns duration dropdown
        :param self:
        :return:
        """
        return Select(self.wait_for_element(self.DURATION_DROPDOWN, 40))

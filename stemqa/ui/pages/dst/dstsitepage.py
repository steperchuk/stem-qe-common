import logging

from selenium.webdriver.common.by import By

from stemqa.ui.pages.page import Page

logger = logging.getLogger(__name__)


class DstSitePage(Page):
    """
    Represents the DST site list page
    """

    def wait_until_loaded(self):
        self.wait_for_element((By.ID, 'address'))
        self.wait_for_element((By.CSS_SELECTOR, 'div.file-drop'))

    def get_name(self):
        """
        Gets the name of the site
        :return:
        """
        return self.wait_for_element((By.CSS_SELECTOR, 'div#address h3')).text

    def get_address(self):
        return self.wait_for_element((By.CSS_SELECTOR, 'div#address div')).text

import logging

from selenium.webdriver.common.by import By
from waiting import wait

from stemqa.ui.pages.dst.dstsitepage import DstSitePage
from stemqa.ui.pages.page import Page

SEARCH_INPUT = (By.CSS_SELECTOR, 'input.form-control')

logger = logging.getLogger(__name__)


class DstSearchPage(Page):
    """
    Represents the DST search page
    """

    URL_SUFFIX = '/powersales/analytics/dashboard'

    def wait_until_loaded(self):
        self.wait_for_element(SEARCH_INPUT)

    def search(self, search_str):
        """
        Searches for the specified string and waits for the search to complete
        :param search_str: search string
        :return:
        """
        self.find_element(*SEARCH_INPUT).send_keys(search_str)

        def cond():
            return len(self.driver.find_elements_by_css_selector('a.location-card')) > 0 \
                or len(self.driver.find_elements_by_xpath("//div[contains(text(), "
                                                          "'No clients could be found in this search')]")) > 0

        wait(cond, timeout_seconds=30)
        return self

    def get_clients_count(self):
        """
        Gets the count of clients returned for a search
        :return:
        """
        res = self.driver.find_elements_by_css_selector('a.location-card')
        return len(res)

    def get_clients_list(self):
        """
        Gets the list of clients returned for a search
        :param self:
        :return: array of client names that match the search string
        """
        res = self.driver.find_elements_by_css_selector('a.location-card')
        results = []
        for el in res:
            results.append(el.find_element_by_css_selector('h4').text)
        return results

    def select_client(self, client_name):
        """
        Clicks a tile representing a client and waits for sites under the client to become visible
        NOTE: doesn't handle waiting for sites when a different client was already selected.
        :param client_name:
        :return:
        """
        self.driver.find_element_by_xpath('//h4[contains(text(), "%s")]' % client_name) \
            .find_element_by_xpath('../..').click()
        self.wait_for_elements((By.CSS_SELECTOR, 'a.location-card header div'))
        return self

    def select_site(self, site_name):
        """
        Selects a site by clicking on the site tile and waits for the new page to load.
        :param site_name:
        :return:
        """
        self.wait_for_element((By.XPATH, '//h4[contains(text(), "%s")]' % site_name)).click()
        return DstSitePage(self.driver)

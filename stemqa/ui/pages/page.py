import logging

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

logger = logging.getLogger(__name__)
DEFAULT_TIMEOUT = 10


class Page(object):
    def __init__(self, driver):
        self.driver = driver
        self.wait_until_loaded()

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def get_title(self):
        return self.driver.title

    def get_url(self):
        return self.driver.current_url

    def hover(self, *locator):
        element = self.find_element(*locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    def wait_until_loaded(self):
        assert self  # making pylint happy
        logger.warn("Base page's wait_until_loaded invoked. Should implement this method in the extending class")

    def wait_for_element(self, locator, duration=DEFAULT_TIMEOUT):
        """
        Waits for an element to be present on the component
        :param locator:
        :param duration:
        :return:
        """
        return WebDriverWait(self.driver, duration).until(
            ec.presence_of_element_located(locator)
        )

    def wait_for_elements(self, locator, duration=DEFAULT_TIMEOUT):
        """
        Waits for all elements identified by the locator to be present on the page
        :param locator:
        :param duration:
        :return:
        """
        return WebDriverWait(self.driver, duration).until(
            ec.presence_of_all_elements_located(locator)
        )

    def wait_for(self, condition, duration=DEFAULT_TIMEOUT):
        """
        Waits for the specified function to return a value x such that bool(x) resolves to True
        :param condition:
        :param duration:
        :return:
        """
        return WebDriverWait(self.driver, duration).until(
            condition()
        )

    def scroll_to_copyright(self):
        """
        Scrolls to bottom of page
        :return:
        """
        copyright_elem = self.find_element(By.CSS_SELECTOR, '.copyright__copy-col')
        actions = ActionChains(self.driver)
        actions.move_to_element(copyright_elem).perform()
        return self

    def scroll_to_text(self, text):
        """
        Scrolls to text in page
        :param text:
        :return:
        """
        elem = self.wait_for_element((By.XPATH, "//*[contains(text(),'%s')]" % text), 10)
        actions = ActionChains(self.driver)
        actions.move_to_element(elem).perform()
        return self

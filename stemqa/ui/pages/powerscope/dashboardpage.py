import logging

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

from stemqa.config.settings import settings
from stemqa.ui.pages.page import Page
from stemqa.ui.pages.powerscope.costexplorerpage import CostExplorerPage
from stemqa.ui.pages.powerscope.loadexplorerpage import LoadExplorerPage
from stemqa.ui.sidepanel import SidePanel

WEB_URL = settings['stem-web-app']['url']

logger = logging.getLogger(__name__)

DEFAULT_WAIT_TIMEOUT = 120


class DashboardPage(Page):
    """
    Represents the Dashboard page
    """

    URL_SUFFIX = '/powerscope/dashboard/'

    # billing summary section items
    COST_IN_BILL_PERIOD = 'Cost in billing period'
    ENERGY_USAGE_IN_PERIOD = 'Energy usage'
    MAX_PEAK_IN_PERIOD = 'Maximum peak'

    def wait_until_loaded(self):
        """
        wait until page loads
        :return:
        """
        self.wait_for_element((By.XPATH, '//*[contains(text(), "PowerScope")]'))

    def get_side_panel(self):
        """
        returns instance of side panel
        :return:
        """
        return SidePanel(self.driver)

    def wait_for_page_refresh(self):
        """
        waits for page to load
        :return:
        """
        # wait for the data load spinners to become invisible
        self.wait_for(lambda:
                      ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-spinner.rotation-spinner')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_all_elements_located((By.CSS_SELECTOR, ".highcharts-container")),
                      duration=DEFAULT_WAIT_TIMEOUT)
        # wait for page elements to be visible
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, '.page-title')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, '.dashboard-page__weather')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, '.dashboard-page__loadchart')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        return self

    def wait_for_bill_summary_section(self):
        """
        Waits for bill summary section at bottom of page
        :return:
        """
        self.wait_for(lambda:
                      ec.visibility_of_all_elements_located((By.CSS_SELECTOR, '.dashboard-page__bill-summary')),
                      duration=60)

    def select_bill_summary_item(self, item_name):
        """
        Select billing period summary item on page
        :param item_name:  one of pages DashboardPage.COST_IN_BILL_PERIOD|ENERGY_USAGE_IN_PERIOD|MAX_PEAK_IN_PERIOD
        :return: selectedPage
        """
        self.wait_for_bill_summary_section()
        self.scroll_to_copyright()
        self.wait_for_element((By.XPATH, "//div[contains(text(),'%s')]" % item_name), 30).click()
        if item_name == self.COST_IN_BILL_PERIOD:
            return CostExplorerPage(self.driver).wait_for_page_refresh()
        if item_name == self.ENERGY_USAGE_IN_PERIOD:
            return LoadExplorerPage(self.driver).wait_for_page_refresh()
        if item_name == self.MAX_PEAK_IN_PERIOD:
            return CostExplorerPage(self.driver).wait_for_page_refresh()
        return self

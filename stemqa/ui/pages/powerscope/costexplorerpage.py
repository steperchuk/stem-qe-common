import logging

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import Select

from stemqa.config.settings import settings
from stemqa.ui.pages.page import Page
from stemqa.ui.sidepanel import SidePanel

WEB_URL = settings['stem-web-app']['url']

logger = logging.getLogger(__name__)

DEFAULT_WAIT_TIMEOUT = 180


class CostExplorerPage(Page):
    """
    Represents the CostExplorer page
    """

    URL_SUFFIX = '/powerscope/costexplorer/'

    def wait_until_loaded(self):
        """
        wait until page loads
        :return:
        """
        self.wait_for_element((By.XPATH, '//*[contains(text(), "PowerScope")]'))

    def get_side_panel(self):
        """
        returns instance of side panel
        :return:
        """
        return SidePanel(self.driver)

    def wait_for_page_refresh(self):
        """
        waits for page to loading spinners to disappear when rendering page
        :return:
        """
        self.wait_for(lambda:
                      ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-spinner.rotation-spinner')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_all_elements_located((By.CSS_SELECTOR, ".highcharts-container")),
                      duration=DEFAULT_WAIT_TIMEOUT)
        return self

    def update_year(self, year):
        """
        Updates dropdown selection to specified year
        :param year:
        :return:
        """
        year_range_selector = self.wait_for_element((By.CSS_SELECTOR, '.year-range-select'))
        year_range_selector.click()

        # https://sqa.stackexchange.com/questions/1355/
        # what-is-the-correct-way-to-select-an-option-using-seleniums-python-webdriver?utm_medium=organic
        # &utm_source=google_rich_qa&utm_campaign=google_rich_qa
        select = Select(self.wait_for_element((By.CSS_SELECTOR, '.year-range-select'), 10))
        logger.debug('Options: %s, selecting %s', [o.text for o in select.options], year)
        select.select_by_visible_text(year)
        self.wait_for_page_refresh()
        return self

    def drilldown_to_savings_period(self, billing_period_locator):
        """
        Clicks on specified element to drilldown to billing period
        :param billing_period_locator:
        :return:
        """
        self.wait_for(lambda: ec.visibility_of_all_elements_located((By.CSS_SELECTOR,
                                                                     billing_period_locator)),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for_element((By.CSS_SELECTOR, billing_period_locator), 10).click()
        self.wait_for_page_refresh()
        return self

    def drilldown_to_bill_period(self, start_date, end_date):
        """
        Navigate to billing period specified by start_date,end_date
        :param start_date:
        :param end_date:
        :return:
        """
        url = 'bill/?selectedStartDate=%s&selectedEndDate=%s' % (start_date, end_date)
        navigate_to = WEB_URL
        navigate_to += self.URL_SUFFIX + url
        logger.debug('Navigating to %s', navigate_to)
        self.driver.get(navigate_to)
        self.wait_for_page_refresh()
        return self

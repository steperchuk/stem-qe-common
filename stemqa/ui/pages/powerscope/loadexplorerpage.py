import logging

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

from stemqa.config.settings import settings
from stemqa.ui.chart import Chart
from stemqa.ui.component import Component
from stemqa.ui.pages.page import Page
from stemqa.ui.sidepanel import SidePanel

WEB_URL = settings['stem-web-app']['url']

logger = logging.getLogger(__name__)

DEFAULT_WAIT_TIMEOUT = 120


class LoadExplorerPage(Page):
    """
    Represents the Load Explorer page
    """

    URL_SUFFIX = '/powerscope/loadexplorer/'

    MAXIMUM_DEMAND_ELEM = (By.XPATH, '//div[contains(text(), "Maximum Demand")]')

    ACTIVATE_BUTTON = '.loading-overlay div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.active'
    DISABLED_BUTTON = '.loading-overlay div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button:not(.active)'

    VIEW_BY_SELECTOR = (By.CSS_SELECTOR, '.loading-overlay .noselect div')

    START_DATE_SELECTOR = (By.ID, 'start_date')
    END_DATE_SELECTOR = (By.ID, 'end_date')

    def show_max_demand(self, show_max_demand=True):
        """
        Clicks the max demand button to show the max demand for the period
        :param show_max_demand:
        :return:
        """
        el = self.wait_for_element(self.MAXIMUM_DEMAND_ELEM)
        el_row = el.find_element_by_xpath('../..')
        button = el_row.find_element_by_css_selector('.btn-show-on-graph.btn.btn-default')
        ActionChains(self.driver).move_to_element(button).perform()
        button.click()

        if show_max_demand:
            button_el = self.ACTIVATE_BUTTON
        else:
            button_el = self.DISABLED_BUTTON
        self.wait_for(lambda:
                      ec.presence_of_element_located((By.CSS_SELECTOR, button_el)),
                      duration=DEFAULT_WAIT_TIMEOUT)
        return self

    def view_by_billing_cycle(self, billing_cycle):
        """
        Views data by selecting specified billing cycle
        :param billing_cycle:
        :return:
        """
        # Find selector for 'View By' and click on it
        self.wait_for(lambda:
                      ec.presence_of_element_located(self.VIEW_BY_SELECTOR),
                      duration=DEFAULT_WAIT_TIMEOUT)
        selector_el = self.wait_for_element(self.VIEW_BY_SELECTOR)
        ActionChains(self.driver).move_to_element(selector_el).perform()
        selector_el.click()

        # Find 'Billing Cycle' option and click on it
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.XPATH, './/div[text() = "Billing Period"]')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        billing_period_el = self.wait_for_element((By.XPATH, './/div[text() = "Billing Period"]'))
        ActionChains(self.driver).move_to_element(billing_period_el).perform()
        billing_period_el.click()

        # wait for billing cycles to be retrieved...
        self.wait_for(lambda:
                      ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-spinner.rotation-spinner')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_all_elements_located((By.CSS_SELECTOR, ".highcharts-container")),
                      duration=DEFAULT_WAIT_TIMEOUT)

        # wait for billing cycle list drop down elem and click on it, wait for presence of all billing cycles
        dropdown_selector = 'div.noselect.col-md-8.col-xs-12 div.noselect'  # div:nth-of-type(2) div
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, dropdown_selector)),
                      duration=DEFAULT_WAIT_TIMEOUT)
        dropdown_selector_el = self.wait_for_element((By.CSS_SELECTOR, dropdown_selector))
        ActionChains(self.driver).move_to_element(dropdown_selector_el).perform()
        dropdown_selector_el.click()
        self.wait_for(lambda:
                      ec.presence_of_all_elements_located((By.CSS_SELECTOR, 'li div')),
                      duration=20)

        # select the billing cycle we want
        billing_cycle_selector = (By.XPATH, '//div[contains(text(), "%s")]' % billing_cycle)
        billing_cycle_el = self.wait_for_element(billing_cycle_selector)
        ActionChains(self.driver).move_to_element(billing_cycle_el).perform()
        billing_cycle_el.click()

        # wait for interval data to refresh...
        self.wait_for(lambda:
                      ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-spinner.rotation-spinner')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_all_elements_located((By.CSS_SELECTOR, ".highcharts-container")),
                      duration=DEFAULT_WAIT_TIMEOUT)
        return self

    def wait_until_loaded(self):
        """
        wait until page loads
        :return:
        """
        self.wait_for_element((By.XPATH, '//*[contains(text(), "PowerScope")]'))

    def get_side_panel(self):
        """
        returns instance of side panel
        :return:
        """
        return SidePanel(self.driver)

    def wait_for_page_refresh(self):
        """
        waits for page to load
        :return:
        """
        # wait for the data load spinners to become invisible
        self.wait_for(lambda:
                      ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-spinner.rotation-spinner')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_all_elements_located((By.CSS_SELECTOR, ".highcharts-container")),
                      duration=DEFAULT_WAIT_TIMEOUT)
        # wait for page elements to be visible
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, '.page-title')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, '.btn-peak-load')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        self.wait_for(lambda:
                      ec.visibility_of_element_located((By.CSS_SELECTOR, '.btn-weather-load')),
                      duration=DEFAULT_WAIT_TIMEOUT)
        return self

    def open_date_picker(self, selector):
        self.wait_for_element((By.CSS_SELECTOR, '.loading-overlay .row'))
        self.wait_for(lambda: ec.visibility_of_element_located(selector), 60)
        date_picker = self.wait_for_element(selector)
        ActionChains(self.driver).move_to_element(date_picker).perform()
        date_picker.click()
        self.wait_for(lambda: ec.visibility_of_element_located((By.CSS_SELECTOR, 'input.picker__input--active')), 60)
        return DatePicker(self.driver)

    def get_chart_component(self):
        """
        returns instance of chart
        """
        return Chart(self.driver)

    def show_full_load_on_chart(self):
        """
        press "Show full load" button
        """
        show_full_load_el = self.wait_for_element((By.CSS_SELECTOR, 'button.btn-peak-load'))
        show_full_load_el.click()

    def show_peak_load_on_chart(self):
        """
        press "Show peak load" button
        """
        show_peak_load_el = self.wait_for_element((By.CSS_SELECTOR, 'button.btn-peak-load.active'))
        show_peak_load_el.click()

    def wait_for_legend(self, num_elem):
        """
        wait for highchart legend
        :param num_elem: num of buttons in legend to wait for
        :return:
        """
        buttons = self.driver.find_elements_by_class_name('chart-legend-button')
        if num_elem != len(buttons):
            logger.debug('Only found %s legend buttons, refreshing page to retry...', len(buttons))
            self.driver.refresh()


class DatePicker(Component):
    """
    Represents the date picker in the load explorer
    """

    DEFAULT_EL_LOCATOR = (By.CSS_SELECTOR, '.picker__box')

    def __init__(self, driver):
        super(DatePicker, self).__init__(driver, None, self.DEFAULT_EL_LOCATOR)
        self.wait_until_loaded()

    def wait_until_loaded(self):
        """
        Wait for basic elements of the date picker to load
        :return:
        """
        super(DatePicker, self).wait_until_loaded()
        self.wait_for_element((By.CSS_SELECTOR, '.picker__holder'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.picker__box'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.picker__header'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.picker__table'), 10)

    def select_date(self, calendar_date):
        """
        Selects date in date picker
        :param calendar_date:
        :return:
        """
        parent = self.driver.find_element_by_css_selector('div.picker--opened table.picker__table')
        date_locator = './/div[contains(@class, "picker__day--infocus") and text() = "%s"]' % calendar_date
        date_elem = parent.find_element_by_xpath(date_locator)
        ActionChains(self.driver).move_to_element(date_elem).perform()
        date_elem.click()
        self.wait_for(lambda: ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.picker--opened')), 60)
        return LoadExplorerPage(self.driver)

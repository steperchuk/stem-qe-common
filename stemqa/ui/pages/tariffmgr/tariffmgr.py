import logging
import time

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.select import Select

from stemqa.ui.component import Component
from stemqa.ui.modal import Modal
from stemqa.ui.pages.page import Page

logger = logging.getLogger(__name__)

# 'Add Tariff' Button
CREATE_NEW_TARIFF = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Add Tariff"]')


class TariffMgrPage(Page):
    """
    Represents the Tariff Manager UI page
    """

    URL_SUFFIX = '/powertariffs/tariff_manager/'
    TARIFF_TABLE = (By.CSS_SELECTOR, 'table.table.table-striped')
    ADD_TARIFF_BUTTON = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Add Tariff"]')
    TARIFF_SEARCH_INPUT = (By.ID, 'searchTariffsTextInput')
    PAGINATION_PARENT = (By.CSS_SELECTOR, 'div.stem-pagination.stem-pagination__navigation')
    RESULTS_LENGTH_SELECTOR = (By.ID, 'results_length_selector')
    TARIFF_FILTER = (By.CSS_SELECTOR, '.connected-data-filters__trigger')

    def __init__(self, driver):
        self.dropdown = None
        super(TariffMgrPage, self).__init__(driver)

    def wait_until_loaded(self):
        self.wait_for(lambda: ec.invisibility_of_element_located((By.CSS_SELECTOR, '.page-loading-status--show')), 60)
        self.wait_for_element((By.XPATH, '//*[contains(text(), "PowerTariffs")]'))
        self.wait_for_element(self.TARIFF_TABLE)
        self.wait_for_element(self.ADD_TARIFF_BUTTON)
        self.wait_for_element(self.TARIFF_SEARCH_INPUT)
        self.wait_for_element(self.PAGINATION_PARENT)
        self.wait_for_element(self.RESULTS_LENGTH_SELECTOR)

    def open_tariff(self, tariff_name):
        """
        Click on a tariff in table on tariff mgr page and go to tariff detail page
        :param tariff_name:
        :return:
        """
        tariff_link_button = self.wait_for_element((By.XPATH, '//*[contains(text(), "%s")]' % tariff_name))
        tariff_link_button.click()
        return TariffDetailsPage(self.driver)

    def open_add_tariff_wizard(self):
        """
        Opens the import tariff wizard and returns it
        :return:
        """
        self.wait_for(lambda: ec.element_to_be_clickable(self.ADD_TARIFF_BUTTON), duration=10)
        add_tariff_button = self.wait_for_element(self.ADD_TARIFF_BUTTON)
        actions = ActionChains(self.driver)
        actions.move_to_element(add_tariff_button).perform()
        add_tariff_button.click()
        self.wait_for_element((By.CSS_SELECTOR, '.stem-modal.stem-modal__content'))
        logger.info('Add Tariff modal is open!')
        return AddTariffWizard(self.driver)

    def open_wizard_second_page(self):
        """
        Opens the import tariff wizard to 'saved' second screen and returns it
        :return:
        """
        self.wait_for(lambda: ec.element_to_be_clickable(self.ADD_TARIFF_BUTTON), duration=10)
        add_tariff_button = self.wait_for_element(self.ADD_TARIFF_BUTTON)
        actions = ActionChains(self.driver)
        actions.move_to_element(add_tariff_button).perform()
        add_tariff_button.click()
        self.wait_for_element((By.CSS_SELECTOR, '.stem-modal.stem-modal__content'))
        logger.info('Add Tariff modal is open!')
        return AddTariffWizard(self.driver)

    def open_filter(self):
        """
        Opens filter for tariff table
        :return:
        """
        el = self.wait_for_element(self.TARIFF_FILTER, 20)
        el.click()
        return FilterPopover(self.driver, top_element_locator=FilterPopover.TOP_ELEM)

    def wait_for_tariff_import(self, tariff_name, timeout=300):
        """
        Refreshes page until timeout
        :return:
        """
        tariff_found = False
        start_time = time.time()
        elapsed_time = 0

        logger.debug("Waiting for tariff '%s' to appear in tariff summary table", tariff_name)
        while elapsed_time < timeout and not tariff_found:
            logger.debug('Sleeping 10sec...')
            time.sleep(10)
            current_time = time.time()
            elapsed_time = current_time - start_time
            logger.debug('Elapsed time is %s secs', elapsed_time)

            # get the list of tariffs and check whether tariff is listed
            logger.info('Checking for tariff in table')
            self.driver.refresh()
            self.wait_until_loaded()
            tariff_list = []
            for row in self.driver.find_elements_by_css_selector("table.table-striped tbody tr.disabled"):
                cell = row.find_elements_by_tag_name("td")[1]
                tariff_list.append(cell.text)
            logger.debug('Tariffs found: %s', tariff_list)
            if tariff_name in tariff_list:
                logger.debug("Tariff '%s' found!", tariff_name)
                tariff_found = True

        return self

    def wait_for_disabled_tariff(self, tariff_name):
        elem = (By.XPATH, './/tr[contains(@class, "disabled")]/td/span[text() = "%s"]' % tariff_name)
        self.wait_for(lambda: ec.visibility_of_element_located(elem), duration=30)
        return self

    def find_utility_by_name(self, tariff_name):
        logger.debug('Finding utility for tariff %s', tariff_name)
        return self._find_tariff_property(tariff_name, 0)

    def find_tariff_id_by_name(self, tariff_name):
        logger.debug('Finding tariff id for tariff %s', tariff_name)
        return self._find_tariff_property(tariff_name, 2)

    def find_legacy_id_by_name(self, tariff_name):
        logger.debug('Finding tariff id for tariff %s', tariff_name)
        return self._find_tariff_property(tariff_name, 3)

    def find_source_by_name(self, tariff_name):
        logger.debug('Finding source for tariff %s', tariff_name)
        return self._find_tariff_property(tariff_name, 4)

    def find_status_by_name(self, tariff_name):
        logger.debug('Finding status for tariff %s', tariff_name)
        return self._find_tariff_property(tariff_name, 5)

    def _find_tariff_property(self, tariff_name, index):
        el = self.wait_for_element((By.XPATH, '//span[contains(text(), "%s")]' % tariff_name))
        el_row = el.find_element_by_xpath('..').find_element_by_xpath('..')
        elems = el_row.find_elements_by_css_selector('.align-left')
        return elems[index].text


class AddTariffWizard(Modal):
    """
    Represents the wizard used for importing genability tariff
    """
    next_step_idx = None

    # wizard steps
    SELECT_UTILITY = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(1).wizard-step--active .select-utility')
    SELECT_TARIFF = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(2).wizard-step--active input#search_tariffs')
    CONFIG_TARIFF = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(3).wizard-step--active .add-details')

    # step 1 fields
    UTILITY_SEARCH_INPUT = (By.ID, 'search_utility_providers')
    UTILITY_SEARCH_RESULTS = (By.CSS_SELECTOR, '.select-utility__tiles')
    # step 2 fields
    TARIFF_NAME_INPUT = (By.ID, 'unmapped_tariff_name')
    TARIFF_RESULTS_TABLE = (By.CSS_SELECTOR, '.wizard-step--active .table-responsive')
    # step 3 fields
    DETAIL_NAME = (By.ID, 'tariff_name')
    DETAIL_NOTES = (By.ID, 'tariff_notes')
    # step 4 fields
    SUCCESS_CHECKMARK = (By.CSS_SELECTOR, '.wizard-step--active .confirmation .checkmark32')

    # FOOTER BUTTONS
    FINAL_SUBMIT_DISABLED = (By.CSS_SELECTOR,
                             '.stem-modal__footer .stem-button.stem-button--primary.disabled.stem-text-heavy')
    FINAL_SUBMIT = (By.CSS_SELECTOR, '.stem-modal__footer .stem-button.stem-button--primary.stem-text-heavy')
    DONE_WIZARD_BUTTON = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Done"]')
    CLOSE_WIZARD_BUTTON = (By.CSS_SELECTOR, '.stem-modal .stem-close-button')

    # OTHER ELEMENTS
    NOTIFY_CONTAINER = (By.CSS_SELECTOR, '.wizard-step--active .wizard-step-body .stem-notification__container--error')

    def wait_until_loaded(self):
        """
        Waits for some elements to be present and visible in the browser.
        :return:
        """
        super(AddTariffWizard, self).wait_until_loaded()

    def open_add_tariff_wizard(self):
        """
        Select the 'Add Tariff' option and wait for the utility search input box
        :return:
        """
        self.wait_until_loaded()
        self.wait_for(lambda: ec.presence_of_element_located(self.SELECT_UTILITY), duration=10)
        self.wait_for(lambda: ec.visibility_of_element_located(self.UTILITY_SEARCH_INPUT), duration=10)
        return self

    def select_utility(self, search_term, utility_name_expected):
        """
        Search and select desired utility for retrieving possible tariffs for import
        :param search_term:
        :param utility_name_expected:
        :return:
        """
        logger.debug('Searching for genability utilities with term: %s', search_term)
        self.wait_for(lambda: ec.visibility_of_element_located(self.SELECT_UTILITY), duration=10)
        el = self.wait_for_element(self.UTILITY_SEARCH_INPUT)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        el.clear()
        el.send_keys(search_term)
        self.wait_for_element(self.UTILITY_SEARCH_RESULTS, 30)

        # look for the expected utility in results and select it's tile
        parent = self.driver.find_element_by_css_selector('.select-utility__tiles')
        children = parent.find_elements_by_css_selector('div.select-utility__tile')
        utility = [a for a in children if a.text == utility_name_expected]
        if not utility:
            raise RuntimeError("Unable to find utility %s", utility_name_expected)
        util_el = utility[0]
        actions = ActionChains(self.driver)
        actions.move_to_element(util_el).perform()
        util_el.click()
        self.wait_for_element(self.SELECT_TARIFF, 30)
        self.next_step_idx = 2
        return self

    def search_for_tariff(self, search_term):
        """
        Search for tariff with search term and wait for results table to be rendered
        :param search_term:
        :return:
        """
        logger.debug('Searching for tariffs with term: %s', search_term)
        self.wait_for(lambda: ec.visibility_of_element_located(self.SELECT_TARIFF), duration=10)
        el = self.wait_for_element(self.SELECT_TARIFF, 10)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        el.clear()
        chars = list(search_term)
        for char in chars:
            el.send_keys(char)
            time.sleep(1)
        self.wait_for(lambda: ec.visibility_of_element_located(self.TARIFF_RESULTS_TABLE), duration=30)
        return self

    def select_tariff(self, tariff):
        """
        Selects desired tariff by name for import
        :param tariff:
        :return:
        """
        logger.debug('Selecting tariff with name: %s', tariff)
        self.wait_for(lambda: ec.visibility_of_element_located(self.TARIFF_RESULTS_TABLE), duration=30)

        self.wait_for_element(self.TARIFF_RESULTS_TABLE, 20)\
            .find_element_by_xpath("//td[contains(text(), '%s')]" % tariff) \
            .find_element_by_xpath('..') \
            .find_element_by_css_selector('td.align-left .stem-radio label div .stem-radio__radio-inner') \
            .click()
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active td.align-left .stem-radio '
                                                'label div .stem-radio__radio-inner--checked'))
        self.next_step_idx = 3
        return self

    def config_tariff(self, powerscope_name, notes):
        """
        Configure name and notes for tariff
        :param powerscope_name
        :param notes
        :return:
        """
        logger.debug('Inputting name and note')
        self.wait_for(lambda: ec.visibility_of_element_located(self.CONFIG_TARIFF), duration=30)
        el = self.wait_for_element(self.DETAIL_NAME, 10)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        el.clear()
        el.send_keys(powerscope_name)
        el = self.wait_for_element(self.DETAIL_NOTES, 10)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        el.clear()
        el.send_keys(notes)
        self.wait_for(lambda:
                      ec.invisibility_of_element_located(self.FINAL_SUBMIT_DISABLED), duration=10)
        self.wait_for(lambda:
                      ec.visibility_of_element_located(self.FINAL_SUBMIT), duration=10)
        self.next_step_idx = 4
        return self

    def submit_import(self):
        """
        Submits configuration for tariff import
        :return:
        """
        logger.debug('Clicking submit button')
        self.wait_for(lambda: ec.visibility_of_element_located(self.FINAL_SUBMIT), duration=10)
        el = self.wait_for_element(self.FINAL_SUBMIT)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        return self

    def wait_for_error_msg(self, msg):
        """
        Waits for error msg
        :return:
        """
        logger.debug('Waiting for error msg')
        self.wait_for(lambda: ec.visibility_of_element_located(self.NOTIFY_CONTAINER), duration=20)
        text_elem = (By.XPATH, './/div[text() = "%s"]' % msg)
        self.wait_for(lambda: ec.visibility_of_element_located(text_elem), duration=20)
        return self

    def wait_for_success(self):
        """
        Waits for success msg
        :return:
        """
        logger.debug('Waiting for SUCCESS checkmark')
        self.wait_for(lambda: ec.visibility_of_element_located(self.SUCCESS_CHECKMARK), duration=20)
        return self

    def close_wizard(self):
        """
        Hits 'X' in upper right corner of Add Tariff wizard
        :return:
        """
        self.wait_for(lambda: ec.visibility_of_element_located(self.CLOSE_WIZARD_BUTTON), duration=10)
        el = self.wait_for_element(self.CLOSE_WIZARD_BUTTON)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        self.wait_for_close()
        return TariffMgrPage(self.driver)

    def submit_tariff_import(self):
        """
        Hits final 'Submit' button 'Add Tariff' wizard
        :return:
        """
        self.wait_for(lambda: ec.visibility_of_element_located(self.DONE_WIZARD_BUTTON), duration=10)
        el = self.wait_for_element(self.DONE_WIZARD_BUTTON)
        actions = ActionChains(self.driver)
        actions.move_to_element(el).perform()
        el.click()
        self.wait_for_close()
        return TariffMgrPage(self.driver)

    def submit(self):
        """
        Clicks next button and waits for modal to update with next section
        :param next_step_idx:
        :return:
        """
        current_step = (By.CSS_SELECTOR, '.wizard-step.wizard-step--active')
        next_step = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(%s)' % self.next_step_idx)

        current_step_text = self.driver.find_element(*current_step).text.split('\n')[0]
        next_step_text = self.driver.find_element(*next_step).text.split('\n')[0]

        logger.info('On step %s, navigating to %s', current_step_text, next_step_text)

        footer = self.wait_for_element((By.CSS_SELECTOR, '.stem-modal__footer'), 10)
        next_el = footer.find_element_by_xpath('.//div[text() = "Next"]')
        next_el.click()

        new_step = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(%s).wizard-step--active' % self.next_step_idx)
        self.wait_for_element(new_step, 30)

        current_step_text = self.driver.find_element(*current_step).text.split('\n')[0]
        logger.info('Moved to step: %s', current_step_text)
        return self


class TariffDetailsPage(Page):
    """
    Represents the Tariff Manager UI page
    """
    URL_SUFFIX = '/powertariffs/tariff_manager/details/'

    ARCHIVE_CONTROL = (By.CSS_SELECTOR, 'div.tariff-operations button.contextual-button')
    TARIFF_DETAILS_TITLE = (By.XPATH, '//*[contains(text(), "Tariff Details")]')
    TARIFF_MGR_BUTTON = (By.XPATH, '//*[contains(text(), "Tariff Manager")]')
    TARIFF_SUMMARY_STATS = (By.CSS_SELECTOR, 'div.tariff-summary')
    REVISION_DROPDOWN = (By.ID, 'select-revision')
    TARIFF_CONTROLS = (By.CSS_SELECTOR, '.popover-content .tariff-operations-controls')
    ARCHIVE_BUTTON = (By.ID, 'tariff-operations-btn-archive')

    def __init__(self, driver):
        self.dropdown = None
        super(TariffDetailsPage, self).__init__(driver)

    def wait_until_loaded(self):
        self.wait_for(lambda:
                      ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-spinner.rotation-spinner')),
                      duration=30)
        self.wait_for_element((By.XPATH, '//*[contains(text(), "PowerTariffs")]'))
        self.wait_for(lambda: ec.visibility_of_all_elements_located((By.CSS_SELECTOR, '.tou_color_area')), duration=30)
        self.wait_for_element(self.ARCHIVE_CONTROL)
        self.wait_for_element(self.TARIFF_DETAILS_TITLE)
        self.wait_for_element(self.TARIFF_MGR_BUTTON)
        self.wait_for_element(self.REVISION_DROPDOWN)
        self.dropdown = Select(self.wait_for_element(self.REVISION_DROPDOWN, 10))

    def open_tariff_mgr(self):
        """
        Return to tariff manager page
        :return:
        """
        self.wait_for_element(self.TARIFF_MGR_BUTTON).click()
        pass
        return TariffMgrPage(self.driver)

    def get_tariff_property(self, tariff_property):
        """
        Returns value for specified tariff property
        :param tariff_property:
        :return:
        """
        tariff_property_locator = (By.XPATH, '//*[contains(text(), "%s")]' % tariff_property)
        tariff_property_el = self.wait_for_element(tariff_property_locator)
        value_el = tariff_property_el.find_element_by_xpath('..').find_element_by_css_selector('.tariff-summary--value')
        logger.debug("Tariff property '%s' value '%s'", tariff_property, value_el.text)
        return value_el.text

    def get_tariff_versions(self):
        """
        Returns list of versions
        :param self:
        :return: array of strings. Empty array if no messages are found
        """
        return [option.text for option in self.dropdown.options]


class FilterPopover(Component):
    """
    Represents filter component for filtering tariffs on tariff manager page
    """

    TOP_ELEM = (By.CSS_SELECTOR, '.connected-data-filters__trigger')
    STATUS_FILTER = (By.XPATH, './/div[contains(@class, "connected-data-filter-form__title") and text() = "Status"]')
    SOURCE_FILTER = (By.XPATH, './/div[contains(@class, "connected-data-filter-form__title") and text() = "Source"]')
    APPLY_BUTTON = (By.XPATH, './/div[contains(@class, "stem-button__child") and text() = "Apply Filters"]')

    ALL_STATUSES = "ALL_STATUSES-"
    PROP_GRADE = "tariff_status-PROPOSAL_GRADE"
    REV_GRADE = "tariff_status-REVENUE_GRADE"
    SIM_GRADE = "tariff_status-SIMULATION_GRADE"

    ALL_SOURCES = "ALL_SOURCES-"
    GENABILITY = "vendor_code-GEN"
    LEGACY = "vendor_code-STEM_DB"
    MANUAL = "vendor_code-STEM_S3"

    def wait_until_loaded(self):
        self.wait_for_element(self.STATUS_FILTER)
        self.wait_for_element(self.SOURCE_FILTER)
        self.wait_for(lambda: ec.element_to_be_clickable(self.APPLY_BUTTON), duration=10)

    def close_filter(self):
        """
        Closes filter selection component
        :return:
        """
        self.wait_for_element(self.APPLY_BUTTON).click()
        self.wait_for(lambda: ec.presence_of_element_located((By.ID, 'tariff-filter-popover')))
        return TariffMgrPage(self.driver)

    def config_filter_status(self, status_list):
        """
        Configures statuses on which to filter
        :param status_list:
        :return:
        """
        if status_list:
            self._uncheck_all_status(self.ALL_STATUSES)
            for status in status_list:
                self.wait_for_element((By.ID, "%s" % status), 20) \
                    .find_element_by_xpath('..') \
                    .find_element_by_css_selector('label.stem-checkbox__label div.stem-checkbox__box div.checkmark') \
                    .click()
                el = (By.XPATH,
                      './/label[contains(@class, "stem-checkbox__label--checked") and contains(@for, "%s")]' % status)
                self.wait_for(lambda: ec.visibility_of_element_located(el), duration=10)
        return self

    def config_filter_source(self, source_list):
        """
        Configures sources on which to filter
        :param status_list:
        :return:
        """
        if source_list:
            self._uncheck_all_status(self.ALL_SOURCES)
            for source in source_list:
                self.wait_for_element((By.ID, "%s" % source), 20) \
                    .find_element_by_xpath('..') \
                    .find_element_by_css_selector('label.stem-checkbox__label div.stem-checkbox__box div.checkmark') \
                    .click()
                el = (By.XPATH,
                      './/label[contains(@class, "stem-checkbox__label--checked") and contains(@for, "%s")]' % source)
                self.wait_for(lambda: ec.visibility_of_element_located(el), duration=10)
        return self

    def _uncheck_all_status(self, filter):
        """
        Unchecks any filters and sets it to show all tariffs
        :return:
        """
        self.wait_for_element((By.ID, "%s" % filter), 20) \
            .find_element_by_xpath('..') \
            .find_element_by_css_selector('label.stem-checkbox__label div.stem-checkbox__box div.checkmark') \
            .click()
        el = (By.XPATH, './/label[contains(@class, "stem-checkbox__label--checked") and contains(@for, "%s")]' % filter)
        self.wait_for(lambda: ec.visibility_of_element_located(el), duration=10)

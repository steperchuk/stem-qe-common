import logging
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.select import Select
from waiting import wait

from stemqa.ui.modal import Modal
from stemqa.ui.pages.page import Page

ADD_CONNECTION_BUTTON = (By.CSS_SELECTOR, 'div.stem-button--primary:not(.disabled) .stem-button__child')
RESULT_SELECTOR = (By.CSS_SELECTOR, 'select#results_length_selector')
SEARCH_INPUT = (By.CSS_SELECTOR, 'input#searchConnectedForm')
FILTER = (By.CSS_SELECTOR, '.connected-data-filters__trigger-label')

logger = logging.getLogger(__name__)


class DataConnectPage(Page):
    """
    Represents the DataConnect page
    """

    URL_SUFFIX = '/powersales/dataconnect/'

    def wait_until_loaded(self):
        self.wait_for_element(ADD_CONNECTION_BUTTON)
        self.wait_for_element(RESULT_SELECTOR)
        self.wait_for_element(SEARCH_INPUT)
        self.wait_for_element(FILTER)

    def create_new_connection(self):
        """
        Clicks on 'Add New Connection' button and returns modal wizard for config of new stream
        :return:
        """
        self.wait_for(lambda: ec.element_to_be_clickable(ADD_CONNECTION_BUTTON), duration=10)
        self.find_element(*ADD_CONNECTION_BUTTON).click()

        logger.info('Waiting for modal to open')
        self.wait_for_element((By.CSS_SELECTOR, '.stem-modal'))
        logger.info('Modal is open!')
        return AddConnectionWizard1(self.driver)

    def get_streams(self):
        """
        Returns configured streams
        :return:
        """
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, '.table-responsive.fadeIn-appear.fadeIn-appear-active')), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.table-responsive .stem-table tbody'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.table-responsive .stem-table tbody tr'), 10)
        table = self.driver.find_element_by_css_selector('.stem-table tbody')
        els = table.find_elements_by_css_selector('.table-responsive .stem-table tbody tr')
        return els

    def get_stream_state(self, index):
        """
        Returns stream state of configured stream
        :param index:
        :return:
        """
        self.get_streams()  # ?
        stream_elem = '.table-responsive .stem-table tbody tr:nth-of-type(%s) td:nth-of-type(5)' % index
        return self.driver.find_element_by_css_selector(stream_elem).text

    def get_stream_timestamp(self, index):
        """
        Returns timestamp value
        :param index:
        :return:
        """
        self.get_streams()  # ?
        stream_elem = '.table-responsive .stem-table tbody tr:nth-of-type(%s) td:nth-of-type(6)' % index
        return self.driver.find_element_by_css_selector(stream_elem).text

    def wait_for_stream_state(self, stream, expected_state):
        """
        Waits for up to 120sec for expectedState for specified stream to show up
        :param stream: stream to wait on(specifies row in External Data Connector page)
        :param expected_state: expected state for specified stream
        :return:
        """
        start_time = time.time()
        state = self.get_stream_state(stream)

        while state not in expected_state:
            logger.info('Expected state is %s and current state is %s. Refreshing in 5secs', expected_state, state)
            time.sleep(5)
            self.driver.refresh()
            state = self.get_stream_state(stream)
            current_time = time.time()
            elapsed_time = current_time - start_time
            logger.info('Elapsed time is %ssecs', elapsed_time)
            if elapsed_time > 300:
                raise RuntimeError("Unable to get to desired stream state in 300 seconds!")
        logger.debug('Stream state: %s', state)
        return self


class AddConnectionWizard1(Modal):
    DATA_TYPE = (By.CSS_SELECTOR, '.select-vendor__data-types .select-vendor__data-type .undefined__INTERVAL_REVENUE')
    CANCEL = (By.CSS_SELECTOR, 'div#wizard-cancel-button .stem-button__child')
    NEXT = (By.CSS_SELECTOR, 'div#wizard-next-button .stem-button__child')

    def wait_until_loaded(self):
        """
        Waits for some elements to be present and visible in the browser.
        :return:
        """
        super(AddConnectionWizard1, self).wait_until_loaded()
        self.wait_for_elements((By.CSS_SELECTOR, '.vendor-tile'))
        self.wait_for_element(self.DATA_TYPE)
        self.wait_for_element(self.CANCEL)
        self.wait_for_element(self.NEXT)

    def click_vendor(self, selection):
        """
        Waits for vendors to be displayed and then clicks on the selection
        """
        el_selector = '[alt = "%s"]' % selection
        self.wait_for_element((By.CSS_SELECTOR, el_selector)).click()
        return self

    def click_next(self):
        """
        Clicks next button and waits for modal to update with next wizard screen
        :return:
        """
        self.wait_for_element((By.CSS_SELECTOR, 'div#wizard-next-button:not(.disabled) .stem-button__child')).click()
        return AddConnectionWizard2(self.driver)

    def get_default_vendor(self):
        """
        Returns active vendor selected
        :return:
        """
        return self.driver.find_element_by_css_selector('.vendor-tile--active').text


class AddConnectionWizard2(Modal):
    CANCEL = (By.CSS_SELECTOR, 'div#wizard-cancel-button .stem-button__child')
    BACK = (By.CSS_SELECTOR, 'div#wizard-back-button .stem-button__child')
    NEXT = (By.CSS_SELECTOR, 'div#wizard-next-button .stem-button__child')
    LOCATE_ACCT = (By.CSS_SELECTOR, '.locate-account__input-and-error')
    SEARCH_DROPDOWN = (By.CSS_SELECTOR, 'select#locate_account_search_by')
    LOADING_BLOCK = (By.CSS_SELECTOR, '.loading-table .loading-block')

    UTILITYAPI_SEARCH_INPUT = (By.CSS_SELECTOR,
                               '.wizard-step:nth-of-type(1).wizard-step--active input#searchVendorInput')
    STEM_SEARCH_INPUT = (By.CSS_SELECTOR,
                         '.wizard-step:nth-of-type(2).wizard-step--active input#searchStemLocationInput')

    TITLE_1 = 'Locate the UtilityAPI meter'
    TITLE_2 = 'Map account'
    TITLE_3 = 'Configure Connection'

    dropdown = None

    def wait_until_loaded(self):
        """
        Waits for some elements to be present and visible in the browser.
        :return:
        """
        super(AddConnectionWizard2, self).wait_until_loaded()
        self.wait_for_element(self.CANCEL)
        self.wait_for_element(self.BACK)
        self.wait_for_element(self.NEXT)
        self.dropdown = Select(self.wait_for_element(self.SEARCH_DROPDOWN))

    def submit(self, step_idx):
        """
        Clicks next button and waits for modal to update with next section
        :param step_idx:
        :return:
        """
        current_step = (By.CSS_SELECTOR, '.wizard-step.wizard-step--active')
        next_step = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(%s)' % step_idx)

        current_step_text = self.driver.find_element(*current_step).text.split('\n')[0]
        next_step_text = self.driver.find_element(*next_step).text.split('\n')[0]

        logger.info('On step %s, navigating to %s', current_step_text, next_step_text)

        self.wait_for_element((By.CSS_SELECTOR, 'div#wizard-next-button:not(.disabled) .stem-button__child')).click()

        new_step = (By.CSS_SELECTOR, '.wizard-step:nth-of-type(%s).wizard-step--active' % step_idx)
        self.wait_for_element(new_step)

        current_step_text = self.driver.find_element(*current_step).text.split('\n')[0]
        logger.info('Moved to step: %s', current_step_text)
        return self

    def select_search_type(self, selection):
        """
        Selects search type- Address, Email, Vendor ID (UID), Vendor ID (SAID)
        :param selection:
        :return:
        """
        self.dropdown.select_by_visible_text(selection)
        self.wait_for_element(self.UTILITYAPI_SEARCH_INPUT)
        return self

    def search(self, search_term):
        """
        Searches for accounts via searchTerm, waits for results to load
        :param self:
        :param search_term:
        :return:
        """
        el = self.wait_for_element(self.UTILITYAPI_SEARCH_INPUT)
        el.click()
        el.clear()
        el.send_keys(search_term)
        self.wait_for_element((By.CSS_SELECTOR, '.loading-table .loading-block'), 30)
        self.wait_for(lambda: ec.invisibility_of_element_located(
            (By.CSS_SELECTOR, '.loading-table .loading-block')), 60)
        self.wait_for_element((By.CSS_SELECTOR, '.table-responsive'), 60)
        return self

    def search_and_return_error(self, search_term):
        """
        Searches for accounts via searchTerm, waits for exception msg box to load
        :param search_term:
        :return:
        """
        el = self.wait_for_element(self.UTILITYAPI_SEARCH_INPUT)
        el.click()
        el.clear()
        el.send_keys(search_term)
        self.wait_for_element((By.CSS_SELECTOR, '.loading-table .loading-block'), 90)
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active .notification-box'), 90)
        return self.driver.find_element_by_css_selector('.wizard-step--active .notification-box').text

    def select_account_by_index(self, index):
        """
        Selects account from list based on index
        :param index:
        :return:
        """
        el_selector = 'table.stem-table tbody tr:nth-of-type(%s) td .stem-radio .stem-radio__radio-inner' % index
        el = self.wait_for_element((By.CSS_SELECTOR, el_selector))
        el.click()
        self.wait_for_element((By.CSS_SELECTOR, 'table.stem-table tbody tr:nth-of-type(%s) '
                                                'td .stem-radio .stem-radio__radio-inner--checked' % index))
        return self

    def select_account_by_vendor_id(self, vendor_id):
        """
        Selects account by vendorId
        :param self:
        :param vendor_id:
        :return:
        """
        table_el = self.wait_for_element((By.CSS_SELECTOR, '.wizard-step:nth-of-type(1) .table-responsive'), 20)

        vendor_el = wait(lambda: table_el.find_element_by_xpath("//td[contains(text(), '%s')]/.." % vendor_id),
                         expected_exceptions=NoSuchElementException, timeout_seconds=10)
        vendor_el.find_element_by_css_selector('td.align-left .stem-radio label div .stem-radio__radio-inner').click()
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step:nth-of-type(1) td.align-left .stem-radio '
                                                'label div .stem-radio__radio-inner--checked'))
        return self

    def select_stem_site_by_location_code(self, location_code):
        """
        Selects account by locationCode
        :param self:
        :param location_code:
        :return:
        """
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step:nth-of-type(2) .table-responsive'), 20) \
            .find_element_by_xpath("//td[contains(text(), '%s')]" % location_code) \
            .find_element_by_xpath('..') \
            .find_element_by_css_selector('td.align-left .stem-radio label div .stem-radio__radio-inner') \
            .click()
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step:nth-of-type(2) td.align-left .stem-radio '
                                                'label div .stem-radio__radio-inner--checked'))
        return self

    def search_non_existent_stem_site(self, location_code):
        """
        Searches for non-existent STEM location and returns error msg
        :param self:
        :param location_code:
        :return:
        """
        el = self.wait_for_element(self.STEM_SEARCH_INPUT)
        el.click()
        el.clear()
        el.send_keys(location_code)
        self.wait_for_element((By.CSS_SELECTOR, '.stem-input__icon'))
        return self.driver.find_element_by_css_selector('.map-account').text

    def get_stem_search_box(self):
        """
        Returns text in search box
        :return:
        """
        return self.driver.find_element(self.STEM_SEARCH_INPUT).text

    def is_interval_data_selected(self):
        """
        Returns whether or not 'Interval Data' checkbox is enabled
        :param self:
        :return:
        """
        interval_select = '#data-types-INTERVAL-selection > .stem-checkbox.stem-checkbox__container > ' \
                          '.stem-checkbox__label--checked'
        return self.driver.find_element_by_css_selector(interval_select).is_displayed()

    def is_historical_data_selected(self):
        """
        Returns whether or not 'Historical Data' checkbox is enabled
        :param self:
        :return:
        """
        historical_select = '#interval-data-types-HISTORICAL-selection > .stem-checkbox.stem-checkbox__container > ' \
                            '.stem-checkbox__label--checked'
        return self.driver.find_element_by_css_selector(historical_select).is_displayed()

    def is_ongoing_data_selected(self):
        """
        Returns whether or not 'Ongoing Data' checkbox is enabled
        :param self:
        :return:
        """
        historical_select = '#interval-data-types-ONGOING-selection > .stem-checkbox.stem-checkbox__container > ' \
                            '.stem-checkbox__label--checked'
        return self.driver.find_element_by_css_selector(historical_select).is_displayed()

    def submit_stream(self):
        """
        Submits stream metadata for creation and waits for modal to disappear and return to DataConnectPage
        with updated table of streams
        :param self:
        :return:
        """
        logger.info('Click submit button')
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active '
                                                '.wizard-step-body .configure-connection'))
        self.wait_for_element((By.CSS_SELECTOR, '.stem-modal.stem-modal__footer '
                                                '.wizard-footer__button-container .wizard-footer__right '
                                                '.stem-button--primary .stem-button__child'))
        time.sleep(.3)  # transitions complete in 300ms
        self.wait_for_element((By.CSS_SELECTOR, 'div#wizard-next-button:not(.disabled) .stem-button__child')).click()

        # Click on last 'Submit' button  in summary view to create stream
        logger.info('Click done button in confirm connection view')
        time.sleep(.3)  # transitions complete in 300ms
        self.wait_for_element((By.XPATH, '//div[contains(text(), "Connect More Accounts")]'))
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active .wizard-step-body .confirmation'))
        self.wait_for_element((By.CSS_SELECTOR, '#wizard-done-button .stem-button__child')).click()

        # wait for modal to disappear and return a new DataConnectPage with new streams
        logger.info('Waiting for modal to close')
        self.wait_for_close()
        self.wait_for_element((By.CSS_SELECTOR, '.table-responsive'), 10)
        return DataConnectPage(self.driver)

    def get_result_rows(self):
        """
        Returns rows from search in current active step
        :param self:
        :return:
        """
        self.wait_for(lambda: ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-table')), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active .table-responsive'))
        els = self.driver.find_elements_by_css_selector('.wizard-step--active .table-responsive .stem-table tbody tr')
        results = []
        for _el in els:
            results.append(_el.text)
        return results

    def get_result_error(self):
        """
        Returns error msg from search results area
        :param self:
        :return:
        """
        self.wait_for(lambda: ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-table')), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active .notification-box', 10))
        return self.driver.find((By.CSS_SELECTOR, '.wizard-step--active .notification-box')).text

    def has_tariff_warning(self, index):
        """
        Returns whether alert is visible
        :param self:
        :param index:
        :return:
        """
        self.wait_for(lambda: ec.invisibility_of_element_located((By.CSS_SELECTOR, '.loading-table')), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.wizard-step--active .table-responsive .stem-table'), 10)
        alert = '.wizard-step--active .table-responsive table.stem-table > tbody > ' \
                'tr:nth-child(%s) > td:nth-child(4) .tariff-alert__tariff-label' % index
        return self.wait_for_element((By.CSS_SELECTOR, alert))

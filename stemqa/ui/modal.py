import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

from stemqa.ui.component import Component


class Modal(Component):
    DEFAULT_EL_LOCATOR = (By.CSS_SELECTOR, '.stem-modal__content')

    def __init__(self, driver):
        super(Modal, self).__init__(driver, None, self.DEFAULT_EL_LOCATOR)
        self.wait_until_loaded()

    def wait_until_loaded(self):
        super(Modal, self).wait_until_loaded()
        time.sleep(0.03)  # modals have a transition time of 300ms

        self.wait_for_element((By.CSS_SELECTOR, '.stem-modal__header'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.stem-modal__body'), 10)
        # self.wait_for_element((By.CSS_SELECTOR, '.stem-modal__footer'), 10)

    def wait_for_close(self):
        self.wait_for(lambda: ec.invisibility_of_element_located(self.top_element_locator), 10)

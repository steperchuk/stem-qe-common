import logging

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

from stemqa.ui.component import Component

logger = logging.getLogger(__name__)


class SidePanel(Component):
    """
    Represents side panel on many pages for navigating to application within a module
    """

    DEFAULT_EL_LOCATOR = (By.CSS_SELECTOR, '.side-panel')

    DASHBOARD = "DASHBOARD"
    LOAD_EXPLORER = "LOAD_EXPLORER"
    COST_EXPLORER = "COST_EXPLORER"
    DEMAND_WATCH = "DEMAND_WATCH"

    CLIENT = 'CLIENT'
    SITE = 'SITE'

    def __init__(self, driver):
        super(SidePanel, self).__init__(driver, None, self.DEFAULT_EL_LOCATOR)
        self.wait_until_loaded()

    def wait_until_loaded(self):
        """
        Wait for basic elements of the side panel to load
        :return:
        """
        super(SidePanel, self).wait_until_loaded()
        self.wait_for_element((By.CSS_SELECTOR, '.side-panel-content'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.dropdown-button-container'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.dropdown-search-input'), 10)
        self.wait_for_element((By.CSS_SELECTOR, '.form-control'), 10)
        # self.wait_for_element((By.CSS_SELECTOR, '.btn.btn-default.rollup-dropdown-button.dropdown-open-client'), 10)
        self.wait_for_elements((By.CSS_SELECTOR, '.btn.btn-default.btn-panel.module-tab.module-tab-link'), 20)

    def select_from_dropdown(self, item_name, item):
        """
        Selects client/site by opening dropdown of clients
        :param item_name:
        :param item:
        :return:
        """

        locator = ".btn.btn-default.rollup-dropdown-button.dropdown-open-%s" % \
                  ('client' if item == self.CLIENT else 'site')
        if item == self.CLIENT:
            parent_locator = '//div[@class="rollup-dropdown-client"]'
        else:
            parent_locator = '//div[@class="rollup-dropdown-site"]'
        starting_client_text = self.wait_for_element((By.CSS_SELECTOR, '.page-toolbar.row'), 10).text
        self.wait_for(lambda: ec.visibility_of_element_located((By.CSS_SELECTOR, locator)), 30)
        self.wait_for_element((By.CSS_SELECTOR, locator), 10).click()
        locator = '%s/div[contains(@style,"ease-out")]' % parent_locator
        self.wait_for_element((By.XPATH, locator), 30)
        self.wait_for(lambda: ec.visibility_of_element_located((By.XPATH, "//*[contains(text(),'%s')]"
                                                                % item_name)), 30)
        self.wait_for_element((By.XPATH, "//*[contains(text(),'%s')]" % item_name), 30).click()
        locator = '%s/div[contains(@style,"ease-in")]' % parent_locator
        self.wait_for_element((By.XPATH, locator), 30)
        self.wait_for(lambda: ec.invisibility_of_element_located((By.XPATH, "//*[contains(text(),'%s')]"
                                                                  % starting_client_text)))

    def select_from_search(self, item_name, item):
        """
        Selects client/site from search box:
        - click on 'Clients/Sites' dropdown
        - wait for search box
        - input search term
        - wait for dropdown list to be updated
        - select client/site
        :param item_name:
        :param item:
        :return:
        """
        locator = ".btn.btn-default.rollup-dropdown-button.dropdown-open-%s" % \
                  ('client' if item == self.CLIENT else 'site')
        if item == self.CLIENT:
            parent_locator = '//div[@class="rollup-dropdown-client"]'
        else:
            parent_locator = '//div[@class="rollup-dropdown-site"]'

        starting_client_text = self.wait_for_element((By.CSS_SELECTOR, '.page-toolbar.row'), 30).text
        self.wait_for(lambda: ec.presence_of_element_located((By.CSS_SELECTOR, locator)), 30)
        self.wait_for(lambda: ec.visibility_of_element_located((By.CSS_SELECTOR, locator)), 30)
        self.wait_for_element((By.CSS_SELECTOR, locator), 10).click()
        locator = '%s/div[contains(@style,"ease-out")]' % parent_locator
        self.wait_for_element((By.XPATH, locator), 30)

        # wait for search box and type in search string
        search_box = self.wait_for_element((By.CSS_SELECTOR, '.rollup-dropdown-client .form-control'))
        search_box.click()
        search_box.clear()
        search_box.send_keys(item_name)

        # wait for all clients/sites to disappear that aren't filtered by search
        self.wait_for_elements((By.XPATH, "//*[contains(@style,'none')]"), 30)

        # wait for element we want to select to be visible, move to it, and select it
        self.wait_for(lambda: ec.visibility_of_element_located((By.XPATH, "//span[contains(text(),'%s')]"
                                                                % item_name)), 30)
        selector = self.wait_for_element((By.XPATH, "//span[contains(text(),'%s')]" % item_name), 30)
        ActionChains(self.driver).move_to_element(selector).perform()
        selector.click()

        # wait for filtered list to dispappear and wait for page to be updated with selected client/location
        locator = '%s/div[contains(@style,"ease-in")]' % parent_locator
        self.wait_for_element((By.XPATH, locator), 30)
        self.wait_for(lambda: ec.invisibility_of_element_located((By.XPATH, "//*[contains(text(),'%s')]"
                                                                  % starting_client_text)))

    def select_client_location(self, client, location_code):
        """
        Navigate to target  client and it site location
        :param client: client name
        :param location_code: location name
        """
        self.select_from_search(client, SidePanel.CLIENT)
        self.select_from_dropdown(location_code, SidePanel.SITE)

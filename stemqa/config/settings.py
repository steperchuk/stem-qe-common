import collections
import json
import os

import yaml


def update(dict1, dict2):
    """
    Recursively updates nested dictionary.

    :param dict1: dict to be updated
    :param dict2: dict that the updated values are read from
    :return:
    """
    if not dict2:
        return dict1

    if not dict1:
        return dict2

    for k, v in dict2.items():
        dv = dict1.get(k, {})
        if isinstance(v, collections.Mapping) and isinstance(dv, collections.Mapping):
            r = update(dv, v)
            dict1[k] = r
        else:
            dict1[k] = dict2[k]
    return dict1


def _dict_from_config():
    """
    Reads in configuration that is used by stem qa code during execution. The defaults are set in config.yml
    and the environment specific overrides can be specified via an environment variable CONF
    which should point to another yml file, preferably generated using `stem-cli deploy-tasks discover-environment`
    command.

    Loading properties this way allows them to be accessible to the common code even when its executed as part
    of some tools, rather than via tests.

    :return: settings as a dict
    """
    with open('src/stemqa/config.yml') as _f:
        data = yaml.safe_load(_f)

    override_config_path = os.environ.get('CONF', None)
    # Handle the new yaml file based config
    if override_config_path:
        # consider the file is yaml
        if not os.path.isfile(override_config_path):
            raise RuntimeError('Invalid configuration file path specified: %s' % override_config_path)

        with open(override_config_path) as _f:
            update(data, yaml.safe_load(_f))

        # TODO: not sure if all this data should be exposed in config. Our setup could significantly change
        # in the future and break some assumptions we made when discovering environments
        # data.update(override_conf)

        # handle services.
        update(data['mysql'], data['svc'].get('mysql', {}))
        update(data['rmq'], data['svc'].get('compose-supervisor', {}))
        update(data['redis'], data['svc'].get('redis', {}))
        update(data['s4server'], data['svc'].get('s4server', {}))
        update(data['vtn'], data['svc'].get('vtn-sim', {}))

        # create URLs used in certain places
        # TODO: why aren't these services creating associated ingresses?
        data['vtn']['url'] = '%s://%s:%s' % (data['vtn']['protocol'], data['vtn']['host'], data['vtn']['app-port'])
        data['stem-ven-endpoints']['url'] = 'http://%s:%s' \
                                            % (data['svc'].get('stem-ven-endpoints', {}).get('host'),
                                               data['svc'].get('stem-ven-endpoints', {}).get('app-port'))
        data['stem-dnp3-master-simulator']['url'] = \
            'http://%s:%s' \
            % (data['svc'].get('stem-dnp3-master-simulator', {}).get('host'),
               data['svc'].get('stem-dnp3-master-simulator', {}).get('http-port'))

        # handle ingresses
        # mapping over to new dicts as I am not sure what I long term strategy with ingresses is. So,
        # best to add a level of indirection between the props we use in tests & the props discovered from env
        data['stem-athena-gateway']['url'] = data['ing'].get('stem-athena-gateway', {}).get('url')
        data['stem-web-app']['url'] = data['ing'].get('stem-web-app', {}).get('url')
        data['stem-tariff']['url'] = data['ing'].get('stem-tariff', {}).get('url')
        data['stem-ms-app']['url'] = data['ing'].get('stem-ms-app', {}).get('url')
        data['stem-business-objects']['url'] = data['ing'].get('stem-business-objects', {}).get('url')
        data['stem-ms-weather']['url'] = data['ing'].get('stem-ms-weather', {}).get('url')
        data['stem-simulation']['url'] = data['ing'].get('stem-simulation', {}).get('url')
        data['stem-forecast']['url'] = data['ing'].get('stem-forecast', {}).get('url')

    # TODO: FIXME: Hack for now, until we find a better solution
    stem_cli_config = os.environ.get('STEM_CLI_CONFIG')
    if stem_cli_config:
        cfg = yaml.safe_load(open(os.path.expanduser(stem_cli_config)))
        vault_url = cfg.get('vault.url')
        if vault_url and 'qa' in vault_url:
            # we are configured to run against qa env, so update vault configs accordingly
            data['vault']['addr'] = 'https://qa-vault.stem.com'
            data['vault']['header'] = 'vault.qa.stem.com'
            data['vault']['role'] = 'qa-account-deploy-iam'
            # in case run against qa env - logs should be pushed to qa Kibana
            data['kibana']['url'] = 'http://kibana.qa.stem.com'

    data['artifacts'] = dict()
    # json file containing info on artifacts. e.g.
    # {
    #     "build-number": 29,
    #     "build-timestamp": 20180611233541,
    #     "git-branch": "master",
    #     "git-commit": "2f8560c",
    #     "name": "stem-openadr-ven",
    #     "semantic-version": "1.2.0-dev1+29.2f8560c",
    #     "short-hash": "431022e285f8",
    #     "type": "helm-chart"
    # }
    artifacts_info = os.environ.get('ARTIFACTS_INFO', None)
    if artifacts_info:
        if not os.path.exists(artifacts_info):
            # Need to search in parent dir for services with common repository. (like stem-ds)
            artifacts_info = os.path.abspath(os.path.join(os.curdir, os.pardir)) + '/' + artifacts_info
            if not os.path.exists(artifacts_info):
                raise RuntimeError('Invalid artifacts info file path specified: %s. '
                                   'Note that this file has to be generated using '
                                   '`kyber-utils get-artifacts` command.' % artifacts_info)

        with open(artifacts_info, 'r') as _f:
            artifacts = json.load(_f)
            for a in artifacts:
                data['artifacts'][a['name']] = a['semantic-version']

        if 'edge-bp' not in data['artifacts']:
            # TODO: This is temporary. This should actually come from artifacts-info.json file instead
            data['artifacts']['edge-bp'] = '0.9.1-155.aefc7b4'

    # print(json.dumps(data, indent=2))
    return data


settings = _dict_from_config()

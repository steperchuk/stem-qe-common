import logging
import os
from logging import config

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning  # pylint: disable=import-error

# disable warning messages pointing to "unverified http requests"
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)  # pylint: disable=no-member

DEFAULT_LOG_FILENAME = "test.log"
DEFAULT_FILE_HANDLER_MODE = "a"
DEFAULT_BACKUP_COUNT = 1024
DEFAULT_MAX_BYTES = 10 * 1024 * 1024
DEFAULT_FORMATTER_FORMAT = \
    '%(asctime)s %(threadName)s %(filename)-12.12s:%(funcName)-10.10s:%(lineno)03d %(levelname)-5s  %(message)s'
DEFAULT_FORMATTER_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

LOGGING_CONFIG = {
    'version': 1,
    'formatters': {
        'default':
            {
                'format': DEFAULT_FORMATTER_FORMAT,
                'datefmt': DEFAULT_FORMATTER_DATE_FORMAT
            }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': 'ext://sys.stdout'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'default',
            'mode': DEFAULT_FILE_HANDLER_MODE,
            'filename': "output/" + DEFAULT_LOG_FILENAME,
            'maxBytes': DEFAULT_MAX_BYTES,
            'backupCount': DEFAULT_BACKUP_COUNT
        }
    },
    'loggers': {
        'stemqa': {
            'level': 'DEBUG',
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'tests': {
            'level': 'DEBUG',
            'handlers': ['console', 'file'],
            'propagate': False
        },
        'tools': {
            'level': 'DEBUG',
            'handlers': ['console', 'file'],
            'propagate': False
        }
    },
    'disable_existing_loggers': False
}

if not os.path.exists('output'):
    os.makedirs('output')  # ensure this folder exists
config.dictConfig(LOGGING_CONFIG)


def get_logger(name):
    """
    All test modules should use this method to get the logger
    :param name: name of logger
    :return: logger object
    """
    return logging.getLogger(name)

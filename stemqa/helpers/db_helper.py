import csv
import logging
import os
import time
from contextlib import contextmanager

import pymysql.cursors
from pymysql.constants.CLIENT import MULTI_STATEMENTS

from stemqa import consts
from stemqa.config.settings import settings

logger = logging.getLogger(__name__)

DB_ANALYTICS = 'analytics'
DB_METER_LOGS = 'meter_logs'
DB_TOPOLOGY = 'topology'
DB_TARIFF = 'tariff'
DB_OPTIMIZATION = 'optimization'
DB_GRID_SERVICES = 'grid_services'
DB_PORTAL = 'portal'
DB_FORECAST = 'forecast'
DB_OPERATIONS = 'operations'
DB_BUSINESS = 'business'


class DbHelper(object):
    """
    Database access to mysql
    """

    def __init__(self, hostname, port, username, password):
        self._host = hostname
        self._port = int(port)
        self._username = username
        self._password = password

    @classmethod
    def default_instance(cls):
        """
        :return: an instance of DB Helper using default settings
        """
        return cls(settings['mysql']['host'], settings['mysql']['db-port'],
                   settings['mysql']['username'], settings['mysql']['password'])

    @contextmanager
    def cursor(self, db):
        """
        Context manager that provides a cursor to a specific database and takes care of
        committing & cleaning up after itself
        :param db: database to connect to
        :return: a dictionary cursor
        """
        connection = pymysql.connect(host=self._host,
                                     port=self._port,
                                     user=self._username,
                                     passwd=self._password,
                                     db=db,
                                     cursorclass=pymysql.cursors.DictCursor,
                                     client_flag=MULTI_STATEMENTS)
        cursor = connection.cursor()
        try:
            yield cursor
        finally:
            connection.commit()
            connection.close()

    def query(self, db, sql):
        """
        Runs the specified query against the database
        :param db: database name
        :param sql: sql query
        :return: fetches all results and returns them
        """
        logger.debug('Executing against db %s, sql: \n%s', db, sql)
        with self.cursor(db) as cursor:
            cursor.execute(sql)
            result = cursor.fetchall()
            logger.debug('Found %d rows.', len(result))
            return result

    def write_csv_file(self, db, sql, _file, include_id_field=False):
        """
        Writes out sql query results to specified file
        :param db:
        :param sql:
        :param _file:
        :param include_id_field: whether to include id field in file
        :return:
        """
        logger.debug('Executing against db %s, sql: \n%s', db, sql)

        # write out contents to file
        with self.cursor(db) as cursor:
            cursor.execute(sql)

            # get headers, if we need to remove headers, remove them
            headers = [i[0] for i in cursor.description]
            if not include_id_field:
                if 'id' in headers:
                    headers.remove('id')

            # get all the results
            results = cursor.fetchall()

            # instantiate writer and print headers to file
            dw = csv.DictWriter(_file, extrasaction='ignore', fieldnames=headers)
            headers = {}
            for n in dw.fieldnames:
                headers[n] = n
            dw.writerow(headers)

            # write out all the rows
            for row in results:
                dw.writerow(row)

            # ensures all buffers are flushed to file before proceeding
            _file.flush()
            os.fsync(_file.fileno())

    def query_with_retry(self, db, sql, retry_count=consts.DB_ACCESS_RETRY_CNT,
                         retry_time=consts.DB_ACCESS_DELAY_SEC):
        """
        Runs specified query while retrying in case no result is returned or an error is encountered
        :param db: database name
        :param sql: sql query
        :param retry_count: number of times to retry
        :param retry_time: duration (in secs) to wait between retries
        :return: fetches all results and returns them
        """
        for i in range(int(retry_count)):
            result = None
            try:
                result = self.query(db, sql)
            except:  # noqa: E722
                # only logging exception here.
                logger.exception('Encountered exception while querying db...')
            finally:
                if result:
                    # Found record so set found flag and break out fo inner for loop
                    return result
                logger.debug('Retrying again. Retry attempt: %s', i + 1)
            time.sleep(retry_time)

        assert 0, 'Failed to find record in the database after retry %s times with %s secs delay' % (
            retry_count, retry_time)

    def get_records_count(self, db_name, table_name):
        """
        Get count of records in specific table
        :param db_name: str
        :param table_name: str
        :return: int
        """

        solution_sql = "SELECT COUNT(*) FROM {table}".format(table=table_name)
        response = self.query(db_name, solution_sql)
        count = response[0]['COUNT(*)']
        return count

    def wait_for_records_update(self, expected_records_count, db_name, table_name, timeout=30):
        """
        Waits for expected records count in specific table
        :param expected_records_count: int
        :param db_name: str
        :param table_name: str
        :param timeout: default 30
        :return: True if records updated and False otherwise
        """
        logger.info("Waiting for {expected} records count in {table}".
                    format(expected=expected_records_count, table=table_name))
        for i in range(0, timeout):
            if self.get_records_count(db_name, table_name) == expected_records_count:
                logger.info("Expected count of records created")
                return True
            time.sleep(1)
        return False

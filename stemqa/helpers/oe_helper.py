import datetime as dt
import json
import logging

from deepdiff import DeepDiff

import stemqa.util.site_util as su
from stemqa.consts import TYPE_FORECAST, TYPE_HISTORICAL
from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_METER_LOGS, DB_FORECAST, DB_TOPOLOGY
from stemqa.rest.bo import BoClient
from stemqa.rest.powerqa import PowerQAClient
from stemqa.util.dataload_util import load_data

logger = logging.getLogger(__name__)

TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
TIME_SHORT = "%Y-%m-%d"

POWERSTORE_SOLUTION_TABLE = 'forecast.powerstore_solution'
SOLUTION_TABLE = 'forecast.solution'

MQ_MSG_BILLING_START = 5  # Number of messages OE sends to RMQ if it is running day before billing cycle starts.
MQ_MSG_NOT_START = 3  # Number of messages OE sends to RMQ if it is running at any other day.
# 6 is the default messages count for solution with power snap messages:
# update_multi_setpoints, sync_setpoint, set_tariff_data,
# set_battery_curves,apply_max_power_snap_offset, apply_max_power_snap_threshold
MQ_MSG_POWER_SNAP = 6


class OEHelper:
    """
    Utilities to use in OE tests
    """

    def __init__(self):
        self.bo = BoClient()
        self.dbhelper = DbHelper.default_instance()

    @staticmethod
    def load_monitor_data(params):
        """
        Prepare and upload monitor 15 minutes data for OE tests
        :param params: {'target_location': <target_location>,
                        'time_update': True/False,
                        'solution_time': <solution_time_dt>,
                        'multiplier': <multiplier>}
        :return:
        """
        logger.info('Import 15 min meter log data for %s.', params['target_location'])
        s3_folder = "s3://stemqa-sampledata/csv_import/meter_logs/realtime/monitor_data_15_minute"
        suffixes = ['site', 'main', 'inverter']

        if params['time_update']:
            file_start = '2018-06-01 00:00:00'  # file_start_time should be set to standard
            file_start_dt = dt.datetime.strptime(file_start, TIME_FORMAT)

            # Current requirement is to have more than one month of history data
            # Technicaly length of "history" could be 32. But to feel more comfortable and have some buffer
            # set it equal to 45
            history_length = 45

            time_delta = params['solution_time'] - file_start_dt
            days = int(time_delta.total_seconds() / 86400) - history_length
            logger.debug("Dates in meter logs will be updated for %d days.", days)
        else:
            logger.debug("Dates in meter logs won't be updated.")
            days = 0

        for each in suffixes:
            s3_file = '%s/SDVS_20180601-20180901_%s.csv' % (s3_folder, each.lower())
            logger.debug("Load data from file %s", s3_file)
            if each.lower() == 'main':
                # main id for accuvim, used in oe meter logs from production site
                # more info: resources/sql/master_create_site_edge.sql l:94
                monitor_id = 'TARGET-am0001'
                new_monitor_id = '%s-am0001' % (params['target_location'])
            else:
                monitor_id = 'TARGET-%s' % each.lower()
                new_monitor_id = '%s-%s' % (params['target_location'], each.lower())

            options = {
                "database": DB_METER_LOGS,
                "table": "monitor_data_15_minute",
                "transform": {"monitor_id": monitor_id, "new_monitor_id": new_monitor_id}
            }

            if days != 0:
                options['transform']['time_offset'] = days
                options['transform']['time_offset_unit'] = 'days'

            load_data(s3_file, options, False)

    def create_billing_cycles(self, start_date,
                              schedule_table_id='131',
                              billing_cycle_name='SDG&E - Read Cycle 131',
                              cycle_cd='OE',
                              service_provider_id='10',
                              cycle_duration=31,
                              total=5):
        """
        Create billing cycles (by default - five) with required start date
        :type cycle_duration: duration of billing cycle (default - 31 days)
        :param billing_cycle_name: Name of billing cycle (default - 'SDG&E - Read Cycle 131')
        :param start_date: Date, when next reading cycle starts
        :param schedule_table_id: Reading schedule ID (default - '131')
        :param cycle_cd: Reading cycle code  (default - 'OE')
        :param service_provider_id: Service provider id (default - '10' that refer to 'SDG&E')
        :param total: number of reading cycle to create in DB. (Default - 5)
        :return:
        """
        # For OE testing we need at least five billing cycles: first should starts month ago,
        # as start_date - happens in next month need to calculate backward 2 month
        last_cycle = cycle_duration * 2 - 1
        cycle_start = start_date - dt.timedelta(days=last_cycle)

        self.bo.create_billing_cycle_periods(start_date=cycle_start,
                                             cycle_duration=cycle_duration,
                                             schedule_table_id=schedule_table_id,
                                             billing_cycle_name=billing_cycle_name,
                                             cycle_cd=cycle_cd,
                                             cycles_amount=total,
                                             service_provider_id=service_provider_id)

    def get_last_solution_id(self, ps_id):
        """
        Get latest solution ID for PowerStore
        :param ps_id: PowerStore ID (str)
        :return: Solution ID (str). ID = 'ERROR', if there is no solution for PowerStore.
        """

        solution_sql = "SELECT MAX(solution_id) FROM forecast.powerstore_solution WHERE powerstore_id = %s;" % ps_id
        response = self.dbhelper.query(DB_FORECAST, solution_sql)

        solution_id = su.db_respond(response, 'MAX(solution_id)')

        return solution_id

    def get_solution_ver(self, solution_id):
        """
        Get version of solution based on solution ID
        :param solution_id: Solution ID (str)
        :return: Version of solution. 'ERROR' if there is no record in forecast.solution
        """

        version_sql = "SELECT version_id FROM forecast.solution WHERE id = %s;" % solution_id
        response = self.dbhelper.query(DB_FORECAST, version_sql)

        ver_id = su.db_respond(response, 'version_id')

        return ver_id

    def get_read_dates(self, test_site):
        """
        Get list of billing cycles
        :param test_site: Test site location code
        :return: Reading dates for the site (list)
        """
        meter_reading_schedule_sql = \
            "SELECT meter_reading_schedule_table_id FROM topology.location_tariff_info WHERE location_code = '%s';" \
            % test_site

        response = self.dbhelper.query(DB_TOPOLOGY, meter_reading_schedule_sql)
        schedule_table_id = response[0]['meter_reading_schedule_table_id']
        tariff_schedule_records = self.bo.get_tariff_schedule_records(schedule_table_id)
        read_date_list = []
        records_amount = len(tariff_schedule_records)
        if records_amount != 0:
            full_record_data = self.bo.get_record_by_id("trf_bill_cycle", tariff_schedule_records[0]["id"])
            billing_cycles = full_record_data.json()['bill_cycle_periods']

            for each in billing_cycles:
                read_date_list.append(each['bill_date'])

        return read_date_list

    @staticmethod
    def generate_solution(powerstore_id, solution_time):
        """
        Call PowerQA to generate OE solution
        :param powerstore_id: PowerStore ID (str)
        :param solution_time: solution time (datetime)
        :return:
        """
        logger.info("Generate solution for %s at %s.", powerstore_id, solution_time)
        with PowerQAClient(settings['stem-web-app']['url']) as client:
            return client.generate_solution_by_powerstore_id(int(powerstore_id), solution_time)

    def update_solution_timestamp(self, solution_id, new_time_dt):
        """
        Update timestamp of the last solution for specified PowerStore
        :param solution_id: Solution ID
        :param new_time_dt: Time to update to (dt)
        :return:
        """
        update_sql = "UPDATE forecast.solution SET solution_timestamp='%s' WHERE id='%s';" % (new_time_dt, solution_id)
        self.dbhelper.query(DB_FORECAST, update_sql)

    def update_solution_start_time(self, solution_id, new_time_dt):
        """
        Update timestamp of the last solution for specified PowerStore
        :param solution_id: Solution ID
        :param new_time_dt: Time to update to (dt)
        :return:
        """
        update_sql = "UPDATE forecast.solution SET start_time='%s' WHERE id='%s';" % (new_time_dt, solution_id)
        self.dbhelper.query(DB_FORECAST, update_sql)

    def del_monitor_data(self, host_name):
        """
        Delete data in monitor_data_15_minutes for specific hostname
        :param host_name: hostname
        :return:
        """

        monitor_sql = "DELETE FROM meter_logs.monitor_data_15_minute WHERE monitor_id like '%s%%';" % host_name

        self.dbhelper.query(DB_METER_LOGS, monitor_sql)

    def get_solution_start_time(self, solution_id):
        """
        Get solution start time from database
        :param solution_id: solution ID
        :return: solution start_time (dt)
        """

        time_sql = "SELECT start_time FROM forecast.solution WHERE id = %s;" % solution_id

        response = self.dbhelper.query(DB_FORECAST, time_sql)

        solution_time_dt = su.db_respond(response, 'start_time')

        return solution_time_dt

    def get_oe_from_solution(self, solution_ver):
        """
        Get OE version from solution record in forecast.solution_version
        :param solution_ver: solution version
        :return: oe version
        """

        oe_sql = "SELECT oe_version FROM forecast.solution_version where version_id = '%s';" % solution_ver

        response = self.dbhelper.query(DB_FORECAST, oe_sql)

        oe_version = su.db_respond(response, 'oe_version')

        return str(oe_version)

    @staticmethod
    def compare_oe_messages(oe_messages, expected_records_file, verify_power_snap=True, tolerance=3):
        """
        Compares oe messages
        :param oe_messages: list of oe messages
        :param expected_records_file: path of file with expected data
        :param verify_power_snap: bool flag to verify power snap commands in queue
        :param tolerance: in percentage. default value is 3%
        :return: errors list
        """
        errors = list()
        expected_msgs = dict()
        actual_msgs = dict()

        with open(expected_records_file) as infile:
            expected_messages = infile.readlines()

        for i in range(0, len(oe_messages)):
            expected_msgs[json.loads(expected_messages[i])['command']] = json.loads(expected_messages[i])
            actual_msgs[json.loads(oe_messages[i])['command']] = json.loads(oe_messages[i])

        if expected_msgs['set_battery_curves']['args']['hostname'] != \
                actual_msgs['set_battery_curves']['args']['hostname']:
            errors.append("Hostname differs. Expected: {expected}; Actual: {actual}".
                          format(expected=expected_msgs['set_battery_curves']['args']['hostname'],
                                 actual=actual_msgs['set_battery_curves']['args']['hostname']))

        for i in range(0, len(actual_msgs['set_battery_curves']['args']['curves']['curves'])):
            curve_diff = DeepDiff(expected_msgs['set_battery_curves']['args']['curves']['curves'][i],
                                  actual_msgs['set_battery_curves']['args']['curves']['curves'][i])
            if curve_diff:
                errors.append(curve_diff)

        # For daily oe version
        if 'sync_setpoint' in expected_msgs:
            if expected_msgs['sync_setpoint']['args'] != actual_msgs['sync_setpoint']['args']:
                errors.append("Sync setpoint differs. Expected: {expected}; Actual: {actual}".
                              format(expected=expected_msgs['sync_setpoint']['args'],
                                     actual=actual_msgs['sync_setpoint']['args']))

        if 'update_multi_setpoints' in expected_msgs:
            setpoint_data_diffs = DeepDiff(expected_msgs['update_multi_setpoints']['args']['setpoint_data'],
                                           actual_msgs['update_multi_setpoints']['args']['setpoint_data'])
            if setpoint_data_diffs:
                for setpoint_diff in setpoint_data_diffs['values_changed']:
                    new_value = setpoint_data_diffs['values_changed'][setpoint_diff]['new_value']
                    old_value = setpoint_data_diffs['values_changed'][setpoint_diff]['old_value']
                    diff = (abs(new_value - old_value) / old_value) * 100
                    if diff > tolerance:
                        errors.append(setpoint_diff)

        if 'set_tariff_data' in expected_msgs:
            tariff_schedule_diffs = DeepDiff(expected_msgs['set_tariff_data']['args']['value'],
                                             actual_msgs['set_tariff_data']['args']['value'])
            if tariff_schedule_diffs:
                errors.append(tariff_schedule_diffs)

        if verify_power_snap:
            if expected_msgs['apply_max_power_snap_offset']['args'] != \
                    actual_msgs['apply_max_power_snap_offset']['args']:
                errors.append("Apply max power snap offset differs. Expected: {expected}; Actual: {actual}".
                              format(expected=expected_msgs['apply_max_power_snap_offset']['args'],
                                     actual=actual_msgs['apply_max_power_snap_offset']['args']))

            if expected_msgs['apply_max_power_snap_threshold']['args'] != \
                    actual_msgs['apply_max_power_snap_threshold']['args']:
                errors.append("Apply max power snap threshold differs. Expected: {expected}; Actual: {actual}".
                              format(expected=expected_msgs['apply_max_power_snap_threshold']['args'],
                                     actual=actual_msgs['apply_max_power_snap_threshold']['args']))

        return errors

    def del_reading(self, reading_schedule_id='131'):
        """
        Delete from DB reading schedule for specific id
        :param reading_schedule_id: By default method will delete reading schedule created for OE tests (id = '131')
        :return:
        """
        self.bo.delete_tariff_schedule_records(reading_schedule_id=reading_schedule_id)

    def prepare_billing_cycle(self, oe_data):
        """
        Deletes from DB reading schedule and creates new billing cycle for billing start date if any
        :return:
        """
        if oe_data.billing_start:
            self.del_reading()

            reading_date = dt.datetime(oe_data.billing_start.year,
                                       oe_data.billing_start.month,
                                       oe_data.billing_start.day)

            logger.info("Test case will create billing cycles with start day '{date}'.".format(date=reading_date))
            self.create_billing_cycles(reading_date)

    def prepare_previous_solution_records_in_db(self, oe_data):
        """
        Prepares previous solution records in db for specific site ps_id
        :return: (str) solution_id or 'ERROR', if there is no solution for PowerStore.
        """
        logger.info("Test require at least one previous solution for PS {id}.".format(id=oe_data.ps_id))
        last_solution_id = self.get_last_solution_id(oe_data.ps_id)
        if last_solution_id:
            logger.info("Found solution for PS {id}.".format(id=oe_data.ps_id))
        else:
            logger.info("There is no solutions for PS {id} in the DB. Will create one.".format(id=oe_data.ps_id))
            solutions_count = self.dbhelper.get_records_count(DB_FORECAST, POWERSTORE_SOLUTION_TABLE)
            assert self.generate_solution(oe_data.ps_id, oe_data.test_time_dt), "Generate solution task failed"
            self.dbhelper.wait_for_records_update(solutions_count + 1, DB_FORECAST, POWERSTORE_SOLUTION_TABLE)
            last_solution_id = self.get_last_solution_id(oe_data.ps_id)
        logger.info(
            "Last solution for PS {id} should be {age} hours old.".format(id=oe_data.ps_id, age=oe_data.solution_age))
        solution_time_new_dt = oe_data.test_time_dt - dt.timedelta(hours=oe_data.solution_age)

        self.update_solution_start_time(last_solution_id, solution_time_new_dt)
        self.update_solution_timestamp(last_solution_id, solution_time_new_dt)
        return last_solution_id

    @staticmethod
    def mq_message(params):
        """
        Build message to be send to RMQ based on template from file
        :param params:
        :return: message or 'ERROR'
        """

        # Read template message from the file
        file_name = params['file']
        message = json.load(open(file_name))

        msg_time = params['solution_time']

        if 'adhoc' in file_name:
            message['args']['powerstore_id'] = params['ps_id']
            message['args']['start_time'] = msg_time.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
            message['args']['refresh'] = params['refresh']
            message['reply_queue'] = "routable.replies.iterate_command_init_rpc_reply_queue_{id}".format(
                id=params['mac_id'])
        elif 'spc' in file_name:
            message['arr'][0]['data'][0]['t'] = msg_time.strftime("%Y-%m-%dT%H:%M:%S.%f")
            message['arr'][0]['id'] = "pgx.hq.{id}.spc.setpoint_controller.telemetry".format(id=params['mac_id'])
        else:
            raise Exception('Unknown JSON message template.')

        logger.debug("Message to send to RMQ is:\n{message}.".format(message=message))

        return message

    @staticmethod
    def import_weather_data(weather_client):
        """
        Imports weather data (Historical and Forecast)
        :param weather_client: weather client object
        :return:
        """
        weather_client.upload(weather_type=TYPE_FORECAST)
        weather_client.upload(weather_type=TYPE_HISTORICAL)

    @staticmethod
    def delete_weather_data(weather_client):
        """
        Deletes all weather data (Historical and forecast)
        weather_client: weather client object
        :return:
        """
        weather_client.delete_all()

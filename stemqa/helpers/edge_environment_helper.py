import datetime
import logging
import os
import re
import shutil
import time

import edge_api_client.api_client as api_client

from jinja2 import Template

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DB_FORECAST, DB_TOPOLOGY, DbHelper
from stemqa.util import shell_util


class EdgeEnvironment(object):
    namespace = None
    module_name = None
    hostname = None
    is_setup = False
    ms_port_dict = {}
    logger = logging.getLogger(__name__)
    client = None

    def __init__(self, test_module, resource_dir="resources/edge"):
        self.namespace = "%s-%s" % (settings['env-name'], test_module)
        self.module_name = test_module
        self.is_setup = False
        self.resource_dir = "%s/%s" % (resource_dir, self.module_name)

        # Get hostname from config
        config = "%s/config.json" % self.resource_dir
        self.logger.debug('Using configuration: %s', config)
        with open(config) as file:
            self.hostname = re.search('"hostname": "(.*)"', file.read()).groups()[0]

        self.logger.debug('Initializing edge environment %s', self.namespace)

    def setup(self):
        if not self.is_setup:
            self.logger.debug("Setup edge environment %s", self.namespace)
            self.setup_edge_environment()
            self.is_setup = True
            self.client = api_client.EdgeAPIClient(self.namespace)
        else:
            self.logger.debug("%s is already setup", self.namespace)

    def setup_edge_environment(self):
        # Edit options file to add rabbit server from settings
        options_file = "%s/test.options" % (self.resource_dir)
        options = open(options_file, 'r').read().split()
        options = [x for x in options if not x.startswith('--rabbitmq')]
        options += ['--rabbitmq=%s@%s' % (settings['rmq']['username'], settings['rmq']['host']), '\n']
        open(options_file, 'w').write(' '.join(options))

        # Add rabbit server to cloud relay config template
        if not os.path.exists("%s/config" % self.resource_dir):
            os.makedirs("%s/config" % self.resource_dir)
        cloud_relay_template = "resources/edge/cloud_relay_agent.template"
        with open(cloud_relay_template, 'rt') as fin:
            with open("%s/config/cloud_relay_agent.conf" % self.resource_dir, 'wt') as fout:
                t = Template(fin.read())
                fout.write(t.render(port=settings['rmq']['rabbit-port'],
                                    host=settings['rmq']['host'],
                                    user=settings['rmq']['username'],
                                    password=settings['rmq']['password']))
        self.logger.debug('Configuring edge device to connect to %s port %s'
                          % (settings['rmq']['host'], settings['rmq']['rabbit-port']))
        shell_util.run_process('stem-cli deploy-tasks generate-edge-helm '
                               '--env-name %s --version %s --options-file %s'
                               % (self.namespace, settings['artifacts']['edge-bp'], options_file),
                               show_output=True, fail_on_error=True)
        vault_addr = settings['vault']['addr']
        vault_header = settings['vault']['header']
        vault_role = settings['vault']['role']
        vault_version = settings['vault']['version']
        shell_util.run_process('kubectl create namespace %s' % self.namespace, show_output=False, fail_on_error=False)
        shell_util.run_process('stem-cli deploy-tasks install-chart --env-name %s --type %s-chart --version 1 --local '
                               '--set vault.address=%s '
                               '--set vault.header=%s '
                               '--set vault.role=%s '
                               '--set vault.version=%s'
                               % (self.namespace, self.namespace, vault_addr, vault_header, vault_role, vault_version))
        shell_util.run_process('stem-cli deploy-tasks wait-for-environment --env-name %s' % self.namespace)
        self.logger.debug('Edge environment %s is now up and running', self.namespace)

    def reset_state(self):
        self.logger.debug("Reseting the environment")
        pids = self.run("ps -eo pid,cmd | grep /stem/microservices | cut -f3 -d ' '")
        result = self.run("monit restart stem_app")
        pids2 = pids
        while pids == pids2 or pids.count('\n') != pids2.count('\n'):
            pids2 = self.run("ps -eo pid,cmd | grep /stem/microservices | cut -f3 -d ' '")
        time.sleep(5)
        self.logger.debug("Environment successfully reset")
        return result

    def teardown(self, exec_monit_stop=False):
        if self.is_setup:
            self.save_logs(exec_monit_stop=exec_monit_stop)
            self.is_setup = False
            self.logger.debug("Tearing down edge environment %s", self.namespace)
            shell_util.run_process('stem-cli deploy-tasks delete-environment --env-name %s' % self.namespace)
            shutil.rmtree('%s-chart' % self.namespace)
        else:
            self.logger.debug('Cannot teardown %s because it was never setup', self.namespace)

    def setup_database(self):
        dbhelper = DbHelper.default_instance()
        env_in_db = 'SELECT name, hostname FROM `powerstore` WHERE hostname=\'%s\'' % self.hostname
        result = dbhelper.query(DB_FORECAST, env_in_db)
        if result != ():
            self.logger.debug("Environment %s with hostname %s is already in the database", self.namespace,
                              self.hostname)
        else:
            self.logger.debug("Environment %s with hostname %s is now being added to the database", self.namespace,
                              self.hostname)
            with open("resources/sql/create_site_ps_3_1.sql") as file:
                sql = file.read()
                dbhelper.query(DB_TOPOLOGY, sql % (self.module_name.upper(), self.hostname))

    def run(self, cmd):
        """
        Run a shell command in the edge-bp stem-app pod
        """
        stem_app = shell_util.kubectl(self.namespace, "get pods | grep stem-app | cut -f1 -d ' '",
                                      json_output=False, fail_on_error=False)[0].rstrip()
        output = shell_util.kubectl(self.namespace, "exec -ti %s -- %s"
                                    % (stem_app, cmd), json_output=False, fail_on_error=False)[0]
        return output

    def save_logs(self, exec_monit_stop=False):
        # Save older logs to a new directory
        if os.path.isdir("output/%s-logs" % self.namespace):
            os.system("mv output/%s-logs output/%s-logs-%s"
                      % (self.namespace, self.namespace, datetime.datetime.now().isoformat()))
        os.mkdir("output/%s-logs" % self.namespace)
        pods = shell_util.kubectl(self.namespace, "get pods --no-headers -o custom-columns=:metadata.name",
                                  json_output=False, fail_on_error=True)[0].splitlines()
        for pod in pods:
            # Save the logs from each of the emulators
            log = shell_util.kubectl(self.namespace, "logs %s" % (pod),
                                     json_output=False, fail_on_error=True)[0]
            f = open("output/%s-logs/%s.log" % (self.namespace, pod), "w")
            f.write(log)

            # Save the logs from each microservice running in the emulated powermonitor
            if pod.startswith("stem-app"):
                try:
                    if exec_monit_stop:
                        shell_util.kubectl(self.namespace, "exec -ti %s -- monit stop stem_app"
                                           % (pod), json_output=False, fail_on_error=True)
                        command = "exec -ti %s -- ps -ef | grep microservices | wc -l" % (pod)
                        start_time = time.time()
                        svcs = 7
                        while svcs > 0 and time.time() < start_time + 30:
                            output, rc = shell_util.kubectl(self.namespace, command,
                                                            json_output=False, fail_on_error=True)
                            self.logger.debug('output: %s', output)
                            svcs = int(output)
                            time.sleep(2)
                        if svcs > 0:
                            self.logger.warn('Failed to stop svcs in 30s, logs may not be written out properly')
                    shell_util.kubectl(self.namespace,
                                       "exec -ti %s -- tar czf stem-logs.tar.gz /data/stem-logs/" % (pod),
                                       json_output=False, fail_on_error=True)
                    shell_util.kubectl(self.namespace, "cp %s/%s:stem-logs.tar.gz output" % (self.namespace, pod),
                                       json_output=False, fail_on_error=True)
                    os.system("tar xzf output/stem-logs.tar.gz -C output")
                    os.system("mv output/data/stem-logs/* output/%s-logs/" % self.namespace)
                    os.system("rm -r output/data; rm output/stem-logs.tar.gz")
                except Exception as e:
                    self.logger.error('Failed to zip up logs for stem-app! %s', e)

import datetime as dt
import logging

from dateutil import tz
from pytz import timezone

from stemqa.util.openadr_util import vtnc, json_dumps

logger = logging.getLogger(__name__)

str_format = "%Y-%m-%dT%H:%M:%SZ"


class OpenADREvent(object):

    def __init__(self, j):
        """
        Constructor
        """
        self.event_id = list(j.keys())[0]
        self.oadr_event = j[self.event_id]['oadr_event']
        self.grid_events = j[self.event_id]['grid_events']

    def get_event_id(self):
        return self.event_id

    def get_grid_events(self):
        return self.grid_events

    def get_event_length_in_min(self):
        event_start_time_dt = dt.datetime.strptime(self.oadr_event['event_start_time'], str_format)
        event_end_time_dt = \
            event_start_time_dt + \
            dt.timedelta(seconds=self.oadr_event['interval_seconds'] * len(self.oadr_event['charge_payloads']))
        return (event_end_time_dt - event_start_time_dt).total_seconds() / 60

    def get_event_start_time_utc_dt(self):
        t = dt.datetime.strptime(self.oadr_event['event_start_time'], str_format).replace(tzinfo=timezone('UTC'))
        return t

    def get_event_end_time_utc_dt(self):
        event_end_time = self.get_event_start_time_utc_dt() + dt.timedelta(minutes=self.get_event_length_in_min())
        t = event_end_time.replace(tzinfo=timezone('UTC'))
        return t

    def get_event_start_time_local_dt(self):
        t = self.get_event_start_time_utc_dt().astimezone(tz.tzlocal())
        return t

    def get_event_end_time_local_dt(self):
        t = self.get_event_end_time_utc_dt().astimezone(tz.tzlocal())
        return t

    def explicit_cancel(self):
        event_id = self.get_event_id()
        ven_name = self.oadr_event['ven_name']
        response_json = vtnc.cancel_event(event_id, ven_name)
        cancel_event_response_data = response_json['data']
        logger.debug('Event cancelation response data: \n%s', json_dumps(cancel_event_response_data))

    def implicit_cancel(self):
        event_id = self.get_event_id()
        ven_name = self.oadr_event['ven_name']
        response_json = vtnc.delete_event(event_id, ven_name)
        delete_event_response_data = response_json['data']
        logger.debug('Event deletion response data: \n%s', json_dumps(delete_event_response_data))

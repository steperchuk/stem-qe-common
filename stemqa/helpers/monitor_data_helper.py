from datetime import timedelta

from stemqa.consts import S3_FOLDER_MONITOR_DATA_15_MIN
from stemqa.helpers.db_helper import DbHelper, DB_METER_LOGS
from stemqa.util.dataload_util import load_data
from stemqa.util.date_util import calculate_days_offset
from stemqa.config import logconfig

logger = logconfig.get_logger(__name__)


class MonitorFifteenMinData:
    def __init__(self, log_file_name, first_date_record_in_log):
        """
        :param log_file_name: file name without csv extension
        :param first_date_record_in_log: ate/time value of the first log record
        """
        self.log_file_name = log_file_name
        self.first_date_record_in_log = first_date_record_in_log

    def import_monitor_data(self, location_code, log_start_date, days_offset, import_solar):
        """
        Imports meter data to the meter_logs.monitor_data_15_minute table in db
        :param location_code: site location code
        :param log_start_date: date/time from which log records should be started in db
        :param days_offset: offset in days defines days count in the past for which logs would be imported
        :param import_solar: bool. Define if solar data would be imported
        :return:
        """
        logger.debug("Loading meter data for site")
        logger.info('Import 15 min meter log data for {location}.'.format(location=location_code))
        suffixes = ['site', 'main', 'inverter']
        if import_solar:
            suffixes.append('solar')

        for suffix in suffixes:
            s3_file = '{s3_folder}/{file_name}_{suffix}.csv'.format(s3_folder=S3_FOLDER_MONITOR_DATA_15_MIN,
                                                                    file_name=self.log_file_name,
                                                                    suffix=suffix.lower())
            logger.debug("Load data from file {file_name}".format(file_name=s3_file))
            monitor_id = 'TARGET-{suffix}'.format(suffix=suffix.lower())
            new_monitor_id = 'UNINIT_EMUL_{location}-{suffix}'.format(location=location_code, suffix=suffix.lower())
            if suffix == 'solar':
                # monitor id for solar differs
                new_monitor_id = '{location}-as0001'.format(location=location_code)

            options = {
                "database": DB_METER_LOGS,
                "table": "monitor_data_15_minute",
                "transform": {"monitor_id": monitor_id, "new_monitor_id": new_monitor_id}
            }

            if days_offset != 0:
                days_offset = calculate_days_offset(end_time=log_start_date,
                                                    start_time=self.first_date_record_in_log,
                                                    offset=days_offset)

                options['transform']['time_offset'] = days_offset
                options['transform']['time_offset_unit'] = 'days'

            load_data(s3_file, options, False)

    @staticmethod
    def delete_all_monitor_data(location_code):
        """
        Deletes all meter data records from meter_logs.monitor_data_15_minute table for specific location code
        :param location_code: site location code
        :return: query result
        """
        dbhelper = DbHelper.default_instance()
        logger.debug("Deleting all monitor data for {location} location.".format(location=location_code))
        sql_query = "DELETE FROM meter_logs.monitor_data_15_minute WHERE monitor_id like '%{location}%';". \
            format(location=location_code)
        dbhelper.query(DB_METER_LOGS, sql_query)

    @staticmethod
    def delete_monitor_data(location_code, denominator=0, log_end_date=None, offset=None, delete_solar=False):
        """
        Deletes all or based on denominator records from meter_logs.monitor_data_15_minute table for specific location
        code, date and days in the past
        :param location_code: site location code
        :param denominator: used to calculate percentage of records to be deleted
        :param log_end_date: date starting from which data would be deleted
        :param offset: offset in days defines days count in the past for which logs would be deleted
        :param delete_solar: defines if solar data is need to be removed
        :return: query result
        """
        dbhelper = DbHelper.default_instance()
        suffixes = ['site', 'main', 'inverter']
        if delete_solar:
            suffixes.append('solar')

        for suffix in suffixes:
            monitor_id = 'UNINIT_EMUL_{location}-{suffix}%'.format(location=location_code, suffix=suffix)
            if suffix == 'solar':
                # monitor id for solar differs
                monitor_id = '{location}-as0001%'.format(location=location_code)

            logger.debug("Deleting monitor {location}-{suffix} location.".format(location=location_code, suffix=suffix))

            sql_query = "DELETE FROM meter_logs.monitor_data_15_minute WHERE id%{d} = 1 " \
                        "AND monitor_id like '{monitor_id}';". \
                format(d=denominator, monitor_id=monitor_id)

            if log_end_date:
                date = log_end_date - timedelta(days=offset)
                logger.debug("Deleting monitor data before {date} for {location} location."
                             .format(location=location_code, date=log_end_date))

                sql_query = "DELETE FROM meter_logs.monitor_data_15_minute WHERE id%{d} = 1 " \
                            "AND monitor_id like '{monitor_id}' AND sample_end_datetime < '{date}';". \
                    format(d=denominator, monitor_id=monitor_id, date=date)
            dbhelper.query(DB_METER_LOGS, sql_query)

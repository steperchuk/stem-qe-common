from stemqa.consts import S3_WEATHER_DATA_FILE, TYPE_HISTORICAL, TYPE_FORECAST
from stemqa.helpers.db_helper import DbHelper, DB_PORTAL
from stemqa.rest.weather import WeatherSvcClient
from stemqa.config import logconfig

logger = logconfig.get_logger(__name__)


class WeatherData:

    def __init__(self, site_name, start_date, end_date, data_file_path=S3_WEATHER_DATA_FILE):
        """
        :param site_name: site name
        :param start_date: date/time value of the first log record
        :param end_date: date/time value of the last log record
        :param data_file_path: s3 file path for weather file in json format
        """
        self.site_name = site_name
        self.start_date = start_date
        self.end_date = end_date
        self.data_file_path = data_file_path
        self.stream_id = None
        self.weather_client = WeatherSvcClient()

    def upload(self, weather_type=TYPE_HISTORICAL):
        """
        Imports weather data to the portal.wea_history or portal.wea_forecast tables in db
        depending on weather type
        :param weather_type: str historical or forecast
        :return:
        """
        self.weather_client.upload_data(s3_file=self.data_file_path,
                                        site_name=self.site_name,
                                        start_date=self.start_date,
                                        end_date=self.end_date,
                                        weather_type=weather_type)

        # Need to get stream_id value after import to identify which records were imported
        self.stream_id = self.get_stream_id(weather_type)

    def delete(self, weather_type=TYPE_HISTORICAL, start_date=None, end_date=None, percent=100):
        """
        Removes weather data from portal.wea_history or portal.wea_forecast based on specified parameters
        :param weather_type: str historical or forecast
        :param start_date: date time from which records need to be removed
        :param end_date: date time to which records need to be removed
        :param percent: percentage of records to be deleted
        :return:
        """
        dbhelper = DbHelper.default_instance()
        table = self.get_table(weather_type)
        logger.debug("Removing {percent}% of weather data from {table}.".format(percent=percent, table=table))
        percent = float(percent) / 100

        sql_query = "DELETE FROM {weather_table} WHERE RAND() <= {percent};".format(weather_table=table,
                                                                                    percent=percent)

        if end_date and start_date:
            self.stream_id = self.get_stream_id(weather_type)
            logger.debug("Removing weather data before {start} and {end}.".format(start=start_date, end=end_date))

            sql_query = "DELETE FROM {weather_table} WHERE stream_id = '{stream_id}' AND obs_ts > '{start_date}' " \
                        "AND obs_ts < '{end_date}' AND RAND() <= {percent};".format(weather_table=table,
                                                                                    stream_id=self.stream_id,
                                                                                    start_date=start_date,
                                                                                    end_date=end_date,
                                                                                    percent=percent, )

        dbhelper.query(DB_PORTAL, sql_query)

    def delete_all(self):
        """
        Removes all weather data from portal.wea_forecast and portal.wea_history tables
        :return:
        """
        logger.debug("Deleting all weather data from portal.wea_forecast and portal.wea_history tables")
        self.delete(weather_type=TYPE_FORECAST)
        self.delete(weather_type=TYPE_HISTORICAL)

    def get_stream_id(self, weather_type):
        """
        Retrieves stream_id value from weather table based on weather type
        :param weather_type: str historical or forecast
        :return: str stream_id
        """
        dbhelper = DbHelper.default_instance()
        table = self.get_table(weather_type)
        sql_query = "SELECT stream_id FROM portal.{table} order by rev_start_ts desc;".format(table=table)
        result = dbhelper.query(DB_PORTAL, sql_query)[0]['stream_id']
        return result

    @staticmethod
    def get_table(weather_type):
        """
        Retrieves weather table name based of weather type
        :param weather_type: str historical or forecast
        :return: str weather table
        """
        if weather_type == TYPE_FORECAST:
            return 'wea_forecast'
        return 'wea_history'

import copy
import datetime as dt
import logging
import time

import isodate
from pymysql import InternalError

from stemqa.helpers.db_helper import DB_GRID_SERVICES
from stemqa.helpers.db_helper import DbHelper
from stemqa.util.grid_response_util import GridResponseClient
from stemqa.vpp.helper import WEB_URL, STR_FORMAT
from stemqa.vpp.helper import json_dumps, iso8601_utc_datetime, get_random_hex_string
from stemqa.vpp.openadr.common import DISPATCH_LAG
from stemqa.vpp.openadr.vtn_api import vtnc

logger = logging.getLogger(__name__)


class OadrEventException(Exception):
    pass


class OadrEvent(object):

    def __init__(self, name, data, program, dispatch_lag=DISPATCH_LAG, use_dispatch_lag=True):
        self.name = name
        self.data = copy.deepcopy(data)
        self.event_id = name + '::' + get_random_hex_string(10)
        self.program = program
        self.dispatch_lag = dispatch_lag
        self.use_dispatch_lag = use_dispatch_lag
        self.verify_data()
        self.complete_data()
        self.event = {}
        self.creation_response = False
        self.grid_events = []
        self.grid_events_fetched = False

    def toJSONshort(self):
        return 'OadrEvent(name=%s, event_id=%s)' % (self.name, self.event_id)

    def __str__(self):
        return self.toJSONshort()

    def toJSON(self):
        return {'__class__': 'OadrEvent',
                'name': self.name,
                'event_id': self.event_id,
                'data': self.data,
                'program': {'name': self.program['name'], 'ven_name': self.program['ven']['name']}
                }

    def __repr__(self):
        return json_dumps(self.toJSON())

    def fetch_matching_grid_events(self, timeout=80):
        oadr_event = self.get_event()
        wait_interval_seconds = 5
        wait_time = 0
        all_grid_events_found = False
        matching_grid_events = []
        event_intervals = [e for e in oadr_event['charge_payloads'] if e is not None and str(e).upper() != 'NONE']
        logger.debug('Event intervals expected as grid events: %s', event_intervals)
        with GridResponseClient(WEB_URL) as client:
            while not all_grid_events_found and wait_time <= timeout:
                program_grid_events = client.event_list(self.program['id'])['events']
                event_id = self.event_id
                for grid_event in list(program_grid_events.values()):
                    reference_id = grid_event['reference_id']
                    if reference_id and event_id in reference_id:
                        matching_grid_events.append(grid_event)
                if len(event_intervals) == len(matching_grid_events):
                    all_grid_events_found = True
                    # TODO extra checks comparing grid_events with oadr_event
                    #     (payload values, interval lenght, start/end datetime)
                    # raise OpenADREventException("Grid event %s property(%s) different than expected(%)")
                if not all_grid_events_found:
                    logger.debug('Fetched data is missing events. Waiting few seconds for events to get created')
                    time.sleep(wait_interval_seconds)
                    wait_time += wait_interval_seconds

        if not all_grid_events_found:
            logger.debug('Oadr event:\n%s', json_dumps(oadr_event))
            logger.debug('Program grid events:\n%s', json_dumps(program_grid_events))
            raise OadrEventException("Program grid events missing events to match with oadr event intervals")

        self.grid_events = matching_grid_events
        self.grid_events_fetched = True

    def get_grid_events(self, use_cached=True, timeout=80):
        if not self.event:
            raise OadrEventException("Oadr event has to be created(request sent) before grid events can be fetched")
        if not use_cached or not self.grid_events_fetched:
            self.fetch_matching_grid_events(timeout)
            return self.grid_events
        return self.grid_events

    def get_event(self):
        if not self.event:
            raise OadrEventException("Event creation request has not been sent yet or failed")
        return self.event

    def event_length_in_seconds(self):
        return self.event['interval_seconds'] * len(self.event['charge_payloads'])

    def event_length_in_min(self):
        return self.event_length_in_seconds / 60

    def get_utc_start_dt(self, lag=False):
        start_dt = isodate.parse_datetime(self.data['event_start_time'])
        if lag:
            start_dt -= dt.timedelta(seconds=self.dispatch_lag)
        return start_dt

    def get_utc_end_dt(self, lag=False):
        return self.get_utc_start_dt(lag=lag) + dt.timedelta(minutes=self.event_length_in_min())

    def complete_data(self):
        self.data.setdefault('event_id', self.event_id)
        self.data.setdefault('ven_name', self.program['ven']['name'])
        if 'interval_duration' in self.data:
            interval_td = isodate.parse_duration(self.data['interval_duration'])
            self.data['interval_seconds'] = int(interval_td.total_seconds())
            del self.data['interval_duration']
        assert self.data['interval_seconds'] % 60 == 0
        # if not self.data.get('event_start_delta') and self.use_dispatch_lag:
        if self.use_dispatch_lag:
            start_dt = isodate.parse_datetime(self.data['event_start_time'])
            start_dt -= dt.timedelta(seconds=self.dispatch_lag)
            self.data['event_start_time'] = iso8601_utc_datetime(start_dt)

    def verify_data(self):
        assert self.data.get('charge_payloads')
        assert self.data.get('event_start_time')  # or self.data.get('event_start_delta')

    def create(self, data=None, event_start_dt=None):
        if data:
            self.data = data
        if event_start_dt:
            self.data['event_start_time'] = iso8601_utc_datetime(event_start_dt)
        self.verify_data()
        self.complete_data()
        logger.debug('Creating event with:\n%s', json_dumps(self.data))
        creation_response = vtnc.create_event(**self.data)
        logger.debug('Creation response:\n%s', json_dumps(creation_response))
        self.creation_response = creation_response
        self.event = self.creation_response['data']  # TODO check if correct
        self.get_grid_events()

    def modify(self, data):
        mod_data = {
            'event_id': self.data['event_id'],
            'ven_name': self.program['ven']['name'],
        }
        mod_data.update(data)
        logger.debug('Modify event data:\n%s', json_dumps(mod_data))
        modify_response = vtnc.modify_event(**mod_data)
        logger.debug('Event modification response:\n%s', json_dumps(modify_response))
        return modify_response

    def cancel(self, data=None):
        if self.event:
            response_json = vtnc.cancel_event(self.event_id, self.data['ven_name'])
            cancel_event_response_data = response_json['data']
            logger.debug('Event cancelation response data: \n%s', json_dumps(cancel_event_response_data))
            return cancel_event_response_data
        else:
            raise OadrEventException('Event has not been created yet')

    def delete(self, data=None):
        if self.event:
            response_json = vtnc.delete_event(self.event_id, self.data['ven_name'])
            delete_event_response_data = response_json['data']
            logger.debug('Event deletion response data: \n%s', json_dumps(delete_event_response_data))
            return delete_event_response_data
        else:
            raise OadrEventException('Event has not been created yet')


def event_time_info_old(testdata, event, grid_events, log=True):
    dispatch_lag = testdata['dispatch_lag']
    return event_time_info(event, grid_events, dispatch_lag, log)


def event_time_info(event, grid_events, dispatch_lag=DISPATCH_LAG, log=True):
    if log:
        logger.debug('')
        logger.debug(40 * '-')
        logger.debug('event: %s', event['event_id'])
        logger.debug('oadr event:\n%s', json_dumps(event))
        logger.debug('grid events:\n%s', json_dumps(grid_events))
    event_start_dt = isodate.parse_datetime(event['event_start_time'])
    event_duration_td = isodate.parse_duration(event['event_duration'])
    event_end_dt = event_start_dt + event_duration_td
    event_id = event['event_id']
    if log:
        logger.debug('Adding dispatch lag of %ss for correct interval data :', dispatch_lag)
    event_start_dt += dt.timedelta(seconds=dispatch_lag)
    event_end_dt += dt.timedelta(seconds=dispatch_lag)
    event_start = event_start_dt.strftime(STR_FORMAT)
    event_end = event_end_dt.strftime(STR_FORMAT)
    if log:
        logger.debug('Event `%s`: start: %s end: %s',
                     event_id, event_start, event_end)
    return event_start_dt, event_end_dt, event_duration_td


def event_cleanup(program_id=None):
    dbhelper = DbHelper.default_instance()
    logger.debug('Cleaning event data for program id: %s', program_id)

    sql = \
        '''
        delete from event_performance;
        delete from event_allocation;
        delete from event_queue;
        delete from event;
        delete from grid_service_event_received_user;
        delete from grid_service_event_received_status_transaction;
        '''
    dbhelper.query(DB_GRID_SERVICES, sql, )

    retries = 0
    while retries < 3:
        try:
            sql = \
                '''
                delete from resource_dispatch_transaction_log;
                '''
            dbhelper.query(DB_GRID_SERVICES, sql)

            sql = \
                '''
                delete from resource_dispatch_transaction;
                '''
            dbhelper.query(DB_GRID_SERVICES, sql)
        except InternalError:
            logger.debug("`resource_dispatch_transaction` table seems locked!")
            logger.debug("retrying in 10s")
            retries += 1
            if retries >= 3:
                raise
            else:
                time.sleep(10)
                continue
        else:
            break

    sql = \
        '''
        delete from grid_service_event_received;
        '''
    dbhelper.query(DB_GRID_SERVICES, sql)

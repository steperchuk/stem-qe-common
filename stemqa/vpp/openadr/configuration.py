import copy
import json
import logging
import os

import isodate
import yaml

from stemqa.vpp.helper import json_dumps, recursive_setdefault, \
    get_program_info, RESOURCES_PATH
from stemqa.vpp.openadr.common import HOURLY_FORECAST_ID, DAILY_FORECAST_ID

logger = logging.getLogger(__name__)

# DEFAULT CONFIGURATIONS
OADR_RESOURCES_PATH = os.path.join(RESOURCES_PATH, 'openadr')
DEFAULT_TESTDATA_PATH = os.path.join(OADR_RESOURCES_PATH, 'default_testdata.yaml')
DEFAULT_TESTDATA = yaml.load(open(DEFAULT_TESTDATA_PATH, 'r'))
DEFAULT_TEST_CONFIGURATION = 'AUSTIN_SHINES'


class OadrConfigurationError(Exception):
    pass


def add_program_info(testdata):
    """Adding program id to program settings in testdata.
    """
    prog_info = get_program_info(testdata['startup']['program']['name'])
    testdata['startup']['program']['id'] = prog_info['id']
    testdata['startup']['program']['info'] = prog_info
    logger.debug('Added Program info: %s', json_dumps(prog_info))


def display_test_configuration(testdata):
    """Displaying testdata configuration in nice json output
    """
    logger.debug('Test case: %s', testdata['id'])
    logger.debug('Testdata: %s', json_dumps(testdata))


def complete_and_verify_configuration(testdata):
    c_testdata = complete_testdata_with_defaults(testdata)
    program = c_testdata['startup']['program']
    resources = program['resources']
    default_resource_settings = program['default_resource_settings']
    for resource in resources:
        recursive_setdefault(resource, default_resource_settings)
    check_testdata_for_red_flags(c_testdata)
    complete_and_verify_events_verification(c_testdata)
    complete_and_verify_reports_verification(c_testdata)
    return c_testdata


def complete_testdata_with_defaults(testdata):
    testdata_copy = copy.deepcopy(testdata)
    configuration = testdata_copy.get('configuration')
    testdata_default = {}
    if configuration:
        testdata_default = copy.deepcopy(DEFAULT_TESTDATA[configuration])
        testdata_default = complete_testdata_with_defaults(testdata_default)
    recursive_setdefault(testdata_copy, testdata_default)
    return testdata_copy


def check_testdata_for_red_flags(testdata):
    verification_action_groups = testdata['verification']['action_groups']
    action_groups = testdata['scenario']['action_groups']

    valid_action_groups = ['reports', 'events', 'api', 'containers']
    assert all([action_group in valid_action_groups for action_group in action_groups])

    for v_action_group, object_names in list(verification_action_groups.items()):
        for action_object_name in object_names:
            assert action_object_name in action_groups[v_action_group], \
                'Action object `%s` does not exits' % action_object_name


def complete_and_verify_events_verification(testdata):
    default_settings = testdata['verification']['settings']['events']
    event_objects = testdata['scenario']['action_groups']['events']
    event_objects_verification = testdata['verification']['action_groups']['events']
    for event_name, verification in list(event_objects_verification.items()):
        logger.debug('  > CHECKING `%s` VERIFICATION SETTINGS!', event_name)
        recursive_setdefault(verification, default_settings)
        assert event_objects[event_name]
        assert 'verify' in verification
        assert 'verify_frame' in verification
        assert 'relative_tolerance' in verification
        assert 'absolute_tolerance_kw' in verification
        if not verification['verify']:
            logger.debug('  > VERIFICATION OF `%s` EVENT IS DISABLED!!!', event_name)


def complete_and_verify_reports_verification(testdata):
    default_settings = testdata['verification']['settings']['reports']
    report_objects = testdata['scenario']['action_groups']['reports']
    report_objects_verification = testdata['verification']['action_groups']['reports']
    for report_name, verification in list(report_objects_verification.items()):
        logger.debug('  > CHECKING `%s` VERIFICATION SETTINGS!', report_name)
        recursive_setdefault(verification, default_settings)
        create_data = report_objects[report_name]['create']['data']
        report_specifier_id = create_data['report_specifier_id']
        reporting_duration = isodate.parse_duration(create_data['report_interval_duration'])
        report_back_duration = isodate.parse_duration(create_data['report_back_duration'])

        verify = verification['verify']
        verify_frame = verification['verify_frame']
        if verify_frame:
            assert isinstance(verify_frame, list)
            assert len(verify_frame) == 2
        elif verify:
            assert not (report_back_duration and not reporting_duration), \
                'Report `%s` runs forever(use `verify_frame`!:\n%s' % (report_name, json_dumps(create_data))
        assert 'auto_verify_count' in verification
        assert 'auto_verify_schema' in verification
        assert 'auto_verify_values' in verification
        assert 'cancel_report_after_frame' in verification
        assert 'report_wait_lag' in verification
        assert 'relative_tolerance' in verification
        assert 'absolute_tolerance_kw' in verification
        assert 'expected_reports' in verification
        assert 'expected_reports_file' in verification
        assert 'expected_reports_have_metadata' in verification
        if not verification['verify']:
            logger.debug('  > VERIFICATION OF `%s` REPORTS IS DISABLED!!!', report_name)
        expected_reports = verification.get('expected_reports')
        expected_reports_file = verification.get('expected_reports_file')
        if expected_reports and expected_reports_file:
            raise OadrConfigurationError('`%s`: use only one of `expected_reports` or `expected_reports_file`'
                                         % report_name)
        if expected_reports_file:
            json_str = open(os.path.join(OADR_RESOURCES_PATH, expected_reports_file)).read()
            expected_reports = json.loads(json_str)
            verification['expected_reports'] = expected_reports

        if expected_reports and report_specifier_id in [HOURLY_FORECAST_ID, DAILY_FORECAST_ID]:
            raise NotImplementedError('Forecast reports cannot be testeed with static data.'
                                      'Only sanity checks are implemented currently(based on aggregation size)')

        if expected_reports:
            if not verification.get('expected_reports_have_metadata'):
                assert isinstance(expected_reports, list)
                assert isinstance(expected_reports[0], list)
                assert isinstance(expected_reports[0][0], dict)
            else:
                assert isinstance(expected_reports, list)
                assert isinstance(expected_reports[0], dict)

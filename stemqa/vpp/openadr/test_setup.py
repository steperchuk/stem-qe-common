import datetime as dt
import logging
import traceback

from pytz import UTC
from requests import ConnectionError

from stemqa.helpers.db_helper import DB_GRID_SERVICES, DbHelper
from stemqa.vpp.helper import json_dumps, round_time_up, STR_FORMAT
from stemqa.vpp.openadr.actions import verify_event_actions, verify_report_actions, create_container_actions, \
    create_event_actions, create_report_actions, run_actions
from stemqa.vpp.openadr.common import reset_ven_registration, delete_forecast_objects, \
    get_forecast_object_names, filter_forecasts_by_resources, delete_events_in_s4
from stemqa.vpp.openadr.configuration import display_test_configuration, complete_and_verify_configuration, \
    add_program_info
from stemqa.vpp.openadr.event import event_cleanup
from stemqa.vpp.openadr.report import create_forecast_objects
from stemqa.vpp.openadr.vtn_api import vtnc, vtn_sim, VENS
from stemqa.vpp.resource import teardown_resources, setup_resources, delete_resource_telemetry
from stemqa.vpp.telemetry import telemetry_data

logger = logging.getLogger(__name__)


def setup_external_reference_for_program(program_name, external_reference_id):
    dbhelper = DbHelper.default_instance()
    sql_reset = \
        '''
        UPDATE `program`
        SET `external_reference_id` = '',
            `reference_id` = ''
        WHERE `reference_id` = '%s';
        ''' % (external_reference_id)
    dbhelper.query(DB_GRID_SERVICES, sql_reset)

    sql = \
        '''
        UPDATE `program`
        SET `external_reference_id` = '%s',
            `reference_id` = '%s'
        WHERE `name` = '%s';
        ''' % (external_reference_id, external_reference_id, program_name)
    dbhelper.query(DB_GRID_SERVICES, sql)

    logger.debug("Setting external_reference_id '%s' for program '%s'", external_reference_id, program_name)


def set_discharge_filter(program_id, discharge_deny_times=None, discharge_deny_days=None):
    """ Deleting and setting program attributes that filter discharge limitation.
        extra info: JEDI-9996, JEDI-10202,
                    https://bitbucket.org/stemedge/stem-py/pull-requests/526

        example of discharge deny times program attribute value: '(datetime.time(13,0),datetime.time(20,0))'
        example of discharge deny days program attribute value:  '[0,1,2,3,4]'
    """
    dbhelper = DbHelper.default_instance()

    select_deny_times_sql = \
        '''
        SELECT item_id from program_attribute_items as pai
        WHERE pai.unique_id = 'GS_REPORT_DISCHARGE_DENY_TIMES';
        '''
    discharge_deny_times_item_id = dbhelper.query(DB_GRID_SERVICES, select_deny_times_sql)[0]['item_id']

    select_deny_days_sql = \
        '''
        SELECT item_id from program_attribute_items as pai
        WHERE pai.unique_id = 'GS_REPORT_DISCHARGE_DENY_DAYS';
        '''
    discharge_deny_days_item_id = dbhelper.query(DB_GRID_SERVICES, select_deny_days_sql)[0]['item_id']

    delete_sql = \
        '''
        DELETE FROM program_attributes
        WHERE item_id IN (%s, %s) AND
              program_id = %s;
        ''' % (discharge_deny_times_item_id, discharge_deny_days_item_id, program_id)

    dbhelper.query(DB_GRID_SERVICES, delete_sql)

    if discharge_deny_times:
        insert_deny_times_sql = \
            '''
            INSERT INTO `program_attributes`
                (`program_id`, `item_id`, `item_value`, `active`, `available`,
                `created_by`, `update_date`, `created_date`)
            VALUES
                (%s, %s, "%s", 1, 1, 2854, NOW(), NOW());
            ''' % (program_id, discharge_deny_times_item_id, discharge_deny_times)
        dbhelper.query(DB_GRID_SERVICES, insert_deny_times_sql)

    if discharge_deny_days:
        insert_deny_days_sql = \
            '''
            INSERT INTO `program_attributes`
                (`program_id`, `item_id`, `item_value`, `active`, `available`,
                `created_by`, `update_date`, `created_date`)
            VALUES
                (%s, %s, "%s", 1, 1, 2854, NOW(), NOW());
            ''' % (program_id, discharge_deny_days_item_id, discharge_deny_days)
        dbhelper.query(DB_GRID_SERVICES, insert_deny_days_sql)


def calculate_test_start_time(grace_time_seconds, minute_rounded=True):
    """Calculates and returns UTC start time based on current time and arguments.

    Parameters
    ----------
    grace_time_seconds : int
        minimal seconds needed for test environment to get stable
    minute_rounded : bool, optional
        Defaults to True because most test use 1min telemetry tables

    Returns
    -------
    datetime
        calculated test start time in UTC
    """
    test_start_dt = dt.datetime.now(tz=UTC) + dt.timedelta(seconds=grace_time_seconds)
    if minute_rounded:
        test_start_dt = round_time_up(test_start_dt, 60)
    return test_start_dt


def test_cleanup(program,
                 reset_ven=True,
                 restart_ven_container=False,
                 restart_vtn_container=False,
                 clear_telemetry=True,
                 cancel_report_requests=False,
                 clear_grid_events=False,
                 clear_s4_events=False,
                 clear_s4_reports=False,
                 stop_emulators=True,
                 **kwargs):
    """Running cleanup specification inside test configuration

    Used for cleanup ->  testdata['startup']['cleanup'])
         and teardown -> testdata['teardown'])

    Parameters
    ----------
    program : dict
        complete configuration for program. Needed for cleanup -> testdata['startup']['program']
    reset_ven : bool, optional
        clean ven info on vtn before testing starts
    restart_ven_container : bool, optional
        restart ven before testing starts
    restart_vtn_container : bool, optional
        restart vtn before testing starts
    clear_telemetry : bool, optional
        delete telemetry for all resurces in program.
        Useful because of program reuse and test restarts.
    cancel_report_requests : bool, optional
        cancel report requests on ven
    clear_grid_events : bool, optional
        delete all grid events for tested program
    clear_s4_events : bool, optional
        delete all events stored in s4 for tested program
    clear_s4_reports : bool, optional
        delete all forecast reports stored in s4 for tested program resources
    stop_emulators : bool, optional
        stop all emulators that are defined in program resources
    """

    ven_name = program['ven']['name']
    ven = VENS[ven_name]
    resources = program['resources']
    try:
        if not kwargs.get('vtn_sim_optional'):
            if not vtn_sim.responds():
                vtn_sim.restart()

        if cancel_report_requests:
            logger.debug('Cancelling all pending report requests....')
            vtnc.cancel_all_pending_reports(ven_name, kwargs.get('cancel_report_requests_wait', True))

        if restart_ven_container and ven.running():
            ven.restart()

        if not ven.running():
            ven.start()

        if reset_ven:
            vtnc.clear_ven(ven_name)

    except ConnectionError as e:
        logger.error('VTN simulator not running. Connection error. %s', e)
        if not kwargs.get('vtn_sim_optional'):
            raise

    # TODO check if VEN and VTN are alive (every test should start with alive containers) and start them if not
    if restart_vtn_container:
        vtn_sim.restart()

    if clear_grid_events:
        event_cleanup(program['id'])

    if stop_emulators:
        # teardown_all_emulators
        teardown_resources(resources)

    if clear_s4_events:
        delete_events_in_s4(program['id'])

    if clear_s4_reports:
        delete_forecast_objects(resources=resources)

    if clear_telemetry:
        for resource in resources:
            delete_resource_telemetry(resource)


def program_setup(name,
                  resources,
                  external_reference_id,
                  set_external_reference=True,
                  prevent_discharge_time_frame=None,
                  **kwargs):
    """Setting up all program configurations defined in testdata

    Usually passing in complete program settings dict (**testdata['startup']['program'])

    Parameters
    ----------
    name : str
        program name
    resources : list
        settings for resources(and emulators used) used by this program
    external_reference_id : str
        reference string that connects VEN with specifed program
    set_external_reference : bool, optional
        Usually same VEN is used for a lot of tests so we have to set reference on specified
        program and clear reference for other programs that represent same program type.
    """

    logger.debug('Unused program setting: %s', json_dumps(list(kwargs.keys())))
    if set_external_reference:
        setup_external_reference_for_program(name, external_reference_id)

    logger.warning('prevent_discharge_time_frame is not applied(WIP): %s', prevent_discharge_time_frame)
    # TODO prevent_discharge_time_frame

    setup_resources(resources)


def create_forecast_files(program, daily=0, hourly=0, use_cached=False, **kwargs):
    logger.debug('create_forecast_files -> days: %s, hours: %s, use_cached: %s', daily, hourly, use_cached)
    resources = program['resources']
    cached_reports = filter_forecasts_by_resources(get_forecast_object_names(), resources)
    logger.debug('create_forecast_files -> cached_reports (len: %s): \n%s',
                 len(cached_reports), json_dumps(cached_reports))
    expected_cached_forecasts = (daily + hourly) * len(resources)
    logger.debug('create_forecast_files -> expected_len: %s', expected_cached_forecasts)
    cached = len(cached_reports) >= expected_cached_forecasts
    logger.debug('create_forecast_files -> cached: %s', cached)
    if (not cached or not use_cached) and (hourly or daily):
        delete_forecast_objects()
        create_forecast_objects(program, daily, hourly, **kwargs)
    elif not use_cached:
        delete_forecast_objects()


def create_scenario_actions(scenario, start_dt, program, verbose=1):
    """creating all actions for test scenario

    Parameters
    ----------
    scenario : dict
        configuration of action groups fetched like -> testdata['scenario']
    verbose : int, optional
        0 -> no logging of actions created. 1 -> minimalistic, 2 -> full

    Returns
    -------
    [dict]
        {action_group: {action_object: [*actions]}}
    """
    action_groups = {}
    action_objects = {}
    if scenario['containers']:
        container_actions, containers = create_container_actions(scenario['containers'], start_dt, verbose)
        action_groups['containers'] = container_actions
        action_objects['containers'] = containers
    if scenario['events']:
        event_actions, events = create_event_actions(program, scenario['events'], start_dt, verbose)
        action_groups['events'] = event_actions
        action_objects['events'] = events
    if scenario['reports']:
        report_actions, reports = create_report_actions(program, scenario['reports'], start_dt, verbose)
        action_groups['reports'] = report_actions
        action_objects['reports'] = reports
    return action_groups, action_objects


def generic_oadr_test_setup(testdata, custom_report_test=None, custom_event_test=None,
                            telemetry_debug=True, teardown=True):
    """Generic test setup that is driven by uniform testdata specification for openADR testing.

        Sets emulators, imports required data and other required settings based on testdata.
        CreatesopenADR events reports defined in testdata.
        Manipulates with containers/pods at specifed test offsets if demanded.
        It waits for test time specified for events and reports before it starts with their verification.
        Non critical test fails get collected and raised after test ends.
        or until events and reports finish. After that report updates for every report
        are tested with custom report testing function provided.
    """
    # TODO check and verify complete testdata before starting program setup(emulators and actions take too long fail)
    logger.debug('>>> OPENADR TEST STARTUP')
    c_testdata = complete_and_verify_configuration(testdata)
    display_test_configuration(c_testdata)
    program = c_testdata['startup']['program']
    action_groups = c_testdata['scenario']['action_groups']
    verification_groups = c_testdata['verification']['action_groups']
    # testing start_time configuration
    dummy_start_dt = calculate_test_start_time(**c_testdata['start_time']['calculation'])
    # testing if actions can get created with provided testdata
    logger.debug('>>> CREATING DUMMY SCENARIO ACTIONS AND VERIFICATION (testing testdata)')
    dummy_action_groups, dummy_action_objects = create_scenario_actions(action_groups, dummy_start_dt, program,
                                                                        verbose=0)
    verify_event_actions(program, dummy_action_objects['events'], dummy_action_groups['events'],
                         verification_groups['events'], custom_event_test, only_test_settings=True)
    verify_report_actions(program, dummy_action_objects['reports'], dummy_action_groups['reports'],
                          verification_groups['reports'], custom_report_test,
                          base_start_dt=dummy_start_dt, only_test_settings=True)
    add_program_info(c_testdata)  # adding program info should be probably done after program setup setup
    # in case we change some settings or program is created dynamically!
    try:
        test_cleanup(program, **c_testdata['startup']['cleanup'])
        program_setup(**program)
        create_forecast_files(program, **c_testdata['startup']['forecast_files'])
        logger.debug('>>> CALCULATING TEST START TIME')
        start_dt = calculate_test_start_time(**c_testdata['start_time']['calculation'])
        logger.debug(' >> Scenario base start time is at: %s', start_dt.strftime(STR_FORMAT))
        if c_testdata['start_time']['set_start_as_registration_time']:
            # resetting ven registration resets delivered and received energy values
            # but it also cancels all events so we have to use it before we create events.
            logger.debug(' >> Setting registration time to test start time')
            reset_ven_registration(program['external_reference_id'], start_dt)

        # TODO telemetry callback actions (We have commented example before this startup function starts)
        logger.debug('>>> CREATING SCENARIO ACTIONS')
        # NOTE we could create verification actions based on verification frames and report/event end times.
        actions = []
        action_groups, action_objects = create_scenario_actions(action_groups, start_dt, program, verbose=1)
        logger.debug(" >> ACTION GROUPS: \n%s", json_dumps(action_groups, short=True))
        # flattening dictionaries of action groups into list of all actions to be executed
        for action_group in list(action_groups.values()):
            for object_actions in list(action_group.values()):
                actions.extend(object_actions)
        run_actions(actions)
        logger.debug('>>> VERIFICATION OF SCENARIO ACTIONS')
        # TODO if we want to quickly test event verification logic we have to import simulated telemetry
        #     instead of running complete simulation with emulators
        if action_groups.get('events'):
            verify_event_actions(program, action_objects['events'], action_groups['events'],
                                 verification_groups['events'], custom_event_test)
        # TODO for quick test of report verification logic we need to import scenario telemetry
        #     and reduce start grace period and report request time.
        if action_groups.get('reports'):
            verify_report_actions(program, action_objects['reports'], action_groups['reports'],
                                  verification_groups['reports'], custom_report_test, base_start_dt=start_dt)
    finally:
        teardown_generic_test_setup(c_testdata, telemetry_debug, teardown)


def teardown_generic_test_setup(testdata, telemetry_debug, teardown):
    program = testdata['startup']['program']
    resources = program['resources']
    if telemetry_debug:
        logger.debug('\n\n\n>>> TELEMETRY\n' + 50 * '<<<' + '\n' + 50 * '<<<')
        try:
            telemetry_data(resources)
        except Exception as e:  # pylint: disable=W0703
            logger.error('Final telemetry/dispatch data query failed: %s\n%s', e, traceback.format_exc())

    if teardown:
        logger.debug('\n\n\n>>> TEARDOWN\n' + 50 * '<<<' + '\n' + 50 * '<<<')
        try:
            teardown_settings = testdata['teardown']
            test_cleanup(program, **teardown_settings)
        except Exception as e:  # pylint: disable=W0703
            logger.error('Test teardown failed: %s\n%s', e, traceback.format_exc())

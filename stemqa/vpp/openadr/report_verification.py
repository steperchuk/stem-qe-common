import datetime as dt
import difflib
import logging
import time

import isodate
import pytz
# plugin that gives you assume function for collecting errors instead of asserting
# https://github.com/astraw38/pytest-assume
from pytest import assume  # pylint: disable=E0611

from stemqa.vpp.helper import json_dumps, round_time_up, dt_utcnow, STR_FORMAT
from stemqa.vpp.openadr.common import DAILY_FORECASTS_PREFIX, HOURLY_FORECASTS_PREFIX, HOURLY_FORECAST_ID, \
    DAILY_FORECAST_ID, CHARGE_CURVE_ID, DISCHARGE_CURVE_ID, CURRENT_STATUS_ID, NAMEPLATE_ID, \
    get_forecast_object_names, filter_forecasts_by_resources, parse_report_update, parse_report_updates, \
    clean_report_updates, miliseconds_timestamp
from stemqa.vpp.openadr.report import get_forecast_intervals, get_expected_hourly_forecast
from stemqa.vpp.telemetry import telemetry_data

logger = logging.getLogger(__name__)


def verify_expected_reports(report_object, report_updates, verification):
    expected_reports = verification.get('expected_reports')
    if verification.get('expected_reports_have_metadata'):
        cleaned_updates = clean_report_updates(report_updates)
        info = '\nREPORT: %s\nFULL_UPDATES: \n%s\nCLEANED_EXPECTED_REPORTS: \n%s' % \
               (json_dumps(report_object), json_dumps(report_updates), json_dumps(expected_reports))
        diffs = '\nDIFFS:\n'
        d = difflib.Differ()
        diffs += "\n".join(d.compare(json_dumps(cleaned_updates).splitlines(),
                                     json_dumps(expected_reports).splitlines()))
        assume(json_dumps(cleaned_updates) == json_dumps(expected_reports),
               "Report updates don't match expected reports:" + info + diffs)
    else:
        reports = parse_report_updates(report_updates)
        info = '\nREPORT: %s\nPARSED_UPDATES: %s\nEXPECTED_REPORTS: %s' % \
               (json_dumps(report_object), json_dumps(reports), json_dumps(expected_reports))
        diffs = '\nDIFFS:\n'
        d = difflib.Differ()
        diffs += "\n".join(d.compare(json_dumps(reports).splitlines(), json_dumps(expected_reports).splitlines()))
        assume(json_dumps(reports) == json_dumps(expected_reports),
               "Report updates don't match expected reports:" + info + diffs)


def verify_report_schema(report_object, report_updates, report_verification):
    specifier_payloads = report_object.data.get('specifier_payloads')
    rep_req = report_object.report_request
    if specifier_payloads:
        logger.debug('Checking if requested specifier payloads match response')
        assume(rep_req['specifier_payloads'] == specifier_payloads,
               'Report request specifier mismatch: %s\n%s' % (specifier_payloads, json_dumps(rep_req)))
    else:
        specifier_payloads = rep_req['specifier_payloads']

    for rep_update in report_updates:
        update_info = json_dumps(rep_update) + '\n' + json_dumps(rep_req)
        assume(rep_update['intervals'], 'specifier payloads check -> NO interval data in report: %s' % update_info)
        if not rep_update['intervals']:
            continue
        for idx, interval in enumerate(rep_update['intervals']['interval']):
            uid = int(interval['uid']['text'])
            assume(idx == uid, 'Report interval uid mismatch. Expected %s but found %s -> %s ' %
                   (idx, uid, update_info))
            assume(set(specifier_payloads) == set(interval['oadr_report_payload'].keys()),
                   'Specifier payloads are not matching report request -> %s' % update_info)


def verify_report_count(report_object, report_updates, report_verification, end_dt):
    """Generic report verification
       Proper auto calculation of expected counts, which can get complicated if specifics are not known.
       Especially with start time in the past and small report back values, which are very useful, this
       calculation has to be done dynamically for precise estimation of counts. It also calls function for
       verification of proper signal names and value types, based on report request info. There is no
       verification if values are corresponding to actual state, it is just confirming that we are implementing
       openadr reports properly.
    """
    expected_count = report_verification.get('expected_count')
    max_expected_count = report_verification.get('max_expected_count', expected_count)
    report_created_dt = report_object.report_request_created_dt
    rep_req_data = report_object.report_request
    if expected_count is None:
        rep_req = report_object.report_request
        report_back_duration = isodate.parse_duration(rep_req['report_back_duration'])
        report_back_seconds = report_back_duration.total_seconds()
        if report_back_seconds == 0:
            expected_count = 1
            max_expected_count = 1
        else:
            report_start_dt = isodate.parse_datetime(rep_req['report_interval_start'])
            report_duration_td = isodate.parse_duration(rep_req['report_interval_duration'])
            report_duration_seconds = report_duration_td.total_seconds()
            report_started = end_dt > report_start_dt

            if report_created_dt > report_start_dt:
                start_diff = (report_created_dt - report_start_dt).total_seconds()
            else:
                start_diff = 0

            if not report_started:
                expected_count = 0
                max_expected_count = 0
            else:
                updates_end_dt = end_dt
                if report_duration_seconds:
                    if end_dt < report_start_dt + report_duration_td:
                        updates_end_dt = end_dt
                    else:
                        updates_end_dt = report_start_dt + report_duration_td

                logger.debug('AUTO CALCULATING EXPECTED REPORT COUNT...')
                logger.debug(' - REPORT_START_DT: %s', report_start_dt)
                logger.debug(' - REPORT_CREATED_DT: %s', report_created_dt)
                logger.debug(' - CREATED_AFTER_REPORT_START_SECONDS: %s', start_diff)
                logger.debug(' - REPORT_DURATION_SECONDS: %s', report_duration_seconds)
                logger.debug(' - REPORT_BACK_SECONDS: %s', report_back_seconds)
                logger.debug(' - UPDATES_DT: %s', updates_end_dt)
                reporting_seconds = (updates_end_dt - report_start_dt).total_seconds() - start_diff
                logger.debug(' - REPORTING_SECONDS: %s', reporting_seconds)
                expected_count = 1  # initial report
                seconds_until_second_update = report_back_seconds - (start_diff % report_back_seconds)
                logger.debug(' - SECONDS_UNTIL_SECOND_UPDATE: %s', seconds_until_second_update)
                if reporting_seconds > seconds_until_second_update:
                    expected_count += 1  # second report
                    # other reports to follow
                    logger.debug(' - EXPECTED BEFORE FINAL CALCS: %s', expected_count)
                    logger.debug(' - REPORTING_SECONDS_LEFT: %s', (reporting_seconds - seconds_until_second_update))
                    expected_count += (reporting_seconds - seconds_until_second_update) // report_back_seconds
                    logger.debug(' - EXPECTED AFTER FINAL CALCS: %s', expected_count)
                max_expected_count = expected_count + 1  # in case we are very close to next update
    actual_count = len(report_updates)
    logger.debug('ACTUAL COUNT: %s', actual_count)
    logger.debug('EXPECTED COUNT: %s', int(expected_count))
    logger.debug('MAX EXPECTED COUNT: %s', int(max_expected_count))
    assume(actual_count >= expected_count,
           'report update count (%s) smaller than expected (%s): rep_req_data: %s' %
           (actual_count, expected_count, json_dumps(rep_req_data)))
    assume(actual_count <= max_expected_count,
           'report update count (%s) bigger than expected (%s) rep_req_data: %s' %
           (actual_count, max_expected_count, json_dumps(rep_req_data)))


def verify_nameplate_reports(program, report_object, report_updates, report_verification):
    info = '\nREPORT: %s\nUPDATES: %s\n' % (json_dumps(report_object), json_dumps(report_updates))
    if report_verification.get('expected_reports'):
        verify_expected_reports(report_object, report_updates, report_verification)
    else:
        reports = parse_report_updates(report_updates)
        for report in reports:
            assume(len(report) == 1,
                   '[x-NAMPLATE] report should only have one set of values:' + info)
            charge_power = report[0]['x-NAMEPLATE_CHARGE_POWER_MAX']
            assume(charge_power == program['info']['system_size_power'],
                   '[x-NAMEPLATE_CHARGE_POWER_MAX] does not match program size:' + info)
            discharge_power = report[0]['x-NAMEPLATE_DISCHARGE_POWER_MAX']
            assume(discharge_power == program['info']['system_size_power'],
                   '[x-NAMEPLATE_DISCHARGE_POWER_MAX] does not match program size:' + info)
            energy_capacity = report[0]['x-NAMEPLATE_ENERGY_CAPACITY']
            assume(energy_capacity == program['info']['system_size_energy'],
                   '[x-NAMEPLATE_ENERGY_CAPACITY] does not match program size:' + info)


def verify_charge_curves(program, report_object, report_updates, report_verification):
    report_specifier_id = report_updates[0]['report_specifier_id']
    assume(report_specifier_id in [CHARGE_CURVE_ID, DISCHARGE_CURVE_ID])
    info = '\nREPORT: %s\nUPDATES: %s\n' % (json_dumps(report_object), json_dumps(report_updates))
    if report_verification.get('expected_reports'):
        verify_expected_reports(report_object, report_updates, report_verification)
    else:
        rep_req = report_object.report_request
        system_max_power = program['info']['system_size_power']
        system_max_energy = program['info']['system_size_energy']
        prev_soc = 0
        prev_value = 0
        for ridx, report_update in enumerate(report_updates):
            report_info = 'REPORT INFO [created: %s][Idx: %s]:\n[Request info] : %s\n' % \
                          (report_update['created_date_time'], ridx, json_dumps(rep_req))
            # logger.debug(json_dumps(report_update))
            report_intervals = parse_report_update(report_update)
            intervals_length = len(report_intervals)
            for idx, interval in enumerate(report_intervals):
                info = 'INTERVAL IDX: %s\nREPORT:\n%s\n%s' % (idx, json_dumps(report_update), report_info)
                curve_soc = interval['x-CURVE_SOC']
                curve_value = interval['x-CURVE_VALUE']
                if idx == 0:
                    assume(curve_soc == 0)
                    if report_specifier_id == CHARGE_CURVE_ID:
                        assume(curve_value == -system_max_power, info)
                    else:
                        assume(curve_value == 0, info)
                    prev_soc = curve_soc
                    prev_value = curve_value
                    continue
                if idx == intervals_length - 1:
                    assume(curve_soc == system_max_energy, info)
                    if report_specifier_id == CHARGE_CURVE_ID:
                        assume(curve_value == 0, info)
                    else:
                        assume(curve_value == system_max_power, info)

                assume(curve_soc >= prev_soc, info)
                if report_specifier_id == CHARGE_CURVE_ID:
                    assume(prev_value <= curve_value, info)
                else:
                    assume(prev_value <= curve_value, info)

                prev_soc = curve_soc
                prev_value = curve_value


def verify_current_status_reports(program, report_object, report_updates, report_verification, base_start_dt):
    expected_reports = report_verification.get('expected_reports')
    rel_tol = report_verification.get('relative_tolerance', 0.05)
    abs_tol = report_verification.get('absolute_tolerance_kw', 2.5)
    system_max_power = program['info']['system_size_power']
    system_max_energy = program['info']['system_size_energy']
    resources = program['resources']

    # TODO investigate program specific problems!? (QA_OADR_PROG16-21 - same set of resource configuration)
    if system_max_energy == 237.6:
        system_max_energy = 240
        derated_coef = 240 / 237.6
    else:
        derated_coef = 0.99244

    rep_req = report_object.report_request
    rep_req_id = report_object.report_request_id
    report_back_duration = isodate.parse_duration(rep_req['report_back_duration'])
    report_back_seconds = report_back_duration.total_seconds()
    report_start_dt = isodate.parse_datetime(rep_req['report_interval_start'])
    logger.debug('Report start_time: %s', report_start_dt.strftime(STR_FORMAT))
    for idx, report_update in enumerate(report_updates):
        interval_delta = dt.timedelta(seconds=report_back_seconds * idx)
        round_down_delta = dt.timedelta(seconds=report_start_dt.second,
                                        microseconds=report_start_dt.microsecond)
        prev_minute_end_dt = report_start_dt - round_down_delta + interval_delta
        next_minute_start_dt = prev_minute_end_dt + dt.timedelta(minutes=1)
        if report_verification.get('forecasts_present', True):
            current_forecast_intervals = get_forecast_intervals(next_minute_start_dt, program['external_reference_id'],
                                                                program['resources'])
        else:
            current_forecast_intervals = []

        if report_back_seconds:
            report_update_dt = report_start_dt + (idx * report_back_duration)
        else:
            report_update_dt = report_start_dt
        report_specifier_id = report_update['report_specifier_id']
        assume(report_specifier_id == 'x-CURRENT_STATUS')
        report = parse_report_update(report_update)
        rep_info = 'IDX: %s  REQUEST_ID: %s  UPDATE_DATE_TIME: %s' % \
                   (idx, rep_req_id, report_update_dt.strftime(STR_FORMAT))
        info = '%s DATA:\n%s' % (rep_info, json_dumps(report))
        assume(len(report) == 1, 'x-CURRENT_STATUS report should only have one interval of values:' + info)
        logger.debug('\nCURRENT STATUS REPORT [' + rep_info + ']\n' + 120 * '=' + '\n' + json_dumps(report))
        data = report[0]
        real_power = data.get('x-REAL_POWER')
        reactive_power = data.get('x-REACTIVE_POWER')
        avaliable_charge_power = data.get('x-AVAILABLE_CHARGE_POWER')
        avaliable_discharge_power = data.get('x-AVAILABLE_DISCHARGE_POWER')
        avaliable_charge_energy = data.get('x-AVAILABLE_CHARGE_ENERGY')
        avaliable_discharge_energy = data.get('x-AVAILABLE_DISCHARGE_ENERGY')
        delivered_energy = data.get('x-DELIVERED_ENERGY')
        received_energy = data.get('x-RECEIVED_ENERGY')

        # simple program bound checks
        assume(real_power is not None and real_power <= system_max_power, info)
        assume(reactive_power == 0)
        assume(avaliable_charge_power is not None and avaliable_charge_power <= system_max_power, info)
        assume(avaliable_discharge_power is not None and avaliable_discharge_power <= system_max_power, info)
        assume(avaliable_charge_energy is not None and avaliable_charge_energy <= system_max_energy, info)
        assume(avaliable_discharge_energy is not None and avaliable_discharge_energy <= system_max_energy, info)

        delivered_energy_expected = 0
        received_energy_expected = 0
        # NOTE JEDI-8746 - we still need proper current_capacity_kwh
        #     and better derated namplate calculation... if possible

        parsed_telemetry = {}
        if not expected_reports:
            # NOTE delivered and received energy gets reset whit test_start_dt
            # NOTE we need to wait extra minute to make sure we get all telemetry data we need.
            logger.debug('Waiting extra minute to make sure we get all telemtry data(downsampling every 2 minutes)')
            time.sleep(60)
            telemetry_since_last_reset = telemetry_data(resources, base_start_dt, prev_minute_end_dt, log=True)
            telemetry_seconds = (prev_minute_end_dt - base_start_dt).total_seconds()
            telemetry_minutes = telemetry_seconds // 60
            # Testing only reports that were created 60s or more after test_start_dt
            # (we are restarting received and delivered energy at that time)
            if telemetry_minutes <= 0:
                continue
            for resource in resources:
                emulator = resource['emulator_hostname']
                res_tel_data = [row for row in telemetry_since_last_reset if row['hostname'] == emulator]
                telemetry_length_matches = len(res_tel_data) == telemetry_minutes
                if not telemetry_length_matches:
                    assume(telemetry_length_matches,
                           'Our telemetry data timeframe is not in sync with downsampled minute frames:'
                           '\ntest_start_dt: %s\nprev_min_end_dt%s\ntel_minutes: %s\ntelemetry_since_start: %s' %
                           (base_start_dt, prev_minute_end_dt, telemetry_minutes, telemetry_since_last_reset)
                           )
                parsed_telemetry[emulator] = res_tel_data

            # Auto testing: generating expected values based on last minute SOC and telemetry.
            derated_nameplate_kwh = system_max_energy * derated_coef
            approx_charge_energy_avg = 0
            approx_charge_energy_min = 0
            approx_charge_energy_max = 0
            approx_charge_power = 0
            approx_discharge_energy_avg = 0
            approx_discharge_energy_min = 0
            approx_discharge_energy_max = 0
            approx_discharge_power = 0
            aggr = program['info']['aggregations'][0]
            aggr_soc_avg = 0
            aggr_soc_max = 0
            aggr_soc_min = 0
            for resource in resources:
                emulator = resource['emulator_hostname']
                res_soc_avg = parsed_telemetry[emulator][-1]['current_charge_avg']
                res_soc_min = parsed_telemetry[emulator][-1]['current_charge_min']
                res_soc_max = parsed_telemetry[emulator][-1]['current_charge_max']

                # NOTE snippet in case we would need SOC at the start or end of 1min interval
                # if parsed_telemetry[emulator][-1]['kw_total_sum'] > 0:
                #    res_soc = res_soc_min
                # else:
                #    res_soc = res_soc_max

                location = resource['target_location']
                location_energy = aggr['locations'][location]['size_kwh']
                location_power = aggr['locations'][location]['size_kw']
                aggr_soc_avg += res_soc_avg * (location_energy / system_max_energy)
                aggr_soc_max += res_soc_max * (location_energy / system_max_energy)
                aggr_soc_min += res_soc_min * (location_energy / system_max_energy)

                # Reporting avaibale charge energy only if resource can charge
                max_avail_charge_power = resource['setpoint'] - parsed_telemetry[emulator][-1]['site_load_avg']
                charge_power = min(max_avail_charge_power, location_power)
                if charge_power > 0 and res_soc_min <= 0.95:
                    approx_charge_power += charge_power
                    approx_charge_energy_avg += (1 - res_soc_avg) * (location_energy * derated_coef)
                    approx_charge_energy_min += (1 - res_soc_min) * (location_energy * derated_coef)
                    approx_charge_energy_max += (1 - res_soc_max) * (location_energy * derated_coef)

                if res_soc_min >= 0.05:
                    approx_discharge_power += location_power
                    approx_discharge_energy_avg += res_soc_avg * (location_energy * derated_coef)
                    approx_discharge_energy_min += res_soc_min * (location_energy * derated_coef)
                    approx_discharge_energy_max += res_soc_max * (location_energy * derated_coef)

            # Simple check for JEDI-8746 and JEDI-8782
            if aggr_soc_min < 0.001 and aggr_soc_max < 0.001:
                assume(avaliable_discharge_energy == 0,
                       'avaliable_discharge_energy error\n%s' % info)
            if aggr_soc_max > 0.999 and aggr_soc_max < 0.999:
                assume(avaliable_charge_energy < derated_nameplate_kwh * 0.0011,
                       'avaliable_charge_energy error\n%s' % info)

            approx_charge_energy = approx_charge_energy_avg
            approx_discharge_energy = approx_discharge_energy_avg

            for resource_t in list(parsed_telemetry.values()):
                for minute_t in resource_t:
                    avg_power = minute_t['mdm.kw_total_sum/mdm.sample_count']
                    if avg_power > 0:
                        received_energy_expected += abs(avg_power) / 60
                    else:
                        delivered_energy_expected += abs(avg_power) / 60

            real_power_expected = - sum(
                x[-1]['mdm.kw_total_sum/mdm.sample_count'] for x in list(parsed_telemetry.values()))

            # New logic that uses forecast values to filter available power values.
            # ##############################################################################
            f_approx_discharge_power = 0
            f_approx_discharge_energy = 0
            f_approx_charge_power = 0
            f_approx_charge_energy = 0
            for interval_data in current_forecast_intervals:
                f_approx_discharge_power += interval_data['discharge_kw']
                f_approx_discharge_energy += interval_data['discharge_kwh']
                f_approx_charge_power += interval_data['charge_kw']
                f_approx_charge_energy += interval_data['charge_kwh']

            # Taking minimal value from forecasted and current capacity values
            # ##############################################################################
            # approx_discharge_power = min(f_approx_discharge_power, approx_discharge_power)
            # approx_discharge_energy = min(f_approx_discharge_energy, approx_discharge_energy)
            # approx_charge_power = min(f_approx_charge_power, approx_charge_power)
            # approx_charge_energy = min(f_approx_charge_energy, approx_charge_energy)
            approx_discharge_power = 0 if not f_approx_discharge_power else approx_discharge_power
            approx_discharge_energy = 0 if not f_approx_discharge_energy else approx_discharge_energy
            approx_charge_power = 0 if not f_approx_charge_power else approx_charge_power
            approx_charge_energy = 0 if not f_approx_charge_energy else approx_charge_energy
        else:
            # testing based on expected values provided with testdata
            expected = expected_reports[idx][0]
            approx_charge_energy = expected["x-AVAILABLE_CHARGE_ENERGY"]
            approx_charge_power = expected["x-AVAILABLE_CHARGE_POWER"]
            approx_discharge_energy = expected["x-AVAILABLE_DISCHARGE_ENERGY"]
            approx_discharge_power = expected["x-AVAILABLE_DISCHARGE_POWER"]
            delivered_energy_expected = expected["x-DELIVERED_ENERGY"]
            received_energy_expected = expected["x-RECEIVED_ENERGY"]
            real_power_expected = expected["x-REAL_POWER"]

        logger.debug('Exp. charge power: %s', approx_charge_power)
        logger.debug('Rep. charge power: %s', avaliable_charge_power)
        charge_power_diff = abs(approx_charge_power - avaliable_charge_power)
        assume(charge_power_diff < rel_tol * abs(avaliable_charge_power) or charge_power_diff < 5,
               'avaliable_charge_power error (delta: %s)\n%s' % (charge_power_diff, info))

        logger.debug('Exp. discharge power: %s', approx_discharge_power)
        logger.debug('Rep. discharge power: %s', avaliable_discharge_power)
        discharge_power_diff = abs(approx_discharge_power - avaliable_discharge_power)
        assume(discharge_power_diff < rel_tol * abs(avaliable_discharge_power) or discharge_power_diff < 5,
               'avaliable_discharge_power error (delta: %s)\n%s' % (discharge_power_diff, info))

        charge_energy_diff = abs(approx_charge_energy - avaliable_charge_energy)
        discharge_energy_diff = abs(approx_discharge_energy - avaliable_discharge_energy)
        logger.debug('Exp. charge energy: %s', approx_charge_energy)
        logger.debug('Rep. charge energy: %s', avaliable_charge_energy)
        assume(charge_energy_diff < rel_tol * approx_charge_energy or charge_energy_diff < abs_tol,
               'avaliable_charge_energy error (delta: %s),\n%s' % (charge_energy_diff, info))

        logger.debug('Exp. discharge energy: %s', approx_discharge_energy)
        logger.debug('Rep. discharge energy: %s', avaliable_discharge_energy)
        assume(discharge_energy_diff < rel_tol * approx_discharge_energy or discharge_energy_diff < abs_tol,
               'avaliable_discharge_energy error (delta: %s),\n%s' % (discharge_energy_diff, info))

        # emulators + dispatch_lag + timing problems == occasional bigger diff in expected charge/discharge readings
        real_power_tol = 5
        # sign check: negative for charging, positive for discharging - JEDI-8829
        if real_power_expected >= 0 and real_power_expected > 1 and abs(real_power_expected) > real_power_tol:
            assume(real_power >= 0, 'real_power should be positive for discharing,\n%s' % info)
        elif real_power_expected < 1 and abs(real_power_expected) > real_power_tol:
            assume(real_power <= 0, 'real_power should be negative for charging,\n%s' % info)

        real_power_diff = abs(abs(real_power) - abs(real_power_expected))
        assume(real_power_diff < 0.1 * abs(real_power) or real_power_diff < real_power_tol,
               'real_power error (delta: %s),\n%s' % (real_power_diff, info))

        logger.debug('Exp. x-real_power: %s', real_power_expected)
        logger.debug('Rep. x-real_power: %s', real_power)

        # sign check: values should be positive - JEDI-8829
        assume(delivered_energy >= 0, 'delivered_energy is negative,\n%s' % info)
        assume(received_energy >= 0, 'received_energy is negative,\n%s' % info)

        delivered_diff = abs(delivered_energy - delivered_energy_expected)
        received_diff = abs(received_energy - received_energy_expected)
        assume(delivered_diff < rel_tol * abs(delivered_energy) or delivered_diff < 0.15,
               'delivered_energy error (delta: %s),\n%s' % (delivered_diff, info))
        assume(received_diff < rel_tol * abs(received_energy) or received_diff < 0.15,
               'received_energy error (delta: %s),\n%s' % (received_diff, info))
        logger.debug('Exp. delivered_energy: %s', delivered_energy_expected)
        logger.debug('Rep. delivered_energy: %s', delivered_energy)
        logger.debug('Exp. received_energy: %s', received_energy_expected)
        logger.debug('Rep. received_energy: %s', received_energy)

        logger.debug('\n %s \n\n', 120 * '=')


def verify_forecast_reports(program, report_object, report_updates, report_verification):
    program_timezone = program['info']['resource_timezone']
    program_tz = pytz.timezone(program_timezone)
    report_specifier_id = report_updates[0]['report_specifier_id']
    assume(report_specifier_id in [HOURLY_FORECAST_ID, DAILY_FORECAST_ID])
    max_power = program['info']['system_size_power']
    max_energy = program['info']['system_size_energy']
    rep_req_id = report_object.report_request_id
    specifier_payloads = report_object.report_request['specifier_payloads']

    external_reference_id = program['external_reference_id']
    resources = program['resources']
    daily_forecasts = get_forecast_object_names(
        DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    daily_forecasts = filter_forecasts_by_resources(daily_forecasts, resources)
    hourly_forecasts = get_forecast_object_names(
        HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    hourly_forecasts = filter_forecasts_by_resources(hourly_forecasts, resources)

    for ridx, report_update in enumerate(report_updates):
        created_dt = isodate.parse_datetime(report_update['created_date_time'])
        report_info = '\n[Report request Id: %s][Created: %s][Update Idx: %s]\n' % \
                      (rep_req_id, report_update['created_date_time'], ridx)
        assume(report_update['intervals'], 'Report interval data is missing' + report_info)
        if not report_update['intervals']:
            continue
        report = parse_report_update(report_update)
        intervals_length = len(report)
        if report_specifier_id == DAILY_FORECAST_ID:
            assume(intervals_length in (92, 96, 100),
                   'Length of forecast intervals is incorrect: %s %s' %
                   (intervals_length, report_info))
            local_created_dt = created_dt.astimezone(program_tz)
            expected_forecast_dt = local_created_dt
            interval_start_iso_dt = report_update['intervals']['interval'][0]['dtstart']['date-time']
            interval_start_dt = isodate.parse_datetime(interval_start_iso_dt)
            assert expected_forecast_dt == interval_start_dt.astimezone(program_tz)
            # NOTE s3 object format changed with JEDI-11729 bugfix
            # expected_iso_dt = isodate.datetime_isoformat(expected_forecast_dt)
            expected_dt_str = expected_forecast_dt.strftime(STR_FORMAT)
            logger.debug("Expected forecast dt: %s", expected_dt_str)
            dailies_used = [daily for daily in daily_forecasts if expected_dt_str in daily]
            assume(len(dailies_used) == len(resources),
                   'dailies mismatch: \nexpected_dt_str: %s\ndailies: %s, dailies_used: %s info: %s' %
                   (expected_dt_str, daily_forecasts, dailies_used, report_info))
            for idx, interval in enumerate(report):
                interval_info = 'INTERVAL INFO [Idx: %s]:\n[Interval data] : %s\n' % (idx, json_dumps(interval))
                info = report_info + interval_info
                # NOTE not calculated for forecast for now
                assume(interval['x-REAL_POWER'] == 0)
                if 'x-REACTIVE_POWER' in specifier_payloads:
                    assume(interval['x-REACTIVE_POWER'] == 0)
                assume(interval['x-AVAILABLE_DISCHARGE_POWER'] >= 0)
                assume(interval['x-AVAILABLE_DISCHARGE_POWER'] <= max_power,
                       'Discharge power is over max allowed' + info)
                assume(interval['x-AVAILABLE_DISCHARGE_ENERGY'] >= 0)
                assume(interval['x-AVAILABLE_DISCHARGE_ENERGY'] <= max_energy,
                       'Discharge energy is over max allowed' + info)
                assume(interval['x-AVAILABLE_CHARGE_POWER'] >= 0)
                assume(interval['x-AVAILABLE_CHARGE_POWER'] <= max_power,
                       'Charge power is over max allowed' + info)
                assume(interval['x-AVAILABLE_CHARGE_ENERGY'] >= 0)
                assume(interval['x-AVAILABLE_CHARGE_ENERGY'] <= max_energy,
                       'Charge energy is over max allowed' + info)

        else:
            assume(report_specifier_id == HOURLY_FORECAST_ID)
            assume(intervals_length == 16,
                   'Length of forecast intervals is incorrect: %s %s' %
                   (intervals_length, report_info))
            start_iso_dt = report_update['intervals']['interval'][0]['dtstart']['date-time']
            start_dt = isodate.parse_datetime(start_iso_dt)
            if report_update.get('dtstart'):
                assume(start_iso_dt == report_update['dtstart']['date-time'])
            else:
                # Special case for austin shines reports
                # dtstart is not included in reports (ask David Scott if he knows why)
                expected_forecast_dt = round_time_up(created_dt, hours=1)
                assume(start_dt == expected_forecast_dt)
            expected_iso_dt = isodate.datetime_isoformat(start_dt)
            hourlies_used = [hourly for hourly in hourly_forecasts if expected_iso_dt in hourly]
            assert len(hourlies_used) == len(resources)
            logger.debug('report_update: %s', json_dumps(report_update))
            expected_hourly = get_expected_hourly_forecast(expected_iso_dt,
                                                           program['external_reference_id'],
                                                           program['resources'])
            for idx, interval in enumerate(report):
                interval_start_iso_dt = report_update['intervals']['interval'][idx]['dtstart']['date-time']
                verify_forecast_interval(idx, interval, interval_start_iso_dt,
                                         expected_hourly, specifier_payloads, report_info)


def verify_forecast_interval(idx, interval, interval_start_iso_dt, expected_hourly,
                             specifier_payloads, report_info='', abs_tol=0.05, rel_tol=0.01):
    expected_interval = expected_hourly[idx]['interval']
    expected_timestamp = expected_hourly[idx]['timestamp'] - 1000
    expected_start_dt_str = dt.datetime.fromtimestamp(expected_timestamp / 1000).strftime(STR_FORMAT)
    interval_timestamp = miliseconds_timestamp(isodate.parse_datetime(interval_start_iso_dt))
    interval_info = 'Idx: %s, timestamp: %s, exp_timestamp: %s\n[actual] : %s\n[expected] : %s\n' \
                    % (idx, interval_timestamp, expected_timestamp,
                       json_dumps(interval), json_dumps(expected_interval))
    info = report_info + 'INTERVAL: ' + interval_info
    logger.debug('interval_actual_timestamp: %s (%s)', interval_timestamp, interval_start_iso_dt)
    logger.debug('interval_expected_timestamp: %s (%s)', expected_timestamp, expected_start_dt_str)
    assume(interval_timestamp == expected_timestamp, 'interval timestamp mismatch')

    for specifier in specifier_payloads:
        diff = abs(interval[specifier] - expected_interval[specifier])
        assume(interval[specifier] * rel_tol >= diff or abs_tol >= diff,
               "{} doesn't match:\n{}".format(specifier, info))


def verify_reports(program, report_object, report_updates, report_verification, base_start_dt, **kwargs):
    logger.debug('verify_reports -> extra kwargs: %s', kwargs)
    report_specifier_id = report_updates[0]['report_specifier_id']
    report_wait_lag = report_verification['report_wait_lag']
    verify_end_dt = dt_utcnow() - dt.timedelta(seconds=report_wait_lag)
    verify_frame = report_verification['verify_frame']
    if verify_frame:
        verify_start_dt = base_start_dt + dt.timedelta(seconds=verify_frame[0])
        verify_end_dt = base_start_dt + dt.timedelta(seconds=verify_frame[1])
        framed_report_updates = []
        for report_update in report_updates:
            created_dt_iso = report_update['created_date_time']
            created_dt = isodate.parse_datetime(created_dt_iso)
            if created_dt >= verify_start_dt and created_dt <= verify_end_dt:
                framed_report_updates.append(report_update)
        report_updates = framed_report_updates

    if report_verification['auto_verify_count']:
        verify_report_count(report_object, report_updates, report_verification, verify_end_dt)

    if report_verification['auto_verify_schema']:
        verify_report_schema(report_object, report_updates, report_verification)

    if report_verification['auto_verify_values'] or report_verification.get('expected_reports'):
        if report_specifier_id in [HOURLY_FORECAST_ID, DAILY_FORECAST_ID]:
            verify_forecast_reports(program, report_object, report_updates, report_verification)
        elif report_specifier_id in [CHARGE_CURVE_ID, DISCHARGE_CURVE_ID]:
            verify_charge_curves(program, report_object, report_updates, report_verification)
        elif report_specifier_id == CURRENT_STATUS_ID:
            verify_current_status_reports(program, report_object, report_updates, report_verification, base_start_dt)
        elif report_specifier_id == NAMEPLATE_ID:
            verify_nameplate_reports(program, report_object, report_updates, report_verification)
        else:
            assume(False, 'UNKNOWN `report_specifier_id`: %s' % report_specifier_id)

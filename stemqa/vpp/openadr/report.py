import copy
import datetime as dt
import io
import json
import logging
import os
import time

import isodate
import pytz
from pytz import UTC
from requests import ConnectionError
from requests import HTTPError

from stemqa.rest.powerqa import PowerQAClient
from stemqa.vpp.helper import json_dumps, iso8601_utcnow, round_time_up, get_random_hex_string, \
    STR_FORMAT, WEB_URL, dt_utcnow, get_program_info, s4c
from stemqa.vpp.openadr.common import HOURLY_FORECASTS_PREFIX, DAILY_FORECASTS_PREFIX, \
    get_daily_forecast_objects, get_hourly_forecast_objects, parse_report_updates, get_forecast_object_names, \
    filter_forecasts_by_resources, delete_forecast_objects, VPP_BUCKET, miliseconds_timestamp
from stemqa.vpp.openadr.configuration import OADR_RESOURCES_PATH
from stemqa.vpp.openadr.vtn_api import vtnc

logger = logging.getLogger(__name__)


class OadrReportRequestException(Exception):
    pass


class OadrReportRequest(object):

    def __init__(self, name, data, program):
        self.name = name
        self.data = copy.deepcopy(data)
        self.program = program
        self.report_request_id = name + ':' + get_random_hex_string(10)
        self.verify_data()
        self.complete_data()
        self.ven_response = False
        self.report_updates = None
        self.report_request = None
        self.report_request_sent_dt = None
        self.report_request_created_dt = None
        self.cancelled = False

    def toJSONshort(self):
        return 'OadrReportRequest(name=%s, report_request_id=%s)' % (self.name, self.report_request_id)

    def __str__(self):
        return self.toJSONshort()

    def toJSON(self):
        return {'__class__': 'OadrReportRequest',
                'name': self.name,
                'report_request_id': self.report_request_id,
                'data': self.data,
                'program': {'name': self.program['name'],
                            'ven_name': self.program['ven']['name']}
                }

    def __repr__(self):
        return json_dumps(self.toJSON())

    def complete_data(self):
        self.data.setdefault('report_request_id', self.report_request_id)
        self.data.setdefault('ven_name', self.program['ven']['name'])

    def verify_data(self):
        assert self.data.get('report_specifier_id')
        assert self.data.get('report_back_duration')
        assert self.data.get('report_interval_duration')

    def create(self, data=None):
        assert not self.report_request, 'Report creation for %s already called!' % self.report_request_id
        data = data or self.data
        self.data = data
        self.verify_data()
        self.complete_data()
        logger.debug('Creating report request with:\n%s', json_dumps(data))
        # TODO change into single request creation call after adding response verification
        self.report_request_sent_dt = dt_utcnow()
        report_requests, ven_response = vtnc.create_report_requests(data['ven_name'], [data])
        self.report_request_created_dt = dt_utcnow()
        logger.debug('report request data:\n%s', json_dumps(report_requests[0]))
        logger.debug('ven response:\n%s', json_dumps(ven_response))
        self.ven_response = ven_response
        self.report_request = report_requests[0]['response']

    def cancel(self, data=None):
        data = data or self.data
        if self.report_request:
            response = vtnc.cancel_report_requests(data['ven_name'], [data['report_request_id']])
            logger.debug('Report cancelation response: \n%s', json_dumps(response))
            self.cancelled = True
            return response
        else:
            raise OadrReportRequestException('Report has not been created yet')

    def get_report_updates(self, use_cached=False):
        if self.report_request:
            if use_cached:
                return self.report_updates
            self.report_updates = vtnc.get_report_updates(self.data['ven_name'], self.report_request_id)
            return self.report_updates
        else:
            raise OadrReportRequestException('Report has not been created yet')


def display_report_updates(ven_name, prev_updates=None, raise_on_error=True, parse_updates=True):
    prev_updates = prev_updates if prev_updates else {}
    reports_header = '\n'.join(['REPORT UPDATES INFO:', 120 * '='])
    parse_f = parse_report_updates if parse_updates else lambda x: x
    reports_info = ''
    try:
        current_updates = vtnc.get_report_updates(ven_name)
        for report_request, rep_updates in list(current_updates.items()):
            rep_req_name = '*** REPORT REQUEST `{}`'.format(report_request)
            if report_request not in prev_updates:
                rep_req_update = '* Initial report updates: \n{}'.format(
                    json_dumps(parse_f(rep_updates)))
            elif len(current_updates[report_request]) > len(prev_updates[report_request]):
                prev_l = len(prev_updates[report_request])
                rep_req_update = '* New report updates: \n{}'.format(
                    json_dumps(parse_f(rep_updates[prev_l:])))
            elif len(current_updates[report_request]) == len(prev_updates[report_request]):
                rep_req_update = '* No new updates.'
            else:
                rep_req_update = '* Less report updates than previously! (VTN HAS RESTARTED or BUG)\n'
                if raise_on_error:
                    raise Exception(rep_req_update)
            reports_info = '\n'.join([reports_info,
                                      rep_req_name,
                                      rep_req_update,
                                      120 * '='])
    except ConnectionError:
        if raise_on_error:
            raise
        logger.error('VTN simulator not running. Connection error.')
        return {}
    except Exception as e:  # pylint: disable=W0703
        if raise_on_error:
            raise
        import traceback
        err_info = '%s("%s")\n' % (type(e), e)
        err_info += traceback.format_exc() + '\n'
        logger.error('ERROR happened when retriving and parsing report updates from VTN-sim:\n%s',
                     err_info)
        return {}

    if not current_updates:
        reports_info += '* No report updates' + '\n'

    if len(current_updates) < len(prev_updates):
        error_info = '* Less report requests than previously! (VTN HAS RESTARTED or BUG)\n'
        error_info += '* prev_requests: %s\n' % json_dumps(prev_updates)
        error_info += '* current_requests: %s\n' % json_dumps(current_updates)
        reports_info += '\n' + error_info
        if raise_on_error:
            raise Exception(error_info)

    logger.debug('\n'.join([reports_header, reports_info]))
    return current_updates


def get_current_status_report_now(ven_name):
    logger.debug('Getting adhoc current status report...')
    report_requests = [{
        'ven_name': ven_name,
        'report_specifier_id': 'x-CURRENT_STATUS',
        'report_back_duration': 'P0D',
        'report_interval_duration': 'P0D',
        'granularity_duration': 'P0D',
        'report_interval_start': iso8601_utcnow()
    }]
    report_requests, _ = vtnc.create_report_requests(ven_name, report_requests, wait_for_ven_response=True)
    waited = 0
    report_updates = []
    rep_req_data = report_requests[0]
    rep_req = rep_req_data['response']
    request_id = rep_req['report_request_id']
    while waited <= 20:
        logger.debug('Waiting for current status report...')
        time.sleep(2)
        waited += 2
        try:
            report_updates = vtnc.get_report_updates(ven_name, request_id)
        except HTTPError:
            pass

        if report_updates:
            break

    assert report_updates, 'We got no report updates for current status'
    assert len(report_updates) == 1, 'We got more current status updates than requested'
    return report_updates[0]


def anti_deadlock_sleep(seconds=30):
    logger.debug('Sleeping %ss for forecast report creation task to complete', seconds)
    time.sleep(seconds)


def created_forecasts(external_reference_id, resources):
    dailies = get_forecast_object_names(DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    dailies_f = filter_forecasts_by_resources(dailies, resources)
    hourlies = get_forecast_object_names(HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    hourlies_f = filter_forecasts_by_resources(hourlies, resources)
    logger.debug('Daily forecast files found: %s', json_dumps(dailies_f))
    logger.debug('Hourly forecast files found: %s', json_dumps(hourlies_f))
    return dailies_f, hourlies_f


DAILY_TEMPLATE_PATH = os.path.join(OADR_RESOURCES_PATH, 'templates', 'daily_template.json')
HOURLY_TEMPLATE_PATH = os.path.join(OADR_RESOURCES_PATH, 'templates', 'hourly_template.json')
DAILY_TEMPLATE = json.loads(open(DAILY_TEMPLATE_PATH).read())
HOURLY_TEMPLATE = json.loads(open(HOURLY_TEMPLATE_PATH).read())


def modify_forecast_interval(interval, columns, idx, soc_energy, soc_pct, nameplate_power, nameplate_energy,
                             discharge_power, discharge_energy, charge_power, charge_energy, setpoint, average_load,
                             power_cmd):
    optimized_load_i = columns.index('optimized_load')
    site_load_i = columns.index('site_load')
    setpoint_i = columns.index('setpoint')
    power_cmd_i = columns.index('power_cmd')
    soc_i = columns.index('soc')
    nameplate_kw_i = columns.index('nameplate_kw')
    nameplate_kwh_i = columns.index('nameplate_kwh')
    soc_pct_i = columns.index('soc_pct')
    discharge_kw_i = columns.index('discharge_kw')
    discharge_kwh_i = columns.index('discharge_kwh')
    charge_kw_i = columns.index('charge_kw')
    charge_kwh_i = columns.index('charge_kwh')

    interval[power_cmd_i] = power_cmd
    interval[optimized_load_i] = average_load + interval[power_cmd_i]
    interval[site_load_i] = average_load
    interval[setpoint_i] = setpoint
    interval[soc_i] = soc_energy
    interval[nameplate_kw_i] = nameplate_power
    interval[nameplate_kwh_i] = nameplate_energy
    interval[soc_pct_i] = soc_pct
    interval[charge_kw_i] = charge_power
    interval[charge_kwh_i] = charge_energy
    interval[discharge_kw_i] = discharge_power
    interval[discharge_kwh_i] = discharge_energy
    logger.debug('Interval %s after modification: %s', idx, interval)


def create_dummy_forecasts(external_reference_id, program_name, resources, daily_iso_dts, hourly_iso_dts,
                           power_cmd=0, avg_load=None):
    logger.debug('Creating dummy forecasts')
    delete_forecast_objects()
    program = get_program_info(program_name)
    resources_info = program['aggregations'][0]['locations']
    for res in resources:
        res_loc = res['target_location']
        resource_i = resources_info.get(res_loc)
        resource_power = float(resource_i['size_kw'])
        resource_energy = float(resource_i['size_kwh'])
        res_soc_pct = float(res['batterysoc'])
        res_setpoint = float(res['setpoint'])
        # res_avg_load = float(res['avg_load'])
        if not avg_load:
            res_avg_load = res_setpoint - 1  # avg_load is used for site_load calculation
        if power_cmd is None:
            power_cmd = res_setpoint - res_avg_load
        res_soc_energy = resource_energy * res_soc_pct
        discharge_power = resource_power if res_soc_pct > 0.05 else 0
        charge_power = resource_power if res_soc_pct < 0.95 else 0
        discharge_energy = resource_energy * res_soc_pct
        charge_energy = resource_energy * (1 - res_soc_pct)
        # If site_load is bigger or same as setpoint we treat is same in capacity filtering as if power_cmd is negative
        if res_setpoint - res_avg_load <= 0 or power_cmd < 0:
            charge_power = discharge_power = charge_energy = discharge_energy = 0
        logger.debug('Location `%s` [power: %s, energy: %s]', res_loc, resource_power, resource_energy)
        for iso_dt in daily_iso_dts:
            expected_name = '%s/%s/%s' % (DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id),
                                          iso_dt, res_loc)
            logger.debug('Creating dummy forecast: %s', expected_name)
            modified_forecast = copy.deepcopy(DAILY_TEMPLATE)
            # NOTE bugfix JEDI-11792 changed s3 object path format(using local time in path)
            # utc_dt = isodate.parse_datetime(iso_dt)
            utc_dt = dt.datetime.strptime(iso_dt, STR_FORMAT)
            utc_dt = pytz.utc.localize(utc_dt)
            # local_dt = utc_dt.astimezone(program_tz)
            modified_forecast['datetime_str_utc'] = iso_dt
            modified_forecast['datetime_str_index'] = iso_dt
            # NOTE error in forecast (local dt presented in utc format)
            # modified_forecast['datetime_str_local'] = iso_dt
            modified_forecast['location_code'] = res_loc
            optimization_results = json.loads(modified_forecast['optimization_results'])
            data = optimization_results['data']
            index = optimization_results['index']
            columns = optimization_results['columns']
            for idx, interval in enumerate(data):
                interval_start_timestamp = miliseconds_timestamp(utc_dt + dt.timedelta(seconds=1, minutes=idx * 15))
                index[idx] = interval_start_timestamp
                # modify all intervals as if battery is constantly at 50% SOC
                modify_forecast_interval(interval, columns, idx,
                                         res_soc_energy, res_soc_pct, resource_power, resource_energy,
                                         discharge_power, discharge_energy, charge_power, charge_energy,
                                         res_setpoint, res_avg_load, power_cmd)
            modified_forecast['optimization_results'] = json.dumps(optimization_results)
            data = json.dumps(modified_forecast).encode()
            iodata = io.BytesIO(data)
            s4c.put_object(VPP_BUCKET, expected_name, iodata, len(data))

        for iso_dt in hourly_iso_dts:
            expected_name = '%s/%s/%s' % (HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id),
                                          iso_dt, res_loc)
            logger.debug('Creating dummy forecast: %s', expected_name)
            modified_forecast = copy.deepcopy(HOURLY_TEMPLATE)
            utc_dt = isodate.parse_datetime(iso_dt)
            modified_forecast['datetime_str_utc'] = iso_dt
            modified_forecast['datetime_str_index'] = iso_dt
            # NOTE bug in forecast file (local dt presented in utc format)
            # modified_forecast['datetime_str_local'] = iso_dt
            modified_forecast['location_code'] = res_loc
            optimization_results = json.loads(modified_forecast['optimization_results'])
            data = optimization_results['data']
            index = optimization_results['index']
            columns = optimization_results['columns']
            for idx, interval in enumerate(data):
                interval_start_timestamp = miliseconds_timestamp(utc_dt + dt.timedelta(seconds=1, minutes=idx * 15))
                index[idx] = interval_start_timestamp
                # modify all intervals as if battery is constantly at 50% SOC
                modify_forecast_interval(interval, columns, idx,
                                         res_soc_energy, res_soc_pct, resource_power, resource_energy,
                                         discharge_power, discharge_energy, charge_power, charge_energy,
                                         res_setpoint, res_avg_load, power_cmd)
            modified_forecast['optimization_results'] = json.dumps(optimization_results)
            data = json.dumps(modified_forecast).encode()
            iodata = io.BytesIO(data)
            s4c.put_object(VPP_BUCKET, expected_name, iodata, len(data))
    logger.debug('Created dummy forecasts')


# TODO update/merge with openadrutil_reports and refactor forecast tests with generic config
def create_forecast_objects(program, days, hours,
                            anti_deadlock_seconds=30,
                            forecast_timeout=None,
                            verify_forecast_data=True,
                            start_datetime=None,
                            start_time=None,
                            round_down_local_start_dt=True,
                            check_current_capacity_intervals=False):
    program_name = program['name']
    program_timezone = program['info']['resource_timezone']
    logger.debug('Creating forecast files...')
    program_tz = pytz.timezone(program_timezone)
    logger.debug('PROGRAM: %s PROGRAM TIMEZONE: %s', program_name, program_tz)
    utc_dt = isodate.parse_datetime(start_datetime or iso8601_utcnow())
    if start_time:
        utc_time = isodate.parse_time(start_time)
        utc_dt = dt.datetime.combine(utc_dt.date(), utc_time)
    local_dt = utc_dt.astimezone(program_tz)
    # local time of resource - all our openADR programs are in PST for now
    logger.debug('UTC_TIME: %s', utc_dt)
    logger.debug('PROGRAM_TIME: %s', local_dt)
    daily_dts = []
    hourly_dts = []

    with PowerQAClient(WEB_URL) as client:
        if round_down_local_start_dt:
            dt_start_daily_naive = dt.datetime(
                year=utc_dt.year,
                month=utc_dt.month,
                day=utc_dt.day)
            dt_start_daily = program_tz.localize(dt_start_daily_naive)
        else:
            dt_start_daily = local_dt

        for day_ix in range(0, days):
            dt_start_utc = dt_start_daily.astimezone(UTC)
            dt_start_utc_str = dt_start_utc.strftime(STR_FORMAT)
            dt_local = dt_start_daily + dt.timedelta(days=day_ix)
            dt_utc_generated = dt_local.astimezone(UTC)
            dt_utc_generated = round_time_up(dt_utc_generated, days=1)
            daily_dts.append(dt_utc_generated)
            logger.debug('daily forecast start dt: %s', dt_start_utc_str)
            client.all_vpp_daily_forecasts(program_name, start_dt_utc=dt_start_utc_str)
            anti_deadlock_sleep(anti_deadlock_seconds)

        if round_down_local_start_dt:
            dt_start_hourly_naive = dt.datetime(
                year=local_dt.year,
                month=local_dt.month,
                day=local_dt.day,
                hour=local_dt.hour)
            dt_start_hourly = program_tz.localize(dt_start_hourly_naive)
        else:
            dt_start_hourly = local_dt

        for ix in range(0, hours):
            dt_start_utc = dt_start_hourly.astimezone(UTC)
            dt_start_utc_str = dt_start_utc.strftime(STR_FORMAT)
            dt_local = dt_start_hourly + dt.timedelta(hours=ix)
            dt_local_generated = round_time_up(dt_local, hours=1)

            dt_utc_generated = dt_local_generated.astimezone(UTC)
            hourly_dts.append(dt_utc_generated)

            logger.debug('hourly forecast start dt: %s', dt_start_utc_str)
            client.all_vpp_hourly_forecasts(program_name, start_dt_utc=dt_start_utc_str)
            anti_deadlock_sleep(anti_deadlock_seconds)

    verify_forecast_creation(program, daily_dts, hourly_dts, forecast_timeout, verify_forecast_data,
                             check_current_capacity_intervals)


def verify_forecast_creation(program, daily_dts, hourly_dts, forecast_timeout, verify_forecast_data,
                             check_current_capacity_intervals=False):
    external_reference_id = program['external_reference_id']
    program_name = program['name']
    resources = program['resources']
    dailies = len(daily_dts)
    hourlies = len(hourly_dts)
    daily_dts_str = [daily.strftime(STR_FORMAT) for daily in daily_dts]
    hourly_dts_str = [hourly.strftime(STR_FORMAT) for hourly in hourly_dts]
    creation_timeout = forecast_timeout if forecast_timeout else (dailies + hourlies) * 10
    logger.debug('Forecast completion timeout: %ss', creation_timeout)

    waited = 0
    wait_step_seconds = 5
    daily_forecasts = get_daily_forecast_objects(external_reference_id, resources)
    hourly_forecasts = get_hourly_forecast_objects(external_reference_id, resources)
    while (len(daily_forecasts) != dailies * len(resources)
           or len(hourly_forecasts) != hourlies * len(resources)) and waited <= creation_timeout:
        logger.debug('Waiting %ss for until checking again if all forecasts were created', wait_step_seconds)
        time.sleep(wait_step_seconds)
        waited += wait_step_seconds
        daily_forecasts = get_daily_forecast_objects(external_reference_id, resources)
        hourly_forecasts = get_hourly_forecast_objects(external_reference_id, resources)

    resource_locations = [res['target_location'] for res in resources]
    logger.debug('locations: %s', json_dumps(resource_locations))
    daily_iso_dts = [isodate.datetime_isoformat(udt) for udt in daily_dts]
    hourly_iso_dts = [isodate.datetime_isoformat(udt) for udt in hourly_dts]
    logger.debug('local dates sent in request for daily forecast: %s',
                 json_dumps(daily_dts_str))
    logger.debug('daily dates expected: %s', json_dumps(daily_iso_dts))
    logger.debug('local dates sent in request for hourly forecast: %s',
                 json_dumps(hourly_dts_str))
    logger.debug('hourly dates expected: %s', json_dumps(hourly_iso_dts))
    info = """
    RESOURCES: %s\n
    DAILIES REQUESTED: %s\n
    DAILY FORECASTS CREATED: %s\n
    DAILY REQUEST DATES: %s\n
    DAILY EXPECTED DATES: %s\n
    HOURLIES REQUESTED: %s\n
    HOURLY FORECASTS CREATED: %s\n
    HOURLY REQUEST DATES: %s\n
    HOURLY EXPECTED DATES: %s\n'
           """ % (json_dumps(resources),
                  dailies,
                  json_dumps(daily_forecasts),
                  json_dumps(daily_dts_str),
                  json_dumps(daily_iso_dts),
                  hourlies,
                  json_dumps(hourly_forecasts),
                  json_dumps(hourly_dts_str),
                  json_dumps(hourly_iso_dts),
                  )

    assert len(daily_forecasts) == dailies * len(resources), \
        'Number of daily forecasts for this program does not match: \n%s' % info
    assert len(hourly_forecasts) == hourlies * len(resources), \
        'Number of hourly forecasts for this program does not match: \n%s' % info

    for res_loc in resource_locations:
        for iso_dt in daily_iso_dts:
            expected_name = '%s/%s/%s' % (DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id),
                                          iso_dt, res_loc)
            logger.debug('Expected forecast name: %s', expected_name)
            assert expected_name in daily_forecasts, \
                'Daily forecast file not found: %s\n%s' % (expected_name, info)
        for iso_dt in hourly_iso_dts:
            expected_name = '%s/%s/%s' % (HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id),
                                          iso_dt, res_loc)
            logger.debug('Expected forecast name: %s', expected_name)
            assert expected_name in hourly_forecasts, \
                'Hourly forecast file not found: %s\n%s' % (expected_name, info)

    if verify_forecast_data:
        logger.debug('Verifying forecast data of all forecast...')
        verify_forecast_files(resources, external_reference_id, program_name)

    if check_current_capacity_intervals:
        # Making sure we have forecast data available for current and next hour
        now = dt_utcnow()
        current_forecast_intervals = get_forecast_intervals(now, external_reference_id, resources)
        assert len(current_forecast_intervals) == len(resources)
        next_hour = now + dt.timedelta(hours=1)
        forecast_intervals_for_next_hour = get_forecast_intervals(next_hour, external_reference_id, resources)
        assert len(forecast_intervals_for_next_hour) == len(resources)


def get_hourlies(hourly_forecast_iso_dt, external_reference_id, resources):
    hourly_forecasts_path = HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id)
    hourly_forecasts = get_forecast_object_names(hourly_forecasts_path)
    hourly_forecasts = filter_forecasts_by_resources(hourly_forecasts, resources)

    logger.debug('hourly forecast iso datetime: %s', hourly_forecast_iso_dt)
    hourlies = []

    for forecast_name in hourly_forecasts:
        if hourly_forecast_iso_dt in forecast_name:
            hourlies.append(forecast_name)
    logger.debug('latest_hourlies: %s', hourlies)
    assert len(hourlies) == len(resources)
    return hourlies


def get_expected_hourly_forecast(hourly_forecast_iso_dt, external_reference_id, resources):
    logger.debug('Creating opeandr forecast for(iso_dt: %s, resources: %s)', hourly_forecast_iso_dt, resources)
    hourlies = get_hourlies(hourly_forecast_iso_dt, external_reference_id, resources)
    hourlies_data = []
    hourlies_aggregation = []
    for forecast in hourlies:
        forecast_object = s4c.get_object(VPP_BUCKET, forecast)
        forecast_data = json.loads(forecast_object.read())
        optimization_results = json.loads(forecast_data['optimization_results'])
        timestamps = optimization_results['index']
        intervals = optimization_results['data']
        columns = optimization_results['columns']
        assert len(timestamps) == len(intervals)
        if not hourlies_data:
            hourlies_data = [{'timestamp': ts, 'intervals': [], 'columns': columns} for ts in timestamps]
        for idx, (timestamp, interval) in enumerate(zip(timestamps, intervals)):
            assert hourlies_data[idx]['timestamp'] == timestamp
            hourlies_data[idx]['intervals'].append(interval)

    for data in hourlies_data:
        intervals = data['intervals']
        columns = data['columns']
        discharge_kw_i = columns.index('discharge_kw')
        discharge_kwh_i = columns.index('discharge_kwh')
        charge_kw_i = columns.index('charge_kw')
        charge_kwh_i = columns.index('charge_kwh')
        hourlies_aggregation.append(
            {'timestamp': data['timestamp'],
             'interval': {"x-AVAILABLE_DISCHARGE_POWER": sum([interval[discharge_kw_i] for interval in intervals]),
                          "x-AVAILABLE_DISCHARGE_ENERGY": sum([interval[discharge_kwh_i] for interval in intervals]),
                          "x-AVAILABLE_CHARGE_POWER": sum([interval[charge_kw_i] for interval in intervals]),
                          "x-AVAILABLE_CHARGE_ENERGY": sum([interval[charge_kwh_i] for interval in intervals]),
                          "x-REACTIVE_POWER": 0.0,
                          "x-REAL_POWER": 0.0
                          }
             }
        )

    logger.debug('forecast aggregation for (%s): \n%s', hourly_forecast_iso_dt, json_dumps(hourlies_aggregation))
    return hourlies_aggregation


def get_forecast_intervals(forecast_dt, external_reference_id, resources):
    hourly_forecast_dt = round_time_up(forecast_dt, hours=1) - dt.timedelta(hours=1)
    hourly_forecast_iso_dt = isodate.datetime_isoformat(hourly_forecast_dt)
    latest_hourlies = get_hourlies(hourly_forecast_iso_dt, external_reference_id, resources)
    # forecast intervals are rounded to 15 minutes, but always add extra 1 second in timestamps used for data indexes.
    hourly_forecast_interval_dt = round_time_up(forecast_dt, minutes=15) - dt.timedelta(minutes=14, seconds=59)
    logger.debug('current_interval_dt: %s', hourly_forecast_interval_dt)
    hourly_forecast_interval_timestamp = miliseconds_timestamp(hourly_forecast_interval_dt)
    logger.debug('current_interval_timestamp: %s', hourly_forecast_interval_timestamp)

    # forecast intervals are rounded to 15 minutes, but always add extra 1 second in timestamps used for data indexes.
    hourly_forecast_interval_dt = round_time_up(forecast_dt, minutes=15) - dt.timedelta(minutes=14, seconds=59)
    logger.debug('current_interval_dt: %s', hourly_forecast_interval_dt)
    hourly_forecast_interval_timestamp = miliseconds_timestamp(hourly_forecast_interval_dt)
    logger.debug('current_interval_timestamp: %s', hourly_forecast_interval_timestamp)
    current_forecast_intervals = []
    for forecast in latest_hourlies:
        forecast_object = s4c.get_object(VPP_BUCKET, forecast)
        forecast_data = json.loads(forecast_object.read())
        optimization_results = json.loads(forecast_data['optimization_results'])
        index_l = optimization_results['index']
        columns = optimization_results['columns']
        assert hourly_forecast_interval_timestamp in index_l
        interval_index = index_l.index(hourly_forecast_interval_timestamp)
        interval_data = optimization_results['data'][interval_index]
        current_forecast_intervals.append(dict(list(zip(columns, interval_data))))

    logger.debug('forecast intervals for (%s): \n%s', hourly_forecast_interval_dt,
                 json_dumps(current_forecast_intervals))
    return current_forecast_intervals


def verify_forecast_file_interval(columns, interval, idx, forecast_name, max_power, max_energy):
    """
    columns = [
        "optimized_load",
        "site_load",
        "setpoint",
        "power_cmd",
        "soc",
        "nameplate_kw",
        "nameplate_kwh",
        "soc_pct",
        "discharge_kw",
        "discharge_kwh",
        "charge_kw",
        "charge_kwh"
    ]
    """
    info = ' [idx: %s, forecast: %s, interval: %s]' % (idx, forecast_name, interval)

    site_load_i = columns.index('site_load')
    setpoint_i = columns.index('setpoint')
    power_cmd_i = columns.index('power_cmd')
    soc_i = columns.index('soc')
    nameplate_kw_i = columns.index('nameplate_kw')
    nameplate_kwh_i = columns.index('nameplate_kwh')
    soc_pct_i = columns.index('soc_pct')
    discharge_kw_i = columns.index('discharge_kw')
    discharge_kwh_i = columns.index('discharge_kwh')
    charge_kw_i = columns.index('charge_kw')
    charge_kwh_i = columns.index('charge_kwh')

    assert interval[soc_i] >= 0
    assert interval[soc_i] <= max_energy, 'SOC energy is over possible value' + info

    assert interval[nameplate_kw_i] >= 0
    assert interval[nameplate_kw_i] <= max_power, 'Nameplate power is over max allowed' + info

    assert interval[nameplate_kwh_i] >= 0
    assert interval[nameplate_kwh_i] <= max_energy, 'Nameplate energy is over max allowed' + info

    assert interval[soc_pct_i] >= 0
    assert interval[soc_pct_i] <= 1, 'SOC coef. is bigger than one' + info

    max_available_power = max_power
    max_available_energy = max_energy
    if interval[power_cmd_i] < 0 or interval[site_load_i] >= interval[setpoint_i]:
        logger.debug('MAX AVAILABLE POWER AND ENERGY ARE EXPECTED TO BE 0 BECAUSE OF PREDICTED DCM: %s', interval)
        max_available_power = 0
        max_available_energy = 0

    assert interval[discharge_kw_i] >= 0
    assert interval[discharge_kw_i] <= max_available_power, 'Discharge power is over max allowed' + info

    assert interval[discharge_kwh_i] >= 0
    assert interval[discharge_kwh_i] <= max_available_energy, 'Discharge energy is over max allowed' + info

    assert interval[charge_kw_i] >= 0
    assert interval[charge_kw_i] <= max_available_power, 'Charge power is over max allowed' + info

    assert interval[charge_kwh_i] >= 0
    assert interval[charge_kwh_i] <= max_available_energy, 'Charge energy is over max allowed' + info


def verify_forecast_files(resources, external_reference_id, program_name):
    # TODO use program testdata dict after refactoring forecast tests to new configuration
    program = get_program_info(program_name)
    daily_forecasts = get_forecast_object_names(
        DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    daily_forecasts = filter_forecasts_by_resources(daily_forecasts, resources)
    hourly_forecasts = get_forecast_object_names(
        HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    hourly_forecasts = filter_forecasts_by_resources(hourly_forecasts, resources)
    logger.debug('Program `%s` aggregations:\n%s', program_name, json_dumps(program['aggregations']))
    resources = program['aggregations'][0]['locations']

    for daily_forecast in daily_forecasts:
        location = daily_forecast.split('/')[-1]
        resource = resources.get(location)
        assert resource, 'location %s not found in aggregation' % location
        resource_power = resource['size_kw']
        resource_energy = resource['size_kwh']
        forecast_object = s4c.get_object(VPP_BUCKET, daily_forecast)
        forecast_data = json.loads(forecast_object.read())
        optimization_results = json.loads(forecast_data['optimization_results'])
        logger.debug('Verification of `%s`', daily_forecast)
        logger.debug('Location `%s` [power: %s, energy: %s]:\n%s',
                     location, resource_power, resource_energy,
                     json_dumps(optimization_results))
        # NOTE create dts days and check that intervals are shorter/longer if
        #     we won't use UTC day forecast only
        data = optimization_results['data']
        columns = optimization_results['columns']
        assert len(data) == 96, 'Length of daily forecast [%s] intervals is incorrect: %s' % \
                                (daily_forecast, len(data))
        for idx, interval in enumerate(data):
            verify_forecast_file_interval(columns, interval, idx, daily_forecast, resource_power, resource_energy)

    for hourly_forecast in hourly_forecasts:
        location = hourly_forecast.split('/')[-1]
        resource = resources.get(location)
        assert resource, 'location %s not found in aggregation' % location
        resource_power = resource['size_kw']
        resource_energy = resource['size_kwh']
        forecast_object = s4c.get_object(VPP_BUCKET, hourly_forecast)
        forecast_data = json.loads(forecast_object.read())
        optimization_results = json.loads(forecast_data['optimization_results'])
        logger.debug('Verification of `%s`', hourly_forecast)
        logger.debug('Location `%s` [power: %s, energy: %s]:\n%s',
                     location, resource_power, resource_energy,
                     json_dumps(optimization_results))
        columns = optimization_results['columns']
        data = optimization_results['data']
        assert len(data) == 16, 'Length of hourly forecast intervals is incorrect: %s' % hourly_forecast
        for idx, interval in enumerate(data):
            verify_forecast_file_interval(columns, interval, idx, hourly_forecast, resource_power, resource_energy)

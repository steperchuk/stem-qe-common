import datetime as dt
import json
import logging
import os
import time

import isodate
import requests

from stemqa import consts
from stemqa.config.settings import settings
from stemqa.vpp.container import Container
from stemqa.vpp.helper import json_dumps, get_random_hex_string, dt_utcnow, iso8601_utc_datetime, \
    iso8601_utcnow
from stemqa.vpp.openadr.common import clean_create_party_registration, clean_created_party_registration, \
    clean_report_registration, POLL_FREQ
from stemqa.vpp.openadr.configuration import OADR_RESOURCES_PATH, DEFAULT_TESTDATA, DEFAULT_TEST_CONFIGURATION

logger = logging.getLogger(__name__)


# NOTE wait for registration needs VTN pod active and not restarting when when we are using this
# TODO create new reusable decorator for wait_for_registration or reuse the one from vtn-sim
# TODO we should have an endpoint for VEN so we can confirm it is alive an kicking. We can reuse their test endpoints.
# TODO hot to separate containers from api.

class Ven(Container):

    def start(self, timeout=60, **kwargs):
        if kwargs.get('wait_for_registration', True):
            logger.debug('Getting VEN registration info from VTN')
            registrations = vtnc.get_party_registrations()
            registrations_after = []

        super(Ven, self).start(timeout)

        if kwargs.get('wait_for_registration', True):
            logger.debug('Waiting for VEN to register with VTN...')
            wait_time = 0
            while wait_time < timeout:
                registrations_after = vtnc.get_party_registrations()
                if len(registrations.get(self.name, 0)) < len(registrations_after.get(self.name, 0)):
                    logger.debug('VEN is registered: %s',
                                 json_dumps(registrations_after[self.name][-1]))
                    return
                time.sleep(2)
                wait_time += 2

            assert len(registrations[self.name]) < len(registrations_after[self.name]), \
                'Registration of VEN did not happen in requested time'

    def restart(self, timeout=60, **kwargs):
        if kwargs.get('wait_for_registration', True):
            logger.debug('Getting VEN registration info from VTN')
            registrations = vtnc.get_party_registrations()
            registrations_after = []

        super(Ven, self).restart(timeout)

        if kwargs.get('wait_for_registration', True):
            logger.debug('Waiting for VEN to register with VTN...')
            wait_time = 0
            while wait_time < timeout:
                registrations_after = vtnc.get_party_registrations()
                if len(registrations.get(self.name, 0)) < len(registrations_after.get(self.name, 0)):
                    logger.debug('VEN is registered: %s',
                                 json_dumps(registrations_after[self.name][-1]))
                    return
                time.sleep(2)
                wait_time += 2

            assert len(registrations[self.name]) < len(registrations_after[self.name]), \
                'Registration of VEN did not happen in requested time'


vtn_sim = Container('STEM_TEST_VTN_ID', 'vtn-sim', 'vtn-sim',
                    settings['vtn']['host'], settings['vtn']['app-port'])
# Austin SHINES (Doosan VTN)
ven_austin = Ven("STEM_TEST_VEN", 'stem-ven-austin-shines')
# Heco SHINES (Currently uses subset of Austin shines reports. Events implementation not defined yet.)
ven_hecoshines = Ven("STEM_TEST_VEN_HECOSHINES", 'stem-ven-heco-shines')
# set of hawaii fastDR VEN's(fastDR events via AG, no reports)
ven_fastdr_residence_inn = Ven("STEM_TEST_VEN_RESIDENCE_INN",
                               'stem-ven-fastdr-hawaii-residence-inn',
                               chart_name='stem-ven-fastdr-hawaii')
ven_fastdr_sheraton_maui = Ven("STEM_TEST_VEN_SHERATON_MAUI",
                               'stem-ven-fastdr-hawaii-sheraton-maui',
                               chart_name='stem-ven-fastdr-hawaii')
ven_fastdr_sheraton_waikiki = Ven("STEM_TEST_VEN_SHERATON_WAIKIKI",
                                  'stem-ven-sheraton-fastdr-hawaii-waikiki',
                                  chart_name='stem-ven-fastdr-hawaii')
VENS = {"STEM_TEST_VEN": ven_austin,
        "STEM_TEST_VEN_HECOSHINES": ven_hecoshines,
        "STEM_TEST_VEN_RESIDENCE_INN": ven_fastdr_residence_inn,
        "STEM_TEST_VEN_SHERATON_MAUI": ven_fastdr_sheraton_maui,
        "STEM_TEST_VEN_SHERATON_WAIKIKI": ven_fastdr_sheraton_waikiki
        }


def wait_for_reg_dec(decorated_f):
    def wait_for_reg_wrapper(self, ven_name, wait_for_reg=True, timeout=30,
                             restart_ven_on_error=False, **kwargs):
        if wait_for_reg:
            logger.debug('Getting VEN registration info from VTN')
            registrations = self.get_party_registrations()

            resp_data = None
            ven_registration = self.get_current_party_registration()
            if ven_registration.get(ven_name):
                resp_data = decorated_f(self, ven_name, **kwargs)
            else:
                logger.error("VEN isn't registered with VTN. Restarting VEN")
                VENS[ven_name].restart()

            wait_time = 0
            while wait_time < timeout:
                registrations_after = self.get_party_registrations()
                if len(registrations.get(ven_name, 0)) < len(registrations_after.get(ven_name, 0)):
                    logger.debug('VEN is registered: %s',
                                 json_dumps(registrations_after[ven_name][-1]))
                    break
                logger.debug('Waiting for VEN to register with VTN...')
                time.sleep(2)
                wait_time += 2
            if len(registrations[ven_name]) >= len(registrations_after[ven_name]):
                if restart_ven_on_error:
                    logger.error("VEN didn't register again. Restarting VEN")
                    VENS[ven_name].restart()
                else:
                    raise Exception("VEN didn't register again.")
            return resp_data
        else:
            return decorated_f(self, ven_name, **kwargs)

    return wait_for_reg_wrapper


class VtnApiCallFailed(Exception):
    pass


class VtnResponse(object):
    """
    Represents a response from vtn-sim testing endpoints.
    """

    def __init__(self, response):
        self.response = response

    def json(self):
        return self.response.json()

    def data(self):
        json_d = self.json()
        if json_d['status'] != 'fail':
            return json_d["data"]
        else:
            raise VtnApiCallFailed(json_d)

    def content(self):
        return self.response.content

    def headers(self):
        return self.response.headers


class VtnResponseHook(object):
    """
    Hook invoked when we receive a response from VTN. Used primarily for logging the response
    """

    @staticmethod
    def log(response, *args, **kwargs):
        """
        Hook invoked when we receive a response from the Vtn
        :param response: response from server
        :return:
        """
        if response.status_code != 200 or consts.LOG_RESPONSE:
            logger.debug('args: %s, kwargs: %s', args, kwargs)
            logger.debug('status_code: %s', type(response.status_code))
            logger.debug('status_code: %s', response.status_code)
            logger.debug('url: %s', response.url)
            try:
                logger.debug('content: %s', json.dumps(response.json(), indent=2))
            except ValueError:
                logger.debug("content: %s", response.text)


class VtnClient(object):
    """
    Base class for vtn testing client.
    """

    def __init__(self, url, client=None):
        if client:
            self.url = client.url
        else:
            self.url = url
        self.response_archive = []
        self.events_url = "%s/%s" % (self.url, "events")

    def response_handling(self, response):
        self.response_archive.append(response)
        response.raise_for_status()
        return VtnResponse(response)

    def get(self, url, headers=None, data=None, params=None, stream=False):
        response = requests.get(url, headers=headers, data=data, params=params, stream=stream, verify=False,
                                hooks=dict(response=VtnResponseHook.log))
        return self.response_handling(response)

    def post(self, url, headers=None, data=None, params=None, files=None):
        response = requests.post(url, headers=headers, data=data, params=params, files=files, verify=False,
                                 hooks=dict(response=VtnResponseHook.log))
        return self.response_handling(response)

    def patch(self, url, headers=None, data=None, params=None, files=None):
        response = requests.patch(url, headers=headers, data=data, params=params, files=files, verify=False,
                                  hooks=dict(response=VtnResponseHook.log))
        return self.response_handling(response)

    def delete(self, url, headers=None, data=None, params=None, files=None):
        response = requests.delete(url, headers=headers, data=data, params=params, files=files, verify=False,
                                   hooks=dict(response=VtnResponseHook.log))
        return self.response_handling(response)

    def get_party_registrations(self, ven_name=None):
        '''
            # No filter
            curl "http://localhost:5000/partyRegistrations"

            # Filtered by default VEN
            curl "http://localhost:5000/partyRegistrations?ven=STEM_TEST_VEN"

        '''
        url = "%s/%s" % (self.url, "partyRegistrations")
        data = {}
        if ven_name:
            data['ven'] = ven_name

        response = self.get(url, params=data)
        return response.data()

    def get_current_party_registration(self, ven_name=None):
        '''
            # No filter
            curl "http://localhost:5000/currentPartyRegistration"

            # Filtered by default VEN
            curl "http://localhost:5000/currentPartyRegistration?ven=STEM_TEST_VEN"
        '''
        url = "%s/%s" % (self.url, "currentPartyRegistration")
        data = {}
        if ven_name:
            data['ven'] = ven_name

        response = self.get(url, params=data)
        data = response.data()
        return data

    def get_create_party_registration(self, ven_name):
        '''
            # Filtered by default VEN
            curl "http://localhost:5000/currentPartyRegistration?ven=STEM_TEST_VEN"
        '''
        url = "%s/%s" % (self.url, "currentPartyRegistration")
        data = {}
        if ven_name:
            data['ven'] = ven_name

        response = self.get(url, params=data)
        resp_data = response.data()
        if resp_data and ven_name:
            return resp_data['create']
        return resp_data

    def get_created_party_registration(self, ven_name):
        '''
            # Filtered by default VEN
            curl "http://localhost:5000/currentPartyRegistration?ven=STEM_TEST_VEN"
        '''
        url = "%s/%s" % (self.url, "currentPartyRegistration")
        data = {}
        if ven_name:
            data['ven'] = ven_name

        response = self.get(url, params=data)
        resp_data = response.data()
        if resp_data and ven_name:
            return resp_data['created']
        return resp_data

    def get_report_registration(self, ven_name=None, report_specifier_id=None):
        '''
            # no filter
            curl "http://localhost:5000/registeredReports"

            # filtered by default VEN
            curl "http://localhost:5000/registeredReports?ven=STEM_TEST_VEN"

            # filtered by VEN and report specifier
            curl "http://localhost:5000/registeredReports?ven=STEM_TEST_VEN&id=x-CHARGE_CURVE"
        '''
        url = "%s/%s" % (self.url, "registeredReports")
        data = {}
        if ven_name:
            data['ven'] = ven_name
        if report_specifier_id:
            data['id'] = report_specifier_id

        response = self.get(url, params=data)
        return response.data()

    def get_report_updates(self, ven_name=None, report_request_id=None):
        '''
            # No filter
            curl "http://localhost:5000/reportUpdates"

            # Filtered by default VEN
            curl "http://localhost:5000/reportUpdates?ven=STEM_TEST_VEN"

            # Filtered by VEN and report specifier. You should define report
            # request id when creating requests with /reportRequests
            curl "http://localhost:5000/reportUpdates?ven=STEM_TEST_VEN&id=REPORTREQUEST:0"
        '''
        url = "%s/%s" % (self.url, "reportUpdates")
        data = {}
        if ven_name:
            data['ven'] = ven_name
        if report_request_id:
            data['id'] = report_request_id

        response = self.get(url, params=data)
        return response.data()

    # TODO add verification of success by waiting and checking reports response
    def create_report_request(self,
                              ven_name,
                              report_specifier_id,
                              report_interval_start,
                              report_request_id=None,
                              **kwargs):
        '''
            curl -H "Content-Type: application/json" -X POST -d '
            {   "ven" : "STEM_TEST_VEN",
                "report_request_id" : "REPORT_REQUEST:0",
                "report_specifier_id" : "x-NAMEPLATE",
                "report_interval_start" : "2017-10-16T19:07:57.44Z",
                "specifier_payloads" :
                    ["x-NAMEPLATE_CHARGE_POWER_MAX",
                    "x-NAMEPLATE_DISCHARGE_POWER_MAX",
                    "x-NAMEPLATE_ENERGY_CAPACITY"],
                "granularity_duration" : "P0D",
                "report_back_duration" : "P0D",
                "report_interval_duration" : "P0D"
            }' \
            http://localhost:5000/reportRequests
        '''
        url = "%s/%s" % (self.url, "reportRequests")
        data = kwargs.copy()
        data['ven'] = ven_name
        data['report_specifier_id'] = report_specifier_id
        data['report_interval_start'] = report_interval_start
        if report_request_id:
            data['report_request_id'] = report_request_id

        response = self.post(url,
                             headers={'CONTENT-TYPE': 'application/json'},
                             data=json.dumps(data))
        return response.data()

    def get_created_report_responses(self, ven_name=None):
        '''
            # No Filtering, getting report responses for all VEN's registered on VTN
            curl "http://localhost:5000/createdReportResponses"

            # Filtered by default VEN
            curl "http://localhost:5000/createdReportResponses?ven=STEM_TEST_VEN"
        '''
        url = "%s/%s" % (self.url, "createdReportResponses")
        data = {}
        if ven_name:
            data['ven'] = ven_name
        response = self.get(url, params=data)
        return response.data()

    def get_pending_reports(self, ven_name=None, refresh_first=False):
        '''
            # Posting empty cancelReportRequest which will provide oadrCanceledReport response
            # with current state of pending reports, which get automatically cached.
            # Wait 10seconds before query of pending reports because of polling lag.
            curl -H "Content-Type: application/json" -X DELETE -d \
            '{  "ven": "STEM_TEST_VEN",
                "report_request_ids" : [],
                "report_to_follow" : false
            }' \
            http://localhost:5000/cancelReportRequests

            # No Filtering, getting pending reports for all VEN's
            curl "http://localhost:5000/pendingReports"

            # Filtered by default VEN
            curl "http://localhost:5000/pendingReports?ven=STEM_TEST_VEN"
        '''
        if refresh_first:
            assert ven_name
            # NOTE we are sending empty cancellation request just to get current state of pending reports
            self.cancel_report_requests(ven_name)
            # NOTE we could implement this by parsing and waiting canceledReportResponse from VEN.
            time.sleep(10)

        url = "%s/%s" % (self.url, "pendingReports")
        data = {}
        if ven_name:
            data['ven'] = ven_name

        response = self.get(url, params=data)
        return response.data()

    def cancel_report_requests(self,
                               ven_name,
                               report_requests=None,
                               report_to_follow=False):
        '''
            curl -H "Content-Type: application/json" -X DELETE -d \
            '{  "ven": "STEM_TEST_VEN",
                "report_request_ids" : ["REPORT_REQUEST:0",
                                        "REPORT_REQUEST:1",
                                        "REPORT_REQUEST:2",
                                        "REPORT_REQUEST:3"],
                "report_to_follow" : false
            }' \
            http://localhost:5000/cancelReportRequests
        '''
        if not report_requests:
            report_requests = []
        report_request_ids = []
        for report_request in report_requests:
            if isinstance(report_request, dict):
                report_request_ids.append(report_request['report_request_id'])
            else:
                report_request_ids.append(report_request)

        url = "%s/%s" % (self.url, "cancelReportRequests")
        data = {'ven': ven_name,
                'report_request_ids': report_request_ids,
                'report_to_follow': report_to_follow}
        response = self.delete(url,
                               headers={'CONTENT-TYPE': 'application/json'},
                               data=json.dumps(data))
        return response.json()

    def cancel_all_pending_reports(self, ven_name, wait=True,
                                   assert_failed=False, restart_ven_after_fail=True):
        '''This functions simulates following VTN api endpoint so we have a better control
            curl -H "Content-Type: application/json" -X DELETE -d \
            '{ "ven": "STEM_TEST_VEN"}' \
            http://localhost:5000/cancelAllPendingReports
        '''
        ven_c = VENS[ven_name]
        pending_reports = self.get_pending_reports(ven_name)
        if pending_reports and wait:
            self.cancel_report_requests(ven_name, pending_reports)
            wait_time = 0
            while wait_time < POLL_FREQ * 3 and pending_reports:
                logger.debug('Waiting for all reports to get cancelled...')
                time.sleep(2)
                wait_time += 2
                pending_reports = self.get_pending_reports(ven_name)

            if pending_reports:
                if assert_failed:
                    assert False, 'VEN has not cancelled all reports'
                else:
                    logger.error('VEN has not cancelled all reports')

                if restart_ven_after_fail:
                    ven_c.restart(wait_for_registration=True)
        elif pending_reports:
            self.cancel_report_requests(ven_name, pending_reports)

    # TODO make this sane add better verification for reports response
    def create_report_requests(self,
                               ven_name,
                               report_requests,
                               default_report_config=None,
                               wait_for_ven_response=True,
                               assert_ven_response=True,
                               poll_freq=POLL_FREQ):
        if not default_report_config:
            default_report_config = {}
        report_requests_data = []
        if report_requests[0].get('ven'):
            ven_name = report_requests[0].get('ven')
        if wait_for_ven_response:
            created_report_responses = self.get_created_report_responses(ven_name)
        for rep_req in report_requests:
            rep_req_sent = default_report_config.copy()
            rep_req_sent.update(rep_req)
            request_id = rep_req_sent['report_specifier_id'] + '__' + get_random_hex_string(10)
            rep_req_sent.setdefault('report_request_id', request_id)
            logger.debug('Creating report request with report_request_id: %s',
                         rep_req_sent['report_request_id'])
            rep_req_response = self.create_report_request(**rep_req_sent)
            report_requests_data.append({'response': rep_req_response,
                                         'config': rep_req_sent})
        if wait_for_ven_response:
            wait_time = 0
            initial_length = len(created_report_responses)
            responses_after = self.get_created_report_responses(ven_name)
            while initial_length == len(responses_after) and wait_time < 3 * poll_freq:
                logger.debug('Waiting for VEN report creation response...')
                time.sleep(1)
                wait_time += 1
                responses_after = self.get_created_report_responses(ven_name)
            assert initial_length <= len(responses_after), \
                'created report responses length should not be smaller than before.\nB:%s\nA:%s' % \
                (json_dumps(created_report_responses), json_dumps(responses_after))
            if wait_time > 3 * poll_freq and initial_length == len(responses_after):
                if assert_ven_response:
                    raise Exception('VEN has not responded to report requests.\nB:%s\nA:%s' %
                                    (json_dumps(created_report_responses), json_dumps(responses_after)))
                else:
                    return report_requests_data, None

            last_response = responses_after[-1]
            if assert_ven_response:
                assert int(last_response['ei_response']['response_code']) == 200, \
                    'Report request error: %s' % last_response['ei_response']['response_description']
            return report_requests_data, last_response
        return report_requests_data, None

    def query_events(self, event_id=None, ven_name=None, aggr='EVENT', **kwargs):
        '''
            curl http://localhost:5000/events?ven=STEM_TEST_VEN&id=EVENT:0
        '''
        params = kwargs.copy()
        if ven_name:
            params['ven'] = ven_name
        if event_id:
            params['id'] = event_id
        params['aggr'] = aggr
        response = self.get(self.events_url, params=params)
        return response.data()

    def create_event(self, charge_payloads, **kwargs):
        '''
            curl -H "Content-Type: application/json" -X POST -d \
            '{  "event_id": "EVENT:0",
                "ven_name": "STEM_TEST_VEN",
                "event_start_time": "2018-10-24T18:00:00Z",
                "interval_duration": "PT5M",
                "charge_payloads":
                    ["None",null,0,-0,1,-1,2.2,-2.2,100,-50,20.123,-20.321]
            }' \
            http://localhost:5000/events
        '''
        data = kwargs.copy()
        data['charge_payloads'] = charge_payloads
        response = self.post(self.events_url,
                             headers={'CONTENT-TYPE': 'application/json'},
                             data=json.dumps(data))
        return response.json()

    def get_event_responses(self, ven_name=None):
        '''
            # No filter
            curl "http://localhost:5000/eventResponses"

            # Filtered by default VEN
            curl "http://localhost:5000/eventResponses?ven=STEM_TEST_VEN"
        '''
        url = "%s/%s" % (self.url, "eventResponses")
        data = {}
        if ven_name:
            data['ven'] = ven_name
        response = self.get(url, params=data)
        return response.data()

    def modify_event(self, event_id, ven_name=None, **kwargs):
        '''
            curl -H "Content-Type: application/json" -X PATCH -d \
            '{  "event_id": "EVENT:0",
                "ven_name": "STEM_TEST_VEN",
                "charge_payloads":[-1,2,-3,4]
            }' \
            http://localhost:5000/events
        '''
        data = kwargs.copy()
        data['event_id'] = event_id
        if ven_name:
            data['ven_name'] = ven_name
        response = self.patch(self.events_url,
                              headers={'CONTENT-TYPE': 'application/json'},
                              data=json.dumps(data))
        return response.json()

    def cancel_event(self, event_id, ven_name=None, **kwargs):
        '''
            curl -H "Content-Type: application/json" -X PATCH -d \
            '{  "event_id": "EVENT:0",
                "ven_name": "STEM_TEST_VEN",
                "event_status": "cancelled"
            }' \
            http://localhost:5000/events
        '''
        data = kwargs.copy()
        data['event_id'] = event_id
        if ven_name:
            data['ven_name'] = ven_name
        data['event_status'] = 'cancelled'
        response = self.patch(self.events_url,
                              headers={'CONTENT-TYPE': 'application/json'},
                              data=json.dumps(data))
        return response.json()

    def delete_event(self, event_id, ven_name=None, **kwargs):
        '''
            curl -H "Content-Type: application/json" -X DELETE -d \
            '{  "event_id": "EVENT:0",
                "ven_name": "STEM_TEST_VEN",
            }' \
            http://localhost:5000/events
        '''
        data = kwargs.copy()
        data['event_id'] = event_id
        if ven_name:
            data['ven_name'] = ven_name
        response = self.delete(self.events_url,
                               headers={'CONTENT-TYPE': 'application/json'},
                               data=json.dumps(data))
        return response.json()

    def delete_events(self, events="*", **kwargs):
        '''
            curl -H "Content-Type: application/json" -X DELETE -d \
            '{  "events": ["EVENT:0","EVENT:1"],
                "ven_name": "STEM_TEST_VEN",
            }' \
            http://localhost:5000/events
        '''
        data = kwargs.copy()
        data['events'] = events
        logger.debug(data)
        response = self.delete(self.events_url,
                               headers={'CONTENT-TYPE': 'application/json'},
                               data=json.dumps(data))
        return response.json()

    def xmlevent(self, ven_name, distribute_event_xml):
        '''Method that wraps vtn simulators `/xmlevent` endpoint for easier usage

            curl -H "Content-Type: application/json" -X POST -d \
                '{  "ven": "STEM_TEST_VEN_RESIDENCE_INN",
                    "event": EVENT_XML_DATA}' localhost:5000/xmlevent

            This command ^ is hard to run manually from bash because embedding xml data inside
            bash can be tricky. Vtn-sim repository has a python and bash scripts inside `heco_fastdr`
            folder if you want to check example of sending/creating custom openadr events manually.
        '''
        url = "%s/%s" % (self.url, "xmlevent")
        data = {'ven': ven_name, 'event': distribute_event_xml}
        response = self.post(url,
                             headers={'CONTENT-TYPE': 'application/json'},
                             data=json.dumps(data))
        return response.json()

    def create_fastdr_event(self, ven_name, ven_id, event_id, start_dt, duration,
                            modification_number=0, event_status='far', created_dt=None,
                            resource_id='test_resource_id', market_context='test_market_context',
                            vtn_id='STEM_TEST_VTN_ID', request_id=None):
        '''Helper method for creating fastdr event via `/xmlevent` endpoint
        '''
        logger.debug('Creating fastdr event for VEN: %s [%s]', ven_name, ven_id)
        if isinstance(start_dt, dt.datetime):
            start_dt = iso8601_utc_datetime(start_dt)
        if isinstance(duration, dt.timedelta):
            duration = isodate.duration_isoformat(duration)
        if not created_dt:
            created_dt = iso8601_utcnow()
        if not request_id:
            request_id = 'FASTDR_EVENT' + '__' + get_random_hex_string(10)

        xml_templates_path = os.path.join(OADR_RESOURCES_PATH, 'xml_templates')
        with open(os.path.join(xml_templates_path, 'oadrDistributeEvent.xml'), 'r') as fp:
            distribute_event_templ = fp.read()
        with open(os.path.join(xml_templates_path, 'fastdr_oadrEvent.xml'), 'r') as fp:
            fastdr_event_templ = fp.read()
        fastdr_event_kwargs = {'EVENT_ID': event_id,
                               'MODIFICATION_NUMBER': modification_number,
                               'MARKET_CONTEXT': market_context,
                               'CREATED_DT': created_dt,
                               'EVENT_STATUS': event_status,
                               'START_DT': start_dt,
                               'DURATION': duration,
                               'RESOURCE_ID': resource_id,
                               'VEN_ID': ven_id
                               }
        logger.debug('Data used for event creation:\n%s', json_dumps(fastdr_event_kwargs))
        oadr_event = fastdr_event_templ.format(**fastdr_event_kwargs)
        disribute_event_kwargs = {'REQUEST_UID': request_id, 'VTN_ID': vtn_id, 'EVENTS': oadr_event}
        distribute_event_xml = distribute_event_templ.format(**disribute_event_kwargs)
        event_response_before = self.get_event_responses(ven_name)
        logger.debug('Event responses before fastdr event is created:\n%s', json_dumps(event_response_before))
        resp = self.xmlevent(ven_name, distribute_event_xml)
        logger.debug('Waiting 15s (VEN pooling delay) before checking VEN event creation response')
        time.sleep(15)
        event_response_after = self.get_event_responses(ven_name)
        logger.debug('Event responses after event is created:\n%s', json_dumps(event_response_after))
        assert len(event_response_after) > len(event_response_before)
        # VTN response in case we would like to add extra openadr verification
        # {
        #    "fastdr_event_residence_inn__2019-05-21T13:50:00Z": [
        #        {
        #        "opt_type": "optIn",
        #        "qualified_event_id": {
        #            "event_id": "fastdr_event_residence_inn__2019-05-21T13:50:00Z",
        #            "modification_number": "0"
        #        },
        #        "request_id": "FASTDR_EVENT__6e350b0851",
        #        "response_code": "200",
        #        "response_description": "OK"
        #        }
        #    ]
        # }
        return resp

    def delete_fastdr_event(self, ven_name, vtn_id='STEM_TEST_VTN_ID', request_id=None):
        '''Helper method for clearing/deleting fastdr events via `/xmlevent` endpoint
        '''
        logger.debug('Deleting fastdr events for VEN: %s', ven_name)
        if not request_id:
            request_id = 'FASTDR_EVENT' + '__' + get_random_hex_string(10)
        xml_templates_path = os.path.join(OADR_RESOURCES_PATH, 'xml_templates')
        with open(os.path.join(xml_templates_path, 'oadrDistributeEvent.xml'), 'r') as fp:
            distribute_event_templ = fp.read()
        disribute_event_kwargs = {'REQUEST_UID': request_id, 'VTN_ID': vtn_id, 'EVENTS': ''}
        distribute_event_xml = distribute_event_templ.format(**disribute_event_kwargs)
        res = self.xmlevent(ven_name, distribute_event_xml)
        logger.debug('Waiting 15s (VEN pooling delay) before checking VEN event creation response')
        time.sleep(15)
        event_response_after = self.get_event_responses(ven_name)
        logger.debug('Event responses after event is deleted:\n%s', json_dumps(event_response_after))
        return res

    @wait_for_reg_dec
    def request_reregistration(self, ven_name):
        '''
            curl -H "Content-Type: application/json" -X POST -d \
            '{ "ven_name": "STEM_TEST_VEN"}' \
            http://localhost:5000/requestReregistration
        '''
        url = "%s/%s" % (self.url, "requestReregistration")
        data = {'ven_name': ven_name}
        response = self.post(url,
                             headers={'CONTENT-TYPE': 'application/json'},
                             data=json.dumps(data))
        return response.json()

    @wait_for_reg_dec
    def clear_ven(self, ven_name):
        '''
            curl -H "Content-Type: application/json" -X DELETE -d \
            '{ "ven_name": "STEM_TEST_VEN"}' \
            http://localhost:5000/clearVen
        '''
        logger.debug('Clearing VEN data on VTN')
        url = "%s/%s" % (self.url, "clearVen")
        data = {'ven_name': ven_name}
        response = self.delete(url, headers={'CONTENT-TYPE': 'application/json'}, data=json.dumps(data))
        return response.json()


# NOTE every event creation helper already implemented for vtn-sim events endpoint, but
#     still usefull if we decide to skip vtn-ven and directly use stem endpoints
def create_event_args(charge_payloads,
                      event_id=None,
                      ven_name=None,
                      event_start_time=None,
                      event_start_delta=0,
                      interval_duration='PT5M',
                      **kwargs):
    if not event_id:
        event_id = "EVENT-" + get_random_hex_string(10)
    if not event_start_time:
        now_plus_delta = dt_utcnow() + dt.timedelta(seconds=event_start_delta)
        event_start_time = iso8601_utc_datetime(now_plus_delta)
    elif event_start_delta:
        dt_start_plus_delta = isodate.parse_datetime(event_start_time) + dt.timedelta(seconds=event_start_delta)
        event_start_time = iso8601_utc_datetime(dt_start_plus_delta)

    if kwargs.get('interval_seconds'):
        interval_duration = isodate.duration_isoformat(dt.timedelta(seconds=kwargs['interval_seconds']))
    event_args = kwargs.copy()
    event_args.update(
        {
            "event_id": event_id,
            "event_start_time": event_start_time,
            "interval_duration": interval_duration,
            "charge_payloads": charge_payloads,
        }
    )
    if ven_name:
        event_args['ven_name'] = ven_name
    return event_args


# singleton used for testing (we are only using one VTN for testing)
vtnc = VtnClient(settings['vtn']['url'])

if __name__ == "__main__":
    '''Used for easy manual testing of vtn API and creating
       JSON snapshots of party end report registration messsages.
    '''
    consts.LOG_RESPONSE = True
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        handlers=[logging.StreamHandler()])
    ven = DEFAULT_TESTDATA[DEFAULT_TEST_CONFIGURATION]
    vtnc = VtnClient(settings['vtn']['url'])

    def save_party_registration(fpath=OADR_RESOURCES_PATH):
        create_party_registration = vtnc.get_create_party_registration(ven_name=ven)
        created_party_registration = vtnc.get_created_party_registration(ven_name=ven)
        create_party_registration_c = json_dumps(clean_create_party_registration(create_party_registration))
        created_party_registration_c = json_dumps(clean_created_party_registration(created_party_registration))
        logger.debug(create_party_registration_c)
        logger.debug(created_party_registration_c)
        with open(os.path.join(fpath, 'create_party_registration.json'), 'w') as fp:
            fp.write(create_party_registration_c)
        with open(os.path.join(fpath, 'created_party_registration.json'), 'w') as fp:
            fp.write(created_party_registration_c)

    def save_report_registration(fpath=OADR_RESOURCES_PATH):
        report_registration = vtnc.get_report_registration(ven_name=ven)
        cleaned_report_registration = json_dumps(clean_report_registration(report_registration))
        report_registration = json_dumps(report_registration)
        assert report_registration
        logger.debug(cleaned_report_registration)
        with open(os.path.join(fpath, 'report_registration.json'), 'w') as fp:
            fp.write(cleaned_report_registration)

    save_report_registration()

    def print_report_updates(report_specifier_id, request_id=None, wait=10):
        if not request_id:
            request_id = report_specifier_id + '__' + get_random_hex_string(10)
        logger.debug('Created random request id: %s', request_id)
        logger.debug(vtnc.create_report_request(ven_name=ven,
                                                report_specifier_id=report_specifier_id,
                                                report_interval_start=iso8601_utcnow(),
                                                report_request_id=request_id))
        logger.debug('Waiting for report updates for %s', request_id)
        time.sleep(wait)
        logger.debug(json_dumps(vtnc.get_report_updates(ven_name=ven, report_request_id=request_id)))
        logger.debug(json_dumps(vtnc.get_report_updates(ven_name=ven)))

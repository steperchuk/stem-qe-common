import copy
import datetime as dt
import logging
import time
import traceback

import isodate
import pause
# plugin that gives you assume function for collecting errors instead of asserting
# https://github.com/astraw38/pytest-assume
from pytest import assume  # pylint: disable=E0611
from requests import HTTPError

from stemqa.vpp.action import Action, ActionException
from stemqa.vpp.container import Container
from stemqa.vpp.helper import iso8601_utc_datetime, json_dumps, STR_FORMAT, dt_utcnow
from stemqa.vpp.openadr.common import parse_report_update
from stemqa.vpp.openadr.event import OadrEvent, event_time_info
from stemqa.vpp.openadr.event_verification import verify_event_data_new
from stemqa.vpp.openadr.report import OadrReportRequest
from stemqa.vpp.openadr.report_verification import verify_reports
from stemqa.vpp.openadr.vtn_api import VENS, vtn_sim

logger = logging.getLogger(__name__)


def create_container_actions(containers, base_start_dt, verbose=1):
    o_fmt = '%s' if verbose == 1 else '%r'
    container_actions = {}
    container_objects = {}
    if verbose:
        logger.debug('>>> Creating container actions')
        logger.debug('  > actions data: %s', json_dumps(containers))

    for container_name, v in list(containers.items()):
        container_obj = None
        if container_name == vtn_sim.container:
            container_obj = vtn_sim
        else:
            for ven_obj in list(VENS.values()):
                if container_name == ven_obj.container:
                    container_obj = ven_obj
        if not container_obj:
            # TODO test explicit settings
            container_obj = Container(container_name, container_name, **v.get('settings', {}))
        if verbose:
            logger.debug(' >> %r', container_obj)
        container_actions[container_name] = []
        container_objects[container_name] = container_obj

        for method, calls in list(v['actions'].items()):
            for idx, action_offset in enumerate(calls):
                c_action = Action.create_method_call(container_obj, method, idx, base_start_dt,
                                                     int(action_offset))
                container_actions[container_name].append(c_action)
                if verbose:
                    logger.debug('  > Container action: ' + o_fmt, c_action)  # pylint: disable=w1201
    return container_actions, container_objects


def create_event_actions(program, events, base_start_dt, verbose=1):
    events_copy = copy.deepcopy(events)
    o_fmt = '%s' if verbose == 1 else '%r'
    event_actions = {}
    event_objects = {}
    if verbose:
        logger.debug(' >> Creating event actions')
        logger.debug('  > actions data: %s', json_dumps(events))
    for action_object_id, methods in list(events_copy.items()):
        create = methods['create']
        event_start_base_delta = create.get('event_start_delta')
        event_start_delta = create['data'].get('event_start_delta')
        create_request_delta = create['send_request_delta']
        if event_start_base_delta:
            assert not event_start_delta, '%s - Using event_start_delta twice!!' % action_object_id
            event_start_dt = base_start_dt + dt.timedelta(seconds=event_start_base_delta)
            create['data']['event_start_time'] = iso8601_utc_datetime(event_start_dt)
        elif event_start_delta:
            event_start_dt = base_start_dt + dt.timedelta(seconds=create_request_delta) + dt.timedelta(
                seconds=event_start_delta)
            create['data']['event_start_time'] = iso8601_utc_datetime(event_start_dt)
            del create['data']['event_start_delta']

        action_object = OadrEvent(action_object_id, create['data'], program)
        event_actions[action_object_id] = []
        event_objects[action_object_id] = action_object
        if verbose:
            logger.debug(' >> %r', action_object)
        for method, calls in list(methods.items()):
            if not isinstance(calls, (list, tuple)):
                calls = [calls]
            for idx, call in enumerate(calls):
                request_delta = call['send_request_delta']
                kwargs = {'data': call.get('data')}
                event_action = Action.create_method_call(action_object, method, idx,
                                                         base_start_dt, request_delta, kwargs_dict=kwargs)
                event_actions[action_object_id].append(event_action)
                if verbose:
                    logger.debug('  > Event action: ' + o_fmt, event_action)  # pylint: disable=w1201
    return event_actions, event_objects


def create_report_actions(program, reports, base_start_dt, verbose=1):
    o_fmt = '%s' if verbose == 1 else '%r'
    reports_copy = copy.deepcopy(reports)
    report_actions = {}
    report_objects = {}
    if verbose:
        logger.debug(' >> Creating report actions')
        logger.debug('  > actions data: %s', json_dumps(reports))
    for action_object_id, methods in list(reports_copy.items()):
        create = methods['create']
        create_request_delta = create['send_request_delta']
        report_start_base_delta = create.get('report_interval_start_delta')
        report_start_delta = create['data'].get('report_interval_start_delta')
        if report_start_base_delta:
            assert not report_start_delta, '%s - Using report_interval_start_delta twice!!' % action_object_id
            report_start_dt = base_start_dt + dt.timedelta(seconds=report_start_base_delta)
            create['data']['report_interval_start'] = iso8601_utc_datetime(report_start_dt)
        elif report_start_delta:
            report_start_dt = base_start_dt + dt.timedelta(seconds=create_request_delta) + dt.timedelta(
                seconds=report_start_delta)
            create['data']['report_interval_start'] = iso8601_utc_datetime(report_start_dt)
            del create['data']['report_interval_start_delta']
        else:
            report_start_dt = base_start_dt + dt.timedelta(seconds=create_request_delta)
            create['data']['report_interval_start'] = iso8601_utc_datetime(report_start_dt)
        action_object = OadrReportRequest(action_object_id, create['data'], program)
        report_actions[action_object_id] = []
        report_objects[action_object_id] = action_object
        if verbose:
            logger.debug(' >> %r', action_object)
        for method, calls in list(methods.items()):
            if not isinstance(calls, (list, tuple)):
                calls = [calls]
            for idx, call in enumerate(calls):
                request_delta = call['send_request_delta']
                kwargs = {'data': call.get('data')}
                report_action = Action.create_method_call(action_object, method, idx,
                                                          base_start_dt, request_delta, kwargs_dict=kwargs)
                report_actions[action_object_id].append(report_action)
                if verbose:
                    logger.debug('  > Report Action: ' + o_fmt, report_action)  # pylint: disable=w1201
    return report_actions, report_objects


def verify_event_actions(program, event_objects, event_actions, event_verifications, custom_verify_func=None,
                         postpone_assert_error=True, only_test_settings=False):
    event_verification_func = custom_verify_func if custom_verify_func else verify_event_data_new
    telemetry_wait_td = dt.timedelta(seconds=10)
    logger.debug('VERIFICATION OF EVENTS...')
    logger.debug(40 * '=')
    sorted_events = sorted(list(event_objects.items()), key=lambda x: x[1].get_utc_start_dt())
    logger.debug('SORTED EVENTS: %s', json_dumps(sorted_events, True))
    for idx, (event_name, event) in enumerate(sorted_events):
        verification = event_verifications[event_name]
        actions = event_actions[event_name]
        # event_id has random hash postpended to event_name
        event_id = event.event_id
        logger.debug('***Verification of event : %s (start idx: %s)', event_name, idx)
        logger.debug('***Event actions: %s', json_dumps(actions, True))
        logger.debug('***Verification: %s', json_dumps(verification))
        logger.debug(40 * '-')
        verify = True
        if only_test_settings:
            verify = False
        elif not verification['verify']:
            logger.warning('!!!Skipping verification because it is disabled for this event!!!')
            verify = False

        if verify:
            oadr_event = event.get_event()
            grid_events = event.get_grid_events()
            # TODO use event methods to get event time info.
            start_dt, end_dt, duration_td = event_time_info(oadr_event, grid_events)
            logger.debug('***Event starts at: %s', start_dt.strftime(STR_FORMAT))
            logger.debug('***Event will end: %s', end_dt.strftime(STR_FORMAT))
            logger.debug('***Event duration: %ss', duration_td.total_seconds())
            try:
                interval_td = isodate.parse_duration(oadr_event['interval_duration'])
                for interval_idx, interval in enumerate(oadr_event['charge_payloads']):
                    interval_start_dt = start_dt + (interval_td * (interval_idx))
                    interval_end_dt = start_dt + (interval_td * (interval_idx + 1))
                    interval = oadr_event['charge_payloads'][interval_idx]
                    logger.debug(' **Event interval [idx: %s value: %s]', interval_idx, interval)
                    logger.debug(' **  Starts at: %s  ends: %s',
                                 interval_start_dt.strftime(STR_FORMAT),
                                 interval_end_dt.strftime(STR_FORMAT))
                    logger.debug(' **  Waiting until interval ends: %s', interval_end_dt)
                    pause.until(interval_end_dt + telemetry_wait_td)
                pause.until(end_dt + telemetry_wait_td)
                grid_events = event.get_grid_events(use_cached=False)
                event_verification_func(oadr_event, grid_events, verification, program['resources'])
            except AssertionError as e:
                if not postpone_assert_error:
                    raise
                assume(False, 'Event test failed: %s\n%s\nEVENT:%s\n' %
                       (e, traceback.format_exc(), json_dumps([oadr_event, grid_events])))
        logger.debug('Ended testing event: %s', event_id)
        logger.debug(40 * '-')


def verify_report_actions(program, report_objects, report_actions, report_verifications,
                          custom_verify_func, base_start_dt, only_test_settings=False):
    logger.debug('report actions to verify: %s', json_dumps(report_actions, True))
    logger.debug('reports verification data: %s', json_dumps(report_verifications))
    report_verification_func = custom_verify_func if custom_verify_func else verify_reports
    # TODO test out different values and include it into report verification settings
    logger.debug(' >> VERIFICATION OF REPORTS...')
    logger.debug(80 * '=')
    # TODO sorting based on verification end time
    for report_name, report_object in list(report_objects.items()):
        verification = report_verifications[report_name]
        rep_req = report_object.report_request
        request_id = report_object.report_request_id
        logger.debug('Report request: %s', request_id)
        logger.debug(60 * '-')
        logger.debug('\nCreation data: %s', json_dumps(report_object.data))
        logger.debug('\nResponse data: %s', json_dumps(rep_req))
        logger.debug('\nActions: %s', json_dumps(report_actions[report_name], True))
        logger.debug('\nVerification: %s', json_dumps(verification))
        verify = True
        if only_test_settings:
            verify = False
        elif not verification['verify']:
            logger.warning('!!!Skipping verification because it is disabled for this report!!!')
            verify = False

        if verify:
            wait_dt = None
            verify_frame = verification['verify_frame']
            if verify_frame:
                frame_wait_td = dt.timedelta(seconds=verify_frame[-1])
                wait_dt = base_start_dt + frame_wait_td

            if not wait_dt:
                reporting_duration = isodate.parse_duration(rep_req['report_interval_duration'])
                report_back_duration = isodate.parse_duration(rep_req['report_back_duration'])
                # TODO this check should be done when making checks of configuration
                if report_back_duration and not reporting_duration:
                    assume(False, 'This report runs forever(use `verify_frame`!:\n%s' % json_dumps(rep_req))
                    continue
                report_start_dt = isodate.parse_datetime(rep_req['report_interval_start'])
                wait_dt = report_start_dt + reporting_duration

            reports_grace_td = dt.timedelta(seconds=verification['report_wait_lag'])
            wait_dt = wait_dt + reports_grace_td
            logger.debug('Waiting for report updates [request: %s]', request_id)
            # telemetry testing -> callback every minute
            logger.debug('*** Waiting until %s', wait_dt.strftime(STR_FORMAT))
            pause.until(wait_dt)

            if verify_frame and verification.get('cancel_report_after_frame', True):
                if not report_object.cancelled:
                    logger.debug('Cancelling `%s` because of `cancel_report_after_frame` being set!',
                                 report_object.name)
                    report_object.cancel()

            try:
                report_updates = report_object.get_report_updates()
            except HTTPError as e:
                logger.error('Report query failed for `%s`:\nERROR_MSG: %s\nREQUEST_DATA:\n%s\n',
                             request_id, e, json_dumps(rep_req))
                time.sleep(1)
                try:
                    report_updates = report_object.get_report_updates()
                except HTTPError as e:
                    assume(False, 'AFTER retry still no updates for `%s`:\nERROR_MSG: %s\nREQUEST_DATA:\n%s\n' %
                           (request_id, e, json_dumps(rep_req)))
                    continue

            logger.debug('Report updates for report_request_id `%s`', request_id)
            for ridx, report_update in enumerate(report_updates):
                created_dt_iso = report_update['created_date_time']
                if report_update['intervals']:
                    report_update_p = json_dumps(parse_report_update(report_update))
                else:
                    report_update_p = json_dumps(report_update)
                logger.debug('Report update [idx: %s, created: %s]:\n%s', ridx, created_dt_iso, report_update_p)

            report_verification_func(program, report_object, report_updates, verification, base_start_dt)
        logger.debug('Ended testing report request: %s', request_id)
        logger.debug(60 * '-')


def check_actions(actions, until_dt=None, wait_step=30):
    now_dt = dt_utcnow()
    until_dt = until_dt or now_dt
    while until_dt >= now_dt:
        logger.debug('  > Running join on actions to check for errors.')
        for action in actions:
            if action.started and not action.finished:
                action.join()

        if all([a.finished for a in actions]):
            return

        sleep_seconds = (until_dt - dt_utcnow()).total_seconds() + 0.1
        sleep_seconds = sleep_seconds if sleep_seconds < wait_step else wait_step
        time.sleep(sleep_seconds)
        now_dt = dt_utcnow()


def run_actions(actions, last_action_timeout=100, run_sync=False, postone_schedule=False):
    sorted_actions = copy.copy(actions)
    logger.debug(' >> Sorting actions based on start time...')
    sorted_actions.sort(key=lambda a: a.start_dt)
    logger.debug(' >> Actions after sort: \n%s', json_dumps(sorted_actions, short=True))
    last_action_start_dt = sorted_actions[-1].start_dt

    try:
        scheduled_actions = []
        logger.debug(' >> Scheduling all actions')

        if not postone_schedule and not run_sync:
            for action in sorted_actions:
                logger.debug('  > Scheduling action %s (starts at %s)',
                             action.id_, action.start_dt.strftime(STR_FORMAT))
                action.schedule_action_thread()
                scheduled_actions.append(action)
        else:
            run_delta = - dt.timedelta(seconds=30)
            if run_sync:
                run_delta = dt.timedelta(microseconds=1)

            for action in sorted_actions:
                wait_until_dt = action.start_dt + run_delta
                logger.debug(' >> Waiting until %s (next action starts %s)',
                             wait_until_dt.strftime(STR_FORMAT), action.start_dt.strftime(STR_FORMAT))
                check_actions(scheduled_actions, wait_until_dt)
                pause.until(wait_until_dt)
                if run_sync:
                    logger.debug(' >> running action %s', action.id_)
                    action.run_sync()
                else:
                    logger.debug(' >> Scheduling action %s (starts at %s)',
                                 action.id_, action.start_dt.strftime(STR_FORMAT))
                    action.schedule_action_thread()
                    scheduled_actions.append(action)

        assert len(sorted_actions) == len(scheduled_actions)
        check_actions(scheduled_actions, last_action_start_dt + dt.timedelta(seconds=last_action_timeout))
        for action in scheduled_actions:
            action.join()
            if not action.finished:
                raise ActionException("Action %s didnt finish in expected time." % action.id_)
    finally:
        for action in sorted_actions:
            if not action.finished:
                logger.debug(' >> Cancelling action %s', action.id_)
                action.cancel()

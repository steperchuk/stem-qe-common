import copy
import datetime as dt
import json
import logging

import isodate
import requests

from stemqa.config.settings import settings
from stemqa.vpp.helper import s4c, json_dumps, recursive_dict_cleanup, recursive_dict_cleanup_inside_lists

logger = logging.getLogger(__name__)

# VEN ENDPOINTS
REGISTRATION_API_PATH = '/v1/register/'
REGISTRATION_URL = settings['stem-ven-endpoints']['url'] + REGISTRATION_API_PATH

POLL_FREQ = int(settings['vtn']['poll-freq'])
EXTERNAL_REFERENCE_ID = settings['vtn']['vtn-id']

# OPENADR EVENT SPECIFIC
DISPATCH_LAG = 6  # emulators/telemetry lass 6 seconds after dispatch start time
MODIFY = 'MODIFY'
MODIFY_SETPOINT_TABLE = True
ATHENA_VPP_BUCKET = 'athena-vpp'

# OPENADR REPORTS SPECIFIC - Austin SHINES
SYSTEM_REPORT_LAG = 3
VPP_BUCKET = 'stem-ess-optimization'
FORECASTS_PREFIX = 'VPP/forecasts'
DAILY_FORECASTS_PREFIX = 'VPP/forecasts/{ext_ref_id}/daily'
HOURLY_FORECASTS_PREFIX = 'VPP/forecasts/{ext_ref_id}/hourly'
DAILY_FORECAST_ID = 'x-DAILY_FORECAST'
HOURLY_FORECAST_ID = 'x-HOURLY_FORECAST_UPDATE'
NAMEPLATE_ID = 'x-NAMEPLATE'
CHARGE_CURVE_ID = 'x-CHARGE_CURVE'
DISCHARGE_CURVE_ID = 'x-DISCHARGE_CURVE'
CURRENT_STATUS_ID = 'x-CURRENT_STATUS'


def delete_events_in_s4(program, events=None):
    """
    Function that enables us to delete all or specific event objects in s4 for specified opeandr program_name
    """
    program_id = program['id']
    program_events_prefix = '%s/events/' % program_id
    if events:
        events_to_delete = []
        for event_id in events:
            prefix = '%s/%s' % (program_events_prefix, event_id)
            events_to_delete.extend(s4c.list_objects_names(ATHENA_VPP_BUCKET, prefix, True))
    else:
        events_to_delete = s4c.list_objects_names(ATHENA_VPP_BUCKET, program_events_prefix, True)
    logger.debug('Deleting event objects: %s', json_dumps(events_to_delete))
    s4c.remove_objects(ATHENA_VPP_BUCKET, events_to_delete)


def event_identity(event_args):
    return {k: event_args[k] for k in ['ven_name', 'event_id']}


def clean_create_party_registration(json_d):
    '''Cleans create party registration snapshot from volatile metadata'''
    clean_json = json_d.copy()
    recursive_dict_cleanup(clean_json, ['request_id', 'registration_id'])
    return clean_json


def clean_created_party_registration(json_d):
    '''Cleans created party registration snapshot from volatile metadata'''
    clean_json = json_d.copy()
    recursive_dict_cleanup(clean_json, ['registration_id', 'vtn_id', 'ven_id', 'request_id'])
    return clean_json


def clean_report_registration(json_d):
    '''Cleans report registration from volatile metadata'''
    clean_json = json_d.copy()
    recursive_dict_cleanup(clean_json, ['report_request_id', 'created_date_time'])
    return clean_json


def clean_report_updates(json_data):
    '''Cleans report update from volatile metadata'''
    clean_json = copy.deepcopy(json_data)
    recursive_dict_cleanup_inside_lists(clean_json, ['report_request_id', 'created_date_time'])
    return clean_json


def reset_ven_registration(ext_ref_id, reset_datetime):
    registration_payload = {
        "external_reference_id": ext_ref_id,
        "reset_datetime": isodate.datetime_isoformat(reset_datetime)
    }

    headers = {'content-type': 'application/json'}
    resp = requests.post(REGISTRATION_URL,
                         data=json.dumps(registration_payload),
                         headers=headers)
    resp_data = resp.json()
    logger.debug('Registration reset response: %s', json_dumps(resp_data))


def filter_forecasts_by_resources(forecast_names, resources):
    filtered = []
    resource_locations = [res['target_location'] for res in resources]
    for forecast_name in forecast_names:
        for res_loc in resource_locations:
            if res_loc in forecast_name:
                filtered.append(forecast_name)
    return filtered


def get_forecast_object_names(prefix=FORECASTS_PREFIX):
    return s4c.list_objects_names(VPP_BUCKET, prefix, True)


def delete_forecast_objects(prefix=FORECASTS_PREFIX, resources=None):
    logger.debug('Deleting forecast objects')
    forecast_names = get_forecast_object_names(prefix)
    if resources:
        objects_to_delete = filter_forecasts_by_resources(forecast_names, resources)
    else:
        objects_to_delete = forecast_names

    if objects_to_delete:
        logger.debug('Deleting forecast objects: %s', json_dumps(objects_to_delete))
        s4c.remove_objects(VPP_BUCKET, objects_to_delete)
        logger.debug('Deleted forecast objects')
    else:
        logger.debug('No forecast object to delete')


def get_daily_forecast_objects(external_reference_id, resources):
    dailies = get_forecast_object_names(DAILY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    dailies_f = filter_forecasts_by_resources(dailies, resources)
    logger.debug('Daily forecast files created: %s', json_dumps(dailies_f))
    return dailies_f


def get_hourly_forecast_objects(external_reference_id, resources):
    hourlies = get_forecast_object_names(HOURLY_FORECASTS_PREFIX.format(ext_ref_id=external_reference_id))
    hourlies_f = filter_forecasts_by_resources(hourlies, resources)
    logger.debug('Hourly forecast files created: %s', json_dumps(hourlies_f))
    return hourlies_f


def signal_float(signal_info):
    return float(signal_info['payload_float']['value'])


def parse_report_update(report_update):
    parsed_report = []
    intervals = report_update['intervals']['interval']
    for interval in intervals:
        parsed_interval = {}
        for k, v in list(interval['oadr_report_payload'].items()):
            if 'payload_float' in v:
                parsed_interval[k] = signal_float(v)
            else:
                raise Exception('NEW REPORT SIGNAL DATA TYPE - ADD ME NOW!!!: %s' % v)
        parsed_report.append(parsed_interval)
    return parsed_report


def parse_report_updates(report_updates):
    parsed_updates = []
    for report in report_updates:
        parsed_updates.append(parse_report_update(report))
    return parsed_updates


def miliseconds_timestamp(dt_to_convert):
    utc_naive = dt_to_convert.replace(tzinfo=None) - dt_to_convert.utcoffset()
    timestamp = (utc_naive - dt.datetime(1970, 1, 1)).total_seconds()
    return int((timestamp * 1000) + dt_to_convert.microsecond)

import datetime as dt
import logging

# plugin that gives you assume function for collecting errors instead of asserting
# https://github.com/astraw38/pytest-assume
from pytest import assume  # pylint: disable=E0611

from stemqa.helpers.db_helper import DbHelper, DB_GRID_SERVICES
from stemqa.util import math_util
from stemqa.util.grid_response_util import get_battery_output
from stemqa.vpp.helper import STR_FORMAT, json_dumps
from stemqa.vpp.openadr.common import DISPATCH_LAG
from stemqa.vpp.openadr.event import event_time_info
from stemqa.vpp.resource import resource_monitors_locations

logger = logging.getLogger(__name__)


def fetch_event_test_data_old(testdata, event, grid_events, postpone_assert_error=False):
    return fetch_event_test_data(event, grid_events, testdata['resources'],
                                 postpone_assert_error, testdata.get('expect_dispatches', True))


def fetch_event_test_data(event, grid_events, resources, postpone_assert_error=False, expect_dispatches=True):
    assert grid_events, ('Looks like your event has no grid events. Check for errors in workers pods.'
                         'Openadr grid events are created within beat scheduler container every minute')
    dbhelper = DbHelper.default_instance()
    start_dt, end_dt, duration_td = event_time_info(event, grid_events, log=False)
    monitor_id_list, location_codes_list = resource_monitors_locations(resources)

    logger.debug('Getting 1min meter interval data for %s to %s',
                 start_dt.strftime(STR_FORMAT),
                 end_dt.strftime(STR_FORMAT))
    battery_output = get_battery_output(dbhelper,
                                        start_dt,
                                        end_dt,
                                        monitor_id_list,
                                        location_codes_list,
                                        STR_FORMAT)
    minute_intervals = int(duration_td.total_seconds() / 60)
    expected = int(minute_intervals * len(location_codes_list))
    logger.debug('Battery output:\n%s', json_dumps(battery_output))
    error_info = 'Expected num. of intervals: %s, actual num.:%s \nEVENT: %s\nGRID EVENTS: %s' \
                 % (expected, len(battery_output), json_dumps(event), json_dumps(grid_events))
    battery_intervals_missing = len(battery_output) != expected
    if battery_intervals_missing and postpone_assert_error:
        assume(False, error_info)
    elif battery_intervals_missing:
        assert False, error_info
    # get the gs event dispatches
    resource_dispatch_results = get_resource_dispatch_transaction_data_oadr(grid_events, location_codes_list,
                                                                            expect_dispatches)
    logger.debug('Resource dispatch transaction data:\n%s', json_dumps(resource_dispatch_results))
    return battery_output, resource_dispatch_results


def _print_row(i):
    interval_data_str = 'Location: %s GS: %s GS(kw): %s Interval: %s-%s Site: %s Main: %s ' \
                        'Inverter(kw): %s Percent_diff: %s' \
                        % (i['resource'],
                           i['gs_start'],
                           i['gs_power'],
                           i['interval_start'],
                           i['interval_end'],
                           i['interval_site'],
                           i['interval_main'],
                           i['interval_inverter'],
                           i['percent_diff'])
    logger.debug(interval_data_str)


def get_resource_dispatch_transaction_data_oadr(grid_events, location_codes, expect_dispatches=True):
    """
    Gets dispatch values for all location codes for given grid service event
    """
    dbhelper = DbHelper.default_instance()
    grid_event_ids = ','.join(["'%s'" % ge['event_id'] for ge in grid_events])
    locations = ""
    for location_code in location_codes[:-1]:
        locations = locations + "'" + location_code + "',"
    locations = locations + "'" + location_codes[-1] + "'"
    logger.debug('Using locations: %s', locations)

    # https://stemedge.atlassian.net/browse/JEDI-9709
    status_list = '3,9,10'
    sql = "SELECT rdt.gridservice_event_id, " \
          "rdt.resource_id, " \
          "rdt.dispatch_type_id, " \
          "rdt.last_status_id, " \
          "rdt.requested_power, " \
          "rdt.dispatch_start_datetime, " \
          "rdt.dispatch_end_datetime, " \
          "r.location_code " \
          "FROM  grid_services.resource_dispatch_transaction rdt, grid_services.resource r " \
          "WHERE rdt.gridservice_event_id in (%s) " \
          "AND r.location_code in (%s) " \
          "AND rdt.dispatch_type_id in (1,2,4) " \
          "AND rdt.last_status_id in (%s) " \
          "AND r.resource_id = rdt.resource_id order by dispatch_start_datetime;" \
          % (grid_event_ids, locations, status_list)
    result = dbhelper.query(DB_GRID_SERVICES, sql)

    if not result and expect_dispatches:
        logger.error('No dispatch events found! Getting them without event status filtering.')
        sql = "SELECT rdt.gridservice_event_id, " \
              "rdt.resource_id, " \
              "rdt.dispatch_type_id, " \
              "rdt.last_status_id, " \
              "rdt.requested_power, " \
              "rdt.dispatch_start_datetime, " \
              "rdt.dispatch_end_datetime, " \
              "r.location_code " \
              "FROM  grid_services.resource_dispatch_transaction rdt, grid_services.resource r " \
              "WHERE rdt.gridservice_event_id in (%s) " \
              "AND r.location_code in (%s) " \
              "AND r.resource_id = rdt.resource_id order by dispatch_start_datetime;" \
              % (grid_event_ids, locations)
        result = dbhelper.query(DB_GRID_SERVICES, sql)
        logger.error('Dispatches created for gs events %s', json_dumps(grid_event_ids))
        logger.error('**********************************')
        logger.error('\n%s', result)
        logger.error('**********************************')

    resource_dispatch_requests = []
    for line in result:
        resource_dispatch_requests.append({'start': line['dispatch_start_datetime'].strftime(STR_FORMAT),
                                           'end': line['dispatch_end_datetime'].strftime(STR_FORMAT),
                                           'location_code': line['location_code'],
                                           'requested_power': line['requested_power']})

    logger.debug("\ngs event dispatch results:")
    for line in resource_dispatch_requests:
        logger.debug('%s, location: %s requested power: %s', line['start'], line['location_code'],
                     line['requested_power'])
    return resource_dispatch_requests


def verify_aggregation_total_intervals(intervals, verification, rel_tol, abs_tol, abs_tol_inv):
    logger.debug('Testing aggregation totals')
    # get a list of battery intervals that are involved in gs event
    expect_values = verification['total_expected_amount_scheduled_per_interval']
    aggregated_intervals = {}
    default_interval = {'inverter': None, 'dispatched': None}
    for interval in intervals:
        interval_start = interval['interval_start']
        aggregated_intervals.setdefault(interval_start, default_interval.copy())
        gs_dispatch = interval['gs_power']
        if aggregated_intervals[interval_start]['dispatched'] is None:
            if gs_dispatch is not None:
                aggregated_intervals[interval_start]['dispatched'] = gs_dispatch
        elif gs_dispatch:
            aggregated_intervals[interval_start]['dispatched'] += gs_dispatch

        inverter = interval['interval_inverter']
        if aggregated_intervals[interval_start]['inverter'] is None:
            if inverter is not None:
                aggregated_intervals[interval_start]['inverter'] = inverter
        elif inverter:
            aggregated_intervals[interval_start]['inverter'] += inverter

    aggregated_intervals_t = sorted(aggregated_intervals.items())
    logger.debug('aggregated_intervals: %s', json_dumps(aggregated_intervals_t))
    actual_values = []
    for _, val in aggregated_intervals_t:
        actual_values.append({'d': val['dispatched'], 'i': val['inverter']})
    logger.debug('Comparing total aggregated interval amounts:')
    logger.debug('Actual: %s', actual_values)
    logger.debug('Expect: %s', expect_values)
    error_intervals_abs_and_rel = []
    assert len(actual_values) == len(expect_values), 'Length of actual and expected values differ!'
    for actual, expected in zip(actual_values, expect_values):
        if isinstance(expected, dict):
            if actual['i'] is None or expected['i'] is None:
                if expected['i'] is not None or actual['i'] is not None:
                    error_intervals_abs_and_rel.append({'expected': expected, 'actual': actual})
            else:
                ai = math_util.isclose(actual['i'], expected['i'], rel_tol=rel_tol)
                bi = math_util.isclose(actual['i'], expected['i'], abs_tol=abs_tol_inv)
                if not ai and not bi:
                    error_intervals_abs_and_rel.append({'expected': expected, 'actual': actual})
            if actual['d'] is None or expected['d'] is None:
                if expected['d'] is not None or actual['d'] is not None:
                    error_intervals_abs_and_rel.append({'expected': expected, 'actual': actual})
            else:
                ad = math_util.isclose(actual['d'], expected['d'], rel_tol=rel_tol)
                bd = math_util.isclose(actual['d'], expected['d'], abs_tol=abs_tol)
                if not ad and not bd:
                    error_intervals_abs_and_rel.append({'expected': expected, 'actual': actual})
        elif isinstance(expected, int):
            a = math_util.isclose(actual['i'], expected, rel_tol=rel_tol)
            b = math_util.isclose(actual['i'], expected, abs_tol=abs_tol)
            if not a and not b:
                error_intervals_abs_and_rel.append((actual, expected))
        else:
            raise Exception('Wrong verification data: %s' % expect_values)

    if error_intervals_abs_and_rel:
        logger.debug('Errors found: \n%s', error_intervals_abs_and_rel)
    assert not error_intervals_abs_and_rel, 'Errors found when comparing actual and expected outputs!'


def verify_event_data_new(oadr_event, grid_events, verification, resources, dispatch_lag=DISPATCH_LAG):
    """
    Compares the battery output from 1 minute meter log tables for resources in gs event to the requested
    amount from gs event
    :param event_data: list of grid service events that represents the openadr event
    :param testdata:
    :param battery_output:
    :param resource_dispatch_results:
    :return:
    """
    rel_tol = verification['relative_tolerance']
    abs_tol = verification['absolute_tolerance_kw']
    abs_tol_inv = verification.get('absolute_tolerance_inverter_kw', 10)
    expect_dispatches = verification['expect_dispatches']
    if not verification['battery_output'] or not verification['resource_dispatch_results']:
        battery_output, resource_dispatch_results = fetch_event_test_data(oadr_event, grid_events, resources,
                                                                          postpone_assert_error=False,
                                                                          expect_dispatches=expect_dispatches)
    else:
        battery_output = verification['battery_output']
        resource_dispatch_results = verification['resource_dispatch_results']

    assert battery_output, 'No telemetry found for resource!'
    error_intervals_abs_and_rel = []
    intervals = []
    gs_interval_td = dt.timedelta(seconds=oadr_event['interval_seconds'])

    logger.debug('COUPLING BATTERY DATA WITH DISPATCH INTERVALS:')
    for interval in battery_output:
        interval_start_dt = dt.datetime.strptime(interval['start'], STR_FORMAT)
        interval_end_dt = dt.datetime.strptime(interval['end'], STR_FORMAT)
        interval_site = interval['site'] if interval['site'] else None
        interval_main = interval['main'] if interval['main'] else None
        interval_inverter = interval['inverter'] if interval_site else None
        dispatch_for_interval_found = False
        for line in resource_dispatch_results:
            gs_dispatch_resource = line['location_code']
            if gs_dispatch_resource != interval['location_code']:
                continue
            gs_dispatch_start = line['start']
            gs_dispatch_start_dt = dt.datetime.strptime(gs_dispatch_start, STR_FORMAT) + dt.timedelta(
                seconds=dispatch_lag)
            gs_dispatch_end = line['end']
            gs_dispatch_end_dt = dt.datetime.strptime(gs_dispatch_end, STR_FORMAT) + dt.timedelta(seconds=dispatch_lag)
            gs_dispatch_td = gs_dispatch_end_dt - gs_dispatch_start_dt
            assert gs_dispatch_td == gs_interval_td
            gs_dispatch_power = line['requested_power']
            battery_intervals_td_sum = dt.timedelta(seconds=0)
            if interval_start_dt >= gs_dispatch_start_dt and interval_end_dt <= gs_dispatch_end_dt:
                dispatch_for_interval_found = True
                logger.debug('  start: %s', interval_start_dt)
                logger.debug('  end:   %s', interval_end_dt)
                logger.debug('  BATTERY INTERVAL is inside GS DISPATCH interval')
                logger.debug('  BATTERY INTERVAL inverter power: %s', interval_inverter)
                logger.debug(60 * '-')
                battery_intervals_td_sum += interval_end_dt - interval_start_dt
                if gs_dispatch_power is not None and interval_inverter is not None:
                    diff = abs(gs_dispatch_power - interval_inverter)
                    if gs_dispatch_power == 0.0 and interval_inverter == 0.0:
                        val = 0.0
                    elif gs_dispatch_power == 0.0 and interval_inverter != 0.0:
                        gs_dispatch_power = 0.001
                        val = diff / gs_dispatch_power * 100
                    else:
                        val = diff / gs_dispatch_power * 100
                else:
                    val = diff = None

                interval_data = {'resource': gs_dispatch_resource, 'gs_start': gs_dispatch_start,
                                 'gs_power': gs_dispatch_power, 'interval_start': interval['start'],
                                 'interval_end': interval['end'], 'interval_site': interval_site,
                                 'interval_main': interval_main, 'interval_inverter': interval_inverter,
                                 'percent_diff': val, 'diff': diff}

                intervals.append(interval_data)
            elif gs_dispatch_end_dt <= interval_start_dt or gs_dispatch_start_dt >= interval_end_dt:
                pass
            else:
                raise Exception(
                    'We are not including some battery data for this dispatch,'
                    'because our grid service event is not rounded to minutes')

        if not dispatch_for_interval_found:
            interval_data = {'resource': interval['location_code'], 'gs_start': None,
                             'gs_power': None, 'interval_start': interval['start'],
                             'interval_end': interval['end'], 'interval_site': interval_site,
                             'interval_main': interval_main, 'interval_inverter': interval_inverter,
                             'percent_diff': None, 'diff': None}
            intervals.append(interval_data)

    logger.debug('**** INTERVALS')
    for i in intervals:
        _print_row(i)

    expected_amount_scheduled_per_interval = verification.get('expected_amount_scheduled_per_interval')
    if expected_amount_scheduled_per_interval:
        for key in list(expected_amount_scheduled_per_interval.keys()):
            expected_values = expected_amount_scheduled_per_interval[key]
            logger.debug('KEY: %s, expected_values: %s', key, expected_values)
            actual_values = []
            idx = 0
            for i in intervals:
                if key == i['resource']:
                    assert len(expected_values) > idx, 'Expected values lenght is smaller than event length in minutes'
                    if isinstance(expected_values[idx], dict):
                        actual_values.append({'i': i['interval_inverter'], 'd': i['gs_power']})
                    else:
                        actual_values.append(i['interval_inverter'])
                    idx += 1
            logger.debug('Resource: %s Actual Values: %s', key, actual_values)
            logger.debug('Resource: %s Expect Values: %s', key, expected_values)
            assert len(actual_values) == len(expected_values), 'Length of actual and expected values differ!'
            for idx, (actual, expected) in enumerate(zip(actual_values, expected_values)):
                # verify we are within relative(5%) and absolute(5kw) tolerances
                # between actual battery output and requested amount during gs dispatch
                logger.debug('%s actual value: %s expected value %s', key, actual, expected)
                if isinstance(actual, dict):
                    if actual['i'] is None or expected['i'] is None:
                        if expected['i'] is not None or actual['i'] is not None:
                            error_intervals_abs_and_rel.append({'resource': key, 'idx': idx,
                                                                'expected': expected, 'actual': actual})
                    else:
                        ai = math_util.isclose(actual['i'], expected['i'], rel_tol=rel_tol)
                        bi = math_util.isclose(actual['i'], expected['i'], abs_tol=abs_tol_inv)
                        if not ai and not bi:
                            error_intervals_abs_and_rel.append({'resource': key, 'idx': idx,
                                                                'expected': expected, 'actual': actual})
                    if actual['d'] is None or expected['d'] is None:
                        if expected['d'] is not None or actual['d'] is not None:
                            error_intervals_abs_and_rel.append({'resource': key, 'idx': idx,
                                                                'expected': expected, 'actual': actual})
                    else:
                        ad = math_util.isclose(actual['d'], expected['d'], rel_tol=rel_tol)
                        bd = math_util.isclose(actual['d'], expected['d'], abs_tol=abs_tol)
                        if not ad and not bd:
                            error_intervals_abs_and_rel.append({'resource': key, 'idx': idx,
                                                                'expected': expected, 'actual': actual})
                elif isinstance(actual, list):
                    a = math_util.isclose(actual, expected, rel_tol=rel_tol)
                    b = math_util.isclose(actual, expected, abs_tol=abs_tol)
                    if not a and not b:
                        error_intervals_abs_and_rel.append({'resource': key, 'idx': idx,
                                                            'expected': expected, 'actual': actual})
    if error_intervals_abs_and_rel:
        logger.debug('Errors found: \n%s', error_intervals_abs_and_rel)
    assert not error_intervals_abs_and_rel, 'Errors found when comparing actual and expected outputs!'

    # aggregate amount per interval
    if verification.get('total_expected_amount_scheduled_per_interval'):
        verify_aggregation_total_intervals(intervals, verification, rel_tol, abs_tol, abs_tol_inv)

    if verification.get('expected_grid_event_status_per_interval'):
        logger.debug('Verification of grid events final status:')
        logger.debug('grid_events: %s', json_dumps(grid_events))
        logger.debug('expected status list based on event interval order: %s',
                     json_dumps(verification['expected_grid_event_status_per_interval']))
        for idx, grid_event in enumerate(grid_events):
            num_l = len(str(idx))
            interval_idx = int(grid_event['reference_id'][-num_l])
            expected_status = verification['expected_grid_event_status_per_interval'][interval_idx]
            assert grid_event['status'] == expected_status, \
                'Actual status of grid event: %s  expected: %s' % (grid_event['status'], expected_status)

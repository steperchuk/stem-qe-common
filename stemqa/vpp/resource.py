import datetime as dt
import logging
import os
import time

from kombu import Connection
from kombu.pools import producers

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_OPTIMIZATION, DB_FORECAST, DB_METER_LOGS, DB_TARIFF, \
    DB_GRID_SERVICES, DB_BUSINESS
from stemqa.util import dataload_util
from stemqa.util.emulator_util import configure_simple_emulator, reset_emulator_db_entries
from stemqa.util.emulator_util import setup_emulator_container
from stemqa.util.emulator_util import stop_and_delete_emulator
from stemqa.util.powerstore_db_util import set_tariff_data
from stemqa.util.powerstore_db_util import set_tariff_data_optimization_mode
from stemqa.util.powerstore_db_util import set_tariff_data_value
from stemqa.util.shell_util import ShellUtilError
from stemqa.vpp.container import get_pods_info, get_running_pod, run_docker_command
from stemqa.vpp.helper import ExcThread, dt_utcnow
from stemqa.vpp.telemetry import last_telemetry_data

logger = logging.getLogger(__name__)

LOCAL_ENV = os.environ.get('LOCAL_ENV', False)
K8S_NAMESPACE = settings['env-name'] if not LOCAL_ENV else None


def get_resource_emulator(resource):
    if not LOCAL_ENV:
        return get_running_pod(app=resource['emulator_hostname'])
    else:
        cmd_ps = 'docker ps -a -q -f name=%s' % resource['emulator_hostname']
        response, _ = run_docker_command(cmd_ps)
        tag = response.strip()
        return tag


def setup_emulator(resource):
    dbhelper = DbHelper.default_instance()
    if not LOCAL_ENV:
        # if previous tests fail to clean specific emulators, we have to clean them up.
        # especially useful when stoping/restarting tests with local test runner.
        pod_name = get_running_pod(app=resource['emulator_hostname'])
        logger.debug('Pod name: %s', pod_name)
        if pod_name:
            try:
                stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                         location_code=resource['target_location'],
                                         delete_db_entries=False)
            except ShellUtilError as e:
                logger.warning('Emulator deletion failed: %s', e)
                pods_info = get_pods_info(resource['emulator_hostname'])
                logger.debug('PODS INFO: \n%s', pods_info)

            waiting_for_pod_to_delete = 0
            while pod_name and waiting_for_pod_to_delete < 60:
                logger.warning('Waiting for emulator pod to delete. REUSE OF EMULATOR AND FAILED CLEANUP!!!')
                pod_name = get_running_pod(resource['emulator_hostname'])
                logger.debug('Pod name: %s', pod_name)
                if pod_name:
                    waiting_for_pod_to_delete += 4
                    time.sleep(4)
    else:
        cmd_ps = 'docker ps -a -q -f name=%s' % resource['emulator_hostname']
        response, _ = run_docker_command(cmd_ps)
        tag = response.strip()
        if tag:
            cmd_rm = 'docker rm -f %s' % tag
            run_docker_command(cmd_rm)

    try:
        emulator_hostname = resource['emulator_hostname']
        batterysoc = None
        setpoint = None
        if 'batterysoc' in resource:
            batterysoc = resource['batterysoc']
        if 'setpoint' in resource:
            setpoint = resource['setpoint']

        source_location = resource.get('source_location') or 'qa-QA_CLIENT_00_SAMPLE_001-1-M1'
        sample_start_date = resource.get('sample_start_date') or '2018-01-27 08:00:00.0000'
        sample_end_date = resource.get('sample_end_date') or '2018-01-27 09:00:00.5000'

        setup_emulator_container(emulator_name=emulator_hostname)

        # configure emulator to replay telemetry
        configure_simple_emulator(dbhelper,
                                  target_location=resource['target_location'],
                                  emulator_hostname=emulator_hostname,
                                  source_location=source_location,
                                  sample_start_date=sample_start_date,
                                  sample_end_date=sample_end_date,
                                  batterysoc=batterysoc,
                                  setpoint=setpoint)
        set_max_demand_settings(resource)
    except ShellUtilError as e:
        # simple retry logic in case emulator setup is fragile
        logger.warning('Emulator setup failed: %s', e)
        pods_info = get_pods_info(resource['emulator_hostname'])
        logger.debug('EMULATOR %s PODS INFO: \n%s', resource['emulator_hostname'], pods_info)
        raise


def setup_resources(resources):
    """Configuring resources and resource emulator based on settings.
       Running each setup in thread so we can speed this up.
    """
    # setup emulator container
    if len(resources) > 1:
        # setup will be faster if we run each emulator setup in thread.
        threads = []
        for res in resources:
            th = ExcThread(target=setup_resource, args=[res],
                           name='setup-%s' % res['emulator_hostname'])
            threads.append(th)
            th.start()

        for thr in threads:
            thr.join(timeout=1200)
            if thr.isAlive():
                raise Exception('Resource setup is taking more than 20 minutes')
    else:
        for res in resources:
            setup_resource(res)


def set_setpoint_change_table(emulator_hostname, setpoint, only_delete_previous=False):
    """
    modify_setpoint_table: Sets powerstore_setpoint_change with resource setpoint so that proper calculations of
                           available charge power can be used by current status reports and event allocation.
                           Both get the data from -> `stem-lib/stem-gridservice/stem_gs/services/topology_utils.py`
                           This table is supposed to be populated in production.
    """
    dbhelper = DbHelper.default_instance()
    sql = "Delete from powerstore_setpoint_change where hostname='{hostname}';"
    sql = sql.format(hostname=emulator_hostname)
    dbhelper.query(DB_OPTIMIZATION, sql)
    if not only_delete_previous:
        logger.debug('Setting powerstore_setpoint_change table.')
        sql = '''
        Insert into powerstore_setpoint_change
        (id, component_id, hostname, optimization_id, msg_timestamp_change_datetime, set_point)
        values (NULL, '{hostname}-site_load_averager', '{hostname}', '{hostname}-optimization',
        '2018-05-08 16:00:00', {setpoint});
        '''.format(hostname=emulator_hostname, setpoint=setpoint)
        dbhelper.query(DB_OPTIMIZATION, sql)


def delete_resource_telemetry(resource):
    dbhelper = DbHelper.default_instance()
    emulator_hostname = resource['emulator_hostname']
    sql_to_execute = """
    SET @EMU = '%s';
    DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-site');
    DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-inverter');
    DELETE FROM meter_logs.monitor_data where monitor_id = CONCAT(@EMU,'-main');
    DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-site');
    DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-inverter');
    DELETE FROM meter_logs.monitor_data_minute where monitor_id = CONCAT(@EMU,'-main');
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-site');
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-inverter');
    DELETE FROM meter_logs.monitor_data_15_minute where monitor_id = CONCAT(@EMU,'-main');
    DELETE FROM optimization.powerstore_bms where hostname = @EMU;
    DELETE FROM optimization.powerstore_bms_minute where hostname = @EMU;
    DELETE FROM optimization.powerstore_bms_15_minute where hostname = @EMU;
    """
    sql = sql_to_execute % (emulator_hostname)
    dbhelper.query(DB_FORECAST, sql)


def setup_resource(resource):
    # NOTE resource db setup could be more generic when api for program creation is added.
    # https://stemedge.atlassian.net/wiki/spaces/SW/pages/333415571/Data+Files+for+unit+test
    emulator_hostname = resource['emulator_hostname']
    location = resource['target_location']
    if resource.get('delete_telemetry', True):
        delete_resource_telemetry(resource)
    if resource.get('reset_db_entries', False):
        reset_emulator_db_entries(emulator_hostname, location)
    if resource.get('run_emulator', True):
        setup_emulator(resource)

    tarrif_schedule_id = resource.get('tariff_schedule_id', 807)
    if tarrif_schedule_id:
        end_date = '%s-12-31 23:59:00' % dt_utcnow().year
        set_tariffs_schedule_end_date(tarrif_schedule_id, resource.get('tariff_schedule_end_date', end_date))

    if resource.get('setpoint_change_table'):
        set_setpoint_change_table(emulator_hostname, resource['setpoint'])
    else:
        set_setpoint_change_table(emulator_hostname, resource['setpoint'], True)

    if resource['telemetry']['import_15min']:
        import_resource_history_data(resource)
    if resource['telemetry']['import_1min']:
        logger.warning('1min telemetry import is not implemented')
    if resource['telemetry']['import_1s']:
        logger.warning('1s telemetry import is not implemented')

    if resource['weather_data']:
        logger.warning('Weather data import is not implemented')


def teardown_resource(resource, delete_db_entries=False, fail_on_error=True):
    """Remove resource emulator container/pods and optionally associated database entries.
       Optionally you can delete associated database entries.
    """
    try:
        stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                 location_code=resource['target_location'],
                                 delete_db_entries=delete_db_entries)
    except ShellUtilError as err:
        last_telemetry_data([resource])
        if fail_on_error:
            raise
        logger.error('Teardown of emulator %s failed:\n%s',
                     resource['emulator_hostname'], err)


def teardown_resources(resources, delete_db_entries=False, fail_on_error=True):
    """
    Using teardown_resource for teardown of complete list of resources.
    """
    # setup emulator container
    if len(resources) > 1:
        # setup will be faster if we run each emulator setup in thread.
        threads = []
        for res in resources:
            emulator = get_resource_emulator(res)
            if not emulator:
                logger.debug('Emulator `%s` is not running. Skipping teardown.', emulator)
                continue

            th = ExcThread(target=teardown_resource, args=[res, delete_db_entries, fail_on_error],
                           name='teardown-%s' % res['emulator_hostname'])
            threads.append(th)
            th.start()

        for thr in threads:
            thr.join(timeout=600)
            if thr.isAlive():
                raise Exception('Resource teardown is taking more than 10 minutes')


def set_max_demand_settings(resource, tzone='US/Pacific'):
    """Setting emulator configuration with resource settings.
    """
    emulator_hostname = resource['emulator_hostname']
    setpoint = resource['setpoint']

    set_tariff_data(280,
                    setpoint,
                    setpoint,
                    setpoint,
                    amqp_port=resource['emulator_rabbit_port'],
                    emulator=emulator_hostname,
                    host=settings['rmq']['host'])

    set_tariff_data_value('max_stem_demand',
                          setpoint,
                          emulator=emulator_hostname,
                          host=settings['rmq']['host'])

    set_tariff_data_value('max_utility_demand',
                          280,
                          emulator=emulator_hostname,
                          host=settings['rmq']['host'])

    set_tariff_data_optimization_mode(3,
                                      tzone,
                                      settings['rmq']['rabbit-port'],
                                      emulator=emulator_hostname,
                                      host=settings['rmq']['host'])


def set_tariffs_schedule_end_date(schedule_version_id, end_date=None):
    dbhelper = DbHelper.default_instance()

    logger.debug('Setting tariffs schedule version id')
    sql = \
        '''
        update tariff_schedule_version set effective_end_date = '%s'
        where tariff_schedule_version_id = %s;
        ''' % (end_date, schedule_version_id)
    dbhelper.query(DB_TARIFF, sql)


def import_emulator_settings(resource):
    emulator_hostname = resource['emulator_hostname']
    target_location = resource['target_location']
    reset_emulator_db_entries(emulator_hostname, target_location)

    dbhelper = DbHelper.default_instance()

    logger.debug('Setting proper business db tables for resource: %s', target_location)
    sql = \
        '''
        SET @EMU = '{emulator}';
        SET @LOCATION = '{location}';
        UPDATE `location_monitor` SET `monitor_id` = CONCAT(@EMU,'-site')
                                  WHERE `location_code` = CONCAT(@LOCATION,'_SITE');
        UPDATE `location_monitor` SET `monitor_id` = CONCAT(@EMU,'-main')
                                  WHERE `location_code` = CONCAT(@LOCATION,'_MAIN');
        UPDATE `location_monitor` SET `monitor_id` = CONCAT(@EMU,'-inverter')
                                  WHERE `location_code` = CONCAT(@LOCATION,'_INVERTER');
        '''.format(location=target_location, emulator=emulator_hostname)
    dbhelper.query(DB_BUSINESS, sql)


def import_resource_history_data(resource):
    dbhelper = DbHelper.default_instance()
    emulator_hostname = resource['emulator_hostname']
    sql = "DELETE FROM monitor_data_15_minute WHERE monitor_id like '%%%s%%'" % emulator_hostname
    dbhelper.query(DB_METER_LOGS, sql)

    # find offset in minutes
    file_start_time = '2017-06-02 09:45:01'
    file_start_datetime = dt.datetime.strptime(file_start_time, "%Y-%m-%d %H:%M:%S")
    delta = dt.datetime.utcnow() - file_start_datetime
    hours = int(delta.total_seconds() / 3600)
    logger.debug('Number of hours to add to each timestamp in file: %s', hours)

    # import files
    logger.debug('Importing files')
    s3_folder = "s3://stemqa-sampledata/csv_import/meter_logs/realtime/monitor_data_15_minute"
    # resource_folder = "resources/openadr"

    for suffix in ['main', 'inverter', 'site']:
        s3_file = '%s/meter_data_15_minute_emulator-2-%s.csv' % (s3_folder, suffix)
        # resource_file = '%s/meter_data_15_minute_emulator-2-%s.temp.csv' % (resource_folder, suffix)
        monitor_id = 'emulator-2-%s' % suffix
        new_monitor_id = emulator_hostname + '-' + suffix
        options = {"database": DB_METER_LOGS,
                   "table": "monitor_data_15_minute",
                   "transform": {"time_offset_unit": "hours",
                                 "time_offset": hours,
                                 "monitor_id": monitor_id,
                                 "new_monitor_id": new_monitor_id
                                 }
                   }
        # transform & import these files- in mysql container, import logged in /var/log/sqlexecutor.log
        # data- select * from meter_logs.monitor_data_15_minute where monitor_id
        #              like '%emulator%' order by sample_start_datetime;

        dataload_util.load_data(s3_file, options, False)


def resource_monitors_locations(resources):
    # get the monitor_id and location_code for each resource
    monitor_id_list = []
    location_codes_list = []
    for resource in resources:
        monitor_id_list.append(resource['emulator_hostname'])
        location_codes_list.append(resource['target_location'])
    return monitor_id_list, location_codes_list


def setup_aggregation_resources(aggregation_id, resources):
    dbhelper = DbHelper.default_instance()

    select_sql = \
        '''
        SELECT id from `aggregation_resource`
        WHERE `aggregation_id`=%s
        ''' % aggregation_id

    delete_sql = \
        '''
        DELETE from `aggregation_resource`
        WHERE `aggregation_id`=%s
        ''' % aggregation_id

    insert_sql_t = \
        '''
        INSERT INTO `aggregation_resource` (`id`,
                                            `aggregation_id`,
                                            `resource_id`,
                                            `active`,
                                            `available`,
                                            `allocation`,
                                            `allocation_capacity`,
                                            `reserve_allocation`,
                                            `reserve_allocation_capacity`,
                                            `restricted_allocation`,
                                            `restricted_allocation_capacity`)
        VALUES (NULL, %s, %s);
        '''

    result = dbhelper.query(DB_GRID_SERVICES, select_sql)
    if result:
        logger.debug("Deleting following resources for aggregation_id %s:\n%s",
                     aggregation_id,
                     result)
        result = dbhelper.query(DB_GRID_SERVICES, delete_sql)

    logger.debug("Inserting following resources for aggregation_id %s:\n%s",
                 aggregation_id,
                 resources)
    for resource in resources:
        insert_sql = insert_sql_t % (aggregation_id, resource)
        result = dbhelper.query(DB_GRID_SERVICES, insert_sql)
        logger.debug("Insert result: %s", result)


def clear_offset_schedule(amqp_port,
                          host='localhost',
                          username='powermonitor',
                          password='4hN7k4Uf'):
    """Clear schedules on emulator

       Usage:
            input_args = {
                'host': settings['rmq']['host'],
                'username': settings['rmq']['username'],
                'password': settings['rmq']['password'],
                'amqp_port': resource['emulator_rabbit_port']
            }
            clear_offset_schedule(**input_args)
    """

    clean_offset = \
        '''
        {
            "reply_exchange": "pgx",
            "args": {
                "value": {
                    "2": [
                    ],
                    "9": [
                        {
                            "soc_band": [
                                0,
                                99
                            ],
                            "active_periods": {
                                "*": [
                                    [
                                        0,
                                        1440
                                    ]
                                ]
                            }
                        }
                    ]
                }
            },
            "command": "set_offset_schedule"
        }
        '''
    amqp_url = 'amqp://%s:%s@%s:%s//' % (username,
                                         password,
                                         host,
                                         amqp_port)
    logger.debug('Sending msg on: url: %s, exchange: %s, routing key: %s', amqp_url, 'pgx', 'command')
    logger.debug('With payload:\n%s', clean_offset)
    connection = Connection(amqp_url)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(clean_offset,
                         exchange='pgx',
                         routing_key='command')


def stop_all_emulators():
    """
    stop all emulator containers
    """
    logger.debug('Removing all emulator containers')
    cmd_ps = 'docker ps -a -q -f name=%s' % 'emulator*'
    response, _ = run_docker_command(cmd_ps)
    tags = ' '.join(response.splitlines())
    logger.debug('tags: %s', tags)
    if tags:
        cmd_rm = 'docker rm -f %s' % tags
        run_docker_command(cmd_rm)

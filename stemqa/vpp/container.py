import os
import time

from stemqa.config.settings import settings
from stemqa.util.shell_util import kubectl, run_process
from stemqa.vpp.helper import json_dumps
from stemqa.config.logconfig import get_logger

logger = get_logger(__name__)
# K8
LOCAL_ENV = os.environ.get('LOCAL_ENV', False)
logger.debug('LOCAL_ENV: %s', LOCAL_ENV)
K8S_NAMESPACE = settings['env-name'] if not LOCAL_ENV else None
logger.debug('k8_NAMESPACE: %s', K8S_NAMESPACE)

# TODO now we can name containers and charts same. No need for this mapping.
# S4 container and k8s app (should we name them same)
S4_CONTAINER = 's4server'
S4_K8_APP = 'stem-s4-server'

# VTN container and k8s app have the same name
VTN_CONTAINER = VTN_K8_APP = 'vtn-sim'

# Container to app mapping (should be other way around in case k8s app will have many containers?)
container_k8s_app = {
    S4_CONTAINER: S4_K8_APP,
    VTN_CONTAINER: VTN_K8_APP
}


# NOTE get_chart_name function is useful for getting chart artifacts names used in deployment of specific app.
#     Usually app name and chart artifact names match, but Some apps/services can sometimes be put into one chart.
#     Webserver and nginx are a good example of that ... that is why there can be naming diffs between apps and charts.
#     We also experimented with putting multiple VENs into one chart, but that seemed like a wrong approach,
#     because we want to separate deployment and artifacts used
#     so that we can have better and more explicit control of each VEN
def get_chart_name(app):
    return app


class ContainerException(Exception):
    pass


class Container(object):
    """Wrapper for managing compose container and k8s pod(app) uniformly. Hidding all the mess behind uniform methods.
       This could get more complicated if apps will contain more active pods/containers(new generation of emulators,
       could involve 3 active pods/containers)
    """

    def __init__(self, name, container, k8s_app=None, host=None, port=None, version=None,
                 chart_name=None, k8s_ns=K8S_NAMESPACE):
        self.name = name
        self.container = container
        self.k8s_app = k8s_app
        self.host = host
        self.port = port
        self.k8s_ns = k8s_ns
        self.version = version
        self.chart_name = chart_name
        self.complete_data()

    def toJSONshort(self):
        return 'OadrContainer(name=%s, event_id=%s, k8s_app=%s)' % (self.name, self.container, self.k8s_app)

    def __str__(self):
        return self.toJSONshort()

    def toJSON(self):
        return {'__class__': 'Container',
                'name': self.name,
                'container': self.container,
                'k8s_app': self.k8s_app,
                'k8s_ns': self.k8s_ns,
                'host': self.host,
                'port': self.port,
                'version': self.version,
                'chart_name': self.chart_name
                }

    def __repr__(self):
        return json_dumps(self.toJSON())

    def complete_data(self):
        if self.k8s_ns and not self.k8s_app:
            self.k8s_app = container_k8s_app.get(self.container, self.container)
        if self.k8s_ns and not self.chart_name:
            self.chart_name = get_chart_name(self.k8s_app)
        if self.k8s_ns and not self.version:
            self.version = settings['artifacts'].get(self.chart_name)
            if not self.version:
                logger.error('Artifact info for %s is missing.', self.k8s_app)

    def running(self, timeout=None):
        resp = None
        sleep_seconds = 1
        start_t = time.time()
        while not resp and time.time() < start_t + (timeout or sleep_seconds):
            if self.k8s_ns:
                resp = get_running_pod(self.k8s_app, self.k8s_ns)
            else:
                resp = container_id(self.container, running=True)
            if not timeout:
                break
            time.sleep(sleep_seconds)
        return resp

    def responds(self, timeout=10):
        if self.host and self.port:
            return connection_responds(self.host, self.port, timeout)
        else:
            return self.running(timeout)

    def start(self, timeout=60, **kwargs):
        if self.running():
            raise ContainerException('Container already running. Cannot be started again.')

        if self.k8s_ns:
            start_k8s_app(self.k8s_app, self.version, self.host, self.port, self.k8s_ns)
        else:
            start_container(self.container, self.host, self.port, timeout)

    def stop(self, timeout=60):
        if not self.running():
            logger.error('Container is not running. Cannot be stopped again')
            return

        if self.k8s_ns:
            stop_k8s_app(self.k8s_app, self.host, self.port, self.k8s_ns)
        else:
            stop_container(self.container, self.host, self.port, timeout)

    def restart(self, timeout=60, **kwargs):
        if self.k8s_ns:
            restart_k8_app(self.k8s_app, self.host, self.port, timeout)
        else:
            restart_container(self.container, self.host, self.port, timeout)

        if not self.responds(timeout):
            raise ContainerException("Container %s didn't respond after restart in %ss" % (self.name, timeout))


def run_docker_command(cmd):
    response, returncode = run_process(cmd)
    logger.debug('Executing docker command: %s', cmd)
    logger.debug('Docker return code: %s', returncode)
    logger.debug('Docker response: %s', response)
    return response, returncode


def kubectl2(namespace, cmd, json_output=False, show_output=False, fail_on_error=True):
    response, rc = kubectl(namespace, cmd, json_output=json_output, show_output=show_output, fail_on_error=False)
    if rc and response and 'array index out of bounds' in response:
        response = None
    elif rc and fail_on_error:
        response, rc = kubectl(namespace, cmd, json_output=json_output, show_output=show_output, fail_on_error=True)
    return response, rc


def get_running_pod(app=None, namespace=K8S_NAMESPACE):
    """Getting pod for specific app if in Running state.
    """
    k8cmd = 'get pods -l app=%s' % app
    pods_i, _ = kubectl2(namespace, k8cmd)
    for pod_i in pods_i.splitlines()[1:]:
        pod_info = pod_i.split()
        pod_name = pod_info[0]
        if pod_info[2].upper() == 'Running'.upper():
            return pod_name


def get_terminating_pod(app=None, namespace=K8S_NAMESPACE):
    """Getting all pods for specific app that are in Terminating state.
    """
    k8cmd = 'get pods -l app=%s' % app
    pods_i, _ = kubectl2(namespace, k8cmd)
    for pod_i in pods_i.splitlines()[1:]:
        pod_info = pod_i.split()
        pod_name = pod_info[0]
        if pod_info[2].upper() == 'Terminating'.upper():
            return pod_name
        elif pod_info[2].upper().startswith('INIT'):
            return pod_name


def get_pods_info(app=None, namespace=K8S_NAMESPACE):
    k8cmd = 'get pods'
    if app:
        k8cmd += ' -l app=%s' % app
    json_o, _ = kubectl2(namespace, k8cmd, json_output=True)
    return json_o


def get_pod_version(app, namespace=K8S_NAMESPACE):
    """Gets semantic version of pod, parsed from dokcer image used by pod.
       Needed if we want to undeploy and redeploy same pod (VEN for example), and we have no other way
       of getting this info.
    """
    k8cmd = 'get pods -o=jsonpath="{.items[].spec.containers[].image}" -l app=%s' % app
    image, _ = kubectl2(namespace, k8cmd)

    if image:
        version = image.split(':')[1]
        last_dash_idx = version.rfind('-')
        version[last_dash_idx] = '+'
        logger.debug('App: %s -> semantic_version: %s', app, version)
        return version


def netcat_wait_until(return_code, host, port, wait_step, timeout):
    rcode = None
    start_t = time.time()
    wait_step = wait_step if timeout else 1
    while return_code != rcode and time.time() < start_t + (timeout or wait_step):
        resp, rcode = run_process('nc -z -w1 %s %s' % (host, port), fail_on_error=False)
        logger.debug('Response: %s, code: %s', resp, rcode)
        if rcode == return_code:
            break
        if timeout:
            logger.debug('Waiting %s seconds until testing connection again', wait_step)
            time.sleep(wait_step)
        else:
            wait_step = 0
    if return_code != rcode:
        return False
    return True


def connection_responds(host, port, timeout=60):
    logger.debug('Waiting until connection (%s:%s) starts responding', host, port)
    return netcat_wait_until(0, host, port, 5, timeout)


def connection_stops_responding(host, port, timeout=60):
    logger.debug('Waiting until connection (%s:%s) stops responding', host, port)
    return netcat_wait_until(1, host, port, 1, timeout)


def start_k8s_app(app, version, host=None, port=None, namespace=K8S_NAMESPACE,
                  timeout=60, wait_terminating_pods=False):
    logger.debug('Starting k8s app: %s', app)
    running = get_running_pod(app, namespace)
    logger.debug('Running pod: %s', running)
    if running:
        err_info = 'Pod for app %s is already running: %s' % (app, running)
        raise ContainerException(err_info)

    terminating = get_terminating_pod(app, namespace)
    logger.debug('Terminating pod: %s', terminating)
    if terminating and wait_terminating_pods:
        start_t = time.time()
        wait_step = 5
        while terminating and time.time() < start_t + (timeout or wait_step):
            terminating = get_terminating_pod(app, namespace)
            time.sleep(wait_step)
        if terminating:
            err_info = 'Pod for app %s is still terminating: %s' % (app, terminating)
            raise ContainerException(err_info)

    # TODO we need to put this into some config file or extract it from playbook.
    #     all this settings should be default chart values... so in theory we only need this settings
    #     for production deployment.I think default chart values are mostly correct,
    #     we need to double check them. vtn.id should be unique for every ven that uses it as reference_id,
    #     but we probably need some minor test refactoring and review for that.

    if 'stem-ven' in app:
        extra = ('--set k8s.serviceType=NodePort '
                 '--set vtn.id=STEM_TEST_VTN_ID '
                 '--set vtn.protocol=http '
                 '--set vtn.ip=vtn-sim '
                 '--set vtn.port=5000 '
                 )
        if app == 'stem-ven-austin-shines':
            extra += '--set ven.name=STEM_TEST_VEN '
        elif app == 'stem-ven-heco-shines':
            extra += '--set ven.name=STEM_TEST_VEN_HECOSHINES '
        else:
            raise ContainerException('Unknown VEN app: %s' % app)
    else:
        extra = ''
    # deploy the app specified
    chart_name = get_chart_name(app)
    vault_addr = settings['vault']['addr']
    vault_header = settings['vault']['header']
    vault_role = settings['vault']['role']
    vault_version = settings['vault']['version']
    run_process('stem-cli deploy-tasks install-chart '
                '--env-name %s '
                '--type %s '
                '--version %s '
                '--set k8s.pdbEnabled=true '
                '--set k8s.execEnv=compose '
                '--set vault.address=%s '
                '--set vault.header=%s '
                '--set vault.role=%s '
                '--set vault.version=%s '
                '--set ingress.domain=dev.stem.com '
                '%s' %
                (namespace, chart_name, version, vault_addr, vault_header, vault_role, vault_version, extra),
                show_output=True)
    run_process('stem-cli deploy-tasks wait-for-environment --env-name %s' % namespace,
                show_output=True)

    start_t = time.time()
    wait_step = 5
    while not running and time.time() < start_t + (timeout or wait_step):
        running = get_running_pod(app, namespace)
        time.sleep(wait_step)

    if not running:
        err_info = 'Pod for app %s isnt running after waiting for %ss' % (app, timeout)
        raise ContainerException(err_info)

    if host and port and not connection_responds(host, port, timeout):
        err_info = 'Pod for app %s didnt respond on %s:%s' % (app, host, port)
        raise ContainerException(err_info)


def stop_k8s_app(app, host=None, port=None, namespace=K8S_NAMESPACE, timeout=60,
                 raise_already_terminated=False):
    pod = get_running_pod(app, namespace)
    if not pod:
        err_info = 'Pod for app %s is not running.' % app
        pod_stopping = get_terminating_pod(app, namespace)
        if pod_stopping:
            err_info += 'It is already terminating!'
        if raise_already_terminated:
            raise ContainerException(err_info)
        logger.error(err_info)

    chart_name = get_chart_name(app)
    logger.debug('Uninstalling chart : %s', chart_name)
    run_process('stem-cli deploy-tasks uninstall-chart --env-name %s --name %s' % (namespace, chart_name),
                show_output=True)

    start_t = time.time()
    wait_step = 1
    while pod and time.time() < start_t + (timeout or wait_step):
        pod = get_running_pod(app, namespace)
        time.sleep(wait_step)

    if pod:
        err_info = 'Pod for app %s is still running after waiting for %ss' % (app, timeout)
        raise ContainerException(err_info)

    if host and port and not connection_stops_responding(host, port, timeout):
        raise ContainerException('Pod for %s app failed to stop properly. Connection still active.' % app)


def restart_k8_app(app, host=None, port=None, timeout=60, namespace=K8S_NAMESPACE, version=None):
    """Deleting pod for specified app. That automatically means restart, because app was not undeployed.
    """
    pod = get_running_pod(app, namespace)
    if pod:
        logger.debug('Deleting pod %s', pod)
        kubectl(K8S_NAMESPACE, 'delete pod %s' % pod, json_output=False)
        if host and port:
            start_t = time.time()
            wait_step = 5
            while time.time() < start_t + (timeout or wait_step):
                new_pod = get_running_pod(app, namespace)
                if new_pod and new_pod != pod:
                    break
                logger.debug('Waiting %ss for new %s pod', wait_step, app)
                time.sleep(wait_step)

            if not connection_responds(host, port, timeout):
                raise Exception('k8 app failed to start. Connection failed.')
    else:
        logger.error('Cannot restart K8 app `%s`. No active pods. Deploying app...', app)
        version = version or settings['artifacts'].get(app) or get_pod_version(app, namespace)
        start_k8s_app(app, version, host, port, namespace, timeout)


def container_id(container, running=True):
    rf = ' -a' if not running else ''
    cmd_ps = 'docker ps%s -q -f name=%s' % (rf, container)
    response, _ = run_docker_command(cmd_ps)
    if response:
        tags = response.splitlines()
        if len(tags) >= 2:
            return tags
        else:
            return response


def stop_container(container, host=None, port=None, timeout=20):
    logger.debug('Stopping container %s', container)
    cmd = 'docker stop %s' % container
    response, rcode = run_docker_command(cmd)
    if rcode != 0:
        raise ContainerException('Stopping %s returned error: %s code: %s' % (container, response, rcode))
    if host and port:
        if not connection_stops_responding(host, port, timeout):
            raise Exception('Container %s failed to stop. Connection still active.' % container)


def start_container(container, host=None, port=None, timeout=20):
    """Starts provided container.
       Executing 'docker stop, docker start' and checking if connection has stopped and started.
    """
    logger.debug('Starting container %s', container)
    cmd = 'docker start %s' % container
    response, rcode = run_docker_command(cmd)
    if rcode != 0:
        raise ContainerException('Starting %s returned error: %s code: %s' % (container, response, rcode))
    if host and port:
        if not connection_responds(host, port, timeout):
            raise Exception('Container %s failed to start. Connection failed.' % container)


def restart_container(container, host=None, port=None, timeout=20):
    """Restarts provided container.
       Executing 'docker stop, docker start' and checking if connection has stopped and started.
    """
    logger.debug('Restarting %s container', container)
    stop_container(container, host, port, timeout)
    start_container(container, host, port, timeout)

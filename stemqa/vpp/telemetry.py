import datetime as dt
import logging

from stemqa.helpers.db_helper import DbHelper
from stemqa.util import grid_response_util as gr_util
from stemqa.vpp.helper import dt_utcnow, STR_FORMAT

logger = logging.getLogger(__name__)

TELEMETRY_WITH_DISPATCHES_SQL = \
    """
    select mdm.monitor_id, mdm.sample_start_datetime, mdm.sample_end_datetime, rdt.create_datetime as dispatch_created,
           mdm.sample_count as m_count, mdm.kw_total_sum, mdm.kw_total_sum/mdm.sample_count,
           rdt.requested_power, rdt.dispatch_start_datetime, rdt.dispatch_end_datetime,
           lm.location_code, r.resource_id, rdt.gridservice_event_id, pbm.hostname, pbm.current_charge_min,
           pbm.current_charge_max, pbm.current_capacity_avg, pbm.sample_count as p_count, rdts.name as rdt_status
    from meter_logs.monitor_data_minute mdm, business.location_monitor lm, grid_services.resource r,
         grid_services.resource_dispatch_transaction rdt, grid_services.resource_dispatch_transaction_status rdts,
         optimization.powerstore_bms_minute pbm
    where r.resource_id = rdt.resource_id
    and rdts.status_id = rdt.last_status_id
    and mdm.monitor_id = lm.monitor_id
    and lm.location_code = concat(r.location_code,'_INVERTER')
    and mdm.monitor_id = concat(pbm.hostname,'-inverter')
    and mdm.sample_start_datetime >= '{time_frame_start}'
    and mdm.sample_start_datetime < '{time_frame_end}'
    and mdm.monitor_id in ({inverters})
    and rdt.dispatch_start_datetime < SUBTIME(mdm.sample_end_datetime,'0 0:0:30.000000')
    and rdt.dispatch_end_datetime > mdm.sample_start_datetime
    and mdm.sample_start_datetime <= pbm.msg_timestamp_start_datetime
    and pbm.msg_timestamp_end_datetime <= mdm.sample_end_datetime
    and rdt.create_datetime < mdm.sample_end_datetime
    order by mdm.sample_start_datetime, pbm.hostname, dispatch_created desc
    """

LAST_TELEMETRY_SQL = \
    """
    select md.monitor_id, md.sample_start_datetime, md.w_1 + md.w_2 + md.w_3 as inv_kw
    from meter_logs.monitor_data md
    where md.monitor_id = {inverter}
    order by md.sample_start_datetime DESC, md.monitor_id
    limit {limit};
    """

TELEMETRY_SQL = \
    """
    select mdm.monitor_id, mdm.sample_start_datetime, mdm.sample_end_datetime,
           mdm.sample_count as m_count, mdm.kw_total_sum/mdm.sample_count, mdm.kw_total_sum,
           lm.location_code, pbm.hostname, pbm.current_charge_min, pbm.current_charge_avg,
           pbm.current_charge_max, pbm.current_capacity_avg, pbm.sample_count as p_count
    from meter_logs.monitor_data_minute mdm, business.location_monitor lm, optimization.powerstore_bms_minute pbm
    where mdm.monitor_id = lm.monitor_id
    and mdm.monitor_id in ({inverters})
    and mdm.monitor_id = concat(pbm.hostname,'-inverter')
    and mdm.sample_start_datetime <= pbm.msg_timestamp_start_datetime
    and pbm.msg_timestamp_end_datetime <= mdm.sample_end_datetime
    and mdm.sample_start_datetime >= '{time_frame_start}'
    and mdm.sample_start_datetime < '{time_frame_end}'
    order by mdm.sample_start_datetime, pbm.hostname;
    """

SITE_LOAD_SQL = \
    """
    select mdm.monitor_id, mdm.sample_start_datetime, mdm.sample_end_datetime,
           mdm.sample_count as m_count, mdm.kw_total_min, mdm.kw_total_max,
           mdm.kw_total_sum/mdm.sample_count as site_load_avg
    from meter_logs.monitor_data_minute mdm
    where mdm.monitor_id in ({sites})
    and mdm.sample_start_datetime >= '{time_frame_start}'
    and mdm.sample_start_datetime < '{time_frame_end}'
    order by mdm.sample_start_datetime, mdm.monitor_id;
    """


TELEMETRY_WITH_DISPATCHES_SQL_NEW_EMU = \
    """
    select mdm.monitor_id, mdm.sample_start_datetime, mdm.sample_end_datetime, rdt.create_datetime as dispatch_created,
           mdm.sample_count as m_count, mdm.kw_total_sum, mdm.kw_total_sum/mdm.sample_count,
           rdt.requested_power, rdt.dispatch_start_datetime, rdt.dispatch_end_datetime,
           lm.location_code, r.resource_id, rdt.gridservice_event_id, pbm.hostname, pbm.current_charge_min,
           pbm.current_charge_max, pbm.current_capacity_avg, pbm.sample_count as p_count, rdts.name as rdt_status
    from meter_logs.monitor_data_minute mdm, business.location_monitor lm, grid_services.resource r,
         grid_services.resource_dispatch_transaction rdt, grid_services.resource_dispatch_transaction_status rdts,
         optimization.powerstore_bms_minute pbm
    where r.resource_id = rdt.resource_id
    and rdts.status_id = rdt.last_status_id
    and mdm.monitor_id = lm.monitor_id
    and lm.location_code in ({location_codes})
    and mdm.monitor_id in ({inverters})
    and mdm.sample_start_datetime >= '{time_frame_start}'
    and mdm.sample_start_datetime < '{time_frame_end}'
    and mdm.monitor_id in ({inverters})
    and rdt.dispatch_start_datetime < SUBTIME(mdm.sample_end_datetime,'0 0:0:30.000000')
    and rdt.dispatch_end_datetime > mdm.sample_start_datetime
    and mdm.sample_start_datetime <= pbm.msg_timestamp_start_datetime
    and pbm.msg_timestamp_end_datetime <= mdm.sample_end_datetime
    and rdt.create_datetime < mdm.sample_end_datetime
    order by mdm.sample_start_datetime, pbm.hostname, dispatch_created desc
    """


def last_telemetry_data(resources, limit=5, log=True):
    dbhelper = DbHelper.default_instance()
    inverters = ["'%s-inverter'" % res['emulator_hostname'] for res in resources]
    data = ''
    header = 55 * '-' + '\n'
    header += '| %-19s | %-8s | %-8s | \n' % \
              ('sample start time',
               'emulator',
               'inverter'
               )
    header += 55 * '-' + '\n'
    for inverter in inverters:
        sql = LAST_TELEMETRY_SQL.format(inverter=inverter, limit=limit)
        query_data = dbhelper.query(None, sql)
        for row in query_data:
            data += '| %-19s | %-8s | %-8.4f |\n' % \
                    (row['sample_start_datetime'].strftime(STR_FORMAT),
                     row['monitor_id'],
                     row['inv_kw'],
                     )
    info = header + data
    if log:
        logger.debug('Last Telemetry data:\n%s', info)
    return query_data


telemetry_field_order = \
    ('start time',
     'emulator',
     'inverter',
     'site min',
     'site avg',
     'site max',
     'SOC min',
     'SOC max',
     'max kW',
     # 'sec',
     )


def telemetry_data(resources, start_dt=None, end_dt=None, log=True, return_csv=False, new_emulator=False):
    dbhelper = DbHelper.default_instance()
    start_dt = dt_utcnow() - dt.timedelta(days=30) if not start_dt else start_dt
    end_dt = dt_utcnow() if not end_dt else end_dt
    if new_emulator:
        inverters, sites, meters = get_monitors(dbhelper, resources)
    else:
        inverters = ','.join(["'%s-inverter'" % res['emulator_hostname'] for res in resources])
        sites = ','.join(["'%s-site'" % res['emulator_hostname'] for res in resources])

    sql_inverters = TELEMETRY_SQL.format(inverters=inverters,
                                         time_frame_start=start_dt.strftime(STR_FORMAT),
                                         time_frame_end=end_dt.strftime(STR_FORMAT))
    sql_sites = SITE_LOAD_SQL.format(sites=sites,
                                     time_frame_start=start_dt.strftime(STR_FORMAT),
                                     time_frame_end=end_dt.strftime(STR_FORMAT))
    query_data_inv = dbhelper.query(None, sql_inverters)
    query_data_sites = dbhelper.query(None, sql_sites)
    sites_data_len = len(query_data_sites)
    csv_data = ','.join(telemetry_field_order) + '\n'
    data = ''
    header = 111 * '-' + '\n'
    header += '| %-19s | %-12s | %-8s | %-8s | %-8s | %-8s | %-7s | %-7s | %-6s |\n' % telemetry_field_order
    header += 111 * '-' + '\n'
    for idx, row in enumerate(query_data_inv):
        if sites_data_len > idx:
            site_row = query_data_sites[idx]
        else:
            site_row = None
        if site_row and row['sample_start_datetime'] == site_row['sample_start_datetime']:
            site_load_min = site_row['kw_total_min']
            site_load_avg = site_row['site_load_avg']
            site_load_max = site_row['kw_total_max']
        else:
            site_load_min = 0.0
            site_load_avg = 0.0
            site_load_max = 0.0

        row['site_load_min'] = site_load_min
        row['site_load_avg'] = site_load_avg
        row['site_load_max'] = site_load_max

        data += '| %-19s | %-12s | %-8.3f | %-8.3f | %-8.3f | %-8.3f | %-7.3f | %-7.3f | %-6s |\n' % \
                (row['sample_start_datetime'].strftime(STR_FORMAT),
                 row['hostname'],
                 float(row['mdm.kw_total_sum/mdm.sample_count']),
                 float(site_load_min),
                 float(site_load_avg),
                 float(site_load_max),
                 float(row['current_charge_min']),
                 float(row['current_charge_max']),
                 row['current_capacity_avg'],
                 # row['m_count'],
                 )
    info = header + data
    if log:
        logger.debug('Telemetry data:\n%s', info)
    if return_csv:
        return csv_data
    return query_data_inv


tel_and_disp_field_order = \
    ('start time',
     'emulator',
     'dispatch',
     'inverter',
     'site min',
     'site avg',
     'site max',
     'SOC min',
     'SOC max',
     'max kW',
     'event',
     'dispatch created',
     'dispatch start',
     'dispatch end.',
     'dispatch status'
     )


def get_monitors(dbhelper, locations):
    inverters, sites, meters = gr_util.get_monitors_as_list(dbhelper, locations)
    return (', '.join('"' + item + '"' for item in inverters)), \
           (', '.join('"' + item + '"' for item in sites)), \
           (', '.join('"' + item + '"' for item in meters))


def telemetry_and_dispatch_data(resources, start_dt=None, end_dt=None, last_dispatch_only=True, log=True,
                                return_csv=False, new_emulator=False):
    dbhelper = DbHelper.default_instance()
    start_dt = dt_utcnow() - dt.timedelta(days=30) if not start_dt else start_dt
    end_dt = dt_utcnow() if not end_dt else end_dt

    if new_emulator:
        inverters, sites, meters = get_monitors(dbhelper, resources)
        location_codes_as_str = (', '.join('"' + res + '_INVERTER"' for res in resources))
        sql = TELEMETRY_WITH_DISPATCHES_SQL_NEW_EMU.format(inverters=inverters,
                                                           location_codes=location_codes_as_str,
                                                           time_frame_start=start_dt.strftime(STR_FORMAT),
                                                           time_frame_end=end_dt.strftime(STR_FORMAT))
    else:
        inverters = ','.join(["'%s-inverter'" % res['emulator_hostname'] for res in resources])
        sites = ','.join(["'%s-site'" % res['emulator_hostname'] for res in resources])
        sql = TELEMETRY_WITH_DISPATCHES_SQL.format(
            inverters=inverters,
            time_frame_start=start_dt.strftime(STR_FORMAT),
            time_frame_end=end_dt.strftime(STR_FORMAT))

    query_data = dbhelper.query(None, sql)
    if not query_data:
        csv_data = ','.join(telemetry_field_order)
        data = telemetry_data(resources, start_dt, end_dt, return_csv=return_csv, new_emulator=new_emulator)
        return data

    sql_sites = SITE_LOAD_SQL.format(sites=sites,
                                     time_frame_start=start_dt.strftime(STR_FORMAT),
                                     time_frame_end=end_dt.strftime(STR_FORMAT))
    query_data_sites = dbhelper.query(None, sql_sites)
    csv_data = ','.join(tel_and_disp_field_order) + '\n'
    data = ''
    header = 220 * '-' + '\n'
    header += ('| %-19s | %-12s | %-8s | %-8s | %-8s | %-8s | %-8s | %-7s | %-7s | %-6s '
               '| %-5s | %-19s | %-19s | %-19s | %s\n') % tel_and_disp_field_order
    header += 220 * '-' + '\n'
    previous_row = {}
    filtered_data = []
    for row in query_data:
        site_row_match = False
        for site_row in query_data_sites:
            if row['sample_start_datetime'] == site_row['sample_start_datetime']:
                site_load_min = site_row['kw_total_min']
                site_load_avg = site_row['site_load_avg']
                site_load_max = site_row['kw_total_max']
                site_row_match = True
        if not site_row_match:
            site_load_min = 0.0
            site_load_avg = 0.0
            site_load_max = 0.0

        row['site_load_min'] = site_load_min
        row['site_load_avg'] = site_load_avg
        row['site_load_max'] = site_load_max

        if last_dispatch_only and previous_row:
            if row['sample_start_datetime'] == previous_row['sample_start_datetime'] and \
                    row['hostname'] == previous_row['hostname']:
                continue
        data_tuple = \
            (row['sample_start_datetime'].strftime(STR_FORMAT),
             row['hostname'],
             float(row['requested_power']),
             float(row['mdm.kw_total_sum/mdm.sample_count']),
             float(site_load_min),
             float(site_load_avg),
             float(site_load_max),
             float(row['current_charge_min']),
             float(row['current_charge_max']),
             row['current_capacity_avg'],
             row['gridservice_event_id'],
             row['dispatch_created'].strftime(STR_FORMAT),
             row['dispatch_start_datetime'].strftime(STR_FORMAT),
             row['dispatch_end_datetime'].strftime(STR_FORMAT),
             row['rdt_status']
             )
        data += ('| %-19s | %-12s | %-8.3f | %-8.3f | %-8.3f | %-8.3f | %-8.3f | %-7.3f | %-7.3f | %-6s '
                 '| %-5s | %-19s | %-19s | %-19s | %s\n') % data_tuple
        csv_data += ','.join([str(d) for d in data_tuple]) + '\n'
        filtered_data.append(row)
        previous_row = row
    if log:
        pretty_output = header + data
        logger.debug('Comparison of telemetry output vs dispatches:\n%s', pretty_output)
    if return_csv:
        return csv_data
    return filtered_data

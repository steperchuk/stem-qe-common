import datetime as dt
import json
import random
import string
import sys
import threading
import traceback
import logging
import multiprocessing as mp
from collections import OrderedDict
from json import JSONEncoder

import isodate
import pause
import yaml
from dateutil import tz
from pytz import UTC
from requests import HTTPError

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_GRID_SERVICES
from stemqa.util.grid_response_util import get_program_id, GridResponseClient
from stemqa.util.s4_util import S4Util
from stemqa.config.logconfig import get_logger
from stemqa.config.logconfig import LOGGING_CONFIG

logger = get_logger(__name__)

s4c = S4Util()

# TEST SETTINGS
DATE_FORMAT = "%Y-%m-%d"
STR_FORMAT = "%Y-%m-%d %H:%M:%S"
STR_FORMAT_Z = "%Y-%m-%dT%H:%M:%SZ"
WEB_URL = settings['stem-web-app']['url']
RESOURCES_PATH = 'resources'


def dt_str(dtime):
    return dtime.strftime(format=STR_FORMAT)


def round_time_up(time_now=None, seconds=0, minutes=0, hours=0, days=0, ref_date=dt.datetime.min):
    """
    Rounds time to next nearest interval based on rounding seconds
    that are calculated by summing all time parameters accordingly.
    By default it rounds it to 1 second if no time parameters are provided.
    """
    if time_now is None:
        time_now = dt_utcnow()
    if time_now.microsecond:
        time_now = time_now - dt.timedelta(0, 0, time_now.microsecond) + dt.timedelta(0, 1)
    total_seconds = (time_now.replace(tzinfo=None) - ref_date).total_seconds()
    round_to_seconds = (seconds + minutes * 60 + hours * 60 * 60 + days * 60 * 60 * 24)
    if not round_to_seconds:
        round_to_seconds = 1
    rounding_diff = total_seconds % round_to_seconds
    if rounding_diff:
        rounded_start_dt = time_now + dt.timedelta(0, round_to_seconds - rounding_diff)
    else:
        rounded_start_dt = time_now
    return rounded_start_dt


def waiting(seconds, message='', round_to_minute=False):
    wait_dt = dt.datetime.now(tz=UTC) + dt.timedelta(seconds=seconds)
    if round_to_minute:
        wait_dt = round_time_up(wait_dt)
    localtime = wait_dt.astimezone(tz.tzlocal())
    logger.debug('Waiting until %s.%s',
                 localtime.strftime(STR_FORMAT), message)
    pause.until(localtime)


def ordered_load(stream, Loader=yaml.Loader):
    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return OrderedDict(loader.construct_pairs(node))

    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def dump_ordered_dict(data, Dumper=yaml.Dumper, **kwargs):
    class OrderedDumper(Dumper):
        pass

    def dict_representer(dumper, data):
        return dumper.represent_dict(iter(list(data.items())))

    OrderedDumper.add_representer(OrderedDict, dict_representer)
    return yaml.dump(data, Dumper=OrderedDumper, **kwargs)


def object_serialization_helper(o):
    if isinstance(o, dt.datetime):
        return o.__str__()


class ShortJSONEncoder(JSONEncoder):
    def default(self, o):  # pylint: disable=E0202
        if hasattr(o, 'toJSONshort'):
            return o.toJSONshort()
        if object_serialization_helper(o):
            return object_serialization_helper(o)
        return JSONEncoder.default(self, o)


class VerboseJSONEncoder(JSONEncoder):
    def default(self, o):  # pylint: disable=E0202
        if hasattr(o, 'toJSON'):
            return o.toJSON()
        if object_serialization_helper(o):
            return object_serialization_helper(o)
        return JSONEncoder.default(self, o)


def json_dumps(json_d, short=False, sort_keys=True):
    """
    Consistant serialization of python dictionary into json string.
    """
    enc_cls = ShortJSONEncoder if short else VerboseJSONEncoder
    return json.dumps(json_d, sort_keys=sort_keys, indent=2, cls=enc_cls)


def yaml_dumps(yaml_d, default_flow_style=False, **kwargs):
    """
    Consistant serialization of yaml config files.
    (We retain order with OrderDict when loading yaml files)
    """
    return dump_ordered_dict(yaml_d, default_flow_style=default_flow_style, **kwargs)


def recursive_setdefault(data, default_data):
    for k, d in list(data.items()):
        if isinstance(d, dict) and k in default_data:
            recursive_setdefault(d, default_data[k])
    for k, d in list(default_data.items()):
        data.setdefault(k, d)


LOCATION_TIMEZONE_SQL = \
    """
    select timezone
    from topology.location
    where location_code = '{location_code}'
    """


def get_program_info(program_name):
    '''
        Returns program info (id, aggregations) and calculates system_size_power
        and system_size_energy based on aggregations. Very simplifed for now
        (Doesn't consider cases where aggregations (or resources in aggregation) would only have partial capacity).
        check `grid_services.aggregation_resources` for possible restrictions ect.
    '''
    dbhelper = DbHelper.default_instance()
    with GridResponseClient(settings['stem-web-app']['url']) as client:
        program_id = get_program_id(client, program_name)
        attributes = None
        program_timezone = None
        try:
            attributes = client.get_program_attributes(program_id)
            program_timezone = attributes['GS_PROGRAM_TIMEZONE']
        except HTTPError as e:
            logger.error('Getting attributes from gridresponse endpoint failed: %s', e)
        aggregations_d = client.get_aggregations_for_program(program_id)
        aggregation_ids = [aggr['aggregation_id'] for aggr in aggregations_d['aggregations']]
        aggregations_l = []
        system_size_power = 0
        system_size_energy = 0
        resource_timezone = None
        err_info = None
        for id_ in aggregation_ids:
            aggr = {'id': id_}
            aggregation_details = client.aggregation_details(program_id).json()
            aggr.update(aggregation_details['data'])
            for location in list(aggregation_details['data'].get('locations', {}).values()):
                system_size_power += location['size_kw']
                system_size_energy += location['size_kwh']
                sql = LOCATION_TIMEZONE_SQL.format(location_code=location['location_code'])
                query_data = dbhelper.query(None, sql)
                rtimezone = query_data[0]['timezone']
                if not resource_timezone:
                    resource_timezone = rtimezone
                elif rtimezone != resource_timezone:
                    err_info = 'Program has aggregations with resources from different timezones (%s, %s)!!' % \
                               (rtimezone, resource_timezone)
            aggregations_l.append(aggr)

        if resource_timezone != program_timezone:
            logger.warning('Program(%s) and resource(%s) timezone are different!!', program_timezone, resource_timezone)

    select_program_timezone_sql = \
        '''
        SELECT item_value from program_attributes
        WHERE program_id= %s AND item_id = 70;
        ''' % program_id
    program_timezone_2 = eval(dbhelper.query(DB_GRID_SERVICES,  # pylint: disable=W0123
                                             select_program_timezone_sql)[0]['item_value'])

    info = {'id': program_id,
            'attributes': attributes,
            'timezone': program_timezone_2,
            'program_timezone': program_timezone or program_timezone_2,
            'resource_timezone': resource_timezone,
            'aggregations': aggregations_l,
            'system_size_power': system_size_power,
            'system_size_energy': system_size_energy
            }
    assert not err_info, err_info + '\n' + json_dumps(info)
    return info


def fetch_program_id(program_name):
    with GridResponseClient(settings['stem-web-app']['url']) as client:
        program_id = get_program_id(client, program_name)
    return program_id


def dt_utcnow():
    """
    Current time with UTC timezone.
    """
    return dt.datetime.now(tz=UTC)


def iso8601_utcnow():
    """
    Retruns current UTC datetime in iso8601 format
    """
    return isodate.datetime_isoformat(dt_utcnow())


def iso8601_utc_datetime(dt_timezoned):
    return isodate.datetime_isoformat(dt_timezoned.astimezone(UTC))


HEXDIGITS = list(set(string.hexdigits.lower()))


def get_random_hex_string(length=20):
    """Returns random hexadecimal string of desired length
    """
    return ''.join(random.choice(HEXDIGITS) for _ in range(length))


def recursive_dict_cleanup(dict_d, keys):
    '''Recursive deletion of all keys inside dictionary'''
    root_keys = list(dict_d.keys())
    for key in root_keys:
        if key in keys:
            del dict_d[key]
        elif isinstance(dict_d[key], dict):
            recursive_dict_cleanup(dict_d[key], keys)


def recursive_dict_cleanup_inside_lists(dict_or_list, keys):
    '''Recursive deletion of all keys inside dictionary'''
    if isinstance(dict_or_list, (list, set, tuple)):
        for item in dict_or_list:
            recursive_dict_cleanup_inside_lists(item, keys)
    elif isinstance(dict_or_list, dict):
        d_copy = dict_or_list.copy()
        for key, item in list(d_copy.items()):
            if key in keys:
                del dict_or_list[key]
            elif isinstance(item, (dict, tuple, set, list)):
                recursive_dict_cleanup_inside_lists(item, keys)


class WorkerThreadFailed(Exception):
    pass


class ExcThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(ExcThread, self).__init__(*args, **kwargs)
        self.exc = None
        self.setDaemon(True)

    def run(self):
        try:
            super(ExcThread, self).run()
        except:  # pylint: disable=W0702 # noqa: E722
            self.exc = sys.exc_info()

    def join(self, timeout=None, raise_exception=True):  # pylint: disable=W0221
        super(ExcThread, self).join(timeout=timeout)
        if self.exc and raise_exception:
            msg = "Thread '%s' threw an exception: %s(%s)" % (self.getName(), self.exc[0].__name__, self.exc[1])
            new_exc = WorkerThreadFailed(msg)
            raise new_exc.__class__(new_exc).with_traceback(self.exc[2])


class ExcTimer(threading.Timer):  # pylint: disable=W0212

    def __init__(self, interval, function, args=(), kwargs=None):
        kwargs = kwargs if kwargs else {}
        super(ExcTimer, self).__init__(interval, function, args, kwargs)
        self.exc = None

    def run(self):
        try:
            super(ExcTimer, self).run()
        except:  # pylint: disable=W0702 # noqa: E722
            self.exc = sys.exc_info()

    def join(self, timeout=None, raise_exception=True):  # pylint: disable=W0221
        super(ExcTimer, self).join(timeout=timeout)
        if self.exc and raise_exception:
            msg = "Thread '%s' threw an exception: %s(%s)" % (self.getName(), self.exc[0].__name__, self.exc[1])
            new_exc = WorkerThreadFailed(msg)
            raise new_exc.__class__(new_exc).with_traceback(self.exc[2])


WORKER_FORMATTER_FORMAT = \
    '%(asctime)s %(processName)s %(filename)-12.12s:%(funcName)-10.10s:%(lineno)03d %(levelname)-5s  %(message)s'


class WorkerProcessFailed(Exception):
    pass


class ExcProcess(mp.Process):
    def __init__(self, *args, **kwargs):
        mp.Process.__init__(self, *args, **kwargs)
        self._pconn, self._cconn = mp.Pipe()
        self._exception = None

    def run(self):
        LOGGING_CONFIG['formatters']['default']['format'] = WORKER_FORMATTER_FORMAT
        logging.config.dictConfig(LOGGING_CONFIG)
        try:
            mp.Process.run(self)
            self._cconn.send(None)
        except Exception as e:
            tb = traceback.format_exc()
            self._cconn.send(tb)
            raise e

    @property
    def exception(self):
        if self._pconn.poll():
            self._exception = self._pconn.recv()
        return self._exception

    def join(self, timeout=None, raise_exception=True):  # pylint: disable=W0221
        mp.Process.join(self, timeout=timeout)
        if self.exception and raise_exception:
            msg = "Process '%s' threw an exception:\n%s" % (self.name, self._exception)
            raise WorkerProcessFailed(msg)

import datetime as dt

from stemqa.vpp.helper import ExcTimer, dt_utcnow, STR_FORMAT, json_dumps
from stemqa.config.logconfig import get_logger

logger = get_logger(__name__)


class ActionException(Exception):
    pass


class Action(object):
    def __init__(self, id_, func, start_dt, args=None, kwargs=None, action_object=None):
        self.id_ = id_
        self.func = func
        self.start_dt = start_dt
        self.args = args if args else []
        self.kwargs = kwargs if kwargs else {}
        self.action_object = action_object
        self.timer_thread = None
        self.cancelled = False
        self.started = False
        self.finished = False
        self.failed = False

    def toJSONshort(self):
        return 'Action(id_=%s, start_dt=%s)' % (self.id_, self.start_dt.strftime(STR_FORMAT))

    def __str__(self):
        return self.toJSONshort()

    def toJSON(self):
        return {"__class__": "Action",
                "id_": self.id_,
                "start_dt": self.start_dt.strftime(STR_FORMAT),
                "args": self.args,
                "kwargs": self.kwargs,
                "action_object": self.action_object
                }

    def __repr__(self):
        return json_dumps(self.toJSON())

    def schedule_action_thread(self):
        assert not self.cancelled, 'Action `%s` is already cancelled' % self.id_
        dt_now = dt_utcnow()
        if dt_now < self.start_dt:
            td_until = self.start_dt - dt_now
            seconds = td_until.total_seconds()
        else:
            seconds = 0
            late = (dt_now - self.start_dt).total_seconds()
            logger.warning('Action `%s` is already %ss past execution time', self.id_, late)
        self.timer_thread = ExcTimer(seconds, self.func, self.args, self.kwargs)
        self.timer_thread.name = self.id_
        self.timer_thread.daemon = True
        self.started = True
        self.timer_thread.start()

    def join(self, timeout=0.001, raise_thread_exception=True):
        """Calling join on timer thread and setting proper status of Action based on thread

        timeout : float, optional
            how long are we waiting for timer thread to finish
            (the default is 0.001, which is used to quickly raise exception if thread failed)
        raise_thread_exception : bool, optional
            reraise thread exception on current(usually main) thread if true (the default is True)
        """

        if self.timer_thread and self.started:
            self.timer_thread.join(timeout, raise_exception=raise_thread_exception)
            if self.timer_thread.exc:
                self.failed = True

        if self.timer_thread and self.timer_thread.finished.is_set():
            self.finished = True

    def cancel(self):
        if self.timer_thread and not self.cancelled:
            self.timer_thread.cancel()
        self.cancelled = True

    def run_sync(self, check_start_dt=True, raise_failure=True):
        assert not self.cancelled, 'Action `%s` is already cancelled' % self.id_
        if dt_utcnow() > self.start_dt or not check_start_dt:
            self.started = True
            try:
                self.func(*self.args, **self.kwargs)
            except Exception as e:  # pylint: disable=W0703
                self.failed = True
                if raise_failure:
                    raise
                logger.error('Action `%s` has failed: %r', self.id_, e)
            finally:
                self.finished = True
        else:
            logger.debug('Action `%s` is still waiting for start time(%s)', self.id_, self.start_dt)

    @classmethod
    def create_method_call(cls, action_object, method_name, method_call_id, base_start_dt, send_request_delta,
                           args_list=None, kwargs_dict=None):
        assert getattr(action_object, method_name)
        # TODO checking args, kwargs?
        args_list = args_list if args_list else []
        kwargs_dict = kwargs_dict if kwargs_dict else {}
        action_id = '%s::%s::%s' % (action_object.name, method_name, method_call_id)
        action_start_dt = base_start_dt + dt.timedelta(seconds=send_request_delta)

        def action_func(obj, method, *args, **kwargs):
            start_dt = dt_utcnow()
            logger.debug('Action %s just started: %s', action_id, start_dt)
            # executing method on action object with provided arguments
            getattr(obj, method)(*args, **kwargs)

            dt_now = dt_utcnow()
            runtime = (dt_now - start_dt).total_seconds()
            logger.debug('Action %s just stopped (runtime_seconds: %s)', action_id, runtime)

        return cls(action_id, action_func, action_start_dt, [action_object, method_name], kwargs_dict, action_object)

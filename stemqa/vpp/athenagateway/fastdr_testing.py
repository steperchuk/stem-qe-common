
import datetime as dt
import time
import traceback
import logging

import isodate
import pause
import pytz
from dateutil import tz
from pytz import UTC

from stemqa.vpp.athenagateway.api_util import AgApiClient
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_setup
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_event_info
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_telemetry_and_dispatch_info
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_test_cleanup
from stemqa.vpp.athenagateway.generic_testing import event_scenario_calculations
from stemqa.vpp.athenagateway.generic_testing import verifications_after_event_ends
from stemqa.vpp.athenagateway.generic_testing import reruns_possible, parallel_testing_possible
from stemqa.vpp.athenagateway.generic_testing import DURING_DCM_INHIBIT
from stemqa.vpp.athenagateway.generic_testing import DURING_EVENT
from stemqa.vpp.athenagateway.generic_testing import DURING_EVENT_CANCEL
from stemqa.vpp.athenagateway.generic_testing import DURING_CHARGE_INHIBIT

from stemqa.vpp.athenagateway.athenautil import round_to_resolution
from stemqa.vpp.athenagateway.athenautil import get_last_gs_event
from stemqa.vpp.athenagateway.athenautil import FASTDR_TESTING_PROGRAM_ATTRIBUTES
from stemqa.vpp.athenagateway.event_verifications import verify_event_queues_after_creation
from stemqa.vpp.helper import round_time_up, STR_FORMAT
from stemqa.vpp.telemetry import telemetry_and_dispatch_data
from stemqa.vpp.openadr.vtn_api import vtnc
from stemqa.vpp.openadr.test_setup import setup_external_reference_for_program


logger = logging.getLogger(__name__)
ag_client = AgApiClient()


def fastdr_test_setup(testdata, *args, reuse_emulators=False, retest_previous=False, **kwargs):
    """ Function loads and sets everything needed before we can start generic fastdr tests.

        It can also be reused for tests that only use generic setup definitions. Usage of generic
        scenario and verification testdata is optional when using this setup. Also useful to run before starting
        parallel testing jobs to make sure we avoid possible setup clashes and also remove some timing flakiness
        along the way which can come with longer DB setup and especially startup of emulators.
    """
    ven_name = testdata['ven_name']
    # Clearing ven data in VTN forces reregistration.
    # Contains check if tested ven is still responsive. (waits for registration)
    vtnc.clear_ven(ven_name)
    testdata['default_program_attributes'] = FASTDR_TESTING_PROGRAM_ATTRIBUTES
    setup_external_reference_for_program(testdata['program_name'], ven_name)
    generic_gs_testing_setup(testdata, *args, reuse_emulators=reuse_emulators,
                             retest_previous=retest_previous, **kwargs)
    event_scenario = testdata['event']
    openadr_data = event_scenario.get('openadr')
    api_data = event_scenario.get('api')
    assert openadr_data or api_data


@reruns_possible
@parallel_testing_possible(fastdr_test_setup)
def test_fastdr_generic(testdata, reuse_emulators=False, retest_previous=False, **kwargs):
    """ Generic FASTDR event testing driver.
        testdata: Test configuration object loaded from yaml for specific testcase.
                  Can be loaded with defaults before we pass it to this function otherwise that will
                  be done inside `fastdr_test_setup` routine.

        [special flags that are usually only used for test development]:
        reuse_emulators: Special flag that enables us to retest same test without waiting for emulators.
        retest_previous: Special flag that enables us to retest and develop verifications for longer event testcases.
                         It only reruns event verifications that happens after event ends.
    """
    errors = []  # only used to collect all verification errors when event ends.
    try:
        event_scenario = testdata['event']
        creation_only = event_scenario.get('creation_only')
        # creating and verifying fastdr event based on test data provided.
        event_data = fastdr_event_creation(testdata, retest_previous)
        # Filling event_data with info needed by testing loop and verifications
        event_data = event_scenario_calculations(testdata, event_data, retest_previous)
        # Running fastdr specific test loop so we can execute scenario and check telemetry every minute.
        if not (creation_only or retest_previous):
            fastdr_testing_loop_until_event_ends(testdata, event_data)

        # running complex GS event verifications after event has completed
        if not creation_only:
            verifications_after_event_ends(testdata, event_data, errors, retest_previous)

    except Exception:  # pylint: disable=W0703
        errors.append(traceback.format_exc())
    finally:
        # Useful test info to sum up what was going on before failure or after test passed
        try:
            generic_gs_testing_event_info(testdata)
            generic_gs_testing_telemetry_and_dispatch_info(testdata)
        except Exception:
            logger.debug('Test info function failed: {}', traceback.format_exc())

        # Checking if we need to do some LCR specific test clenup
        try:
            generic_gs_testing_test_cleanup(testdata, retest_previous)
        except Exception:
            errors.append(traceback.format_exc())

        assert not errors, 'Collection of errors:\n%s' % "\n\n" + 90 * "=" + "\n\n".join(errors)


def calculate_fastdr_event_start(dispatch_start_resolution, dispatch_scheduling_buffer, start_delta=None):
    # Auto Calculating expected event start time and making sure we wait for time frame where this gets hard to do.
    utcnow = dt.datetime.now(tz=UTC).replace(microsecond=0) + dt.timedelta(seconds=15)  # VEN Polling delay
    logger.debug('*** Utcnow: %s', utcnow.strftime(STR_FORMAT))
    if start_delta:
        logger.debug('*** Calculating event start based on requested time delta')
        logger.debug('*** Delta: %s', start_delta)
        event_start_dt = \
            round_to_resolution(utcnow + dt.timedelta(seconds=start_delta), dispatch_start_resolution)
    else:
        logger.debug('*** Calculating next possible event start based on dispatch buffer')
        delta = dt.timedelta(minutes=dispatch_scheduling_buffer)
        logger.debug('*** Delta: %s', delta)
        event_start_dt = \
            round_to_resolution(utcnow + dt.timedelta(minutes=dispatch_scheduling_buffer), dispatch_start_resolution)
    logger.debug('*** Calculated fastDR event start: %s', event_start_dt.strftime(STR_FORMAT))
    return event_start_dt


def schedule_and_verify_fastdr_event(testdata, event_start_dt, reuse_previous_event=False):
    """ Generic LCR event creation based on test settings passed.
        testdata: test case setting used to store and get all required info for event creation
                  and verification.
        event_start_dt: start of event that will be used for either openadr or api event creation
        reuse_previous_event: Used when we want to rerun verifications on previously completed event.
                              It knows how to mock response so we can retest without additonal hassle.
        program_id and requested_kw: are used for verification of event creation.
    """
    program_id = testdata['program_id']
    event_scenario = testdata['event']
    openadr = event_scenario.get('openadr')
    api = event_scenario.get('api')
    program_attributes = testdata['program_attributes']
    requested_kw = program_attributes['event_commitment']
    event_duration = event_scenario['duration']
    event_end_dt = event_start_dt + dt.timedelta(seconds=event_duration)
    event_start_iso = isodate.datetime_isoformat(event_start_dt)

    if openadr:
        last_gs_event_before_creation = get_last_gs_event(program_id)
        if not reuse_previous_event:
            ven_id = testdata['ven_id']
            ven_name = testdata['ven_name']
            if openadr.get('duration'):
                assert event_duration == isodate.parse_duration(openadr['duration']).total_seconds()
            else:
                openadr['duration'] = isodate.duration_isoformat(dt.timedelta(seconds=event_duration))

            openadr['start_dt'] = event_start_iso
            event_id = openadr.get('event_id')
            if event_id:
                openadr['event_id'] = event_id + '__' + event_start_iso
            else:
                openadr['event_id'] = 'fastdr-test-event' + '__' + event_start_iso
            vtnc.create_fastdr_event(ven_name, ven_id, **openadr)
        # using dummy event creation logic to get last gs event data and to verify we have proper new GS event
        data = ag_client.create_event(program_id, ven_name, ven_name, requested_kw,
                                      event_start_dt, event_end_dt, reuse_previous_event=True)
        if reuse_previous_event:
            assert last_gs_event_before_creation, 'You need to have existing event if you want to reuse it'
            assert data['grid_service_event_id'] == last_gs_event_before_creation['grid_service_event_id']
        elif last_gs_event_before_creation:
            assert data['grid_service_event_id'] != last_gs_event_before_creation['grid_service_event_id']
    else:
        scheduler_id = api.get('scheduler_id', testdata['id']) + '__' + event_start_iso
        data = ag_client.create_event(program_id, testdata['ven_name'], scheduler_id, requested_kw,
                                      event_start_dt, event_end_dt, reuse_previous_event=False)

    return data


def fastdr_event_creation(testdata, retest_previous=False):
    event_data = {}
    testdata['event_data'] = event_data
    program_attributes = testdata['program_attributes']
    event_duration = testdata['event']['duration']
    event_data['duration_seconds'] = event_duration
    event_data['requested_kw'] = program_attributes['event_commitment']
    dispatch_interval_seconds = testdata['program_attributes']['dispatchable_length_resolution'] * 60
    # TODO move into intial program attribute check if necessary

    # Getting program attributes that are needed for event calculations and checks
    program_attributes = testdata['program_attributes']
    event_start_buffer = program_attributes['dispatch_scheduling_buffer']
    dispatchable_start_resolution = program_attributes['dispatchable_start_resolution']

    # Event start calculation
    # Auto Calculating expected event start time and making sure we avoid timeframes where that prediction transitions.
    if not retest_previous:
        start_after = testdata['event'].get('start_relative_to_creation')
        event_start_dt = calculate_fastdr_event_start(dispatchable_start_resolution, event_start_buffer, start_after)

    # Starting LCR event
    data = schedule_and_verify_fastdr_event(testdata, event_start_dt, retest_previous)
    gs_event_id = data['grid_service_event_id']
    logger.debug("New grid service event with id %s created ", gs_event_id)
    if not retest_previous:
        time.sleep(5)  # extra wait time to make sure queues get commited into db
        verify_event_queues_after_creation(gs_event_id, event_duration, dispatch_interval_seconds)
        logger.debug("new event passed 'verify_event_queues_after_creation'")

    # Collecting api response data and checking if it event data matches to what we expect
    depl_start_dt = data['deployment_start']
    depl_end_dt = data['deployment_end']
    logger.debug('deply_start: %s', depl_start_dt.strftime(STR_FORMAT))
    logger.debug('deply_end: %s', depl_end_dt.strftime(STR_FORMAT))
    if retest_previous:
        event_start_dt = pytz.utc.localize(depl_start_dt)
    else:
        assert event_start_dt == pytz.utc.localize(depl_start_dt)
    event_end_dt = pytz.utc.localize(depl_end_dt)
    assert event_end_dt == event_start_dt + dt.timedelta(seconds=event_data['duration_seconds'])

    event_data['gs_event_id'] = gs_event_id
    event_data['start_dt'] = event_start_dt
    event_data['end_dt'] = event_end_dt
    # some test scenarios will change end_dt of event so it is we are caching initial end_dt
    event_data['initial_end_dt'] = event_end_dt
    return event_data


def fastdr_testing_loop_until_event_ends(testdata, event_data):
    # test config needed for test scenario execution
    program_id = testdata['program_id']
    event_scenario = testdata['event']
    cancel_event = event_scenario.get('cancel')

    # Calculating event scenario duration
    event_completion_wait = max(event_data['charge_inhibit'], event_data['settlement_buffer']) + 60
    if cancel_event:
        event_wait_dt = event_data['end_dt']
    else:
        event_wait_dt = event_data['end_dt'] + dt.timedelta(seconds=event_completion_wait)
    logger.debug('Expected event end_dt: %s', event_data['end_dt'])
    logger.debug('Test scenario loop will run every minute until: %s', event_wait_dt)
    # We are going to check if task need to be run every minute and provide telemetry and reports too
    current_minute_dt = None
    next_minute_dt = round_time_up(dt.datetime.now(tz=UTC), minutes=1)
    while dt.datetime.now(tz=UTC) < event_wait_dt:
        # calculating next round minute so we can wait until then.
        if current_minute_dt:
            next_minute_dt += dt.timedelta(minutes=1)
        localtime_next_minute = next_minute_dt.astimezone(tz.tzlocal())
        logger.debug('*** Waiting until next minute: %s', localtime_next_minute.strftime(STR_FORMAT))
        # Waiting for next round minute
        pause.until(localtime_next_minute)

        current_minute_dt = next_minute_dt
        prev_minute_dt = current_minute_dt - dt.timedelta(minutes=1)

        # Calculating state of event based on current time and scenario dates.
        event_state = None
        if cancel_event and event_data['cancel_at_dt'] == current_minute_dt:
            info = "DURING EVENT CANCEL"
            event_state = DURING_EVENT_CANCEL
        elif current_minute_dt >= event_data['end_dt']:
            info = "DURING CHARGE INHIBIT"
            event_state = DURING_CHARGE_INHIBIT
        elif current_minute_dt >= event_data['start_dt']:
            info = "DURING EVENT"
            event_state = DURING_EVENT
        elif current_minute_dt < event_data['start_dt']:
            info = "DURING DCM INHIBIT"
            event_state = DURING_DCM_INHIBIT
            logger.debug('Current minute: %s', current_minute_dt)
            logger.debug('Event start: %s', event_data['start_dt'])
            logger.debug('Seconds from event start: %s', (event_data['start_dt'] - current_minute_dt).total_seconds())
        else:
            raise Exception("Unknown state. We are missing one of event states.")

        logger.debug('################################  %s  ##################################', event_state)

        # Running scenario actions if current time matches
        if cancel_event and event_data['cancel_at_dt'] == current_minute_dt:
            logger.debug('*** Cancelling event at %s.', current_minute_dt.strftime(STR_FORMAT))
            if cancel_event.get('openadr'):
                vtnc.delete_fastdr_event(testdata['ven_name'])
            else:
                ag_client.cancel_event(event_data['gs_event_id'], program_id)
            return

        # Waiting for latest telemetry data
        if testdata['run_emulators']:
            logger.debug('*** %s Waiting extra 5s so we get all the telemetry data.', info)
            pause.until(current_minute_dt + dt.timedelta(seconds=5))
            # NOTE 2min downsampling cron job ... we will miss previous minute half of the time.
            # NOTE When redis historians will cache data we could retrieve it from there.
            if (dt.datetime.now(tz=UTC) - current_minute_dt).total_seconds() < 40:
                telemetry_and_dispatch_data(testdata['resources'], prev_minute_dt - dt.timedelta(minutes=5))

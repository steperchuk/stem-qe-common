import logging
import time

from stemqa.helpers.db_helper import DbHelper, DB_GRID_SERVICES
from stemqa.vpp.helper import json_dumps
from stemqa.vpp.athenagateway.athenautil import get_gs_event

logger = logging.getLogger(__name__)

str_format = "%Y-%m-%d %H:%M:%S"


def count_event_queue(gs_event_id, service_id):
    dbhelper = DbHelper.default_instance()
    sql = '''
        SELECT COUNT(*) FROM event_queue
        JOIN event ON event.event_id = event_queue.event_id
        JOIN grid_service_event_received as gs ON gs.grid_service_event_id = event.grid_service_event_id
        WHERE gs.grid_service_event_id = "{gs_event_id}"
        AND event_queue.service_id = "{service_id}";
        '''.format(**{'gs_event_id': gs_event_id, 'service_id': service_id})

    count = dbhelper.query(DB_GRID_SERVICES, sql)[0]['COUNT(*)']
    return count


def count_event_queue_by_status(gs_event_id, service_id, status_id):
    dbhelper = DbHelper.default_instance()
    sql = '''
        SELECT COUNT(*) FROM event_queue
        JOIN event ON event.event_id = event_queue.event_id
        JOIN grid_service_event_received as gs ON gs.grid_service_event_id = event.grid_service_event_id
        WHERE gs.grid_service_event_id = "{gs_event_id}"
        AND event_queue.service_id = "{service_id}"
        AND event_queue.status_id = "{status_id}";
        '''.format(**{'gs_event_id': gs_event_id, 'service_id': service_id, 'status_id': status_id})

    count = dbhelper.query(DB_GRID_SERVICES, sql)[0]['COUNT(*)']
    return count


def get_event_queue(gs_event_id, service_id):
    dbhelper = DbHelper.default_instance()
    sql = '''
        SELECT * FROM event_queue
        JOIN event ON event.event_id = event_queue.event_id
        JOIN grid_service_event_received as gs ON gs.grid_service_event_id = event.grid_service_event_id
        WHERE gs.grid_service_event_id = "{gs_event_id}"
        AND event_queue.service_id = "{service_id}"
        ORDER BY event_queue.id DESC
        LIMIT 1;
        '''.format(**{'gs_event_id': gs_event_id, 'service_id': service_id})

    res = dbhelper.query(DB_GRID_SERVICES, sql)[0]
    return res


def event_performance(gs_event_id):
    dbhelper = DbHelper.default_instance()
    sql = '''
        SELECT COUNT(*) FROM event_performance
        JOIN event ON event.event_id = event_performance.event_id
        JOIN grid_service_event_received as gs ON gs.grid_service_event_id = event.grid_service_event_id
        WHERE gs.grid_service_event_id = "{grid_service_event_id}"
        '''.format(**{'grid_service_event_id': gs_event_id})

    res = dbhelper.query(DB_GRID_SERVICES, sql)[0]
    return res


def calc_dispatch_average(resource_dispatches, testdata, event_duration, rel_tol=0.70, dispatch_resolution=5,
                          stop_event=False, assert_diff=True):
    program_attributes = testdata['program_attributes']
    requested_power = program_attributes['event_commitment']
    dispatches_per_resource = event_duration // 60 // dispatch_resolution
    if stop_event and stop_event['after'] >= 0:
        dispatches_per_resource += 1
    expected_dispatches = len(testdata['resources']) * dispatches_per_resource
    logger.debug('Expected dispatches per resources: %s', dispatches_per_resource)
    logger.debug('Expected dispatches: %s', expected_dispatches)
    logger.debug('Actual dispatches: %s', len(resource_dispatches))
    logger.debug('Resource dispatces:\n%s', json_dumps(resource_dispatches))
    assert len(resource_dispatches) == expected_dispatches
    average_dispatch = sum(dis['requested_power'] for dis in resource_dispatches) / dispatches_per_resource
    dispatch_diff = abs(abs(average_dispatch) - requested_power)
    rel_diff = dispatch_diff / max(abs(average_dispatch), requested_power)
    logger.debug('Average dispatch: %s, requested power: %s, diff: %s, relative diff.: %s',
                 average_dispatch, requested_power, dispatch_diff, rel_diff)
    if assert_diff:
        assert rel_diff < rel_tol


# verify event_queues after event creation
def verify_event_queues_after_creation(grid_service_event_id, event_duration, dispatch_interval_seconds):
    assert count_event_queue(grid_service_event_id, 2) == 1
    expected_dispatch_intervals = event_duration // dispatch_interval_seconds
    assert count_event_queue(grid_service_event_id, 3) == expected_dispatch_intervals
    assert count_event_queue(grid_service_event_id, 4) == 1
    assert count_event_queue(grid_service_event_id, 5) == 1


def verify_event_stop(gs_event_id, event_initial_duration, event_duration, stopped_after, dispatch_resolution,
                      after_event_ended):
    logger.debug('------ verify event stop (after_event_complete: %s) --------', after_event_ended)
    logger.debug('initial_duration: %s', event_initial_duration)
    logger.debug('event_duration: %s', event_duration)
    logger.debug('stopped_after: %s', stopped_after)
    logger.debug('dispatch_resolution: %s', dispatch_resolution)

    if not after_event_ended and stopped_after % (dispatch_resolution * 60) == 0:
        logger.debug('Sleeping 10s to make sure we get correct results: %s', event_duration)
        time.sleep(10)

    assert count_event_queue_by_status(gs_event_id, 2, 3) == 2
    max_completed_dispatch_events = (event_duration // 60 // dispatch_resolution)
    # extra dispatch gets created when stop happens (instead of previous dispatch that gets cancelled)
    if stopped_after >= 0:
        max_completed_dispatch_events += 1
    if after_event_ended:
        expected_completed_dispatch_events = max_completed_dispatch_events
    elif stopped_after >= 0:
        expected_completed_dispatch_events = 2 + (stopped_after // 60 // dispatch_resolution)
    elif stopped_after < 0:
        expected_completed_dispatch_events = 0

    actual_completed_dispatch_events = count_event_queue_by_status(gs_event_id, 3, 3)
    logger.debug('expected_completed_dispatches: %s', expected_completed_dispatch_events)
    logger.debug('actual_completed_dispatches: %s', actual_completed_dispatch_events)
    if after_event_ended:
        # hard to predict exactly if called in the middle of dispatch change.
        assert expected_completed_dispatch_events == actual_completed_dispatch_events
    else:
        assert abs(abs(expected_completed_dispatch_events) - abs(actual_completed_dispatch_events)) <= 1

    if after_event_ended:
        expected_new_dispatch_events = 0
    else:
        expected_new_dispatch_events = max_completed_dispatch_events - actual_completed_dispatch_events
    actual_new_dispatch_events = count_event_queue_by_status(gs_event_id, 3, 1)
    logger.debug('expected_new_dispatches: %s', expected_new_dispatch_events)
    logger.debug('actual_new_dispatches: %s', actual_new_dispatch_events)
    if after_event_ended:
        # hard to predict exactly if called in the middle of dispatch change.
        assert expected_new_dispatch_events == actual_new_dispatch_events
    else:
        assert abs(abs(expected_new_dispatch_events) - abs(actual_new_dispatch_events)) <= 1

        # extra dispatch gets created when stop happens (instead of previous dispatch that gets cancelled)
    all_dispatch_events = (event_initial_duration // 60 // dispatch_resolution)
    if stopped_after >= 0:
        all_dispatch_events += 1
    expected_cancelled_num = all_dispatch_events - max_completed_dispatch_events
    actual_cancelled_num = count_event_queue_by_status(gs_event_id, 3, 5)
    logger.debug('expected_canceled_dispatches: %s', expected_cancelled_num)
    logger.debug('actual_canceled_dispatches: %s', actual_cancelled_num)
    assert actual_cancelled_num == expected_cancelled_num
    assert count_event_queue_by_status(gs_event_id, 4, 5) == 1
    assert count_event_queue_by_status(gs_event_id, 5, 3 if after_event_ended else 1) == 1
    assert count_event_queue_by_status(gs_event_id, 5, 5) == 1


def verify_finalizer(gs_event_id, dispatch_interval_seconds, event_duration, stop_event=False):
    grid_service_event_id = get_gs_event(gs_event_id)['grid_service_event_id']
    assert get_gs_event(gs_event_id)['status_id'] == 6
    assert count_event_queue_by_status(grid_service_event_id, 2, 3) == 2 if stop_event else 1
    expected_dispatch_intervals = event_duration // dispatch_interval_seconds
    # extra dispatch gets created when stop happens (instead of previous dispatch that gets cancelled)
    if stop_event and stop_event['after'] >= 0:
        expected_dispatch_intervals += 1
    assert count_event_queue_by_status(grid_service_event_id, 3, 3) == expected_dispatch_intervals
    assert count_event_queue_by_status(grid_service_event_id, 5, 3) == 1


def verify_settlement(gs_event_id):
    grid_service_event_id = get_gs_event(gs_event_id)['grid_service_event_id']
    assert count_event_queue_by_status(grid_service_event_id, 4, 3) == 1


def check_event_performance(gs_event_id):
    dbhelper = DbHelper.default_instance()
    event_id_sql = '''
        SELECT eq.event_id from event_queue as eq, event as e
        WHERE type_id = 3 AND e.event_id = eq.event_id
        and e.grid_service_event_id = {gs_event_id}
        ORDER BY event_id desc;
        '''.format(gs_event_id=gs_event_id)
    event_queue_data = dbhelper.query(DB_GRID_SERVICES, event_id_sql)
    assert event_queue_data
    event_id = event_queue_data[0]['event_id']

    perf_data = []

    event_performance_sql = '''
        SELECT * from event_performance
        WHERE event_id = %s;
        ''' % event_id
    perf_data = dbhelper.query(DB_GRID_SERVICES, event_performance_sql)
    assert perf_data

    for resource_event_data in perf_data:
        logger.debug(resource_event_data)

    return resource_event_data


def verify_event_cancellation(gs_event_id, initial_event_duration, dispatch_interval_seconds, cancelled_after):
    gs_event = get_gs_event(gs_event_id)
    logger.debug('Verifying cancellation of gs event: %s', gs_event_id)
    logger.debug('GS event data:\n %s', json_dumps(gs_event))
    logger.debug('Initial_event_duration: %s', initial_event_duration)
    logger.debug('Dispatch_interval_seconds: %s', dispatch_interval_seconds)

    cancelled_scheduler_service_q = count_event_queue_by_status(gs_event_id, 2, 5)
    logger.debug('cancelled_scheduler_service_q: %s', cancelled_scheduler_service_q)
    assert cancelled_scheduler_service_q == 1

    cancelled_dispatcher_service_q = count_event_queue_by_status(gs_event_id, 3, 5)
    logger.debug('cancelled_dispatcher_service_q: %s', cancelled_dispatcher_service_q)
    if cancelled_after >= 0:
        dispatches = initial_event_duration // dispatch_interval_seconds
        dispatches_cancelled = dispatches - (cancelled_after // dispatch_interval_seconds) - 1
    else:
        dispatches = initial_event_duration // dispatch_interval_seconds
        dispatches_cancelled = dispatches
    logger.debug('Expected dispatches: %s', dispatches)
    logger.debug('Cancelled dispatches: %s', dispatches_cancelled)
    assert cancelled_dispatcher_service_q == dispatches_cancelled

    # NOTE for some reason settlement queue can stay in NEW state
    # cancelled_settlement_service_q = count_event_queue_by_status(gs_event_id, 4, 5)
    # logger.debug('cancelled_settlement_service_q: %s', cancelled_settlement_service_q)
    # assert cancelled_settlement_service_q == 1

    cancelled_finalizer_service_q = count_event_queue_by_status(gs_event_id, 5, 5)
    logger.debug('cancelled_finalizer_service_q: %s', cancelled_finalizer_service_q)
    assert cancelled_finalizer_service_q == 1

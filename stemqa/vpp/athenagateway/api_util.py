import datetime as dt
import json
import logging
import time

import requests

from stemqa.config.settings import settings
from stemqa.rest.serviceclient import SvcClient, authentication_required
from stemqa.rest.gridresponse import GridResponseClient
from stemqa.vpp.athenagateway.athenautil import get_program_reference_id
from stemqa.vpp.athenagateway.athenautil import get_last_gs_event, get_gs_event
from stemqa.vpp.helper import json_dumps

logger = logging.getLogger(__name__)
str_format = "%Y-%m-%d %H:%M:%S"
LOG_RESPONSE = True


class AgApiException(Exception):
    pass


class AgApiResponse(object):
    """
    Represents a response from AG testing endpoints.
    """

    def __init__(self, response):
        self.response = response

    def json(self):
        return self.response.json()

    def data(self):
        json_d = self.json()
        logger.debug("response: %s", self.response)
        logger.debug("response json: %s", json_dumps(json_d))
        return json_d

    def content(self):
        return self.response.content

    def headers(self):
        return self.response.headers


class AgApiResponseHook(object):
    """
    Hook invoked when we receive a response from AG. Used primarily for logging the response
    """

    @staticmethod
    def log(response, *args, **kwargs):
        """
        Hook invoked when we receive a response from the Ag
        """
        if response.status_code != 200 or LOG_RESPONSE:
            logger.debug('args: %s, kwargs: %s', args, kwargs)
            logger.debug('status_code: %s', type(response.status_code))
            logger.debug('status_code: %s', response.status_code)
            logger.debug('url: %s', response.url)
            try:
                logger.debug('content: %s', json_dumps(response.json()))
            except ValueError:
                logger.debug("content: %s", response.text)


class AgApiClient(SvcClient):
    """
    Wrapper Class for Athena gateway microservice api. Currently only focused on
    LCR programs and quirks needed for DNP3
    """

    def __init__(self, url=None, version='v1', client=None):
        super().__init__()
        if client:
            self.ag_url = client.url
        elif url:
            self.ag_url = url
        else:
            self.ag_url = settings['stem-athena-gateway']['url']
        self.version = version
        self.response_archive = []
        self.schedules_url = "%s/%s/%s" % (self.ag_url, version, "schedules")
        self.reports_url = "%s/%s/%s" % (self.ag_url, version, "reports")
        self.ping_url = "%s/%s/%s" % (self.ag_url, version, "ping")

    def response_handling(self, response):
        self.response_archive.append(response)
        response.raise_for_status()
        return AgApiResponse(response)

    def get(self, url, headers=None, data=None, params=None, stream=False):
        response = requests.get(url, headers=headers, data=data, params=params, stream=stream, verify=False,
                                hooks=dict(response=AgApiResponseHook.log))
        return self.response_handling(response)

    def post(self, url, headers=None, data=None, params=None, files=None):
        response = requests.post(url, headers=headers, data=data, params=params, files=files, verify=False,
                                 hooks=dict(response=AgApiResponseHook.log))
        return self.response_handling(response)

    def put(self, url, headers=None, data=None, params=None, files=None):
        response = requests.put(url, headers=headers, data=data, params=params, files=files, verify=False,
                                hooks=dict(response=AgApiResponseHook.log))
        return self.response_handling(response)

    def patch(self, url, headers=None, data=None, params=None, files=None):
        response = requests.patch(url, headers=headers, data=data, params=params, files=files, verify=False,
                                  hooks=dict(response=AgApiResponseHook.log))
        return self.response_handling(response)

    def delete(self, url, headers=None, data=None, params=None, files=None):
        response = requests.delete(url, headers=headers, data=data, params=params, files=files, verify=False,
                                   hooks=dict(response=AgApiResponseHook.log))
        return self.response_handling(response)

    def get_ping(self):
        '''
            curl "http://localhost:8282/v1/ping"
        '''
        response = self.get(self.ping_url)
        return response.data()

    def _flip_dnp3_event_switch(self, program_id, start, requested_capacity_kw, reuse_previous_event=False):
        '''Simulating dnp3 start stop api calls. Same as starting&stopping via outstation.
            reuse_previous_event: Flag used for debugging event verfication.
                                  It mocks api call with previous event data.
            program_id:
                When testing LCR events via dnp3 protocol we only use reference_id which we get from program:
                reference_id: Limits which data is actually used based on program_type.
                            For LCR program event start and its end are auto-calculated based on time
                            of api call and all the program attributes that affect it.
                scheduler_id: Used for distinction between events aka event_id, but dnp3 event
                            creation logic between master and outstation has no id, so we cannot actually
                            use it in most real use cases it is actually same as reference_id.
                            We are always updating latest events created for that reference_id (relates to one program).
                            If we are trying to create event when another event is still active or when prohibited by
                            program attributes, we will get response with all validation values as keys.
                            (failed checks will have False value).
            start: True for event start, False for event stop
            requested_capacity: only used for verification of api response (for LCR program program
                                attribute event_commitment is used)

            curl -X POST \
            http://localhost:8282/v1/schedules \
            -H 'Content-Type: application/json' \
            -d '{"reference_id": "QA_LCR_DNP3"
                 "scheduler_id": "QA_LCR_DNP3"
                }'

        '''
        reference_id = get_program_reference_id(program_id)
        kwargs = {
            "reference_id": reference_id,
            "scheduler_id": reference_id,
            "deployment_end": None,  # for dnp3 programs it is automated based on api call time and attributes
            "deployment_start": None,  # for dnp3 programs it is automated based on api call time and attributes
            "requested_kw": requested_capacity_kw,
            # only used for verification. Api polish ignores it for dnp3 programs
            "reuse_previous_event": reuse_previous_event  # Flag used for debugging event verfication.
            # It mocks api call with previous event data.
        }
        if start:
            return self.create_event(program_id, **kwargs)
        else:
            return self.update_event(program_id, **kwargs)

    def start_dnp3_event(self, program_id, requested_capacity_kw, reuse_previous_event=False):
        '''Check _flip_dnp3_event_switch for documentation'''
        return self._flip_dnp3_event_switch(program_id, True, requested_capacity_kw, reuse_previous_event)

    def stop_dnp3_event(self, program_id, requested_capacity_kw, reuse_previous_event=False):
        '''Check _flip_dnp3_event_switch for documentation'''
        return self._flip_dnp3_event_switch(program_id, False, requested_capacity_kw, reuse_previous_event)

    def _verify_event_operation(self, start, program_id, response, previous_event, creation_data,
                                reuse_previous_event=False):
        """ Verification of event creation or its update.
            Waits for event to be created/updated inside DB and then confirms that values match.
        """
        operation = 'creation' if start else 'update'
        logger.debug('Event %s verification for program_id: %s', operation, program_id)
        logger.debug('Previous program event: %s', previous_event)
        if not reuse_previous_event:
            wait_time = 0
            event_persisted = False
            event_id_r = response.get('grid_service_event_id')
            if not event_id_r:
                raise AgApiException('Looks like event validation failed:\n%s' % json_dumps(response))

            previous_event_id = previous_event.get('grid_service_event_id')
            previous_update_date = previous_event.get('update_date')
            while not event_persisted:
                logger.debug('Waiting for event to be persisted...')
                time.sleep(2)  # making sure table grid_service_event_received gets populated
                wait_time += 2
                last_event = get_last_gs_event(program_id)
                logger.debug('Last program event: %s', last_event)
                if last_event:
                    if start and previous_event_id != last_event['grid_service_event_id']:
                        assert event_id_r == last_event['grid_service_event_id']
                        event_persisted = True
                        break
                    elif not start and previous_update_date != last_event['update_date']:
                        event_persisted = True
                        break
                if wait_time > 10:
                    raise AgApiException('Event %s not in `grid_service_event_received` table after 10s!' % event_id_r)
        else:
            last_event = previous_event

        logger.debug('Last grid service event for program_id %s: %s', program_id, last_event)
        if creation_data.get('requested_kw') is not None:
            assert creation_data['requested_kw'] == last_event['requested_kw']

        response['deployment_start'] = last_event['deployment_start']
        response['deployment_end'] = last_event['deployment_end']
        if reuse_previous_event:
            response['grid_service_event_id'] = last_event['grid_service_event_id']
            response['requested_kw'] = last_event['requested_kw']

        # TODO deployment_start and end_time asserts should probably live in this function.
        # TODO we could also add event_queue checks here and also remove this reponse mangling to check event

        return response

    @authentication_required
    def create_event(self, program_id=None, reference_id=None, scheduler_id=None, requested_kw=None,
                     deployment_start=None, deployment_end=None, reuse_previous_event=False):
        """When program creates event through athena schedule api,
           we will checks whether there is a plugin for it, otherwise run generic logic.

            In the example playload,
            {
                "requested_kw": 12,
                "deployment_start": "2018-07-24 21:24:00",
                "deployment_end": "2018-07-24 22:24:00",
                "reference_id": "LCR6_QA_GS_CLIENT_Resource11",
                "scheduler_id": "SCE DNP3"
            }

            Only scheduler_id, reference_id is passed to core function process_event(),
                deployment_start, deployment_end will be overwritten.

            requested_kw is constant value based on sce contract.

            deployment_start = now() + 5 min buffer and rounding
            deployment_end = deployment_start + event_duration
        """
        previous_event = get_last_gs_event(program_id)
        actual_reference_id = get_program_reference_id(program_id)
        if reference_id:
            assert reference_id == actual_reference_id
        else:
            reference_id = actual_reference_id

        if not reuse_previous_event:
            if isinstance(deployment_start, dt.datetime):
                deployment_start = deployment_start.strftime(str_format)
            if isinstance(deployment_end, dt.datetime):
                deployment_end = deployment_end.strftime(str_format)
            data = {
                "requested_kw": requested_kw,
                "deployment_start": deployment_start,
                "deployment_end": deployment_end,
                "reference_id": reference_id,
                "scheduler_id": scheduler_id or reference_id
            }
            response = self.post(self.schedules_url, data=json.dumps(data), headers=self.headers).data()
            logger.debug('Schedule start api call response: %s', json_dumps(response))
        else:
            data = {}
            response = {}

        return self._verify_event_operation(True, program_id, response, previous_event, data, reuse_previous_event)

    @authentication_required
    def update_event(self, program_id=None, reference_id=None, scheduler_id=None, requested_kw=None,
                     deployment_start=None, deployment_end=None, reuse_previous_event=False):
        """ Used for modification of event after it gets created.
            Same paramaters as for event creation. reference_id and scheduler_id must match to update wanted event.
            In case of dnp3 programs you can only update last event that was created.
        """
        previous_event = get_last_gs_event(program_id)
        actual_reference_id = get_program_reference_id(program_id)
        if reference_id:
            assert reference_id == actual_reference_id
        else:
            reference_id = actual_reference_id

        if not reuse_previous_event:
            data = {
                "requested_kw": requested_kw,
                "deployment_start": deployment_start,
                "deployment_end": deployment_end,
                "reference_id": reference_id,
                "scheduler_id": scheduler_id or reference_id
            }
            response = self.put(self.schedules_url, data=json.dumps(data), headers=self.headers).data()
            logger.debug('Schedule start api call response: %s', json_dumps(response))
        else:
            data = {}
            response = {}

        return self._verify_event_operation(False, program_id, response, previous_event, data, reuse_previous_event)

    @authentication_required
    def cancel_event(self, gs_event_id, program_id, reference_id=None, scheduler_id=None):
        """ Used for modification of event after it gets created.
            Same paramaters as for event creation. reference_id and scheduler_id must match to update wanted event.
            In case of dnp3 programs you can only update last event that was created.
        """
        prog_reference_id = get_program_reference_id(program_id)
        if reference_id:
            assert prog_reference_id == reference_id
        else:
            reference_id = prog_reference_id

        gs_event_data = get_gs_event(gs_event_id)
        if scheduler_id:
            assert scheduler_id == gs_event_data['reference_id']
        else:
            scheduler_id = gs_event_data['reference_id']

        with GridResponseClient(settings['stem-web-app']['url']) as client:
            events = client.event_list(program_id)
            if list(events['events'].keys()):
                data = {"reference_id": reference_id,
                        "scheduler_id": scheduler_id}
                response = self.delete(self.schedules_url, data=json.dumps(data), headers=self.headers).data()
                data = client.event_list(program_id)
                cancelled_event = data['events'][str(gs_event_id)]
                wait_start = dt.datetime.now()
                while cancelled_event['status'] not in ['CANCELLED', 'PENDING_CANCEL'] and \
                        dt.datetime.now() < (wait_start + dt.timedelta(seconds=90)):
                    logger.debug("Event status: %s", cancelled_event)
                    time.sleep(15)
                    data = client.event_list(program_id)
                    cancelled_event = data['events'][str(gs_event_id)]
            else:
                raise AgApiException('No events found for program_id: %s' % program_id)

            return response

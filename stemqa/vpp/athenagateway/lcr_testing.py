import datetime as dt
import time
import traceback
import logging

import pause
import pytz
from dateutil import tz
from pytz import UTC

from stemqa.vpp.athenagateway.api_util import AgApiClient
from stemqa.vpp.athenagateway.dnp3_util import MasterApiClient
from stemqa.vpp.athenagateway.dnp3_util import check_dnp3_values
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_setup
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_event_info
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_telemetry_and_dispatch_info
from stemqa.vpp.athenagateway.generic_testing import generic_gs_testing_test_cleanup
from stemqa.vpp.athenagateway.generic_testing import event_scenario_calculations
from stemqa.vpp.athenagateway.generic_testing import verifications_after_event_ends
from stemqa.vpp.athenagateway.generic_testing import reruns_possible, parallel_testing_possible
from stemqa.vpp.athenagateway.generic_testing import BEFORE_EVENT_CREATION
from stemqa.vpp.athenagateway.generic_testing import AFTER_EVENT_CREATION
from stemqa.vpp.athenagateway.generic_testing import DURING_DCM_INHIBIT
from stemqa.vpp.athenagateway.generic_testing import DURING_EVENT
from stemqa.vpp.athenagateway.generic_testing import DURING_EVENT_STOP
from stemqa.vpp.athenagateway.generic_testing import DURING_CHARGE_INHIBIT
from stemqa.vpp.athenagateway.generic_testing import AFTER_EVENT_ENDS

from stemqa.vpp.athenagateway.athenautil import modify_gs_event, mock_redis_monitor_data, \
    get_aggregation_redis_data, round_to_resolution, LCR_TESTING_PROGRAM_ATTRIBUTES
from stemqa.vpp.athenagateway.event_verifications import verify_event_queues_after_creation
from stemqa.vpp.athenagateway.event_verifications import verify_event_stop
from stemqa.vpp.helper import round_time_up, STR_FORMAT
from stemqa.vpp.telemetry import telemetry_and_dispatch_data

logger = logging.getLogger(__name__)
ag_client = AgApiClient()


def lcr_test_setup(testdata, *args, reuse_emulators=False, retest_previous=False, **kwargs):
    testdata['default_program_attributes'] = LCR_TESTING_PROGRAM_ATTRIBUTES
    generic_gs_testing_setup(testdata, *args, reuse_emulators=reuse_emulators,
                             retest_previous=retest_previous, **kwargs)
    event_scenario = testdata['event']
    # DNP3 specific setup
    dnp3 = event_scenario.get('dnp3')
    dnp3_values = testdata.setdefault('dnp3_values', {})
    dnp3_used = not retest_previous and (dnp3_values or dnp3)
    # Creating dnp3 master if needed.
    if dnp3_used:
        logger.debug('Recreating DNP3 SCE master!')
        testdata['master_client'] = MasterApiClient('SCE', recreate=False)
    else:
        testdata['master_client'] = None

    if dnp3_values:
        dnp3_values.setdefault('stage_retries', 0)
        dnp3_values.setdefault('value_interval', 1)
        dnp3_values.setdefault('display_all_stages', True)

        # Mocked data that can be used to test dnp3 signals we are reporting without running emulators.
        mocked_redis_data = dnp3_values.get('mocked_redis_data')
        if mocked_redis_data:
            dnp3_values['mocking'] = True

        if testdata.get('run_emulators', True) and mocked_redis_data:
            raise Exception("Mocking redis telemetry data when emulators are running makes no sense."
                            "Comment mocking out or disable emulators in testdata ('run_emulators: false')")


@reruns_possible
@parallel_testing_possible(lcr_test_setup)
def test_lcr_dnp3_generic(testdata, reuse_emulators=False, retest_previous=False, **kwargs):
    """Generic LCR event (via dnp3 signals or AG api) and dnp3 signal testing.
        testdata: Test configuration object loaded from yaml for specific testcase.
                  Can be loaded with defaults before we pass it to this function otherwise that will
                  be done inside `lcr_test_setup` routine.

        [special flags that are usually only used for test development]:
        reuse_emulators: Special flag that enables us to retest same test without waiting for emulators.
        retest_previous: Special flag that enables us to retest and develop verifications for longer event testcases.
                         It only reruns event verifications that happen after event ends.
                         Dnp3 signals cannot be retested. Event creation_only option cannot be retested RN.
    """
    # We could use assume (pytest extension) instead of this global error list. It is very simmilar.
    errors = []  # only used to collect all verification errors when event ends.
    try:
        event_scenario = testdata['event']
        creation_only = event_scenario.get('creation_only')
        master_client = testdata['master_client']

        # creating and verifying LCR event based on test data provided.
        event_data = lcr_dnp3_event_creation(testdata, master_client, retest_previous)

        # Filling event_data with info needed by testing loop and verifications
        event_scenario_calculations(testdata, event_data, retest_previous)

        if not (creation_only or retest_previous):
            lcr_dnp3_testing_loop_until_event_ends(testdata, event_data, master_client)

        # Checking dnp3 values after event ends. Only does checks if testdata config demands it.
        if not retest_previous:
            try:
                check_dnp3_values_when(AFTER_EVENT_ENDS, testdata, master_client)
            except AssertionError:
                errors.append(traceback.format_exc())

        if not creation_only:
            # running complex GS event verifications after event has completed
            verifications_after_event_ends(testdata, event_data, errors, retest_previous)

    except Exception:  # pylint: disable=W0703
        errors.append(traceback.format_exc())
    finally:
        # Useful test info to sum up what was going on before failure or after test passed
        try:
            generic_gs_testing_event_info(testdata)
            generic_gs_testing_telemetry_and_dispatch_info(testdata)
        except Exception:
            logger.debug('Test info function failed: %s', traceback.format_exc())

        # Checking if we need to do some LCR specific test clenup
        try:
            cleanup = testdata.get('cleanup', {})
            if cleanup.get('stop_event', False):
                logger.debug('*** LCR event cleanup: Stopping lcr event')
                stop_lcr_event(testdata, master_client)
        except Exception:
            errors.append(traceback.format_exc())

        # Checking if we need to do some LCR specific test clenup
        try:
            generic_gs_testing_test_cleanup(testdata, retest_previous)
        except Exception:
            errors.append(traceback.format_exc())

        assert not errors, 'Collection of errors:\n%s' % "\n\n" + 90 * "=" + "\n\n".join(errors)


def calculate_LCR_event_start(dispatch_start_resolution, dispatch_scheduling_buffer):
    # Auto Calculating expected event start time and making sure we wait for time frame where this gets hard to do.
    logger.debug('*** Calculating event start based on AG LCR polish')
    utcnow = dt.datetime.now(tz=UTC).replace(microsecond=0)
    if utcnow.minute % dispatch_start_resolution == 0 and utcnow.second > 50:
        logger.debug('*** Waiting 20 seconds to make sure we can calculate expected start time')
        time.sleep(20)
        utcnow = dt.datetime.now(tz=UTC).replace(microsecond=0)
    elif dispatch_start_resolution == 1 and utcnow.second < 10:
        logger.debug('*** Waiting 20 seconds to make sure we can calculate expected start time')
        pause.until(utcnow + dt.timedelta(seconds=20))
    delta = dt.timedelta(minutes=dispatch_scheduling_buffer)
    logger.debug('*** Delta: %s', delta)
    logger.debug('*** Utcnow: %s', utcnow.strftime(STR_FORMAT))
    event_start_dt = \
        round_to_resolution(utcnow + dt.timedelta(minutes=dispatch_scheduling_buffer), dispatch_start_resolution)
    logger.debug('*** Calculated event start: %s', event_start_dt.strftime(STR_FORMAT))
    return event_start_dt


def lcr_dnp3_event_creation(testdata, master_client, retest_previous=False):
    event_data = {}
    testdata['event_data'] = event_data
    program_attributes = testdata['program_attributes']
    event_duration = program_attributes['maximum_event_length']
    event_data['duration_seconds'] = event_duration
    event_data['requested_kw'] = program_attributes['event_commitment']
    dispatch_interval_seconds = testdata['program_attributes']['dispatchable_length_resolution'] * 60

    # TODO move into intial program attribute check if necessary
    # Checking dnp3 values before event gets created. Only does checks if testdata config demands it.
    if not retest_previous:
        check_dnp3_values_when(BEFORE_EVENT_CREATION, testdata, master_client)

    # Getting program attributes that are needed for event calculations and checks
    program_attributes = testdata['program_attributes']
    event_start_buffer = program_attributes['dispatch_scheduling_buffer']
    dispatchable_start_resolution = program_attributes['dispatchable_start_resolution']

    # Auto Calculating expected event start time and making sure we avoid timeframes where that prediction transitions.
    if not retest_previous:
        event_start_dt = calculate_LCR_event_start(dispatchable_start_resolution, event_start_buffer)
        logger.debug('*** Calculated LCR start: %s', event_start_dt.strftime(STR_FORMAT))

    # Starting LCR event
    data = schedule_lcr_event(testdata, master_client, retest_previous)
    gs_event_id = data['grid_service_event_id']
    logger.debug("New grid service event with id %s created ", gs_event_id)
    if not retest_previous:
        time.sleep(5)  # extra wait time to make sure queues get commited into db
        verify_event_queues_after_creation(gs_event_id, event_duration, dispatch_interval_seconds)
        logger.debug("new event passed 'verify_event_queues_after_creation'")
    # Collecting api response data and checking if it matches to what we expect
    depl_start_dt = data['deployment_start']
    depl_end_dt = data['deployment_end']
    logger.debug('deply_start: %s', depl_start_dt.strftime(STR_FORMAT))
    logger.debug('deply_end: %s', depl_end_dt.strftime(STR_FORMAT))
    if retest_previous:
        event_start_dt = pytz.utc.localize(depl_start_dt)
    else:
        assert abs((event_start_dt - pytz.utc.localize(depl_start_dt)).total_seconds()) <= 60
        event_start_dt = pytz.utc.localize(depl_start_dt)
    event_end_dt = pytz.utc.localize(depl_end_dt)

    # Checking dnp3 values after event gets created. Only does checks if testdata config demands it.
    if not retest_previous:
        check_dnp3_values_when(AFTER_EVENT_CREATION, testdata, master_client)

    # Filling event_data with useful info before returning it.
    event_data['gs_event_id'] = gs_event_id
    event_data['start_dt'] = event_start_dt
    event_data['end_dt'] = event_end_dt
    # some test scenarios will change end_dt of event_data, this is we cache it as initial_end_dt
    event_data['initial_end_dt'] = event_end_dt
    return event_data


def stop_lcr_event(testdata, master_client=None):
    """ Cleanup function that stops LCR event.
    """
    program_id = testdata['program_id']
    event_scenario = testdata['event']
    dnp3 = event_scenario.get('dnp3')
    event_data = testdata.get('event_data')
    if dnp3 and master_client:
        master_client.operate_latch(dnp3['index'], False, program_id)
        data = ag_client.stop_dnp3_event(program_id, event_data['requested_kw'], reuse_previous_event=True)
    else:
        data = ag_client.stop_dnp3_event(program_id, event_data['requested_kw'], reuse_previous_event=False)
    return data


def lcr_dnp3_testing_loop_until_event_ends(testdata, event_data, master_client):
    # test config needed for test scenario execution
    program_id = testdata['program_id']
    gs_event_id = event_data['gs_event_id']
    event_scenario = testdata['event']
    stop_event = event_scenario.get('stop')
    cancel_event = event_scenario.get('cancel')
    modify_event = event_scenario.get('modify')
    dnp3 = event_scenario.get('dnp3')
    dnp3_values = testdata.get('dnp3_values')

    # Calculating event scenario duration
    event_completion_wait = max(event_data['charge_inhibit'], event_data['settlement_buffer']) + 60
    event_wait_dt = event_data['end_dt'] + dt.timedelta(seconds=event_completion_wait)
    logger.debug('Expected event end_dt: %s', event_data['end_dt'])
    logger.debug('Test scenario loop will run every minute until: %s', event_wait_dt)
    # caches for storing snapshots needed for dnp3 data point checks
    bias_snapshots = []
    bias_load = None
    # We are going to check if task need to be run every minute and provide telemetry and reports too
    current_minute_dt = None
    next_minute_dt = round_time_up(dt.datetime.now(tz=UTC), minutes=1)
    while dt.datetime.now(tz=UTC) <= event_wait_dt:
        # calculating next round minute so we can wait until then.
        if current_minute_dt:
            next_minute_dt += dt.timedelta(minutes=1)
        localtime_next_minute = next_minute_dt.astimezone(tz.tzlocal())
        logger.debug('*** Waiting until next minute: %s', localtime_next_minute.strftime(STR_FORMAT))
        # Waiting for next round minute
        pause.until(localtime_next_minute)

        current_minute_dt = next_minute_dt
        prev_minute_dt = current_minute_dt - dt.timedelta(minutes=1)

        # Calculating state of event based on current time and scenario dates.
        event_state = None
        get_main_snapshot = False
        if current_minute_dt >= event_data['end_dt']:
            info = "DURING CHARGE INHIBIT"
            event_state = DURING_CHARGE_INHIBIT
        elif stop_event and current_minute_dt >= event_data['stop_at_dt']:
            info = "DURING EVENT STOP"
            event_state = DURING_EVENT_STOP
        elif modify_event and current_minute_dt >= event_data['modify_at_dt']:
            info = "AFTER EVENT MODIFICATION"
            event_state = DURING_EVENT
        elif current_minute_dt >= event_data['start_dt']:
            info = "DURING EVENT"
            event_state = DURING_EVENT
        elif current_minute_dt < event_data['start_dt']:
            info = "DURIN DCM INHIBIT"
            event_state = DURING_DCM_INHIBIT
            logger.debug('Current minute: %s', current_minute_dt)
            logger.debug('Event start dt: %s', event_data['start_dt'])
            if (event_data['start_dt'] - current_minute_dt).total_seconds() == 60:
                info = "1min before event starts"
                get_main_snapshot = True
        else:
            raise Exception("Unknown state. We are missing one of event states.")

        logger.debug('################################  %s  ##################################', event_state)

        # Running scenario actions if current time matches
        if cancel_event and event_data['cancel_at_dt'] == current_minute_dt:
            logger.debug('*** Cancelling event at %s.', current_minute_dt.strftime(STR_FORMAT))
            ag_client.cancel_event(event_data['gs_event_id'], program_id)

        if modify_event and event_data['modify_at_dt'] == current_minute_dt:
            logger.debug('*** Modifing event at %s.', current_minute_dt.strftime(STR_FORMAT))
            modify_gs_event(event_data['gs_event_id'],
                            event_data['end_after_modify_dt'].strftime(STR_FORMAT),
                            modify_event['requested_kw'])

        if stop_event and event_data['stop_at_dt'] == current_minute_dt:
            # TODO pack this into function
            time.sleep(10)  # Making sure we are not exactly on minute mark which can be hard to predict
            logger.debug('*** Stopping event at %s.', current_minute_dt.strftime(STR_FORMAT))
            if dnp3:
                master_client.operate_latch(dnp3['index'], False, program_id)
                data = ag_client.stop_dnp3_event(program_id, event_data['requested_kw'], reuse_previous_event=True)
            else:
                data = ag_client.stop_dnp3_event(program_id, event_data['requested_kw'], reuse_previous_event=False)
            depl_start_dt = data['deployment_start']
            assert event_data['start_dt'] == pytz.utc.localize(depl_start_dt)
            depl_end_dt = data['deployment_end']
            assert event_data['end_dt'] == pytz.utc.localize(depl_end_dt)
            assert event_data['duration_seconds'] == (depl_end_dt - depl_start_dt).total_seconds()
            if not modify_event and stop_event.get('verify', True):
                logger.debug('*** Waiting 15s to give system time to handle event queues')
                time.sleep(15)
                logger.debug('*** Verification of event stoppage at %s', current_minute_dt.strftime(STR_FORMAT))
                verify_event_stop(gs_event_id, event_data['initial_duration'], event_data['duration_seconds'],
                                  stop_event['after'], event_data['dispatch_resolution'], False)

        # Checking dnp3 values based on state of event
        if dnp3_values:
            if event_state == DURING_DCM_INHIBIT:
                check_dnp3_values_when(DURING_DCM_INHIBIT, testdata, master_client, bias_snapshots)
            elif event_state == DURING_EVENT:
                check_dnp3_values_when(DURING_EVENT, testdata, master_client, bias_snapshots)
            elif event_state == DURING_EVENT_STOP:
                check_dnp3_values_when(DURING_EVENT_STOP, testdata, master_client, bias_snapshots)
            elif event_state == DURING_CHARGE_INHIBIT:
                check_dnp3_values_when(DURING_CHARGE_INHIBIT, testdata, master_client, bias_snapshots)

        if get_main_snapshot and dnp3_values:
            logger.debug('*** %s - Getting snapshot of main for biasLoad testing', info)
            value_interval = dnp3_values.get('value_interval')
            if value_interval:
                pause.until(current_minute_dt + dt.timedelta(seconds=59 - value_interval))
                for x in range(0, value_interval + 1):
                    if x != 0:
                        time.sleep(1)
                    data = get_aggregation_redis_data(testdata['resources'], testdata['emulator_rollup'])
                    bias_snapshots.append(data['main'])
                bias_load = sum(bias_snapshots) / len(bias_snapshots)
                logger.debug('*** Snapshots of main: %s  average bias_load: %s', bias_snapshots, bias_load)

        # Waiting for latest telemetry data
        if testdata['run_emulators']:
            logger.debug('*** %s Waiting extra 5s so we get all the telemetry data.', info)
            pause.until(current_minute_dt + dt.timedelta(seconds=5))
            # NOTE 2min downsampling cron job ... we will miss previous minute half of the time.
            # NOTE When redis historians will cache data we could retrieve it from there.
            if (dt.datetime.now(tz=UTC) - current_minute_dt).total_seconds() < 40:
                telemetry_and_dispatch_data(testdata['resources'], prev_minute_dt - dt.timedelta(minutes=5))


def check_dnp3_values_when(event_state, testdata, master_client, bias_load=None):
    # Mocking redis telemetry data if we want to test reports or dnp3 values without emulators.
    # checking if this test even tests dnp3 values
    dnp3_values = testdata.get('dnp3_values')
    if dnp3_values:
        # checking if we want to display complete signals output
        display_dnp3_values = dnp3_values.get('display_all_stages', True)
        # getting expected values and settings for current event_state
        dnp3_values_now = dnp3_values.get(event_state, {})
        if dnp3_values_now or display_dnp3_values:
            mocked_redis_data = dnp3_values.get('mocked_redis_data', {})
            if mocked_redis_data.get(event_state):
                logger.debug('We are not runnings emulators! Inserting mock data into redis so we can '
                             'test reports in current state: %s!', event_state)
                time.sleep(10)
                mock_redis_monitor_data(testdata['resources'],
                                        testdata['emulator_rollup'],
                                        mocked_redis_data[event_state])

            logger.debug('Testing dnp3 signals during this event state: %s', event_state)
            # setting for how long should we wait after event_state change
            after = dnp3_values_now.get('after', 0)
            # setting for expected dnp3 signals. Only signals provided will be asserted.
            # There also special templating option for telemetry signals so we can compare values based
            # on last few seconds (value_interval) of redis telemetry.
            # More about this should be documented in check_dnp3_values function.
            expected = dnp3_values_now.get('expected_values', None)
            # setting that defines how long we should collect telemetry values in redis before comparing them to those
            # that are reported in dnp3 values that we get from master_client
            value_interval = dnp3_values.get('value_interval', 4)
            stage_retries = dnp3_values.get('stage_retries', 2)
            # generalized function that will check expected values to those that
            # dnp3 master is getting from outstation
            check_dnp3_values(master_client, testdata['resources'], expected, after,
                              value_interval, stage_retries, display_dnp3_values,
                              emulator_rollup=testdata.get('emulator_rollup'),
                              bias_load=bias_load)


def schedule_lcr_event(testdata, master_client, reuse_previous_event=False):
    """ Generic LCR event creation based on test settings passed.
        master_client: master outstation client that is used if we are testing with dnp3
        ag_client: athena gateway client used for event creation and also for response mocking when retesting.
        dnp3_config: settings used by master_client for dnp3 event creation
        reuse_previous_event: Used when we want to rerun verifications on previously completed event.
                              It knows how to mock response so we can retest without additonal hassle.
        program_id and requested_kw: are used for verification of event creation.
    """
    program_id = testdata['program_id']
    event_scenario = testdata['event']
    dnp3_config = event_scenario.get('dnp3')
    program_attributes = testdata['program_attributes']
    requested_kw = program_attributes['event_commitment']
    # If we are retesting modify test case then we have a corner case for requested power check.
    modify_event = event_scenario.get('modify')
    if modify_event and reuse_previous_event:
        requested_kw = modify_event['requested_kw']

    if dnp3_config:
        if not reuse_previous_event:
            master_client.operate_latch(dnp3_config['index'], True, program_id)
        data = ag_client.start_dnp3_event(program_id, requested_kw, reuse_previous_event=True)
    else:
        data = ag_client.start_dnp3_event(program_id, requested_kw, reuse_previous_event=reuse_previous_event)

    return data

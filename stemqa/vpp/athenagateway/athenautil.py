import copy
import datetime as dt
import logging
import time
import traceback

import pytz
import redis

from stemqa.config.settings import settings
from stemqa.helpers.db_helper import DbHelper, DB_GRID_SERVICES, DB_METER_LOGS, DB_OPTIMIZATION
from stemqa.rest.gridresponse import GridResponseClient
from stemqa.rest.powerconfig import PowerConfigClient
from stemqa.util import dataload_util
from stemqa.util.emulator_util import configure_simple_emulator
from stemqa.util.emulator_util import reset_emulator_db_entries
from stemqa.util.emulator_util import setup_emulator_container
from stemqa.util.emulator_util import stop_and_delete_emulator
from stemqa.vpp.helper import fetch_program_id, json_dumps, ExcThread, STR_FORMAT

logger = logging.getLogger(__name__)

# Dictionaries of important default program attributes for testing that we can change by providing
# custom attributes dictionary to update_program_attributes function.
# Some default values are modified from what is used in production for easier testing.
# (max_event_count_per_period, hours_of_eligibility)

COMMON_TESTING_PROGRAM_ATTRIBUTES = {
    "dispatch_adjustment_milestones": {
        "id": 26,
        "default": "(.0, .083333, .166666, .25, .33333, 0.4166666, .5, 0.5833333, 0.6666666, 0.75, 0.833333, 0.916666)"
    },
    "minimum_allocation_adjustment_interval":
        {"id": 28,
         "default": 300},
    "charge_inhibit_window":
        {"id": 42,
         "default": 120},
    "hours_of_eligibility":
        {"id": 54,
         "default": "(datetime.time(0, 0), datetime.time(23, 59))"},
    "annual_days_of_eligibility":
        {"id": 56,
         "default": "(((1, 1), (12, 31)),)"},
    "max_event_count_per_period":
        {"id": 64,
         "default": '{"MINUTE": None, "HOUR": None, "DAY": 1000, "WEEK": None}'},
    "weekday_eligibility":
        {"id": 66,
         "default": "[0, 1, 2, 3, 4, 5, 6]"},
    "program_duration":
        {"id": 68,
         "default": "(datetime.datetime(2016, 9, 1), datetime.datetime(2222, 1, 1))"},
    "program_timezone":
        {"id": 70,
         "default": '"US/Pacific"'},
    "dispatch_cancellation_buffer":
        {"id": 90,
         "template": '{"TYPE": "TIME_BEFORE_DISPATCH", "VALUE": ("MINUTE", %s)}',
         "default": 5},
    "dispatchable_start_resolution":
        {"id": 100,
         "template": '("MINUTE", %s)',
         "default": 5},
    "dispatchable_length_resolution":
        {"id": 102,
         "template": '("MINUTE", %s)',
         "default": 5},
    "dispatch_scheduling_buffer":
        {"id": 104,
         "template": '{"TYPE": "TIME_BEFORE_DISPATCH", "VALUE": ("MINUTE", %s)}',
         "default": 5},
    "max_total_scheduled_event_time":
        {"id": 149,
         "default": '{"WEEK": None, "MONTH": 288000, "YEAR": 1620000}'},
}

# Default testing attributes for LCR programs

LCR_TESTING_PROGRAM_ATTRIBUTES = {
    "minimum_event_length":
        {"id": 60,
         "default": 0},
    "maximum_event_length":
        {"id": 62,
         "default": 14400},
    "DCM_inhibit_enable":
        {"id": 150,
         "default": 'True'},
    "DCM_inhibit_schedule":
        {"id": 152,
         "template": ('{"START": {"TYPE": "RELATIVE_TIME", "ANCHOR": "DEPLOYMENT_START","VALUE": {"DELTA": %s}}'
                      ',"END": {"TYPE": "RELATIVE_TIME","ANCHOR": "DEPLOYMENT_START","VALUE": {"DELTA": 0}}}'),
         "default": -86400},
    "settlement_job_scheduling_buffer":
        {"id": 165,
         "default": 120},
    "event_commitment":
        {"id": 167,
         "default": 10},
}

# Default testing attributes for FastDR programs.

FASTDR_TESTING_PROGRAM_ATTRIBUTES = {
    "minimum_event_length":
        {"id": 60,
         "default": 0},
    "maximum_event_length":
        {"id": 62,
         "default": 3600},
    "dispatch_cancellation_buffer":
        {"id": 90,
         "template": '{"TYPE": "TIME_BEFORE_DISPATCH", "VALUE": ("MINUTE", %s)}',
         "default": 5},
    "DCM_inhibit_enable":
        {"id": 150,
         "default": 'False'},
    "UMI_length":
        {"id": 156,
         "default": 900},
    "Charge_inhibit_restriction_type":
        {"id": 160,
         "default": '"RESTRICTION_TYPE_UMI_AWARE"'},
    "settlement_job_scheduling_buffer":
        {"id": 165,
         "default": 120},
    "event_commitment":
        {"id": 167,
         "default": 50},
}


def update_program_attributes(program_id, custom_values, default_attributes, retest_previous_run=False):
    """
    Helper function for updating program attributes based on provided default and custom attributes.
    :param program_id: Program id for which we want to apply program attributes
    :type program_id: int
    :param custom_values: Custom values that we usually get from testdata settings.
                          We support dicts without ids, name and value of attributes suffice, but
                          those attribute names must exist in COMMON_TESTING_PROGRAM_ATTRIBUTES or
                          in default_attributes dict that is provided. Schema: {`attr_name` : `value`}
    :type custom_values: dict
    :param default_attributes: Default settings for set of tests cases or program type. Needs same structure as
                               COMMON_TESTING_PROGRAM_ATTRIBUTES. If there are some special attributes you want
                               to change with custom_values settings, then you need to provide it also with this dict.
                               so that we can map the attribute ids to that updates values.
    :type default_attributes: dict
    :param retest_previous_run: Used if you are retesting completed test scenario and want to speed it up.
    :type retest_previous_run: bool, optional
    :return: We return dict that matches custom_values in schema and also adds all the default values missing
             that will be applied. We use it for convinient cache of program attributes when doing calculation
             of test scenario and its verfication.
    :rtype: dict
    """
    dbhelper = DbHelper.default_instance()
    logger.debug('Setting and updating program attributes for program_id: %s', program_id)
    attribute_settings = copy.deepcopy(COMMON_TESTING_PROGRAM_ATTRIBUTES)
    attribute_settings.update(default_attributes)
    applied_values = copy.deepcopy(custom_values)
    applied_values.update({attr_name: attr['default'] for attr_name, attr in list(attribute_settings.items())
                           if attr_name not in custom_values})
    for name, value in list(applied_values.items()):
        attribute_settings[name]['value'] = value

    if retest_previous_run:
        return applied_values

    multi_statement_sql = ""
    for attr_name, attr in list(attribute_settings.items()):
        logger.debug('Setting and updating program attribute `%s` with folowing:\n%s',
                     attr_name, json_dumps(attr))
        attr_value = attr['template'] % attr['value'] if attr.get('template') else attr['value']
        value_fmt = "%s" if isinstance(attr_value, (int, float)) else "'%s'"

        dtutc = dt.datetime.utcnow()
        sql = '''
            INSERT INTO program_attributes(program_id, item_id, item_value, active, available, update_date)
            VALUES ({program_id}, {item_id}, {item_value}, 1, 1, '{dtutc}')
            ON DUPLICATE KEY UPDATE item_value = {item_value}, update_date = '{dtutc}';
            '''.format(program_id=program_id,
                       item_id=attr['id'],
                       item_value=value_fmt % attr_value,
                       dtutc=dtutc.strftime(STR_FORMAT))
        multi_statement_sql += sql + '\n\n'

    dbhelper.query(DB_GRID_SERVICES, multi_statement_sql)

    return applied_values


def import_data():
    """
    Added by Andy when we started with LCR testing because we thought we would need more historic data
    to support proper dispatching algo. We figured later that 15min telemetry was enough when running emulators.
    Perhaps we could something similar to test GS dispatching tests without not running emulators.
    We have tests where everything works without emualtors, but dispatches are always 0 because of no recent data or
    perhaps because it detects that device is not running? Meaning that telemetry would need to be imported ahead
    of test start time or emulated without actually running emulators. Only caveat is that dispatch schedules will
    never be confirmed to be received or cancelled since no confirmation comes from edge device (no emulators).
    NOTE: currently limited to location UNINIT_EMUL_QA_GS_CLIENT_Resource11 . Based on this it was probably used for
          tests without emulators.
    :return:
    """
    dbhelper = DbHelper.default_instance()
    sql = "delete from monitor_data_15_minute where monitor_id like 'UNINIT_EMUL_QA_GS_CLIENT_Resource11%';"
    dbhelper.query(DB_METER_LOGS, sql)
    sql = "delete from monitor_data_minute where monitor_id like 'UNINIT_EMUL_QA_GS_CLIENT_Resource11%';"
    dbhelper.query(DB_METER_LOGS, sql)
    sql = "delete from powerstore_bms_15_minute where component_id like 'UNINIT_EMUL_QA_GS_CLIENT_Resource11%';"
    dbhelper.query(DB_OPTIMIZATION, sql)
    sql = "delete from powerstore_bms_minute where component_id like 'UNINIT_EMUL_QA_GS_CLIENT_Resource11%';"
    dbhelper.query(DB_OPTIMIZATION, sql)

    # figure out the next 15min inteval in the future- set this to 'new_now'
    now = dt.datetime.now()
    seconds = (now - now.min).seconds
    rounding = (seconds + 900 / 2) // 900 * 900
    new_now = now + dt.timedelta(0, rounding - seconds, -now.microsecond)
    # if new_now.minute < now.minute:
    #    new_now = new_now + dt.timedelta(minutes=15)
    logger.debug('Using current time: %s and rounding it to: %s', now, new_now)

    # the import file has ~ 2hr15min of data and the first starttime is 2018-07-20 18:45:01
    # so, we need to take new_now (which is our next 15min interval) and subtract the 2hrs15min
    # to find the new starttime for data in the file
    file_start_time_dt = dt.datetime.strptime('2018-07-20 18:45:01', STR_FORMAT)
    start_time_dt = new_now
    diff = start_time_dt - file_start_time_dt
    days, seconds = diff.days, diff.seconds

    # monitor_data_15_minute.csv:  2018-07-20 18:45:01 -> 2018-07-20 21:00:00
    hours = (days * 24 + seconds // 3600) + 1
    s3_file = "s3://stemqa-sampledata/csv_import/meter_logs/realtime/monitor_data_15_minute" \
              "/monitor_data_15_minute_QA_GS_CLIENT_Resource11.csv"
    options = {
        "database": DB_METER_LOGS,
        "table": "monitor_data_15_minute",
        "transform": {
            "time_offset": hours,
            "time_offset_unit": 'hours'
        }
    }
    dataload_util.load_data(s3_file, options, False)

    # monitor_data_minute.csv:  2018-07-20 18:47:01 -> 2018-07-20 21:00:01
    minutes = days * 24 * 60 + seconds // 60
    s3_file = "s3://stemqa-sampledata/csv_import/meter_logs/realtime/monitor_data_minute" \
              "/monitor_data_minute_QA_GS_CLIENT_Resource11.csv"
    options = {
        "database": DB_METER_LOGS,
        "table": "monitor_data_minute",
        "transform": {
            "time_offset": minutes,
            "time_offset_unit": 'minutes'
        }
    }
    dataload_util.load_data(s3_file, options, False)

    # powerstore_bms_15_minute.csv:  2018-07-20 18:45:01 -> 2018-07-20 21:00:00
    s3_file = "s3://stemqa-sampledata/csv_import/optimization/realtime" \
              "/powerstore_bms_15_minute_QA_GS_CLIENT_Resource11.csv"
    options = {
        "database": DB_OPTIMIZATION,
        "table": "powerstore_bms_15_minute",
        "transform": {
            "time_offset": hours,
            "time_offset_unit": 'hours'
        }
    }
    dataload_util.load_data(s3_file, options, False)

    # powerstore_bms_minute.csv:  2018-07-20 18:47:01 -> 2018-07-20 21:00:01
    s3_file = "s3://stemqa-sampledata/csv_import/optimization/realtime" \
              "/powerstore_bms_minute_QA_GS_CLIENT_Resource11.csv"
    options = {
        "database": DB_OPTIMIZATION,
        "table": "powerstore_bms_minute",
        "transform": {
            "time_offset": minutes,
            "time_offset_unit": 'minutes'
        }
    }
    dataload_util.load_data(s3_file, options, False)


def cancel_active_events(program_id):
    sql = """\
    SELECT grid_service_event_id, deployment_start, deployment_end, status_id
    FROM grid_service_event_received
    WHERE program_id = '%s'
    """ % program_id
    dbhelper = DbHelper.default_instance()
    events_to_cancel = []
    for event in dbhelper.query(DB_GRID_SERVICES, sql):
        event_id = event['grid_service_event_id']
        status_id = event['status_id']
        if status_id in [1, 2, 3, 7]:
            events_to_cancel.append(event_id)

    if not events_to_cancel:
        return

    with GridResponseClient(settings['stem-web-app']['url']) as client:
        for event_id in events_to_cancel:
            # AG api wasn't work properly if reference_id and scheduler_id are the same
            client.cancel_dispatch_event(program_id, event_id)

        for event_id in events_to_cancel:
            data = client.event_list(program_id)
            cancelled_event = data['events'][str(event_id)]
            wait_start = dt.datetime.now()
            while cancelled_event['status'] not in ['CANCELLED', 'PENDING_CANCEL', 'STOPPED'] and \
                    dt.datetime.now() < wait_start + dt.timedelta(seconds=90):
                logger.debug("Event status: %s", cancelled_event)
                time.sleep(15)
                data = client.event_list(program_id)
                cancelled_event = data['events'][str(event_id)]


def gs_cancel_event(program_id, gridservice_event_id):
    with GridResponseClient(settings['stem-web-app']['url']) as client:
        return client.cancel_dispatch_event(program_id, gridservice_event_id)


def update_aggregation_program_id(aggregation_id, program_id):
    dbhelper = DbHelper.default_instance()
    sql = '''
        SET @ORIGINAL_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
        SET @ORIGINAL_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;

        UPDATE program_aggregation
        SET aggregation_id = {aggregation_id}
        WHERE program_id = {program_id};

        SET FOREIGN_KEY_CHECKS=@ORIGINAL_FOREIGN_KEY_CHECKS;
        SET UNIQUE_CHECKS=@ORIGINAL_UNIQUE_CHECKS;
        '''.format(aggregation_id=aggregation_id, program_id=program_id)

    dbhelper.query(DB_GRID_SERVICES, sql)


def import_15min_telemetry(resource, s3_file, emulator_rollup=True):
    base_monitor_id = get_base_monitor_id(resource, emulator_rollup)
    dbhelper = DbHelper.default_instance()
    sql = """DELETE FROM monitor_data_15_minute
             WHERE monitor_id like '{0}%';""".format(base_monitor_id)
    dbhelper.query(DB_METER_LOGS, sql)
    # import 20 days of historical 15min meter day
    file_start_time = '2018-08-01 04:00:01'
    file_start_datetime = dt.datetime.strptime(file_start_time, "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.UTC)
    N = 20
    today = dt.datetime.utcnow() - dt.timedelta(days=N)
    today_datetime = dt.datetime(today.year, today.month, today.day, today.hour,
                                 today.minute, today.second, tzinfo=pytz.utc)
    delta = today_datetime - file_start_datetime
    hours = int(delta.total_seconds() / 3600) + 2
    logger.debug('Number of hours diff %s', hours)
    monitor_id = 'emulator-21'
    new_monitor_id = base_monitor_id
    options = {
        "database": DB_METER_LOGS,
        "table": "monitor_data_15_minute",
        "transform": {
            "time_offset": hours,
            "time_offset_unit": 'hours',
            "monitor_id": monitor_id,
            "new_monitor_id": new_monitor_id
        }
    }
    dataload_util.load_data(s3_file, options, False)

    if not emulator_rollup:
        sql = """UPDATE monitor_data_15_minute
                 SET monitor_id = '{0}'
                 WHERE monitor_id = '{1}';""".format(base_monitor_id + '_M1', base_monitor_id + '-main')
        dbhelper.query(DB_METER_LOGS, sql)


def setup_emulated_resource(resource, default_load='high_and_volatile'):
    """
    Iterates through list of resources defined in test data and creates and starts emulators.
    Load, SOC and setpoint values are configurable based on testdata settings for every resource.
    """
    dbhelper = DbHelper.default_instance()
    # setup emulator container
    batterysoc = None  # if we configure with None value SOC will be 1(100%)
    setpoint = None  # if we configure with None value setpoint from forecast.configuration will be used
    if 'batterysoc' in resource:
        batterysoc = resource['batterysoc']
    if 'setpoint' in resource:
        setpoint = resource['setpoint']

    # Setting resource default loads based on default_load parameter
    if default_load == 'high_and_volatile':
        resource.setdefault('source_location', 'qa-QA_CLIENT_00_SAMPLE_001-1-M1')
        resource.setdefault('sample_start_date', '2017-01-11 18:30:00.5500')
        resource.setdefault('sample_end_date', '2017-01-11 21:29:59.6329')
    elif default_load == 'low':
        resource.setdefault('source_location', 'qa-QA_CLIENT_00_SAMPLE_001-1-M1')
        resource.setdefault('sample_start_date', None)
        resource.setdefault('sample_end_date', None)
    elif default_load == 'high':
        resource.setdefault('source_location', 'qa-QA_CLIENT_00_SAMPLE_001-1-M1')
        resource.setdefault('sample_start_date', '2018-01-27 08:00:00.0000')
        resource.setdefault('sample_end_date', '2018-01-27 09:00:00.5000')
    else:
        raise Exception('Unknown `default_load` value: %s' % default_load)

    source_location = resource.get('source_location', 'qa-QA_CLIENT_00_SAMPLE_001-1-M1')
    sample_start_date = resource.get('sample_start_date', '2017-01-11 18:30:00.5500')
    sample_end_date = resource.get('sample_end_date', '2017-01-11 21:29:59.6329')

    reset_emulator_db_entries(resource['emulator_hostname'], resource['target_location'])

    setup_emulator_container(emulator_name=resource['emulator_hostname'])

    # configure emulator to replay telemetry
    configure_simple_emulator(dbhelper,
                              target_location=resource['target_location'],
                              emulator_hostname=resource['emulator_hostname'],
                              source_location=source_location,
                              sample_start_date=sample_start_date,
                              sample_end_date=sample_end_date,
                              batterysoc=batterysoc,
                              setpoint=setpoint)
    # Importing telemetry based on resource import settings
    if resource.get('import'):
        s3_15min_file = resource['import']['s3_file']
        logger.debug('S3_file: %s', s3_15min_file)
        import_15min_telemetry(resource, s3_15min_file)


def get_base_monitor_id(resource, emulator_rollup):
    if resource.get('rollup', emulator_rollup):
        if resource.get('emulator_hostname'):
            base_monitor_id = resource['emulator_hostname']
        else:
            base_monitor_id = 'UNINIT_EMUL_' + resource['target_location'] + '_CLONE'
            resource['emulator_hostname'] = base_monitor_id
    else:
        base_monitor_id = 'UNINIT_EMUL_' + resource['target_location']
    return base_monitor_id


def cleanup_resources_without_emulators(resources, emulator_rollup):
    # TODO preventive check of powerstore table to check if emulator names match so we can clean it up better.
    # SELECT hostname FROM forecast.powerstore where hostname not like '%{LOCATION}%';
    for resource in resources:
        base_monitor_id = get_base_monitor_id(resource, emulator_rollup)
        if resource.get('rollup', emulator_rollup):
            reset_emulator_db_entries(base_monitor_id, resource['target_location'])
        else:
            dbhelper = DbHelper.default_instance()
            sql_to_execute = """
            SET @HOSTNAME = '%s';

            DELETE FROM meter_logs.monitor_data where monitor_id like CONCAT(@HOSTNAME,'%%');
            DELETE FROM meter_logs.monitor_data_minute where monitor_id like CONCAT(@HOSTNAME,'%%');
            DELETE FROM meter_logs.monitor_data_15_minute where monitor_id like CONCAT(@HOSTNAME,'%%');
            """
            sql = sql_to_execute % (base_monitor_id)
            dbhelper.query(None, sql)


def setup_resources_without_emulators(resources, emulator_rollup):
    cleanup_resources_without_emulators(resources, emulator_rollup)
    for resource in resources:
        base_monitor_id = get_base_monitor_id(resource, emulator_rollup)
        if resource.get('rollup', emulator_rollup):
            batterysoc = None  # if we configure with None value SOC will be 1(100%)
            setpoint = None  # if we configure with None value setpoint from forecast.configuration will be used
            if 'batterysoc' in resource:
                batterysoc = resource['batterysoc']
            if 'setpoint' in resource:
                setpoint = resource['setpoint']
            source_location = resource.get('source_location', 'qa-QA_CLIENT_00_SAMPLE_001-1-M1')
            sample_start_date = resource.get('sample_start_date', '2017-01-11 18:30:00.5500')
            sample_end_date = resource.get('sample_end_date', '2017-01-11 21:29:59.6329')
            emulator_hostname = base_monitor_id
            target_location = resource['target_location']
            with PowerConfigClient(settings['stem-web-app']['url']) as client:
                client.deploy_emulator_to_host(target_location, emulator_hostname,
                                               batterysoc=batterysoc, setpoint=setpoint)
                client.update_powerstore_configuration(sample_data_monitor_id=source_location,
                                                       powerstore_hostname=emulator_hostname,
                                                       sample_start_date=sample_start_date,
                                                       sample_end_date=sample_end_date,
                                                       real_time_speed=True)

        if resource.get('import'):
            s3_15min_file = resource['import']['s3_file']
            logger.debug('S3_file: %s', s3_15min_file)
            import_15min_telemetry(resource, s3_15min_file, resource.get('rollup', emulator_rollup))


def setup_athena_resources(testdata):
    if testdata.get('reuse_emulators'):
        logger.debug('SETUP RESOURCES: reusing emulators')
        # NOTE we could restart telemetry replay?
        # TODO check if emulators are running and run them if not?
        pass
    elif not testdata['emulator_rollup'] and not testdata['run_emulators']:
        logger.debug('SETUP RESOURCES: no emulator rollup and not running emulators.'
                     'Just resetting resource and importing telemetry if needed.')
        setup_resources_without_emulators(testdata['resources'], testdata['emulator_rollup'])
    elif testdata['emulator_rollup'] and not testdata['run_emulators']:
        # Emulator_rollup means that we configure resources as if we would run emulators.
        # This makes GS logic testable without emulators acutally running.
        # We just need to import needed 15min telemetry to make settlement job work.
        # In the future we should think of disabling mock baseline and just import what we want for
        # more flexible allocation testing.
        logger.debug('SETUP RESOURCES: setting emulators rollup but not running emulators')
        setup_resources_without_emulators(testdata['resources'], testdata['emulator_rollup'])
    elif testdata['run_emulators']:
        # We have some predefined site loads that we can use. Default is very volatile which is
        # useful for dnp3 telemetry signals checks. For allocation testing this is perhaps overly volatile.
        logger.debug('SETUP RESOURCES: running emulators')
        default_site_load = testdata.get('default_site_load', 'high_and_volatile')
        setup_athena_emulators(testdata, default_load=default_site_load)
    else:
        logger.debug('SETUP RESOURCES: just using default resource settings. No changes')


def setup_athena_emulators(testdata, default_load='high_and_volatile'):
    """
    configure the emulators container and start them
    :default_load: Helper flag that sets default telemetry source settings for all resources.
                   If we provide explicit source data.
            'high_and_volatile': load from 150kW to 300kW
            'low': load from 12kW to 17kW
            'high': load from 270kW to 280kW

            You can search for your own preferred load frame inside golden DB:

                SELECT monitor_id, sample_start_datetime, (w_1 + w_2 + w_3)
                FROM meter_logs.monitor_data
                WHERE monitor_id like '%qa-QA_CLIENT_00_SAMPLE_001-1-M1%'
                AND sample_start_datetime like '2017%'

    :retest_previous_run: Helper flag for retesting previous runs. Currently just prevents
                          emulator data deletion and emulator setup.
    :return:
    """
    resources = testdata['resources']
    if len(resources) > 1:
        # setup will be faster if we run each emulator setup in thread.
        threads = []
        for res in resources:
            th = ExcThread(target=setup_emulated_resource, args=[res, default_load],
                           name='setup-%s' % res['emulator_hostname'])
            threads.append(th)
            th.start()

        for thr in threads:
            thr.join(timeout=600)
            if thr.isAlive():
                raise Exception('Resource setup is taking more than 10 minutes')
    else:
        for res in resources:
            setup_emulated_resource(res, default_load)


def delete_athena_emulators(testdata, delete_db_entries=False):
    for resource in testdata['resources']:
        try:
            stop_and_delete_emulator(emulator_hostname=resource['emulator_hostname'],
                                     location_code=resource['target_location'],
                                     delete_db_entries=delete_db_entries)
        except Exception as e:  # pylint: disable=W0703
            tb = traceback.format_exc()
            logger.error('Emulator teardown failed: %s\n%s', str(e), tb)


def setup_aggregation_resources(testdata):
    program_id = fetch_program_id(testdata['program_name'])
    dbhelper = DbHelper.default_instance()
    sql = '''
        SELECT aggregation_id
        FROM program_aggregation
        WHERE program_id = {program_id};
        '''.format(program_id=program_id)

    data = dbhelper.query(DB_GRID_SERVICES, sql)
    assert len(data) == 1, "We are only testing for one aggregation per program!"
    aggregation_id = data[0]['aggregation_id']
    if testdata.get('aggregation_id'):
        assert aggregation_id == testdata.get('aggregation_id')

    sql = '''
        SELECT *
        FROM aggregation_resource as ar, resource as r
        WHERE ar.aggregation_id = {aggregation_id} AND
              ar.resource_id = r.resource_id;
        '''.format(aggregation_id=aggregation_id)

    data = dbhelper.query(DB_GRID_SERVICES, sql)
    logger.debug('AGGREGATION_DATA:\n%s', data)

    sql_delete = '''
        DELETE FROM aggregation_resource
        WHERE aggregation_id = %s AND
              resource_id = %s;
        '''
    resources = testdata['resources']
    for resource_i in data:
        location_in_testdata = 0
        for res in resources:
            if resource_i['location_code'] == res['target_location']:
                location_in_testdata = 1

        if not location_in_testdata:
            sql = sql_delete % (aggregation_id, resource_i['resource_id'])
        dbhelper.query(DB_GRID_SERVICES, sql)

    sql_select = '''
        SELECT * FROM resource as r
        WHERE r.location_code = '%s';
        '''
    sql_insert = '''
        INSERT INTO aggregation_resource
        (`aggregation_id`,
        `resource_id`,
        `active`,
        `available`,
        `allocation`,
        `allocation_capacity`,
        `reserve_allocation`,
        `reserve_allocation_capacity`,
        `restricted_allocation`,
        `restricted_allocation_capacity`)
        VALUES (%s, %s, 1, 1, 100.00, 100.00, 0.00, 100.00, 0.00, 100.00)
        '''

    for res in resources:
        location_in_testdata = 0
        resource_in_aggr = False
        for resource_i in data:
            if resource_i['location_code'] == res['target_location']:
                resource_in_aggr = True

        if resource_in_aggr:
            continue

        sql = sql_select % (res['target_location'])
        res_id = dbhelper.query(DB_GRID_SERVICES, sql)[0]['resource_id']
        sql = sql_insert % (aggregation_id, res_id)
        dbhelper.query(DB_GRID_SERVICES, sql)


def reset_aggregation_resources(testdata):
    program_id = fetch_program_id(testdata['program_name'])
    dbhelper = DbHelper.default_instance()
    sql = '''
        SELECT aggregation_id
        FROM program_aggregation
        WHERE program_id = {program_id};
        '''.format(program_id=program_id)

    data = dbhelper.query(DB_GRID_SERVICES, sql)
    assert len(data) == 1, "We are only testing for one aggregation per program!"
    aggregation_id = data[0]['aggregation_id']
    if testdata.get('aggregation_id'):
        assert aggregation_id == testdata.get('aggregation_id')

    # JEDI-14163 AG report don't support active flag for resources
    # sql_update = '''
    #    UPDATE aggregation_resource
    #    SET active = %s
    #    WHERE aggregation_id = %s
    #    '''
    # sql = sql_update % (1, aggregation_id)

    sql_delete = '''
        DELETE FROM aggregation_resource
        WHERE aggregation_id = %s
        '''
    sql = sql_delete % (aggregation_id)
    dbhelper.query(DB_GRID_SERVICES, sql)


def modify_gs_event(gs_event_id, event_end_dt, requested_kw):
    dbhelper = DbHelper.default_instance()
    sql = '''
        UPDATE grid_service_event_received
        SET deployment_end = "{event_end_dt}",
            requested_kw = {requested_kw}
        WHERE grid_service_event_id = {gs_event_id};
        '''.format(gs_event_id=gs_event_id, requested_kw=requested_kw,
                   event_end_dt=event_end_dt)
    dbhelper.query(DB_GRID_SERVICES, sql)


def fetch_redis_monitor_data(inverter_route_key, site_route_key):
    r = redis.Redis(host=settings['redis']['host'], port=settings['redis']['port-6379'])
    inverter_dict = r.hgetall('POWER_TELEMETRY:%s' % inverter_route_key)
    site_dict = r.hgetall('POWER_TELEMETRY:%s' % site_route_key)
    logger.debug('Redis values for inverter (%s): %s', inverter_route_key, inverter_dict)
    logger.debug('Redis values for site (%s): %s', site_route_key, site_dict)
    inverter_load_kw_total = float(inverter_dict[b'kw_1']) \
        + float(inverter_dict[b'kw_2']) + float(inverter_dict[b'kw_3'])
    site_load_kw_total = float(site_dict[b'kw_1']) + float(site_dict[b'kw_2']) + float(site_dict[b'kw_3'])
    return inverter_load_kw_total, site_load_kw_total


def get_route_keys(resource, emulator_rollup):
    base_monitor_id = get_base_monitor_id(resource, emulator_rollup)
    inverter_route_key = resource.get('inverter_route_key')
    if not inverter_route_key:
        inverter_route_key = '%s.vmonitor.inverter' % base_monitor_id
    site_route_key = resource.get('site_route_key')
    if not site_route_key:
        site_route_key = '%s.vmonitor.site' % base_monitor_id
    return inverter_route_key, site_route_key


def mock_resource_redis_monitor_data(inverter_route_key, site_route_key, inverter_value, site_value):
    inverter_d = {'pf_3': '', 'p_calc_1': '', 'pf_1': '', 'p_tot_calc': '', 'pf_2': '', 'phi_i_3': '',
                  'p_calc_3': '', 'phi_i_1': '', 'kw_derived_1': '', 'kw_derived_3': '',
                  'kw_derived_2': '', 'phi_v_1': '', 'phi_v_2': '', 'phi_v_3': '',
                  'msg_timestamp': '1540370904.412618', 'q_tot': '', 'i_2': '-1',
                  'kw_3': '0.0', 'kw_2': '0.0', 'kw_1': '0.0',
                  'p_calc_2': '', 'v_1': '-1', 'v_2': '-1', 'v_3': '-1',
                  'phi_i_2': '', 'msg_received_ts': '1540370907.534425',
                  'i_3': '-1', 'p_tot': '', 'i_1': '-1',
                  's_tot': '', 'f': '60', 'pf_tot': ''}
    site_d = {'pf_3': '', 'p_calc_1': '', 'pf_1': '', 'p_tot_calc': '',
              'pf_2': '', 'phi_i_3': '', 'p_calc_3': '', 'phi_i_1': '',
              'kw_derived_1': '', 'kw_derived_3': '', 'kw_derived_2': '',
              'phi_v_1': '', 'phi_v_2': '', 'phi_v_3': '', 'msg_timestamp': '1540370904.412618',
              'q_tot': '', 'i_2': '310.248199',
              'kw_3': '84.521202', 'kw_2': '84.521202', 'kw_1': '84.521202',
              'p_calc_2': '', 'v_1': '460.814392', 'v_2': '463.481812', 'v_3': '462.769104',
              'phi_i_2': '', 'msg_received_ts': '1540370907.526599', 'i_3': '323.995697',
              'p_tot': '', 'i_1': '339.061188', 's_tot': '', 'f': '', 'pf_tot': ''}
    r = redis.Redis(host=settings['redis']['host'], port=settings['redis']['port-6379'])
    # msg_ts = '1540370904.412618'
    msg_ts = (dt.datetime.utcnow() - dt.datetime(1970, 1, 1)).total_seconds()
    inverter_d.update({"kw_1": inverter_value / 3.0,
                       "kw_2": inverter_value / 3.0,
                       "kw_3": inverter_value / 3.0,
                       "msg_timestamp": msg_ts,
                       "msg_received_ts": msg_ts})
    site_d.update({"kw_1": site_value / 3.0,
                   "kw_2": site_value / 3.0,
                   "kw_3": site_value / 3.0,
                   "msg_timestamp": msg_ts,
                   "msg_received_ts": msg_ts})
    r.hmset('POWER_TELEMETRY:%s' % inverter_route_key, inverter_d)
    r.hmset('POWER_TELEMETRY:%s' % site_route_key, site_d)


def mock_redis_monitor_data(resources, emulator_rollup, mocked_redis_data):
    """ This functions enables us to simulate data needed for reporting telemetry

        We are only mocking 'kw_x' and 'msg_timestamp' values for now,
        but complete value looks like this:
    """
    for resource in resources:
        res_mock = mocked_redis_data[resource['target_location']]
        inverter_value = res_mock['inverter']
        site_value = res_mock['site']
        inverter_route_key, site_route_key = get_route_keys(resource, emulator_rollup)
        mock_resource_redis_monitor_data(inverter_route_key, site_route_key, inverter_value, site_value)


def get_aggregation_redis_data(resources, emulator_rollup):
    resource_aggregation = {"site": 0, "inverter": 0}
    for resource in resources:
        inverter_route_key, site_route_key = get_route_keys(resource, emulator_rollup)
        inverter_load, site_load = fetch_redis_monitor_data(inverter_route_key, site_route_key)
        logger.debug('Redis total values for location: %s', resource['target_location'])
        logger.debug(' - inverter values: %s', inverter_load)
        logger.debug(' - site values: %s', site_load)
        resource_aggregation['site'] += site_load
        resource_aggregation['inverter'] += inverter_load
    resource_aggregation['main'] = resource_aggregation['site'] + resource_aggregation['inverter']
    resource_aggregation['inv_neg'] = - resource_aggregation['inverter']
    return resource_aggregation


def get_program_reference_id(program_id):
    """ Helper function that provides us reference_id used for event creation.
    """
    dbhelper = DbHelper.default_instance()
    sql = "SELECT reference_id FROM program WHERE program_id= '%s';" % program_id
    return dbhelper.query(DB_GRID_SERVICES, sql)[0]['reference_id']


LAST_GS_EVENT_SQL = """\
SELECT *
FROM grid_service_event_received
WHERE program_id = '%s'
ORDER BY grid_service_event_id desc
LIMIT 1;
"""


def get_last_gs_event(program_id):
    """ Provides us last event data for specified program id
    """
    dbhelper = DbHelper.default_instance()
    last_event_sql = LAST_GS_EVENT_SQL % program_id
    data = dbhelper.query(DB_GRID_SERVICES, last_event_sql)
    if data:
        return data[0]
    return {}


GS_EVENT_SQL = """\
SELECT *
FROM grid_service_event_received
WHERE grid_service_event_id = %s;
"""


def get_gs_event(gs_event_id):
    dbhelper = DbHelper.default_instance()
    sql = GS_EVENT_SQL % gs_event_id
    res = dbhelper.query(DB_GRID_SERVICES, sql)[0]
    return res


# NOTE added by Sikun to better match predicted start time of LCR events (based on AG code)
def round_to_resolution(now, resolution, factor=0.2):
    """ Ex. with resolution=5, forgiveness_factor=0.2
        now = 12:00:59 round_time = 12:00:00
        now = 12:01:00 round_time = 12:05:00

        Ex. with resolution=30, forgiveness_factor=0.2
        now = 12:05:59 round_time = 12:00:00
        now = 12:06:00 round_time = 12:30:00
    """
    temp = now + dt.timedelta(seconds=int(round(resolution * (1 - factor) * 60)))
    add = 60 + temp.minute // resolution * resolution if (temp.hour - now.hour) % 24 == 1 \
        else temp.minute // resolution * resolution
    return now.replace(minute=0, second=0, microsecond=0) + dt.timedelta(minutes=add)

import datetime as dt
import time
import traceback
import logging
from functools import wraps

import pause
import pytz
from dateutil import tz

from stemqa.helpers.db_helper import DbHelper
from stemqa.util.grid_response_util import get_battery_output, calc_diffs
from stemqa.util.grid_response_util import get_monitor_ids_and_location_codes
from stemqa.util.grid_response_util import get_resource_dispatch_transaction_data
from stemqa.util.shell_util import ShellUtilError
from stemqa.vpp.athenagateway.api_util import AgApiClient
from stemqa.vpp.athenagateway.athenautil import update_aggregation_program_id, cancel_active_events, \
    setup_athena_emulators, setup_aggregation_resources, reset_aggregation_resources, \
    delete_athena_emulators, setup_athena_resources, cleanup_resources_without_emulators
from stemqa.vpp.athenagateway.athenautil import update_program_attributes
from stemqa.vpp.athenagateway.event_verifications import calc_dispatch_average
from stemqa.vpp.athenagateway.event_verifications import verify_event_stop
from stemqa.vpp.athenagateway.event_verifications import verify_finalizer
from stemqa.vpp.athenagateway.event_verifications import verify_settlement
from stemqa.vpp.athenagateway.event_verifications import verify_event_cancellation
from stemqa.vpp.athenagateway.event_verifications import check_event_performance
from stemqa.vpp.helper import round_time_up, json_dumps, get_program_info, fetch_program_id, STR_FORMAT
from stemqa.vpp.helper import ExcProcess, ExcThread
from stemqa.vpp.telemetry import telemetry_and_dispatch_data
# TODO move event_cleanup to vpp generic modules
from stemqa.vpp.openadr.event import event_cleanup

logger = logging.getLogger(__name__)

# Event stages that can be used to add testing at particular event state
# For now they are used to test dnp3 signal values at varius event stages
BEFORE_EVENT_CREATION = 'before_event_creation'
AFTER_EVENT_CREATION = 'after_event_creation'
DURING_DCM_INHIBIT = 'during_dcm_inhibit'
DURING_EVENT = 'during_event'
DURING_EVENT_STOP = 'during_event_stop'
DURING_EVENT_CANCEL = 'during_event_cancel'
DURING_CHARGE_INHIBIT = 'during_charge_inhibit'
AFTER_EVENT_ENDS = 'after_event_ends'

ag_client = AgApiClient()


def event_scenario_calculations(testdata, event_data, retest_previous=False):
    """ Fills provided event_data dict with information needed for
        event scenarion execution and verification
    """
    event_scenario = testdata['event']
    program_attributes = testdata['program_attributes']
    stop_event = event_scenario.get('stop')
    cancel_event = event_scenario.get('cancel')
    modify_event = event_scenario.get('modify')
    event_data['charge_inhibit'] = program_attributes['charge_inhibit_window']
    event_data['settlement_buffer'] = program_attributes['settlement_job_scheduling_buffer']
    event_data['start_buffer'] = program_attributes['dispatch_scheduling_buffer']
    event_data['dispatch_resolution'] = program_attributes['dispatchable_length_resolution']
    event_data['minimal_duration'] = program_attributes['minimum_event_length']
    if event_scenario.get('duration'):  # Used by fastdr event scenarios
        event_data['initial_duration'] = event_scenario['duration']
    else:
        # DNP3 outstation calls AG without deployment times. It uses program attributes
        # and current time to calculate start and stop.
        event_data['initial_duration'] = program_attributes['maximum_event_length']

    event_start_dt = event_data['start_dt']
    event_end_dt = event_start_dt + dt.timedelta(seconds=event_data['duration_seconds'])
    if not retest_previous:
        assert event_end_dt == event_data['initial_end_dt']
    event_data['end_dt'] = event_end_dt

    if modify_event:
        # TODO by default this should be a list of modifications
        # TODO if retesting... we should expect initial_event_end_dt to be the same as last modification or when stopped
        event_data['duration_after_modify'] = modify_event['event_duration']
        event_data['end_after_modify_dt'] = event_start_dt + dt.timedelta(seconds=event_data['duration_after_modify'])
        event_data['modify_at_dt'] = event_start_dt + dt.timedelta(seconds=modify_event['at'])
        event_data['modify_requested_kw'] = modify_event['requested_kw']

    if stop_event:
        assert stop_event['after'] % 60 == 0
        event_data['stop_at_dt'] = event_start_dt + dt.timedelta(seconds=stop_event['after'])
        if stop_event.get('expected_event_duration'):
            event_data['duration_seconds'] = stop_event['expected_event_duration']
            # expected event_end based on expected_event_duration (correct also if we mix modify and stop)
            event_data['end_dt'] = event_start_dt + dt.timedelta(seconds=event_data['duration_seconds'])
        else:
            minimal_end_dt = event_start_dt + dt.timedelta(seconds=event_data['minimal_duration'])
            dispatch_round_up_dt = round_time_up(event_data['stop_at_dt'] + dt.timedelta(seconds=1),
                                                 minutes=event_data['dispatch_resolution'])
            # Calculating expected event_end based on program_attributes and time of when stop will happen
            event_data['end_dt'] = max(dispatch_round_up_dt, minimal_end_dt)
            event_data['duration_seconds'] = (event_data['end_dt'] - event_start_dt).total_seconds()

        if retest_previous:
            assert event_data['end_dt'] == event_data['initial_end_dt']

    if cancel_event:
        assert cancel_event['after'] % 60 == 0
        event_data['cancel_at_dt'] = event_start_dt + dt.timedelta(seconds=cancel_event['after'])
        event_data['end_dt'] = event_data['cancel_at_dt']
        if event_start_dt < event_data['cancel_at_dt']:
            event_data['duration_seconds'] = (event_data['cancel_at_dt'] - event_start_dt).total_seconds()
        else:
            event_data['duration_seconds'] = 0

    return event_data


def dispatch_verification(testdata, event_data, resource_dispatches, errors):
    try:
        event_scenario = testdata['event']
        stop_event = event_scenario.get('stop')
        event_verification = testdata['event'].get('verification')
        if event_verification:
            # checking dispatches if event_verification checks are provided in testdata.
            # only in use for modify test case for now but we could reuse it for other cases.
            rel_tol = event_verification.get('rel_tol', 0.1)
            abs_tol = event_verification.get('abs_tol', 2)
            expected = event_verification['event_dispatches']
            query_data_event = telemetry_and_dispatch_data(testdata['resources'],
                                                           start_dt=event_data['start_dt'],
                                                           end_dt=event_data['end_dt'], log=True)
            actual_dispatches = [row['requested_power'] for row in query_data_event]
            logger.debug('Comparing actual dispatches with expected')
            logger.debug('Actual  : %s', actual_dispatches)
            logger.debug('Expected: %s', expected)
            for idx, disp in enumerate(actual_dispatches):
                expected_disp = expected[idx]
                logger.debug('Comparing actual: %s with expected: %s', disp, expected_disp)
                assert disp - 0.1 < 0  # There shouldn't be any charging during event
                diff = abs(expected_disp) - abs(disp)
                assert diff < rel_tol or diff < abs_tol

        # JEDI-14258  - we cannot test this part with stop scenarios
        # if not stop_event:
        rel_tol = event_scenario.get('rel_tol', 0.5)
        assert_diff = event_scenario.get('fail_on_average_diff', False)
        calc_dispatch_average(resource_dispatches, testdata, event_data['duration_seconds'],
                              rel_tol, dispatch_resolution=event_data['dispatch_resolution'],
                              stop_event=stop_event, assert_diff=assert_diff)
    except AssertionError:
        if not isinstance(errors, list):
            raise
        errors.append(traceback.format_exc())


def charge_inhibit_verification(testdata, event_data, errors):
    try:
        charge_inhibit_end_dt = event_data['end_dt'] + dt.timedelta(seconds=event_data['charge_inhibit'])
        query_data_inhibit = telemetry_and_dispatch_data(testdata['resources'],
                                                         start_dt=event_data['end_dt'],
                                                         end_dt=charge_inhibit_end_dt, log=True)
        inhibit_dispatches = [row['requested_power'] for row in query_data_inhibit]
        logger.debug('Comparing actual dispatches with expected:')
        assert len(inhibit_dispatches) == (len(testdata['resources']) * event_data['charge_inhibit'] // 60)
        assert not any(inhibit_dispatches)
    except AssertionError:
        if not isinstance(errors, list):
            raise
        errors.append(traceback.format_exc())


def check_deletion_of_old_dispatches(testdata, event_data, errors):
    try:
        charge_inhibit_end_dt = event_data['end_dt'] + dt.timedelta(seconds=event_data['charge_inhibit'])
        query_data = telemetry_and_dispatch_data(testdata['resources'], log=False)
        # Making sure any old dispatches arent't still present on battery after event charge inhbit is complete
        row = query_data[-1]
        logger.debug('Checking that old dipatches are not applied on battery after event ends:\n%s', row)
        row_start_time = pytz.utc.localize(row['sample_start_datetime'])  # pylint: disable=E1126
        if row_start_time > charge_inhibit_end_dt:
            old_dispatch = float(row['requested_power'])  # pylint: disable=E1126
            inverter = float(row['mdm.kw_total_sum/mdm.sample_count'])  # pylint: disable=E1126
            assert inverter >= 0 or (abs(old_dispatch) * 0.15) > abs(inverter)
    except AssertionError:
        if not isinstance(errors, list):
            raise
        errors.append(traceback.format_exc())


def verifications_after_event_ends(testdata, event_data, errors, retest_previous=False):
    # test config and calculations needed for test scenario verifications
    dbhelper = DbHelper.default_instance()
    event_scenario = testdata['event']
    stop_event = event_scenario.get('stop')
    cancel_event = event_scenario.get('cancel')
    modify_event = event_scenario.get('modify')
    runs_emulators = testdata.get('run_emulators', True)
    program_attributes = testdata['program_attributes']
    dispatch_interval_seconds = program_attributes['dispatchable_length_resolution'] * 60
    event_duration = event_data['duration_seconds']  # Calculated duration based on scenario (stop and cancellation)
    initial_duration = event_data['initial_duration']
    gs_event_id = event_data['gs_event_id']

    if cancel_event:
        verify_event_cancellation(gs_event_id, initial_duration, dispatch_interval_seconds, cancel_event['after'])
        return

    # TODO add verifications for modify and modify + stop combination
    if stop_event and not modify_event:
        try:
            verify_event_stop(gs_event_id, initial_duration, event_duration,
                              stop_event['after'], event_data['dispatch_resolution'], after_event_ended=True)
        except AssertionError:
            errors.append(traceback.format_exc())

    # Collecting telemetry and dispatches
    # Get the 1 minute interval output for all the monitors
    location_codes_list, monitor_id_list = get_monitor_ids_and_location_codes(testdata)
    if runs_emulators:
        logger.debug('Getting 1min meter interval data for %s to %s',
                     event_data['start_dt'].strftime(STR_FORMAT),
                     event_data['end_dt'].strftime(STR_FORMAT))
        # TODO get battery data with our telemetry functions and get rid of gridservice code relieance
        battery_output = get_battery_output(dbhelper, event_data['start_dt'], event_data['end_dt'],
                                            monitor_id_list, location_codes_list, STR_FORMAT)
        try:
            assert len(battery_output) == (event_duration // 60) * len(testdata['resources']), \
                'Expected num intervals to be event_duration, actual num of intervals %s' % len(battery_output)
        except AssertionError:
            errors.append(traceback.format_exc())

    # Get the gs event dispatches
    resource_dispatches = get_resource_dispatch_transaction_data(dbhelper, gs_event_id,
                                                                 location_codes_list, STR_FORMAT)

    # Comparing battery to dispatch values for full event scenario
    if runs_emulators and not stop_event and not modify_event:
        # compare the 1 minutue interval output to the gs dispatch amounts requested
        try:
            testdata['requested_capacity_kw'] = event_data['requested_kw']
            calc_diffs(battery_output, resource_dispatches, STR_FORMAT, testdata, rel_tol=0.15)
        except AssertionError:
            errors.append(traceback.format_exc())

    # Finalizer event queue check
    try:
        verify_finalizer(gs_event_id, dispatch_interval_seconds, event_duration, stop_event)
        logger.debug("passed 'verify_finalizer' ")
    except AssertionError:
        errors.append(traceback.format_exc())

    # Verification of settlement
    settlement_job_scheduling_buffer = program_attributes['settlement_job_scheduling_buffer']
    settlement_dt = event_data['end_dt'] + dt.timedelta(seconds=settlement_job_scheduling_buffer)
    settlement_time = settlement_dt.astimezone(tz.tzlocal())
    logger.debug('*** Waiting until settlement_time: %s', settlement_time)
    pause.until(settlement_time)
    try:
        verify_settlement(gs_event_id)
        check_event_performance(gs_event_id)
    except AssertionError:
        errors.append(traceback.format_exc())

    # Dispatch and charge inhibit verification
    if runs_emulators:
        if not retest_previous:
            # Waiting 2min for latest telemetry (1min tables in DB get updated every 2min)
            logger.debug('Waiting 2 minutes to get all telemetry info after charge inhibit ends.')
            time.sleep(120)
        dispatch_verification(testdata, event_data, resource_dispatches, errors)
        charge_inhibit_verification(testdata, event_data, errors)
        check_deletion_of_old_dispatches(testdata, event_data, errors)


def add_missing_test_setup_data(testdata, reuse_emulators):
    logger.debug("Test case: %s", testdata['id'])
    program_id = testdata.get('program_id', fetch_program_id(testdata['program_name']))
    testdata['program_id'] = program_id
    logger.debug('Program: %s (id=%s)', testdata['program_name'], program_id)
    testdata.setdefault('run_emulators', True)
    testdata.setdefault('emulator_rollup', True)
    testdata.setdefault('reuse_emulators', False)
    testdata.setdefault('program_attributes', {})
    if reuse_emulators:
        testdata['reuse_emulators'] = True
    if testdata['run_emulators'] or testdata['emulator_rollup']:
        assert testdata.get('resources')
    else:
        testdata.setdefault('resources', [])


def apply_testdata_program_config(testdata, retest_previous, display_program_info=True):
    program_id = testdata.get('program_id', fetch_program_id(testdata['program_name']))
    testdata['program_id'] = program_id
    if testdata.get('aggregation_id'):
        update_aggregation_program_id(testdata.get('aggregation_id'), program_id)

    # Updating program_attributes with program_attributes_override if they exist.
    # Useful when rusing complex program attributes
    if testdata.get('program_attributes_override'):
        testdata['program_attributes'].update(testdata['program_attributes_override'])
    logger.debug('Attributes before populating with defaults: %s', json_dumps(testdata['program_attributes']))
    default_attributes = testdata['default_program_attributes']
    testdata['custom_program_attributes'] = testdata['program_attributes']
    updated_attributes = update_program_attributes(program_id, testdata['program_attributes'], default_attributes,
                                                   retest_previous_run=retest_previous)
    testdata['program_attributes'] = updated_attributes
    logger.debug('Attributes after populating with defaults: %s', json_dumps(updated_attributes))
    setup_aggregation_resources(testdata)
    program_info = get_program_info(testdata['program_name'])
    testdata['program_info'] = program_info
    if display_program_info:
        logger.debug('Program info:\n%s', json_dumps(program_info))


def generic_gs_testing_event_info(testdata):
    logger.debug('\n\n%s %s %s\n\n', 60 * '=', 'TEST EVENT INFO', 60 * '=')
    event_data = testdata.get('event_data', {})
    if event_data.get('start_dt'):
        logger.debug('event_data %s', event_data)
        charge_inhibit_end_dt = event_data['end_dt'] + dt.timedelta(seconds=event_data['charge_inhibit'])
        settlement_end_dt = event_data['end_dt'] + dt.timedelta(seconds=event_data['settlement_buffer'])
        logger.debug('event_start: %s, event_end: %s, charge_inhibit_end: %s, settlement_end: %s',
                     event_data['start_dt'].strftime(STR_FORMAT),
                     event_data['end_dt'].strftime(STR_FORMAT),
                     charge_inhibit_end_dt.strftime(STR_FORMAT),
                     settlement_end_dt.strftime(STR_FORMAT))
    else:
        logger.warning('This test case has not event data to display')


def generic_gs_testing_telemetry_and_dispatch_info(testdata, all_dispatches=False, csv=True):
    logger.debug('\n\n%s %s %s\n\n', 60 * '=', 'TEST TELEMETRY AND DISPATCH INFO', 60 * '=')
    if testdata['resources'] and testdata['run_emulators']:
        logger.debug('\n\n%s %s %s\n\n', 60 * '=', 'AGGREGATION INFO', 60 * '=')
        logger.debug('\n\nAGGREGATION INFO:\n\n%s', json_dumps(testdata['program_info'].get('aggregations')))
        logger.debug('\n\n%s %s %s\n\n', 60 * '=', 'TELEMETRY AND DISPATCH INFO', 60 * '=')
        logger.debug('\n\nCSV OUTPUT:\n\n%s',
                     telemetry_and_dispatch_data(testdata['resources'], return_csv=csv))
        if all_dispatches:
            logger.debug('\n\n%s %s %s\n\n', 60 * '=', 'TELEMETRY AND DISPATCH INFO (ALL DISPATCHES)', 60 * '=')
            logger.debug('\n\nCSV OUTPUT (ALL DISPATCHES):\n\n%s',
                         telemetry_and_dispatch_data(testdata['resources'], last_dispatch_only=False, return_csv=csv))
    else:
        logger.warning("This test case isn't running emulators. We cannot provide any telemetry data")


def generic_gs_testing_test_cleanup(testdata, retest_previous=False):
    """ Collection of GS test cleanup based on testdata"""
    cleanup = testdata.get('cleanup', {})
    # explicit event cancelation request after test completes.
    if cleanup.get('cancel_event', True):
        logger.debug('*** Cleanup of event requested. Cancelling all events for this program.')
        cancel_active_events(testdata['program_id'])
    # resetting aggregation(removing resources) to make sure we dont affect other tests
    reset_aggregation_resources(testdata)
    # Auto cleaning emulator related stuff based on testdata settings
    if not retest_previous and not testdata['reuse_emulators']:
        # We always delete emulator DB entries before we start emulators, but deleting it after usage is safer
        # because we could be using different emulator name in other test cases (that use same target location).
        if testdata.get('run_emulators', True):
            delete_athena_emulators(testdata, delete_db_entries=True)
        elif testdata.get('emulator_rollup'):
            cleanup_resources_without_emulators(testdata['resources'], testdata['emulator_rollup'])


def reruns_possible(test_func):
    @wraps(test_func)
    def run_test_multiple_times(testdata, *args, reruns=0, reuse_emulators=False,
                                raise_exceptions=True, raise_external_command_error=False, **f_kwargs):
        """Simple function that reruns same test case multiple times.
        It collects all assertion errors (and other errors based on exception/error arguments)
        Testdata: test configuration dict where we can set `reruns` and `reuse_emulators` to enable
                  reruns and set if reruns should handle emulators setup. Also needed for resource settings
                  in case we have to handle emulator setup inside this func.
        f_args: list of args to be applied to test function as *f_args
        f_kwargs: dict of args to be applied to test function as **f_kwargs
        reruns: how many times we want to rerun this test function
        reuse_emulators: useful to shorten execution time on multiple short tests with emulators.
                         Warning: old emulators can stop working after 2 hours.
        raise_exception: should we raise and stop reruns or collect non Assertion error
        raise_external_command_error: should we raise or collect ShellUtilError. Default is to collect
                                      because of occasional emulator setup and cluster flakiness.
        """
        reruns = reruns or testdata.get("reruns", 0)
        f_args = (testdata,) + args
        if not reruns:
            return test_func(*f_args, **f_kwargs)

        if not reuse_emulators:
            reuse_emulators = testdata.get('reuse_emulators', False)
        else:
            testdata['reuse_emulators'] = reuse_emulators

        # Ugly hack if we want to support emulator support for parallel tests.
        if testdata.get('parallel'):
            all_resources = []
            for testd in testdata['parallel']:
                testd['reuse_emulators'] = reuse_emulators
                all_resources.extend(testd.get('resources', []))
            testdata['resources'] = all_resources

        logger.debug('Rerunning test  `%s`  %s times', testdata['id'], reruns)
        logger.debug('Reusing emulators: %s', reuse_emulators)
        logger.debug('Resources used: %s', testdata.get('resources', []))
        emulators_already_up = False
        fails = []
        try:
            for x in range(1, reruns + 1):
                try:
                    # NOTE setup_athena_resources could perhaps reuse_emulators in more smart fashion.
                    #      reuse_emulators would be useful also for normal development without reruns.
                    if reuse_emulators and not emulators_already_up:
                        load = testdata.get('load', 'high_and_volatile')
                        setup_athena_emulators(testdata, default_load=load)
                        emulators_already_up = True
                    test_func(*f_args, **f_kwargs)
                except AssertionError as e:
                    logger.debug('Run %s assertion errors:\n%s', x, e)
                    fails.append('\n%s\nRun #%s errors:\n%s' % (120 * '#', x, str(e)))
                except ShellUtilError as e:
                    logger.debug('Run %s has external command error: %s', x, e)
                    if raise_external_command_error:
                        raise
                    else:
                        delete_athena_emulators(testdata)
                        emulators_already_up = False
                        error_stack = traceback.format_exc()
                        fails.append('\n%s\nRun #%s errors:\n%s' % (120 * '#', x, error_stack))
                except Exception as e:  # pylint: disable=W0703
                    error_tb = traceback.format_exc()
                    logger.debug('\n' + '>' * 120 + '\n')
                    logger.debug('Run %s has non-assert error: %s\n%s', x, e, error_tb)
                    logger.debug('\n' + '<' * 120 + '\n')
                    if raise_exceptions:
                        raise
                    else:
                        fails.append('\n%s\nRun #%s errors:\n%s' % (120 * '#', x, error_tb))
                else:
                    logger.debug('\n' + '>' * 120 + '\n')
                    logger.debug('Run %s has no errors! w00t!', x)
                    logger.debug('\n' + '<' * 120 + '\n')
        finally:
            if testdata.get('reuse_emulators', False):
                delete_athena_emulators(testdata)
            no_fails = not bool(len(fails))
            logger.debug('\n\n')
            logger.debug("We had %s fails in %s test reruns", len(fails), reruns)
            logger.debug('\n' + '^' * 120 + '\n')
            assert no_fails, "\nCollection of multiple run errors:\n%s" % ('\n'.join(fails))

    return run_test_multiple_times


def parallel_testing_possible(test_setup_func=None):
    """ Retruns decorator that can run multiple tests in parallel if requested by testdata setup.
            test_setup_func: function that enables running part of test setup in sequential mode.
                             useful if we there are some timing constraints.
    """

    def decorator(test_func):
        @wraps(test_func)
        def run_multiple_tests_in_paralell(testdata, *args, **kwargs):
            parallel_tests = testdata.get("parallel", [])
            if not parallel_tests:
                fargs = [testdata] + list(args)
                if test_setup_func:
                    test_setup_func(*fargs, **kwargs)
                return test_func(*fargs, **kwargs)

            timeout = testdata['timeout']
            test_workers = []
            for td in parallel_tests:
                fargs = [td] + list(args)
                test_setup_func(*fargs, **kwargs)
                if testdata.get('threads'):
                    worker = ExcThread(target=test_func, name='WORKER-%s' % td['id'], daemon=True,
                                       args=fargs, kwargs=kwargs)
                else:
                    worker = ExcProcess(target=test_func, name='WORKER-%s' % td['id'], daemon=True,
                                        args=fargs, kwargs=kwargs)
                test_workers.append(worker)

            for worker in test_workers:
                worker.start()

            errors = []
            jobs_ended = []
            waiting = 0
            while waiting < timeout:
                still_working = False
                for worker in test_workers:
                    if worker in jobs_ended:
                        continue
                    try:
                        worker.join(timeout=10)
                    except Exception:
                        error_stack = traceback.format_exc()
                        errors.append('\n%s\n%s errors:\n%s' % (120 * '#', worker.name, error_stack))
                    if worker.is_alive():
                        still_working = True
                    else:
                        jobs_ended.append(worker)
                waiting += 10
                if not still_working:
                    break

            assert not errors, 'Collection of errors in test workers:\n%s' % "\n\n" + 90 * "#" + "\n\n".join(errors)

            for worker in test_workers:
                if worker.is_alive():
                    raise Exception('Test worker [%s] is taking more than %ss!!' % (worker.name, timeout))
            return

        return run_multiple_tests_in_paralell

    return decorator


def generic_gs_testing_setup(testdata, *args, reuse_emulators=False, retest_previous=False, **kwargs):
    # Applying program attributes and resources settings
    add_missing_test_setup_data(testdata, reuse_emulators)
    apply_testdata_program_config(testdata, retest_previous)

    # cleaning previous event data
    if testdata.get('event_cleanup', False) and not retest_previous:
        event_cleanup(testdata['program_id'])

    # Cancelling all active events for testing program
    if not retest_previous:
        cancel_active_events(testdata['program_id'])

    # TODO reuse emulators check should move into setup_athena_resources
    if not retest_previous and not testdata['reuse_emulators']:
        try:
            # Setting up resources based on test configuration
            setup_athena_resources(testdata)
        except Exception:
            if testdata.get('run_emulators', True):
                delete_athena_emulators(testdata, delete_db_entries=True)
            elif testdata.get('emulator_rollup'):
                cleanup_resources_without_emulators(testdata['resources'], testdata['emulator_rollup'])

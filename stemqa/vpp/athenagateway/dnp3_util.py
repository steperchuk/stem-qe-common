import datetime as dt
import json
import logging
import math
import time

import pause
import requests
from requests import HTTPError
from retry import retry

from stemqa.config.settings import settings
from stemqa.vpp.athenagateway.athenautil import get_aggregation_redis_data
from stemqa.vpp.athenagateway.athenautil import get_last_gs_event
from stemqa.vpp.helper import json_dumps, waiting

logger = logging.getLogger(__name__)
LOG_RESPONSE = False


class MasterApiCallFailed(Exception):
    pass


class MasterApiResponse(object):
    """
    Represents a response from master test harness endpoints.
    """

    def __init__(self, response):
        self.response = response

    def json(self):
        return self.response.json()

    def data(self):
        json_d = self.json()
        return json_d

    def content(self):
        return self.response.content

    def headers(self):
        return self.response.headers


class MasterApiResponseHook(object):
    """
    Hook invoked when we receive a response from Master simulator api. Used primarily for logging the response.
    """

    @staticmethod
    def log(response, *args, **kwargs):
        """
        Hook invoked when we receive a response from the Ag
        """
        if response.status_code != 200 or LOG_RESPONSE:
            logger.debug('args: %s, kwargs: %s', args, kwargs)
            logger.debug('status_code: %s', type(response.status_code))
            logger.debug('status_code: %s', response.status_code)
            logger.debug('url: %s', response.url)
            try:
                logger.debug('content: %s', json_dumps(response.json()))
            except ValueError:
                logger.debug("content: %s", response.text)


class MasterApiClient(object):
    """
    Wrapper for DNP3 master simulator.
    """

    def __init__(self, master_name, recreate=True, url=None, client=None):
        """ Creating DNP3 master with specific name.
            Settings used are currently hardcoded in `config.ini` and inside create_master function.
            It matches the settings needed for our outstation used for SCE.

            master_name: simulator provides api that enables multiple masters. This name is used as unique
                         ID for accessing and manipulating specfic master.
            recreate: forces recreation of master even if it already exists
            url: we can specify url to master simulator, but by default we already get correct values from settings.
        """
        self.master_name = master_name
        if client:
            self.url = client.url
        elif url:
            self.url = url
        else:
            self.url = '{}/'.format(settings['stem-dnp3-master-simulator']['url'])
        self.response_archive = []
        self.bo_cmd_url = self.url + self.master_name + '/bo/cmd'
        if not self.get_values(raise_exc=False):
            self.create_master()
        elif recreate:
            self.delete_master()
            self.create_master()

    def response_handling(self, response):
        self.response_archive.append(response)
        response.raise_for_status()
        return MasterApiResponse(response)

    def get(self, url, headers=None, data=None, params=None, stream=False):
        response = requests.get(url, headers=headers, data=data, params=params, stream=stream, verify=False,
                                hooks=dict(response=MasterApiResponseHook.log))
        return self.response_handling(response)

    def post(self, url, headers=None, data=None, params=None, files=None):
        response = requests.post(url, headers=headers, data=data, params=params, files=files, verify=False,
                                 hooks=dict(response=MasterApiResponseHook.log))
        return self.response_handling(response)

    def put(self, url, headers=None, data=None, params=None, files=None):
        response = requests.put(url, headers=headers, data=data, params=params, files=files, verify=False,
                                hooks=dict(response=MasterApiResponseHook.log))
        return self.response_handling(response)

    def patch(self, url, headers=None, data=None, params=None, files=None):
        response = requests.patch(url, headers=headers, data=data, params=params, files=files, verify=False,
                                  hooks=dict(response=MasterApiResponseHook.log))
        return self.response_handling(response)

    def delete(self, url, headers=None, data=None, params=None, files=None):
        response = requests.delete(url, headers=headers, data=data, params=params, files=files, verify=False,
                                   hooks=dict(response=MasterApiResponseHook.log))
        return self.response_handling(response)

    def create_master(self, outstation_settings=None):
        '''Creates master with master simulator api
           outstation_settings: settings dict used for connection values to outstation
        '''
        if not outstation_settings:
            outstation_settings = settings['stem-dnp3-outstation']
        logger.debug("Creating dnp3 master: %s", self.master_name)
        logger.debug("Url used for master creation: %s", self.url)
        data = {
            "name": self.master_name,
            "app-layer-settings": {
                "response-timeout-ms": 5000,
                "disable-unsolicited-on-startup": True,
                "task-retry-period-ms": 5000,
                "task-start-timeout-ms": 10000,

                "enable-unsolicited-class-1": False,
                "enable-unsolicited-class-2": False,
                "enable-unsolicited-class-3": False,

                "startup-integrity-class-0": True,
                "startup-integrity-class-1": False,
                "startup-integrity-class-2": False,
                "startup-integrity-class-3": False
            },
            "link-layer-settings": {
                "master-address": 0,
                "outstation-address": 153
            },
            "tcp-settings": {
                "host": outstation_settings['internal-host'],
                "port": int(outstation_settings['internal-port'])
            }
        }
        logger.debug("Data used for master creation is:\n%s", json_dumps(data))

        data_json = json.dumps(data)
        response = self.post(self.url, data=data_json)
        return response

    def delete_master(self):
        ''' Deletes master with master simulator api '''
        url = self.url + self.master_name
        logger.debug("Deleting dnp3 master: %s", self.master_name)
        logger.debug("Url for master deletion: %s", self.url)
        response = self.delete(url)
        return response

    def get_values(self, raise_exc=True, expect_values=True):
        ''' Gets all DNP3 signal values that master can provide '''
        url = self.url + self.master_name
        try:
            response = self.get(url)
        except HTTPError as e:
            logger.debug('get master %s error: %s', self.master_name, e)
            if raise_exc:
                raise
            return None
        data = response.data()
        if raise_exc and expect_values:
            assert data['ai'], "There seems to be no communication with outstation"
        return data

    def operate_latch(self, index, switch_on, program_id, timeout=30):
        '''Simulates manipulation of latches from master as if we were SCE
            index: selection of binary index we want to change.
                   currently usef to simualate event start and stop signals [4(SCEW) and 104(SCEC)]
            switch_on: True for 1 (event start) and False for 0 (event_stop)
            program_id: used for verification (making sure event was created)
            timeout: seconds acceptable for even creation
        '''
        previous_event = get_last_gs_event(program_id)
        code = 3 if switch_on else 4
        url = self.bo_cmd_url
        data = {
            "index": index,
            "mode": "select-and-operate",
            "code": code,
            "count": 1,
            "on-time": 100,
            "off-time": 100
        }
        logger.debug("Operating latch on master: %s", self.master_name)
        logger.debug("Url used latch manipulation: %s", url)
        logger.debug("Data used for latch manipulation:\n%s", json_dumps(data))
        data_json = json.dumps(data)
        response = self.post(url, data=data_json)
        logger.debug("Latch operation response:\n%s", response)

        wait = 0
        event_changed = False
        while wait < timeout and not event_changed:
            time.sleep(3)
            wait += 3
            last_event = get_last_gs_event(program_id)
            logger.debug('last_event: %s', last_event.get('grid_service_event_id'))
            if switch_on and last_event.get('grid_service_event_id') != previous_event.get('grid_service_event_id'):
                event_changed = True
            elif not switch_on and last_event.get('update_date') != previous_event.get('update_date'):
                event_changed = True
        if wait >= timeout:
            raise MasterApiCallFailed('Event was not created or updated.')
        return response


def remap_dnp3_values(values):
    """ Remaps DNP3 master values response into more usable form for verification
        {"bi": {
                  "1" : 1
                  "2" : 0
          }
          "bo": {
                  "1" : 0
                  "2" : 1
        } }  ->  {"bi_1":1,"bi_2":0,"bo_1":0,"bo_2":1}
    """
    remapped = {}
    for signal_group, signal_list in list(values.items()):
        for signal in signal_list:
            remapped[signal_group + '_' + str(signal['id'])] = signal['value']
    return remapped


def isclose(a, b, rel_tol=1e-03, abs_tol=0.001):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def check_dnp3_values(master_client, resources, expected, after=0, value_interval=4, stage_retries=3,
                      display_dnp3_values=True, fail_on_comparison=True, round_to_minute=False,
                      emulator_rollup=True, bias_load=None):
    """ Specialized function that compares redis telemetry values with expected values.
        master_client: used to get all DNP3 signal
        resources: list of resources that we want to check (emulators need to run or values need to be mocked)
        expected: signal values that we expect (they can have templated telemetry values)
    """
    waiting(after, "Making sure we get dnp3 values at specific time.", round_to_minute)

    @retry(tries=stage_retries + 1, delay=0)
    def fetch_values_and_verify():
        redis_values = []
        logger.debug('Collecting %s seconds of redis telemetry', value_interval)
        now = dt.datetime.utcnow()
        for x in range(0, value_interval):
            aggr_redis_data = get_aggregation_redis_data(resources, emulator_rollup)
            logger.debug('[%s] Redis aggregation data before dnp3 checks: %s', x, aggr_redis_data)
            redis_values.append(aggr_redis_data)
            pause.until(now + dt.timedelta(seconds=x))
        values = remap_dnp3_values(master_client.get_values())
        # collecting redis data after getting dnp3 data points (for even better average)
        aggr_redis_data = get_aggregation_redis_data(resources, emulator_rollup)
        logger.debug('[%s] Redis aggregation data after getting dnp3 report: %s', x, aggr_redis_data)
        redis_values.append(aggr_redis_data)

        if display_dnp3_values:
            logger.debug('All dnp3 values remapped:\n%s', json_dumps(values))
            logger.debug('Expected values:\n%s', json_dumps(expected))

        if expected:
            bias_load_value = 0
            if expected.get('AI_103', None) is not None:
                bias_load_value = values['ai_103']
            elif expected.get('AI_3', None) is not None:
                bias_load_value = values['ai_3']

            for idx, data in enumerate(redis_values):
                data['bias_load'] = bias_load[idx] if bias_load else 0
                if bias_load_value:
                    data['pseudo_gen'] = (bias_load_value * 10) - data['main']
                else:
                    data['pseudo_gen'] = 0

            for k, v in list(expected.items()):
                lk = k.lower()
                if lk.startswith('ai') and isinstance(v, str):
                    s = v
                    v = [math.trunc(redis_data[s] * 0.1) for redis_data in redis_values]
                    logger.debug('Symbol `%s` replaced with its current values for %s: %s', s, value_interval, v)
                logger.debug('Comparing [%s]: actual ( %s ) with expected ( %s )', k, values[lk], v)
                if not fail_on_comparison:
                    pass
                elif lk.startswith('ai'):
                    if lk in ('ai_0', 'ai_1', 'ai_9', 'ai_100', 'ai_101', 'ai_109'):
                        assert isclose(values[lk], v)
                    elif isinstance(v, list):
                        assert any([isclose(values[lk], lv, 0.05, 1) for lv in v])
                    else:
                        assert isclose(values[lk], v, 0.015, 0.002)
                elif lk.startswith('ao'):
                    assert isclose(values[lk], v)
                else:
                    assert values[lk] == v

    fetch_values_and_verify()

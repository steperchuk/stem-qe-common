"""
Manage the rabbitmq mapping between queues, bindings and exchanges
"""

import json
import logging
import threading
import requests

logger = logging.getLogger(__name__)


class RmqData(object):
    """
    Manage the rabbitmq mapping between queues, bindings and exchanges
    """

    def __init__(self, username, password, host, admin_port):
        """
        :param username: User name
        :param password: User password
        :param host:
        :param admin_port:
        """
        self._username = username
        self._password = password
        self._admin_url = 'http://%s:%s' % (host, admin_port)

        # Instance level lock
        self._lock = threading.Lock()
        # RLock is a class level lock shared by all instances
        self._rlock = threading.RLock()

    @staticmethod
    def gen_qmaps_by_exchangetype(exchange_types, qmaps_info):
        """
        Retrieve the qmaps as generator expression based on exchange as a filter
        :param exchange_types: Supported execution type are direct, fanout, topic and header
        :param qmaps_info: Info about the queues, exchange and binding
        :return: Returns generator expression for qmaps filtered by exchange_types
        """
        return (qmap
                for exchange_type in exchange_types
                for qmap in qmaps_info[exchange_type])

    @staticmethod
    def gen_qmaps_by_exchangetype_queue(exchange_types, queues, qmaps_info):
        """
        Retrieve the qmaps as generator expression based on exchange and queue as a filter
        :param exchange_types: Supported execution type are direct, fanout, topic and header
        :param qmaps_info: Info about the queues, exchange and binding
        :param queues: Specified message queues
        :return: Returns generator expression for qmaps filtered by exchange_types and queue
        """
        qmaps = RmqData.gen_qmaps_by_exchangetype(exchange_types, qmaps_info)
        return (qmap for qmap in qmaps if qmap['queue'] in queues)

    def get_data(self, url):
        """
        Retrieve data from rabbitmq
        :param url: url to the rabbitmq and admin
        :return: Return data retrived from requests
        """
        response = requests.get(url, auth=(self._username, self._password))
        response.raise_for_status()
        data = json.loads(response.text)
        return data

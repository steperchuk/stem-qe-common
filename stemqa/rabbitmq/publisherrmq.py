"""
Publish rabbitmq messages

"""

import json
import logging

from kombu import Connection, Producer

logger = logging.getLogger(__name__)


class PublisherRmq(object):
    """
    Publish rabbitmq messages
    """

    def __init__(self, input_args):
        """
        Set up logging and initialize local variables needed to connect to rabbitmq
        :param input_args: Input argument is a dictionary
        """
        self._host = input_args['host']
        self._username = input_args['username']
        self._password = input_args['password']
        self._amqp_port = input_args['rabbit-port']
        self._amqp_url = 'amqp://%s:%s@%s:%s//' % (self._username,
                                                   self._password,
                                                   self._host,
                                                   self._amqp_port)

    def __repr__(self):
        return str(list(self.__dict__.keys()))

    def publish_message(self, exchange, routing_key, payload):
        """
        Publish message based on the exchange and routing key
        User should use this method call in normal cases
        :param exchange: Name of exchange
        :param routing_key: Routing key or routing pattern
        :param payload: Message payload
        :return: Return 0 if success else 1 if failed
        """

        # In order to guarantee publish, two safety features are used
        # confirm_publish is enabled so that client will wait for ack from broker
        # mandatory is set to True so that if there is no route from the exchange to a destination,
        #  message is returned to producer.

        # noinspection PyUnusedLocal
        def returned_message(err, exch, rtg_key, msg):
            raise Exception('No destination for message published on [{}] with routing key [{}]'.format(exch, rtg_key))

        with Connection(self._amqp_url, transport_options={'confirm_publish': True}) as connection:
            producer = Producer(connection.channel(), on_return=returned_message)
            producer.publish(json.dumps(payload),
                             exchange=exchange,
                             routing_key=routing_key,
                             mandatory=True)
            logger.debug("Published on [{}] msg [{}]".format(routing_key, json.dumps(payload, indent=2)))

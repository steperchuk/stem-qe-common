import json
import logging

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class TariffMaskDocumentClient(StemClient):
    """
    Client for interacting with tariff mask document api
    see https://stemedge.atlassian.net/wiki/spaces/TRF/pages/341082829/Tariff+Manager+APIs#TariffManagerAPIs-RestAPIs
    """

    def get_tariff_document(self, tariff_id, app_location_code, module, json_format='UI'):
        """
        Returns tariff mask document for given location_code, tariff_id
        :param tariff_id:
        :param app_location_code:
        :param module:
        :param json_format:
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/get_tariff_document")
        data = {'tariff_id': tariff_id,
                'json_format': json_format,
                'app_location_code': app_location_code,
                'module': module}
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def get_utilities(self, page=1, per_page=100, name=None):
        """
        Returns list of utilities in STEM DB
        :param page:
        :param per_page:
        :param search:
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/utilities")
        data = {'page': page,
                'per_page': per_page}
        if name:
            data['name'] = name
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def create_utility(self, utility_id_vendor, name=None, description=None, vendor_code=None, utility_id=None):
        """
        Import new utility from 3rd party vendor to stem DB.
        New utlity mapping is not created if a third party vendor utility is already mapped to a utility in the db.
        The associated mapping will be returned in this case.
        :param utility_id_vendor:
        :param name:
        :param description:
        :param vendor_code:
        :param utility_id:
        :return:
        """
        url = "%s/%s/" % (self.url, "tariff_mask_document/utilities")
        data = {'utility_id_vendor': utility_id_vendor}
        if name:
            data['name'] = name
        if description:
            data['description'] = description
        if vendor_code:
            data['vendor_code'] = vendor_code
        if utility_id:
            data['utility_id'] = utility_id
        logger.info("invoking POST on url(%s) with data(%s)", url, data)
        headers = {'content-type': 'text/plain'}
        response = self.post(url, data=data, headers=headers)
        return response

    def get_tariffs(self, search=None, status=None, source=None, utility_id=None, page=1, per_page=100,
                    sort=None, sortOrder=None):
        """
        Returns tariffs based on search params
        :param utility_id:
        :param search:
        :param status:
        :param source: acceptable values- STEM_DB, STEM_S3, GEN
        :param page:
        :param per_page:
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/tariffs")
        data = {'page': page,
                'per_page': per_page}
        if utility_id is not None and utility_id > -1:
            data['utility_id'] = utility_id
        if search:
            data['search'] = search
        if status:
            data['status'] = status
        if source:
            data['source'] = source
        if sort:
            data['sort'] = sort
        if sortOrder:
            data['sortOrder'] = sortOrder
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def create_tariff(self, utility_id, tariff_id_vendor, tariff_id=None, name=None, description=None,
                      display_name=None, vendor_code=None, version_id_vendor=None, applicability_keys=None):
        """
        Creates new tariff for a utility from third party vendor to the Stem database.
        A new tariff schedule is not created if the 3rd party vendor tariff is already mapped to a stem tariff in DB.
        The associated mapping will be returned in this case.
        :param utility_id:
        :param tariff_id_vendor:
        :param tariff_id:
        :param name:
        :param description:
        :param display_name:
        :param vendor_code:
        :param version_id_vendor:
        :param applicability_keys:
        :return:
        """
        url = "%s/%s/" % (self.url, "tariff_mask_document/tariffs")
        data = {'utility_id': utility_id,
                'tariff_id_vendor': tariff_id_vendor}
        if tariff_id:
            data['tariff_id'] = tariff_id
        if name:
            data['name'] = name
        if description:
            data['description'] = description
        if display_name:
            data['display_name'] = display_name
        if vendor_code:
            data['vendor_code'] = vendor_code
        if version_id_vendor:
            data['version_id_vendor'] = version_id_vendor
        if applicability_keys:
            data["applicabilitykeys"] = json.dumps(applicability_keys)
        logger.info("invoking POST on url(%s) with data(%s)", url, data)
        headers = {'content-type': 'text/plain'}
        response = self.post(url, data=data, headers=headers)
        return response

    def get_tariff(self, tariff_id, start_date=None, end_date=None,
                   json_format='DOCUMENT', location_code=None, timezone=None):
        """
        Returns tariff based on search params
        :param tariff_id:
        :param start_date: '%Y-%m-%d'
        :param end_date: '%Y-%m-%d'
        :param json_format: UI, DOCUMENT, LEGACY
        :param location_code:
        :param timezone:
        :return:
        """
        url = "%s/%s/%s" % (self.url, "tariff_mask_document/tariffs", tariff_id)
        data = {'json_format': json_format}
        if start_date:
            data['start_date'] = start_date
        if end_date:
            data['end_date'] = end_date
        if location_code:
            data['location_code'] = location_code
        if timezone:
            data['timezone'] = timezone
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def delete_tariff(self, tariff_id):
        url = "%s/%s/%s/?" % (self.url, "tariff_mask_document/tariffs", tariff_id)
        logger.info("invoking DELETE on url(%s)", url)
        response = self.delete(url)
        return response

    def get_vendor_utilities(self, name, vendor_code=None, page=1, per_page=100):
        """
        Returns list of utilities, by default we return ones found in Genability('GEN')
        :param search_name:
        :param vendor_code:
        :return:
        """
        url = "%s/%s" % (self.url, "tariff_mask_document/vendor_utilities/?")
        data = {
            'name': name,
            'page': page,
            'per_page': per_page}
        if vendor_code:
            data['vendor_code'] = 'GEN'
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def get_vendor_utility_tariffs(self, vendor_utility_id, vendor_code=None, search=None, page=1, per_page=100):
        """
        Returns list of tariffs from third party vendor that are associated with the vendor utility
        :param vendor_utility_id:
        :param vendor_code:
        :param search:
        :param page:
        :param per_page:
        :return:
        """
        url = "%s/%s/%s/tariffs/?" % (self.url, "tariff_mask_document/vendor_utilities", vendor_utility_id)
        data = {
            'search': search,
            'page': page,
            'per_page': per_page}
        if vendor_code:
            data['vendor_code'] = vendor_code
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def get_vendor_utility_tariff_applicability_keys(self, vendor_tariff_id, vendor_code='GEN'):
        """
        Returns list of the applicability keys for a vendor tariff for a utility.
        :param vendor_tariff_id: master tariff id
        :return:
        """
        url = "%s/%s/%s/applicability_keys/?" % (self.url, "tariff_mask_document/vendor_tariffs", vendor_tariff_id)
        data = {'vendor_code': vendor_code}
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url, params=data)
        return response

    def get_tariff_sources(self):
        """
        Returns list of sources for tariff- Genability, Legacy(DB), Manual currently (5/1/18
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/sources")
        logger.info("invoking GET on url(%s)", url)
        response = self.get(url)
        return response

    def get_tariff_import_errors(self):
        """
        Returns list of errors for tariff import process
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/errors")
        logger.info("invoking GET on url(%s)", url)
        response = self.get(url)
        return response

    def get_tariff_import_statuses(self):
        """
        Returns list of statuses for tariff import process
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/statuses")
        logger.info("invoking GET on url(%s)", url)
        response = self.get(url)
        return response

    def get_unmapped_utilities(self, vendor_code='GEN'):
        """
        Returns list of unmapped utilities
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/get_unmapped_utilities")
        data = {'vendor_code': vendor_code}
        logger.info("invoking GET on url(%s) with params(%s)", url, data)
        response = self.get(url)
        return response

    def translate_tariff(self, tariff_json, tariff_params, vendor_params):
        """
        Endpoint used to translate genability tariffs for QA testing of Genability tariff translation process
        This endpoint invokes stem.tariff_mask_document.tariff_json_mask_translator.translate_tariff_mask_from_json()
            used in translate_gen_json_into_tariff_doc task see tariff_mask_document/tasks.py
        :param tariff_json: actual genability tariff json
        :param tariff_params: tariff metadata used to translate genability tariff json
        :param vendor_params: utility metadata used to translate genability tariff json
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/tariffs/translate")
        data = {"tariff_json": tariff_json, "tariff_params": tariff_params, "vendor_params": vendor_params}
        # logger.info("invoking POST on url(%s) \ndata (%s)", url, pformat(data))
        headers = {'accept': 'application/json'}
        return self.post(url, data=json.dumps(data), headers=headers)

    def translate_tariff_put(self, tariff_json, tariff_params, vendor_params):
        """
        Endpoint used in tariff translation error testing- not a REAL endpoint, should return 405 exception
        :param tariff_json: actual genability tariff json
        :param tariff_params: tariff metadata used to translate genability tariff json
        :param vendor_params: utility metadata used to translate genability tariff json
        :return:
        """
        url = "%s/%s/?" % (self.url, "tariff_mask_document/tariffs/translate")
        data = {"tariff_json": tariff_json, "tariff_params": tariff_params, "vendor_params": vendor_params}
        # logger.info("invoking PUT on url(%s) \ndata (%s)", url, pformat(data))
        headers = {'accept': 'application/json'}
        return self.put(url, data=json.dumps(data), headers=headers)

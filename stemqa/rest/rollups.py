import logging
from pprint import pformat

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class RollUpsClient(StemClient):

    def get_rollup(self, location_code):
        """
        retreives user rollup based on the given location_code
        :param location_code:
        :return:
        """
        url = "%s/%s" % (self.url, "rollups/get_rollup/")
        data = {"location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

    def get_client_children(self, location_code):
        """
        Gets children (sites) for a client
        :param location_code:
        :return:
        """
        url = "%s/%s" % (self.url, "rollups/get_client_children/")
        data = {"location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

    def salesforce_sync(self, location_code):
        """
        Performs single on demand salesforce sync for given location code
        :param location_code:
        :return:
        """
        url = "%s/%s" % (self.url, "rollups/%s/salesforce_sync/" % location_code)
        logger.info("invoking url(%s)", url)
        response = self.put(url)
        logger.info("\n%s", pformat(response.json()))
        return response

    def get_salesforce_sync_history(self, location_code):
        """
        Gets history for salesforce sync for given location code
        :param location_code:
        :return:
        """
        url = "%s/%s" % (self.url, "rollups/%s/salesforce_sync/" % location_code)
        logger.info("invoking url(%s)", url)
        response = self.get(url)
        logger.info("\n%s", pformat(response.json()))
        return response

import datetime
import logging
import time

from stemqa.rest.qarestexception import QaRestException
from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class PowerQAClient(StemClient):

    def _run_command(self, command, **kwargs):
        """
        Invokes command on powerqa page
        kwargs takes all extra params required for operation
        :param command:
        :param **kwargs:
        :return:
        """
        url = "%s/%s/" % (self.url, "powerqa/run_command")
        data = {
            'command': command
        }
        # add options
        if kwargs:
            for key, value in list(kwargs.items()):
                data[key] = value
        logger.info("Invoking %s with params: %s ", command, str(kwargs))
        response = self.post(url, data=data)
        task_id = response.json()['task_id']
        if command not in ['add_to_locations_to_skip',
                           'clear_tariff_calc_locations_to_skip',
                           'clear_redis_cache_for_finalized_bills',
                           'all_vpp_daily_forecasts',
                           'all_vpp_hourly_forecasts',
                           'fetch_genability_utility_info'
                           ]:
            # some calls are synchronous, so no need to wait
            self.wait_for_task_completion(task_id)
        return response

    def add_to_locations_to_skip(self, location_code):
        """
        Adds location code to the locations_to_skip list in redis cache
        :return:
        """
        logger.info('Adding location_code to redis cache: %s', location_code)
        return self._run_command('add_to_locations_to_skip', location_code=location_code)

    def clear_tariff_calc_locations_to_skip(self):
        """
        Removes redis list containing locations to skip for tariff calcs
        :return:
        """
        logger.info('Running clear tariff calc locations to skip task')
        return self._run_command('clear_tariff_calc_locations_to_skip')

    def clear_redis_cache_for_finalized_bills(self, location_code):
        """
        Removes finalized bills in redis cache
        :return:
        """
        logger.info('Clearing redis cache for finalized bills for location: %s', location_code)
        return self._run_command('clear_redis_cache_for_finalized_bills', location_code=location_code)

    def run_tariff_calc_incremental(self, current_utc_dt=datetime.datetime.utcnow()):
        """
        Kicks off the incremental task
        :return:
        """
        logger.info('Running incremental task')
        return self._run_command('incremental', current_utc_dt=current_utc_dt)

    def run_tariff_calc_zamboni(self, current_utc_dt=datetime.datetime.utcnow()):
        """
        Kicks off the zamboni task
        :return:
        """
        logger.info('Running zamboni task')
        return self._run_command('run_tariff_calc_zamboni', current_utc_dt=current_utc_dt)

    def run_tariff_referee(self, current_utc_dt=datetime.datetime.utcnow()):
        """
        Kicks off the cleanup (referee) task
        :return:
        """
        logger.info('Running cleanup (Referee) task')
        return self._run_command('run_tariff_referee', current_utc_dt=current_utc_dt)

    def run_salesforce_sync(self):
        """
        Kicks off SF sync task
        :return:
        """
        logger.info('Running SF sync task')
        return self._run_command('run_salesforce_sync')

    def run_finalize_bills(self, lookback_days, current_utc_dt=datetime.datetime.utcnow()):
        """
        Kicks off the finalize task for all locations
        :return:
        """
        kwargs = {'lookback_days': lookback_days,
                  'current_utc_dt': current_utc_dt}
        logger.info('Running finalize_bills task')
        return self._run_command('finalize_bills', **kwargs)

    def initial_tariff_calc_by_location(self, location_code):
        """
        Runs the initial tariff calc and waits for it to complete. Waits for the location code to show up in
        a row in tariff_location_calculation_apercu table.
        :param location_code: location code
        :return:
        """
        logger.info('Running initial tariff calculation for location: %s', location_code)
        return self._run_command("initial_tariff_calc_by_location", location_code=location_code)

    def get_tariff_updates(self, tariff_id):
        """
        Retrieves versions for specified tariff id
        :param tariff_id:
        :return:
        """
        logger.info('Getting genability tariff versions for tariff: %s', tariff_id)
        return self._run_command("run_driver_vendor_tariff_translation", tariff_id=tariff_id)

    def run_forecast_for_day(self, start_date, end_date, current_date, location_code, tariff_id):
        """
        Executes forecast for days between start_date, end_date for location_code using specified tariff
        :param start_date: start date for calculation
        :param end_date: end date for calculation
        :param current_date: current time in the site's timezone
        :param location_code:
        :param tariff_id:
        :return:
        """
        logger.info('Running forecast calc for location: %s, start: %s end: %s', location_code, start_date, end_date)
        kwargs = {'tariff_id': tariff_id, 'location_code': location_code, 'start_date': start_date,
                  'end_date': end_date, 'current_date': current_date}
        return self._run_command("run_forecasted_tariff_calcs_for_date", **kwargs)

    def generate_solution_by_powerstore_id(self, ps_id, solution_time):
        """
        Execute OE task for location specified by PowerStore ID
        :return:
        """
        logger.info('Running OE task for PowerStore ID %s', ps_id)
        kwargs = {'powerstore_id': ps_id, 'solution_time': solution_time}
        try:
            self._run_command("generate_solution_by_powerstore_id", **kwargs)
        except QaRestException as e:
            logger.info("Task raised: {exception}".format(exception=e))
            error_message = e.get_message()
            if "'status': 'FAILURE'" in error_message and "'PENDING': 0" in error_message:
                return False
        return True

    def generate_solutions_oe(self):
        """
        Celery task for OE orchestration
        :return:
        """
        logger.info("Selecting sites for OE.")
        try:
            self._run_command("generate_solutions_oe")
        except QaRestException as e:
            logger.info("Task raised: {exception}".format(exception=e))

    def cache_rollups_powerscope(self):
        """
        Refresh cached rollups
        :return:
        """
        logger.info("Refreshing cached information in rollups.")
        return self._run_command("cache_rollups_powerscope")

    def all_vpp_daily_forecasts(self, program_name,
                                start_dt_utc=None,
                                forecast_horizon_days=None,
                                daily_forecast_span_days=None,
                                hourly_forecast_span_days=None):
        logger.info('Running all_vpp_daily_forecasts for program_name: %s', program_name)
        kwargs = {'program_name': program_name,
                  'start_dt_utc': start_dt_utc,
                  'forecast_horizon_days': forecast_horizon_days,
                  'daily_forecast_span_days': daily_forecast_span_days,
                  'hourly_forecast_span_days': hourly_forecast_span_days}
        return self._run_command("all_vpp_daily_forecasts", **kwargs)

    def all_vpp_hourly_forecasts(self, program_name, start_dt_utc,
                                 forecast_horizon_days=None,
                                 daily_forecast_span_days=None,
                                 hourly_forecast_span_days=None):
        logger.info('Running all_vpp_hourly_forecasts for program_name: %s', program_name)
        kwargs = {'program_name': program_name,
                  'start_dt_utc': start_dt_utc,
                  'forecast_horizon_days': forecast_horizon_days,
                  'daily_forecast_span_days': daily_forecast_span_days,
                  'hourly_forecast_span_days': hourly_forecast_span_days}
        return self._run_command("all_vpp_hourly_forecasts", **kwargs)

    def run_cache_ops_data_topology(self):
        logger.info('Running run_cache_ops_data_topology')
        return self._run_command("run_cache_ops_data_topology")

    def run_cache_ops_data_tariff(self):
        logger.info('Running run_cache_ops_data_tariff')
        return self._run_command("run_cache_ops_data_tariff")

    def run_cache_ops_data_telemetry(self):
        logger.info('Running run_cache_ops_data_telemetry')
        return self._run_command("run_cache_ops_data_telemetry")

    def get_task_status(self, task_id):
        """
        :param task_id: task id
        :return: task status
        """
        url = "%s/%s/" % (self.url, "powerqa/get_task_status")
        logger.info("getting status for task id: %s ", task_id)
        response = self.get(url, params={'task_id': task_id})
        return response

    def wait_for_task_completion(self, task_id, time_out=900):
        """
        Waits for a task to complete it's execution
        :param task_id: task id
        :param time_out: time out
        :return: raises exception if time out is hit or if task completes with error state.
        """
        start_time = time.time()
        success = False
        response = None
        while time.time() < start_time + time_out:
            response = self.get_task_status(task_id)
            status = response.json()['data']['status']
            if status not in ['SUCCESS', 'FAILURE']:
                time.sleep(5)
                continue
            if status == 'SUCCESS':
                logger.info('Task completed successfully')
                success = True
                break
            if status == 'FAILURE':
                logger.error('Task failed. Response: \n%s', str(response.json()))
                raise QaRestException('Task %s failed with response: %s' % (task_id, response.json()))

        if not success:
            logger.error('Timed out waiting for task to complete. Last response: \n%s', str(response.json()))
            raise QaRestException('Timed out waiting for task to complete after %d secs. Last response: %s'
                                  % (time_out, response.json()))

    def run_scraps_task(self, command, **kwargs):
        return self._run_command(command, **kwargs)

import logging

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class PowerScopeClient(StemClient):

    def pull_group_data(self, location_code, timezone, start, end):
        url = "%s/%s" % (self.url, "powerscope/pull_powerscope_group_data")
        url = "%s/%s/monitor/15_min/convert_tz/%s/%s/%s" % (url, location_code, timezone, start, end)

        logger.info("invoking url(%s)", url)
        return self.get(url)

    def tariff_daily_structure_second(self, tariff_id, start_date, end_date):
        """
        Returns tariff structure
        :param tariff_id:
        :param start_date: format- 20180207
        :param end_date:   format- 20180309
        :return:
        """
        url_suffix = "tariff_daily_structure_second/%s/%s/%s/0/" % (tariff_id, start_date, end_date)
        url = "%s/%s" % (self.url, url_suffix)
        logger.info('invoking url(%s)', url)
        return self.get(url)

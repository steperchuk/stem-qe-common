import os
import requests
import logging
from stemqa.rest.serviceclient import SvcClient, authentication_required
from stemqa.util.s3_util import S3Util
from stemqa.config.settings import settings

logger = logging.getLogger(__name__)


class SimulationClient(SvcClient):

    def __init__(self):
        super().__init__()
        self.url = settings['stem-simulation']['url']
        self.check_if_alive()

    def check_if_alive(self):
        """
        Check whether microservice is alive
        :return:
        """
        url = "{url}/v1/ping/".format(url=self.url)
        response = requests.get(url)
        if response.status_code != 200:
            logger.info("Simulation Service is not available.")
            raise ConnectionError

    @authentication_required
    def upload_data_file(self, s3_sample_file_path, site_name, data_type='load', file_name=None):
        """
        Uploads a data file for use in simulation
        Filename is required. Data must be in a strict CSV format. File is parsed to JSON and stored in S3.
        :param s3_sample_file_path: string
        :param site_name: string
        :param data_type: string ('load' or 'solar')
        :param file_name: string. file name which will be stored on s3 for simulation. Default is used if not specified
        :return: response json
        """
        s3_util = S3Util()
        logger.info("Loading monitor data on simulation service s3 bucket")
        # Monitor data should be stored in standard location at S3 and has file extension 'csv'
        # File will be downloaded and temporary saved locally
        current_dir = os.getcwd()
        if file_name is None:
            file_name = "15min_interval_data.csv"
        local_file = current_dir + "/" + file_name
        s3_util.download_s3_file(s3_file=s3_sample_file_path, local_output_file=local_file)
        logger.info("Monitor data from {s3} file temporary saved to {local}".format(s3=s3_sample_file_path,
                                                                                    local=local_file))

        file_name = os.path.basename(local_file)
        file = {'file': open(local_file, 'rb')}
        params = {'site_name': site_name,
                  'data_type': data_type}
        url = "{url}/v1/data/".format(url=self.url)
        response = requests.post(url, data={'filename': file_name}, params=params, files=file, headers=self.headers)

        try:
            os.remove(local_file)
            logger.info("Temporary saved {file} removed".format(file=local_file))
        except Exception:
            logger.info("Temp monitor data file {file} removing failed.".format(file=local_file))

        return response.json()

    @authentication_required
    def get_files_list(self):
        """
        Retrieve list of uploaded data files
        :return: response json
        """
        logger.info("Getting list of uploaded data files")
        url = "{url}/v1/data/".format(url=self.url)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def get_data_file_for_site(self, site_name, file_name):
        """
        Retrieve uploaded data file for specific site
        :param site_name: str
        :param file_name: str
        :return: response json
        """
        logger.info("Getting uploaded data file '{file_name}' for site: {site_name}".format(file_name=file_name,
                                                                                            site_name=site_name))
        url = "{url}/v1/data/{site_name}/{file_name}".format(url=self.url, site_name=site_name, file_name=file_name)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def submit_simulations_suite(self, simulation_suite, sync=True):
        """
        Submits a suite of simulations This the main entrypoint to the Simulation Service. The user submits a JSON
        file describing a suite of simulations. Processing is synchronous by default, may be asynchronous using the
        “sync=False” parameter.
        :param sync: bool. default True. Defines if async or sync simulation should be started
        :param simulation_suite: simulation_suite from SimulationTestData object
        :return: response json
        """
        logger.info("Starting simulation with the following parameters: {suite}".format(suite=simulation_suite))
        url = "{url}/v1/simulations/?sync={sync}".format(url=self.url, sync=str(sync).lower())
        response = requests.post(url, json=simulation_suite, headers=self.headers)
        return response.json()

    @authentication_required
    def get_simulations_list(self, site_name):
        """
        Obtains a list of all simulation suites
        :type site_name: str
        :return: response json
        """
        logger.info("Retrieving list of simulations")
        url = "{url}/v1/simulations/".format(url=self.url)
        params = {'site_name': site_name}
        response = requests.get(url, params=params, headers=self.headers)
        return response.json()

    @authentication_required
    def get_simulation(self, site_name, user_name=None, simulation_name=None):
        """
        Obtains metadata/status of all scenarios in suite
        :param site_name: string
        :param user_name: string
        :param simulation_name: string
        :return: response json
        """
        logger.info("Retrieving simulation: {name}".format(name=simulation_name))
        url = "{url}/v1/simulations/".format(url=self.url)
        params = {'site_name': site_name,
                  'user': user_name,
                  'name': simulation_name}
        response = requests.get(url, headers=self.headers, params=params)
        return response.json()

    @authentication_required
    def get_simulation_by_id(self, simulation_id):
        """
        Get simulation data by simulation id
        :param simulation_id: suite id
        :return: response json
        """
        logger.info("Getting simulation with id: {id}".format(id=simulation_id))
        url = "{url}/v1/simulations/{simulation_id}".format(url=self.url, simulation_id=simulation_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def get_simulation_scenario_by_id(self, simulation_id, scenario_id):
        """
        Get simulation scenario data by simulation id and scenario id
        :param simulation_id:
        :param scenario_id:
        :return: response json
        """
        logger.info("Getting scenario for simulation {s} with id: {id}".format(s=simulation_id, id=scenario_id))
        url = "{url}/v1/simulations/{simulation_id}/scenarios/{scenario_id}".format(url=self.url,
                                                                                    simulation_id=simulation_id,
                                                                                    scenario_id=scenario_id)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def complete_simulation(self, simulation_instance):
        """
        Completes simulation by simulation_instance
        :param simulation_instance:
        :return: response json
        """
        logger.info("Completing simulation with simulation_instance {i}".format(i=simulation_instance))
        url = "{url}/v1/simulations/{simulation_instance}/complete".format(url=self.url,
                                                                           simulation_instance=simulation_instance)
        response = requests.post(url, headers=self.headers)
        return response.json()

    @authentication_required
    def get_baseline(self, simulation_instance_uid):
        """
        Report of baseline calculations for instance 'simulation_instance_uid'
        :param simulation_instance_uid: string
        :return: response json
        """
        logger.info("Retrieving simulation baseline by : {uid}".format(uid=simulation_instance_uid))
        url = "{url}/v1/simulations/{uid}/baseline".format(url=self.url,
                                                           uid=simulation_instance_uid)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def get_sites_list(self):
        """
        Get list of sites with topology location information
        :return: response json
        """
        logger.info("Retrieving sites list")
        url = "{url}/v1/sites".format(url=self.url)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def get_site(self, site_name):
        """
        Get Information for one site
        :param site_name: str
        :return: response json
        """
        logger.info("Getting information about {site_name} site".format(site_name=site_name))
        url = "{url}/v1/sites/{site_name}".format(url=self.url, site_name=site_name)
        response = requests.get(url, headers=self.headers)
        return response.json()

    @authentication_required
    def create_site(self, site):
        """
        Create a site for purposes of simulation
        :param site: dict with site info
        """
        logger.info("Creating site: {site_name} for purposes of simulation".format(site_name=site['site_name']))
        url = "{url}/v1/sites".format(url=self.url)
        params = {'site_name': site['site_name'],
                  'timezone': site['timezone'],
                  'latitude': site['latitude'],
                  'longitude': site['longitude']
                  }
        if 'salesforce_customer_no' in site:
            params['salesforce_customer_no'] = site['salesforce_customer_no']
        if 'salesforce_id' in site:
            params['salesforce_id'] = site['salesforce_id']
        if 'salesforce_system_id' in site:
            params['salesforce_system_id'] = site['salesforce_system_id']
        response = requests.post(url, json=params, headers=self.headers)
        return response.json()

    @authentication_required
    def delete_all_sites(self):
        """
        Delete all sites and associated topology location information
        :return: response json
        """
        logger.info("Deleting all sites")
        url = "{url}/v1/sites".format(url=self.url)
        response = requests.delete(url, headers=self.headers)
        return response.json()

import datetime as dt
import logging
from pprint import pformat
from urllib.parse import urljoin

import requests

from stemqa import consts
from stemqa.config.settings import settings
from stemqa.rest.serviceclient import SvcClient, authentication_required

BO_URL = settings['stem-business-objects']['url']
# Business object API
BO_API_SAVE = '/v1/bo/save'
BO_API_QUERY = '/v1/bo/query'
BO_API_GET = 'v1/bo/{bo_definition_name}/{record_id}'
BO_API_SEC_PERMISSION = '/v1/bo/sec_permission'

logger = logging.getLogger(__name__)


class BoClient(SvcClient):

    def __init__(self):
        super().__init__()
        self.bo_url = settings['stem-business-objects']['url']

    @authentication_required
    def save_operation(self, data):
        """
        Bulk operations in db (insert/merge/delete/update).
        :param data: operations document
        :return: response
        """
        bo_save_url = urljoin(self.bo_url, BO_API_SAVE)
        logger.info("Save operations document to db trough BO: %s", bo_save_url)
        logger.debug("data:\n%s", pformat(data))
        response = requests.post(bo_save_url, json=data, headers=self.headers)
        if response.status_code != 200:
            logger.error("ERROR while saving operation:\n%s", pformat(response.text))
            raise Exception('Operation failed: %s' % response.text)
        else:
            logger.debug("Operation done successfully:\n%s", pformat(response.text))
        return response

    @authentication_required
    def query_request(self, data):
        """
        Simple Query operation in db
        :param data: formatted request body for query in db
        example:
        {
            "params": {},
            "queries": [
                {
                    "id": "q1",
                    "query_name": "q1"
                    "select": ["id"],
                    "from": "trf_bill_cycle",
                    "where": ["eq", ".ext_id_stem_v1_db", "99"],
                    "limit": 1,
                    "offset": null
                }
            ]
        }
        :return:
        """
        bo_query_url = urljoin(self.bo_url, BO_API_QUERY)
        logger.info("Perform query operation to db trough BO: %s", bo_query_url)
        logger.debug("data:\n%s", pformat(data))
        response = requests.post(bo_query_url, json=data, headers=self.headers)
        if response.status_code != 200:
            logger.error("ERROR while query request")
            raise Exception('Operation failed: %s' % response.text)
        else:
            logger.debug("Query request performed successfully:\n%s", pformat(response.text))
        return response

    @authentication_required
    def get_record_by_id(self, bo_definition_name, record_id):
        """
        Retrieve record from db by it bo_definition name ad unique id like: GET:/v1/bo/trf_bill_cycle/YBpmZ9BfRqqo
        :param bo_definition_name: Name of BO definition that describe table in db or subset of tables
        :param <String> record_id: unique record id (primary_key)
        :return: record | error
        """
        record_endpoint = BO_API_GET.format(bo_definition_name=bo_definition_name, record_id=record_id)
        bo_get_url = urljoin(self.bo_url, record_endpoint)
        logger.info("Get record from db trough BO: %s", bo_get_url)
        response = requests.get(bo_get_url, headers=self.headers)
        if response.status_code != 200:
            logger.error("ERROR while get record")
            raise Exception('Operation failed: %s' % response.text)
        else:
            logger.debug("Operation performed successfully:\n%s", pformat(response.text))
        return response

    @authentication_required
    def create_billing_cycle_periods(self,
                                     start_date,
                                     cycle_duration,
                                     schedule_table_id,
                                     billing_cycle_name,
                                     cycle_cd,
                                     service_provider_id,
                                     cycles_amount):

        billing_cycles = []
        for counter in range(0, cycles_amount):
            bill_date = start_date + dt.timedelta(days=cycle_duration)
            end_date = bill_date - dt.timedelta(days=1)
            billing_cycles.append(
                {
                    "name": "%s_%s" % (cycle_cd, bill_date.strftime(consts.TIME_SHORT)),
                    "year": str(start_date.year),
                    "period": str(cycle_duration),
                    "bill_date": str(bill_date.strftime(consts.TIME_SHORT)),
                    "bill_days": None,
                    "start_date": str(start_date.strftime(consts.TIME_SHORT)),
                    "end_date": str(end_date.strftime(consts.TIME_SHORT)),
                    "desc_text": None,
                    "status": "Active",
                    "insert_by": "root",
                    "last_upd_by": "root",
                    "rev": 1
                }
            )
            start_date = bill_date
        # prepare BO payload to save
        data = {
            "operations": [
                {
                    "name": billing_cycle_name,
                    "cycle_cd": cycle_cd,
                    "desc_text": None,
                    "ext_id_stem_v1_db": schedule_table_id,
                    "status": "Active",
                    "insert_by": "root",
                    "last_upd_by": "root",
                    "rev": 1,
                    "bill_cycle_periods": billing_cycles,
                    "service_provider": {
                        "ext_id_stem_v1_db": service_provider_id
                    },
                    "_bo_type": "trf_bill_cycle"
                }
            ]
        }

        logger.info("save reading cycles to database")
        self.save_operation(data)

    @authentication_required
    def get_tariff_schedule_records(self, reading_schedule_id):
        """
        Get tariff schedule by reading_schedule_id
        :param reading_schedule_id:
        :return: [tariff schedule records]
        """
        logger.info("check if record with reading_schedule_id=%s exist in db", reading_schedule_id)
        query_data = {
            "params": {},
            "queries": [
                {
                    "id": "q1",
                    "query_name": "q1",
                    "select": ["id", "name", "ext_id_stem_v1_db", "service_provider_id"],
                    "from": "trf_bill_cycle",
                    "where": ["eq", ".ext_id_stem_v1_db", str(reading_schedule_id)],
                    "limit": 1,
                    "offset": None
                }
            ]
        }
        response_data = self.query_request(query_data).json()["data"]
        record_count = response_data[0]["count"]
        if record_count != 0:
            logger.info("Found %s reading schedule records", record_count)
            return response_data[0]["rows"]

        logger.info("No records found for reading_schedule_id = %s", reading_schedule_id)
        return []

    @authentication_required
    def delete_tariff_schedule_records(self, reading_schedule_id):
        """
        Delete from DB reading schedule for specific id
        :param reading_schedule_id: By default method will delete reading schedule created for OE tests (id = '131')
        :return:
        """

        tariff_schedule_records = self.get_tariff_schedule_records(reading_schedule_id)
        records_amount = len(tariff_schedule_records)
        if records_amount != 0:
            for record in tariff_schedule_records:
                billing_cycle_record_id = record["id"]
                billing_cycle_name = record["name"]
                service_provider_id = record["service_provider_id"]
                # prepare delete record query
                delete_query = {
                    "operations": [
                        {
                            "_op_type": "delete",
                            "id": billing_cycle_record_id,
                            "name": billing_cycle_name,
                            "service_provider_id": service_provider_id,
                            "_bo_type": "trf_bill_cycle"
                        }
                    ]
                }
                self.save_operation(delete_query)
                logger.info("Record %s with name=[%s] and related cycles deleted successfully", billing_cycle_record_id,
                            billing_cycle_name)
        else:
            logger.info('Nothing to delete')

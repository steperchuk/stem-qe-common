import logging
from pprint import pformat

from requests import Session, HTTPError

from stemqa import consts

logger = logging.getLogger(__name__)


class StemResponse(object):
    """
    Represents a response from the Stem app's REST endpoints. We have it here as a basic wrapper around requests lib's
    response object. In future, we can add more methods here for validation and transformation of response if we
    see certain patterns.
    """

    def __init__(self, response):
        self.response = response

    def json(self):
        return self.response.json()

    def data(self):
        return self.json()["data"]

    def content(self):
        return self.response.content

    def text(self):
        return self.response.text

    def headers(self):
        return self.response.headers

    def status_code(self):
        return self.response.status_code


class StemClientException(HTTPError):

    def __init__(self, ex):
        self.ex = ex
        super(StemClientException, self).__init__(request=ex.request, response=ex.response)

    def __str__(self):
        return '%s (Response Body: %s)' % (self.ex, self.ex.response.content)


class StemClient(object):
    """
    Base class for clients for the various apps. Implements methods so that we can use it with the "with" statement
    and avoid explicit login/logout every time.
    """

    def __init__(self, url, username=consts.DEFAULT_USERNAME, password=consts.DEFAULT_PASSWORD, client=None):
        if client:
            self.url = client.url
            self.username = client.username
            self.password = client.password
            self.session = client.session
        else:
            self.url = url
            self.username = username
            self.password = password
            self.session = Session()

    def __enter__(self):
        self.login()
        return self

    def __exit__(self, *args):
        try:
            self.logout()
        except Exception:
            logger.exception('Failed logging out as user %s' % self.username)

    def get(self, url, headers=None, data=None, params=None, stream=False):
        response = self.session.get(url, headers=headers, data=data, params=params, stream=stream, verify=False,
                                    hooks=dict(response=ResponseHook.log))
        self.raise_for_status(response)
        return StemResponse(response)

    def post(self, url, headers=None, data=None, params=None, files=None, json=None):
        response = self.session.post(url, headers=headers, data=data, params=params, files=files, verify=False,
                                     hooks=dict(response=ResponseHook.log), json=json)
        self.raise_for_status(response)
        return StemResponse(response)

    def put(self, url, headers=None, data=None, params=None, files=None):
        response = self.session.put(url, headers=headers, data=data, params=params, files=files, verify=False,
                                    hooks=dict(response=ResponseHook.log))
        self.raise_for_status(response)
        return StemResponse(response)

    def delete(self, url, headers=None, data=None, params=None, files=None):
        response = self.session.delete(url, headers=headers, data=data, params=params, files=files, verify=False,
                                       hooks=dict(response=ResponseHook.log))
        self.raise_for_status(response)
        return StemResponse(response)

    @staticmethod
    def raise_for_status(response):
        try:
            response.raise_for_status()
        except HTTPError as e:
            raise StemClientException(e)

    def login(self):
        """
        login to the Stem web app
        :return: response
        """
        url = "%s/%s" % (self.url, "authenticate_user/")
        return self.post(url, data={"username": self.username, "password": self.password})

    def logout(self):
        """
        logout from Stem web app
        :return: response
        """
        url = "%s/%s" % (self.url, "sign_off/")
        return self.get(url)

    @staticmethod
    def _raise_if_error(response):
        # FIXME: this function should detect an error if the json payload
        # FIXME: contains an application error code
        response.raise_for_status()


class ResponseHook(object):
    """
    Hook invoked when we receive a response from the web app. Used primarily for logging the response
    """

    @staticmethod
    def log(response, *args, **kwargs):
        """
        Hook invoked when we receive a response from the web app
        :param response: response from server
        :return:
        """
        if consts.LOG_RESPONSE:
            logger.debug('status_code: %s', response.status_code)
            logger.debug('url: %s', response.url)
            logger.debug('history: %s', response.history)
            logger.debug('response.request.headers: %s', response.request.headers)
            logger.debug('response.headers: %s', response.headers)
            try:
                logger.debug('content: %s', pformat(response.json()))
            except ValueError:
                logger.debug("content: %s", response.text)

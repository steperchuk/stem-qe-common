import json
import logging
from pprint import pformat

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class GridResponseClient(StemClient):
    """
    Client for interacting with grid response service api
    """

    def get_all_programs(self):
        """
        Returns list of all grid response programs
        :return:
        """
        url = "%s/%s/" % (self.url, "gridresponse/get_all_programs")
        logger.info("invoking url(%s)", url)
        response = self.get(url)
        return response.data()

    def get_aggregations_for_program(self, program_id):
        """
        Collection of resources available for use in program
        :param program_id:
        :return:
        """
        url = "%s/%s/" % (self.url, "gridresponse/get_aggregations_for_program")
        data = {"program_id": program_id}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response.data()

    def aggregation_status(self, program_id):
        url = "%s/%s/" % (self.url, "gridresponse/aggregation_status")
        data = {"program_id": program_id}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        return self.get(url, params=data)

    def event_list(self, program_id):
        """
        Queries event logs for event requests sent TO devices
        :param program_id:
        :return:
        """
        url = "%s/%s/" % (self.url, "gridresponse/event_list")
        data = {"program_id": program_id}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        return self.get(url, params=data).data()

    def query_event_logs(self, program_id, grid_service_event_id, dispatch_type=None):
        """
        Queries event logs for event responses sent FROM devices
        :param program_id:
        :param grid_service_event_id:
        :param dispatch_type:
        :return:
        """
        url = "%s/%s/" % (self.url, "gridresponse/query_event_logs")
        data = {"program_id": program_id,
                "grid_service_event_id": grid_service_event_id}
        if dispatch_type is not None:
            data['dispatch_type'] = dispatch_type
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        # logger.info("\n%s" % pformat(response.json()))
        return response.data()

    def aggregation_details(self, program_id):
        url = "%s/%s/" % (self.url, "gridresponse/aggregation_details")
        data = {"program_id": program_id}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        return self.get(url, params=data)

    def create_dispatch_event(self, program_id, deployment_start, deployment_end, requested_kw, timezone=None):
        """
        Creates dispatch event
        :param program_id:
        :param deployment_start:
        :param deployment_end:
        :param requested_kw:
        :param timezone:
        :return:
        """
        url = "%s/%s/" % (self.url, "gridresponse/create_dispatch_event")
        data = {"program_id": program_id,
                "deployment_start": deployment_start,
                "deployment_end": deployment_end,
                "requested_kw": requested_kw}
        if timezone is not None:
            data['timezone'] = timezone
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=data)
        logger.info("\n%s", pformat(response.json()))
        return response.data()

    def cancel_dispatch_event(self, program_id, grid_service_event_id):
        """
        Cancels existing dispatch event
        :param program_id:
        :param grid_service_event_id:
        :return:
        """
        url = "%s/%s/" % (self.url, "gridresponse/cancel_dispatch_event")
        data = {"program_id": program_id,
                "grid_service_event_id": grid_service_event_id}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=data)
        logger.info("\n%s", pformat(response.json()))
        return response.data()

    def get_program_attributes(self, program_id, attribute_name=None):
        url = "%s/%s/" % (self.url, "gridresponse/get_program_attributes")
        data = {"program_id": program_id}
        if attribute_name is not None:
            data['attribute_name'] = attribute_name
        logger.info("invoking url(%s) with data (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response.data()

    def set_program_attributes(self, program_id, attribute_map):
        url = "%s/%s/" % (self.url, "gridresponse/set_program_attributes")
        data = {"program_id": program_id,
                "attributes": attribute_map}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=json.dumps(data))
        return response.data()

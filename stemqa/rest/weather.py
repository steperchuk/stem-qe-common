import datetime as dt
import json
import logging
import os
from time import sleep

import requests

from stemqa.consts import HOURS_IN_DAY, SECONDS_IN_HOUR, TYPE_HISTORICAL, TYPE_FORECAST, REQUEST_RETRIES_COUNT
from stemqa.config.settings import settings
from stemqa.rest.serviceclient import SvcClient, authentication_required
from stemqa.util import site_util
from stemqa.util.date_util import convert_to_iso_date
from stemqa.util.s3_util import S3Util

logger = logging.getLogger(__name__)


class WeatherSvcClient(SvcClient):

    def __init__(self):
        super().__init__()
        self.url = settings['stem-ms-weather']['url']

    @authentication_required
    def get_data(self, site_name, start_time, end_time, weather_type=TYPE_HISTORICAL, interval='hourly'):
        """
        Provide forecast weather data for given location and period (default 15 days ahead).
        :param site_name: mandatory, external location reference
        :param start_time: mandatory, start time of the period to query
        :param end_time: mandatory, end time of the period to query
        :param weather_type: str historical or forecast data
        :param interval: the sample period over which the observation or forecast is done.
        Available values : hourly, daily, monthly
        :return: data in json format
        """

        start_time = convert_to_iso_date(start_time)
        end_time = convert_to_iso_date(end_time)

        params = {'external_ref': site_name, 'start_time': start_time, 'end_time': end_time, 'interval': interval}
        # Choose to upload historical or forecast weather data
        url = self._compose_url(weather_type)
        logger.info("Invoke {url} to get weather data for {site} between {start} - {end} dates."
                    .format(url=url, site=site_name, start=start_time, end=end_time))
        response = requests.get(url, params=params, headers=self.headers)
        return response.json()

    @authentication_required
    def upload_data(self, s3_file, site_name, start_date, end_date, weather_type=TYPE_HISTORICAL):
        """
        Populate weather database with given weather data, location and period
        :param s3_file: Full weather data file path on s3
        :param site_name: mandatory, external location reference
        :param start_date: optional start time of the period to query
        :param end_date: optional end time of the period to query
        :param weather_type: str historical or forecast data
        :return: response
        """
        response = None
        s3_util = S3Util()
        logger.info("Loading weather data to Weather service")
        # Weather data should be stored in standard location at S3 and has file extension 'json'
        # File will be downloaded and temporary saved locally
        current_dir = os.getcwd()
        local_file = current_dir + "/weather.json"
        s3_util.download_s3_file(s3_file=s3_file, local_output_file=local_file)
        logger.info("Weather data from {s3} file temporary saved to {local}".format(s3=s3_file, local=local_file))

        # Choose to upload historical or forecast weather data
        url = self._compose_url(weather_type)

        # Data preparation
        with open(local_file) as json_file:
            data = json.load(json_file)
        if not data:
            logger.warning("There is no weather data to upload to the weather service.")
            return
        else:
            logger.info("Preparing data to upload.")
            time_stamp = []
            temperatures = []
            humidities = []
            diffuse_horizontal_radiations = []
            direct_normal_irradiances = []
            downward_solar_radiations = []
            cloud_coverages = []

            # Generate timestamp for each hourly temperature for specified date range
            delta = end_date - start_date
            days, seconds = delta.days, delta.seconds
            records_number = int(days * HOURS_IN_DAY + seconds / SECONDS_IN_HOUR)

            for index in range(0, records_number + 1):
                weather_time_dt = start_date + dt.timedelta(hours=index)
                weather_time = convert_to_iso_date(weather_time_dt)
                time_stamp.append(weather_time)
                fahrenheit_temperature = data['records']['temperatures'][index]
                # need to convert temp value to celsius, because when data retrieved
                #  by service it converted to fahrenheit
                celsius_temperature = (fahrenheit_temperature - 32) * (float(5) / 9)
                temperatures.append(celsius_temperature)
                humidities.append(data['records']['humidities'][index])
                if 'diffuse_horizontal_radiations' in data['records']:
                    diffuse_horizontal_radiations.append((data['records']['diffuse_horizontal_radiations'][index]))
                if 'direct_normal_irradiances' in data['records']:
                    direct_normal_irradiances.append((data['records']['direct_normal_irradiances'][index]))
                if 'downward_solar_radiations' in data['records']:
                    downward_solar_radiations.append((data['records']['downward_solar_radiations'][index]))
                if 'cloud_coverages' in data['records']:
                    cloud_coverages.append((data['records']['cloud_coverages'][index]))

            data['records']['observed_times'] = time_stamp
            data['records']['temperatures'] = temperatures
            data['records']['humidities'] = humidities
            data['records']['diffuse_horizontal_radiations'] = diffuse_horizontal_radiations
            data['records']['direct_normal_irradiances'] = direct_normal_irradiances
            data['records']['downward_solar_radiations'] = downward_solar_radiations
            data['records']['cloud_coverages'] = cloud_coverages

            # Update location info
            latitude, longitude = site_util.get_coordinates(site_name)
            data['location']['external_ref'] = site_name
            data['location']['latitude'] = latitude
            data['location']['longitude'] = longitude

        logger.info("Invoke {url} to upload weather data for {start} - {end} dates range.".
                    format(url=url, start=start_date, end=end_date))

        # Retries added due to bug in Weather service JEDI-14787
        for i in range(0, REQUEST_RETRIES_COUNT):
            try:
                response = requests.post(url, json=data, headers=self.headers)
            except requests.exceptions.ConnectionError:
                logger.info("Requests module raised ConnectionError. Retrying to upload data once again")
                # Sleeping for 5 sec before sending request once again
                sleep(5)
                continue
            break
        if response is None:
            raise Exception("Requests module raised ConnectionError after {}".format(REQUEST_RETRIES_COUNT))

        if response.status_code != 200:
            raise Exception(
                "Weather data upload failed. Response status code: {code}".format(code=response.status_code))
        logger.info("Weather data uploaded successfully")

        try:
            os.remove(local_file)
        except Exception:
            logger.info("Temp weather file {file} removing failed.".format(file=local_file))

        return response

    def _compose_url(self, weather_type):
        api = 'v1/weather/historicals/'
        if weather_type == TYPE_FORECAST:
            api = 'v1/weather/forecasts/'
        url = "{url}/{api}".format(url=self.url, api=api)
        return url

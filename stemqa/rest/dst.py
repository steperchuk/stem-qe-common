import cgi
import json
import logging
import os
import time
from pprint import pformat

from requests.exceptions import ChunkedEncodingError

from stemqa.rest.qarestexception import QaRestException
from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class DSTClient(StemClient):

    def create_site(self, location_code, customer_name, has_address,
                    address_1, address_2, city, state, zipcode, timezone,
                    tariff_code, parent_location_code,
                    parent_salesforce_id, timestamp):
        url = "%s/%s/" % (self.url, "powersales/analytics/create_site")
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        data = {
            "location_code": location_code,
            "customerName": customer_name,
            "hasAddress": has_address,
            "address_1": address_1,
            "address_2": address_2,
            "city": city,
            "state": state,
            "zipcode": zipcode,
            "timezone": timezone,
            "tariff_code": tariff_code,
            "parent_location_code": parent_location_code,
            "parent_salesforce_id": parent_salesforce_id,
            "timestamp": timestamp
        }
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=data, headers=headers)
        return response

    def save_interval_data(self, filename, interval_data_type, power_unit, file_data_time_zone,
                           timezone_offset, content_type='text/csv', **kwargs):
        """
        Saves utility data for analysis
        :param filename:
        :param interval_data_type:
        :param power_unit:
        :param file_data_time_zone:
        :param timezone_offset:
        :param content_type:
        :param kwargs:
        :return:
        """
        files = {}
        data = {}
        data['file_id'] = 1
        data['interval_data_type'] = interval_data_type
        data['power_unit'] = power_unit
        data['file_data_time_zone'] = file_data_time_zone
        data['timezone_offset'] = timezone_offset

        if 'excel_sheet_name' in kwargs:
            data['excel_sheet_name'] = kwargs['excel_sheet_name']
        if 'excel_date_column' in kwargs:
            data['excel_date_column'] = kwargs['excel_date_column']
        if 'excel_value_column' in kwargs:
            data['excel_value_column'] = kwargs['excel_value_column']

        testfile = os.path.basename(filename)
        files['file'] = (testfile, open(filename, 'rb'), content_type, {'Expires': '0'})
        data['action'] = '/powersales/analytics/save_interval_data/'
        url = "%s/%s/" % (self.url, "powersales/analytics/save_interval_data")
        logger.info("invoking url(%s) data(%s) files (%s)", url, data, files)
        response = self.post(url, files=files, data=data)
        return response

    def _get_submitted_scenario_status(self, location_code):
        """
        returns status of analysis for given location code
        :param location_code:
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/get_submitted_scenarios_for_location")
        data = {"timestamp": int(time.time()),
                "location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

    def get_submitted_scenario(self, location_code, timeout=600):
        """
        Waits for scenario analyze execution to either succeed, fail or timeout
        Throws exception if anything but SUCCESS returned
        :param location_code:
        :param timeout:
        :return:
        """
        response = self._get_submitted_scenario_status(location_code)
        status_code = response.data()[0].get('status_code')
        time_end = time.time() + timeout
        start_time = time.time()
        while status_code != "COMPLETE" and status_code != "ERROR" and time.time() < time_end:
            time.sleep(30)
            response = self._get_submitted_scenario_status(location_code)
            status_code = response.data()[0].get('status_code')
            task_percent_complete = response.data()[0].get('task_complete_pct')
            logger.debug('Percent complete: %s', task_percent_complete)

            # check whether we've made any progress after 5min, raise an exception if not...
            if task_percent_complete == 0.0 and time.time() > start_time + 300:
                raise QaRestException("Scenario progress still at 0%% after 5min! "
                                      "Check application logs! Final status code: %s" % status_code)

        if time_end < time.time():
            raise QaRestException("Polling timeout (%d secs) exceeded!" % timeout)
        if status_code == "ERROR":
            raise QaRestException("Analysis result encountered status ERROR, response:\n %s" % response.data()[0])
        return response

    def submit_scenario(self, timestamp, dr_load_calc, location_code_primary,
                        scenario_start_datetime, scenario_end_datetime,
                        add_markdown, optimization_resource_sets,
                        forward_looking_tariff, tariff_code,
                        save_results, interval_seconds, analysis_type,
                        supplemental_datasets, use_cvx, date_range_type='month'):
        """
        submits scenario for analysis
        :param timestamp: e.g 1472827902363
        :param dr_load_calc: Fale or True
        :param location_code_primary: e.g NICHOLAS_MILLER_20160902144307_c
        :param scenario_start_datetime: e.g 2014-01-01
        :param scenario_end_datetime: e.g 2014-12-31
        :param add_markdown: False or True
        :param optimization_resource_sets: list of products
         e.g [
                {
                    "optimization_resource_primary":
                    {
                        "unit_range":[1,2],
                        "optimization_resource_type":"POWERSTORE_3_0"
                    }
                }
            ]
        :param forward_looking_tariff:  True or false
        :param tariff_code:  e.2
        :param save_results:  True or false
        :param interval_seconds: e.g 900
        :param analysis_type: e.g THEORY
        :param supplemental_datasets:
        :param use_cvx: use new convex optimization method
        :return:
        """
        data = dict()
        data["timestamp"] = int(timestamp)
        data["date_range_type"] = date_range_type
        if supplemental_datasets:
            data["supplemental_datasets"] = supplemental_datasets
        else:
            data["supplemental_datasets"] = []
        data["interval_seconds"] = interval_seconds
        data["save_results"] = save_results
        data["tariff_code"] = tariff_code
        data["forward_looking_tariff"] = forward_looking_tariff
        if optimization_resource_sets:
            data["optimization_resource_sets"] = optimization_resource_sets
        else:
            data["optimization_resource_sets"] = []
        if dr_load_calc:
            data["dr_load_calc"] = dr_load_calc
        else:
            data["dr_load_calc"] = False
        data["location_code_primary"] = location_code_primary
        data["scenario_start_datetime"] = scenario_start_datetime.isoformat()
        data["scenario_end_datetime"] = scenario_end_datetime.isoformat()
        data["add_markdown"] = add_markdown
        data["analysis_type"] = analysis_type
        data["use_cvx"] = use_cvx

        url = "%s/%s/" % (self.url, "powersales/analytics/submit_scenario")
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        headers = {'content-type': 'application/x-www-form-urlencoded',
                   'accept': 'application/json'}
        response = self.post(url, data=json.dumps(data), headers=headers)
        return response

    def parse_and_save(self, client_interval_data_id, key_name, location_code, **kwargs):
        """
        Async call to upload and parse data
        :param client_interval_data_id:
        :param key_name:
        :param location_code:
        :param kwargs:
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/parse_final_file_and_save")
        data = {"client_interval_data_id": client_interval_data_id,
                "key_name": key_name,
                "location_code": location_code}
        if kwargs and 'excel_sheet_name' in kwargs:
            data['excel_sheet_name'] = kwargs['excel_sheet_name']
            data['excel_date_column'] = kwargs['excel_date_column']
            data['excel_value_column'] = kwargs['excel_value_column']
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        try:
            response = self.post(url, data=data)
        except ChunkedEncodingError:
            logger.warn('Encountered ChunkedEncodingError. Ignoring it and retrying once more...')
            response = self.post(url, data=data)

        num_steps = response.json().get('steps')
        logger.info("Waiting for  %s steps", num_steps)
        self._wait_for_file_save_progress(client_interval_data_id, num_steps)

    def set_current_site(self, location_code):
        url = "%s/%s/" % (self.url, "powersales/analytics/set_current_site")
        data = {"timestamp": time.time(), "location_code": location_code}
        response = self.get(url, params=data)
        return response.data()

    def pull_data_by_location_code(self, location_code, start_date_time, end_date_time,
                                   resolution=900, start_times='true', format_for_charts='true'):
        """
        Returns meter for given dates
        Called in UI after saving meter upload file
        :param location_code:
        :param start_date_time:
        :param end_date_time:
        :param resolution:
        :param start_times:
        :param format_for_charts:
        :return:
        """
        url = "%s/%s/" % (self.url, "powerdata/pull_data_by_location_code")
        data = {"location_code": location_code,
                "resolution": resolution,
                "timestamp": int(time.time()),
                "start_times": start_times,
                "format_for_charts": format_for_charts,
                "start_date_time": start_date_time,
                "end_date_time": end_date_time}
        logger.info("invoking url(%s) dates: %s - %s", url, pformat(start_date_time), pformat(end_date_time))
        response = self.get(url, params=data)
        return response

    def get_scenario_summary_results(self, optimization_scenario_id, location_code, return_projected=False):
        """
        Returns summary results
        :param optimization_scenario_id:
        :param location_code:
        :param return_projected:
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/get_scenario_summary_results")
        data = {"timestamp": int(time.time()),
                "location_code": location_code,
                "optimization_scenario_id": optimization_scenario_id,
                "return_projected": return_projected}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response.data()

    def get_scenario_analysis_chart_data(self, optimization_scenario_id, location_code,
                                         resource_set_hash, load_definition_code="UTILITY",
                                         result_type="OPTIMIZED"):
        """
        Returns analysis results for specified resource config
        :param optimization_scenario_id:
        :param location_code:
        :param resource_set_hash:
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/get_scenario_analysis_chart_data")
        data = {"timestamp": int(time.time()),
                "location_code": location_code,
                "optimization_scenario_id": optimization_scenario_id,
                "optimization_scenario_resource_set_hash": resource_set_hash,
                "load_definition_code": load_definition_code,
                "result_type": result_type}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response.text()

    def get_scenario_analysis_chart_data_as_csv(self, optimization_scenario_id,
                                                location_code, resource_set_hash,
                                                load_definition_code="UTILITY"):
        """
        Returns analysis detailed interval results for specified resource config
        :param optimization_scenario_id:
        :param location_code:
        :param resource_set_hash:
        :return:
        """

        url = "%s/%s/" % (self.url, "powersales/analytics/get_chart_data_as_csv")
        data = {"timestamp": int(time.time()),
                "location_code": location_code,
                "optimization_scenario_id": optimization_scenario_id,
                "optimization_scenario_resource_set_hash": resource_set_hash,
                "load_definition_code": load_definition_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response.text()

    def get_analysis_csv_results(self, optimization_scenario_id, resource_set_hash, location_code):
        """
        Returns csv from the 'Download Analysis CSV' link on Analysis details page
        :param optimization_scenario_id:
        :param resource_set_hash:
        :param location_code:
        :return:
        """
        url = "%s/%s/?" % (self.url, "powersales/analytics/get_analysis_csv_results")
        data = {"optimization_scenario_id": optimization_scenario_id,
                "optimization_scenario_resource_set_hash": resource_set_hash,
                "location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response.text()

    def _get_file_save_progress(self, client_interval_data_id):
        """
        Polls periodically for save progress
        :param client_interval_data_id: id for data upload
        :return progress of file save operation
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/file_save_progress")
        data = {"client_interval_data_id": client_interval_data_id}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=data)
        return response.json().get('progress')

    def _wait_for_file_save_progress(self, client_interval_data_id, steps, timeout=300):
        """
        Waits after clicking 'Save Data' button after upload and periodically polls for completion
        :param client_interval_data_id: id for data upload
        :param number of steps that we must wait until completion of data save
        """
        progress = self._get_file_save_progress(client_interval_data_id=client_interval_data_id)
        time_end = time.time() + timeout
        while progress != steps and time.time() < time_end:
            time.sleep(10)
            progress = self._get_file_save_progress(client_interval_data_id=client_interval_data_id)
            logger.info("progress is %s out of %s steps", progress, steps)

        if progress != steps:
            raise QaRestException("File not saved within %s seconds" % timeout)

    def get_submitted_scenarios(self, location_code):
        """
        Returns list of submitted scenarios
        :param location_code:
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/get_submitted_scenarios_for_location")
        data = {"timestamp": int(time.time()),
                "location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        return self.get(url, params=data)

    def dr_load_calc_pdf_download(self, location_code, scenario_id):
        """
        Downloads pdf report from s3 and returns its location
        :param location_code:
        :param scenario_id:
        :return:
        """
        CHUNK_SIZE = 1024
        url = "%s/%s/" % (self.url, "powersales/analytics/dr_load_calc_pdf_download")
        data = {"optimization_scenario_id": scenario_id,
                "location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data, stream=True)
        params = cgi.parse_header(response.headers().get('Content-Disposition', ''))[-1]
        if 'filename' not in params:
            raise ValueError('Could not find pdf to download')
        filename = os.path.basename(params['filename'])
        abs_path = os.path.join('resources/dst', filename)
        with open(abs_path, 'wb') as _file:
            for chunk in response.response.iter_content(CHUNK_SIZE):
                _file.write(chunk)
        return abs_path

    def download_csv(self, location_code, scenario_id):
        """
        Downloads csv for specified location_code, scenario_id
        CSV can be accessed in content portion of response
        :return:
        """
        url = "%s/%s/?" % (self.url, "powersales/analytics/get_summary_csv")
        data = {"optimization_scenario_id": scenario_id,
                "location_code": location_code}
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(data))
        return self.get(url, params=data)

    def get_uploaded_files_list(self, location_code):
        """
        Gets list of uploaded files
        :param location_code:
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/uploaded_files_list")
        data = {"location_code": location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=data)
        return response.json()

    def get_defined_tariffs(self):
        """
        Gets list of all possible tariffs in system
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/analytics/get_defined_tariffs")
        logger.info("invoking url(%s)", url)
        return self.get(url)

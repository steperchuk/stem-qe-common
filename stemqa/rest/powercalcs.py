import logging
from pprint import pformat

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)

BILLING_PERIOD = 'BILLING_PERIOD'
ENERGY_REPORT_TREND = 'ENERGY_REPORT_TREND'
MONTHLY = 'MONTHLY'


class PowerCalcsClient(StemClient):

    def get_energy_calculations(self, location_code, app_location_code, timestamp, start_date,
                                end_date, resolution, module):
        url = "%s/%s/" % (self.url, "powercalcs/get_energy_calculations")
        data = {
            "location_code": location_code,
            "app_location_code": app_location_code,
            "timestamp": timestamp,
            "start_date_local": start_date,
            "end_date_local": end_date,
            "resolution": resolution,
            "module": module
        }

        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

    def get_bills_for_location(self, location_code, app_location_code, start_date, end_date,
                               use_api_contract='true', **kwargs):
        """
        PowerScope -> Cost Explorer endpoint
        returns set of bills which include dates between start and end dates
        :param location_code:
        :param app_location_code:
        :param start_date:
        :param end_date:
        :param use_api_contract:
        :param kwargs:
        :return:
        """
        url = "%s/%s" % (self.url, "powercalcs/get_bills_for_location")
        data = {
            "location_code": location_code,
            "app_location_code": app_location_code,
            "module": "COST_EXPLORER",
            "start_date": start_date,
            "end_date": end_date,
            "use_api_contract": use_api_contract
        }
        if kwargs is not None:
            for key, value in list(kwargs.items()):
                data[key] = value
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

    def get_locations_to_skip(self):
        url = "%s/%s/" % (self.url, "powercalcs/get_locations_to_skip")
        logger.info("invoking url(%s)", url)
        response = self.get(url)
        return response

    def get_daily_energy_calcs(self, location_code, start_date, end_date, tariff_id, app_location_code, module):
        """
        Used on powerscope/costexplorer/bill drilldown
        :param location_code:
        :param start_date:
        :param end_date:
        :param tariff_id:
        :param app_location_code:
        :param module:
        :return:
        """
        url = "%s/%s/?" % (self.url, "powercalcs/get_daily_energy_calcs")
        data = {
            "location_code": location_code,
            "app_location_code": app_location_code,
            "module": module,
            "start_date": start_date,
            "end_date": end_date,
            "tariff_id": tariff_id
        }
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

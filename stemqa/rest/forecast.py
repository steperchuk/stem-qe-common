import json

import requests
import logging
from stemqa.config.settings import settings
from stemqa.util import forecast_util
from stemqa.rest.serviceclient import SvcClient, authentication_required
from stemqa.rest.stembase import StemClientException

logger = logging.getLogger(__name__)


class ForecastClient(SvcClient):

    def __init__(self):
        super().__init__()
        self.url_forecast = settings['stem-forecast']['url']

    @authentication_required
    def get_forecast(self, location_code, start_time, end_time, algorithm_id=None, features=None):
        """
        Returns forecast data sets for specific site represented in json format
        :param location_code: site location code
        :param start_time: forecast start time
        :param end_time: forecast end time
        :param algorithm_id: id of forecast algorithm
        :param features: list of features
        :return: response data represented as json
        """

        forecast_start = str(start_time).replace(" ", "T") + 'Z'
        forecast_end = str(end_time).replace(" ", "T") + 'Z'

        data = {
            "location_code": location_code,
            "forecast_start": forecast_start,
            "forecast_end": forecast_end
        }

        if algorithm_id is not None:
            algorithm = forecast_util.get_algorithm_name(algorithm_id)
            data['algorithm'] = algorithm
            data['training_ndays'] = 30
            if features is None:
                raise Exception("Features set should be specified if algorithm specified")
            data['features'] = features

        url = "{url}/v1/stem_ms_forecast/forecast".format(url=self.url_forecast)

        logger.info("invoking url:{url} with the following parameters {data}".format(url=self.url_forecast, data=data))
        try:
            response = requests.post(url, json=data, headers=self.headers)
        except StemClientException as e:
            return e.response.json()
        data = response.json()
        if isinstance(data, str):  # validates id str returned
            return json.loads(data)
        return data

    @authentication_required
    def start_select_forecast_model_job(self, start_time):
        """
        Starts select_forcast_model job
        :param start_time: date time
        :return: str job_id
        """
        start_time = str(start_time).replace(" ", "T") + 'Z'
        data = {
            "start_time": start_time,
        }
        url = "{url}/v1/stem_forecast/jobs/select_model/submit".format(url=self.url_forecast)
        logger.info("invoking url:{url} with the following parameters {data}".format(url=self.url_forecast, data=data))
        response = requests.post(url, json=data, headers=self.headers)
        response_data = response.json()
        job_id = response_data['resource_name']
        logger.info("Job with id: {id} started".format(id=job_id))
        return job_id

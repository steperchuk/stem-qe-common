import logging
from datetime import datetime, timedelta

import requests

from time import sleep
from stemqa import consts
from stemqa.config.settings import settings

logger = logging.getLogger(__name__)
AUTHORIZATION_HEADER = 'Authorization'


def authentication_required(func):
    """
    Decorator: that applied to methods in service class that require authentication before
    :param func: method to wrap
    :return:
    """

    def func_wrapper(self, *args, **kwargs):
        if not self.is_login():
            self.login()
        return func(self, *args, **kwargs)

    return func_wrapper


# This class should be used as base class for any microservice clients
class SvcClient:
    def __init__(self, url=settings['stem-ms-app']['url'],
                 username=consts.SUPERUSER_USERNAME, password=consts.DEFAULT_PASSWORD):
        self.url = url
        self.username = username
        self.password = password
        self.token = None
        self.headers = None

    def __enter__(self):
        return self

    def __exit__(self, *arg):
        # token does not require to logout from service
        return None

    def login(self):
        """
        Preforms authentication and initializes token.
        :return:
        """
        if self.get_status() != 200:
            raise Exception('Service is not available.')
        response = requests.post(self.url + '/v1/app/login',
                                 json={"username": self.username, "password": self.password})
        response.raise_for_status()
        response_headers = response.headers
        if AUTHORIZATION_HEADER not in response.headers:
            raise Exception('Connection is not established.')
        self.token = response_headers[AUTHORIZATION_HEADER]
        self.headers = {"Authorization": self.token}

    def is_login(self):
        return self.token is not None

    def get_status(self):
        """
        Retrieve service status
        :return: status code
        """
        url = "{url}/{api}".format(url=self.url, api="v1/app/status")
        logger.info("Calling {url} to check micro-service status.".format(url=url))
        response = requests.get(url)
        return response.status_code

    @authentication_required
    def get_job_completion_status(self, job_id):
        """
        Gets k8's job status by id
        :return: boolean
        """
        url = "{url}/v1/app/job/{job_id}".format(url=self.url, job_id=job_id)
        response = requests.get(url, headers=self.headers)
        response_json = response.json()
        if response_json['successful_completions'] == 1:
            return True
        return False

    @authentication_required
    def get_job_completion_status_by_name(self, job_name):
        """
        Gets k8's job status by name
        :return: boolean
        """
        url = "{url}/v1/app/jobs?job_name={job_name}".format(url=self.url, job_name=job_name)
        response = requests.get(url, headers=self.headers)
        response_json = response.json()
        if not response_json:
            return False
        if response_json[0]['successful_completions'] == 1:
            return True
        return False

    def wait_job_completion(self, job_id, timeout=300, idle_time=5):
        """
        Waits while job completed
        :param idle_time: idle time between checks (use to reduce check requests)
        :param job_id: str
        :param timeout: default 300
        :return: True if job completed, otherwise False
        """
        logger.info("Awaiting for job completion. Expected time no longer than %s seconds", timeout)
        # by default status would be checked each 5 sec during 5 min.]
        start_time = datetime.now()
        while timedelta.total_seconds(datetime.now() - start_time) < timeout:
            if self.get_job_completion_status(job_id):
                logger.info("{id} job completed in {time} seconds ".format(
                    id=job_id, time=timedelta.total_seconds(datetime.now() - start_time)))
                return True
            else:
                sleep(idle_time)
        logger.info("{id} job is not completed after {timeout} seconds".format(
            id=job_id, timeout=timedelta.total_seconds(datetime.now() - start_time)))
        return False

    @authentication_required
    def kill_job(self, job_name):
        """
        Kill specified k8's job
        :param job_name str
        :return: boolean
        """
        url = "{url}/v1/app/jobs?job_name={job_name}".format(url=self.url, job_name=job_name)
        response = requests.delete(url, headers=self.headers)
        if response.status_code == 200:
            return True
        return False

    @authentication_required
    def wait_for_job_kill(self, job_name, timeout=60):
        """
        Waits for job kill
        :param job_name: str
        :param timeout: default 60
        :return: True if job completed, otherwise False
        """
        logger.info("Awaiting for job killing.")
        # status would be checked each sec during 1 min.
        for _ in range(0, timeout):
            if not self.get_job_completion_status_by_name(job_name):
                logger.info("{name} job completed.".format(name=job_name))
                return True
            sleep(1)
        logger.info("{name} job is not killed after {timeout} seconds".format(name=job_name, timeout=timeout))
        return False

import logging

logger = logging.getLogger(__name__)


class QaRestException(Exception):
    """
    Exception raised for errors encountered in rest api calls
    """

    def __init__(self, message):
        Exception.__init__(self, message)
        self._message = message

    def get_message(self):
        return self._message

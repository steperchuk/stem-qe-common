import json
from pprint import pformat

import requests

from stemqa.config.settings import settings
from stemqa.util.forecast_util import logger
from stemqa.rest.serviceclient import SvcClient, authentication_required

GENABILITY = 'GENABILITY'

SORT_NAME = 'name'
SORT_ID = 'id'
SORT_STATUS = 'status'
SORT_EVENT = 'event_type'
SORT_UTIL = 'utility_name'
SORT_CAL_DATE = 'cal_date'
ASC = 'asc'
DESC = 'desc'
ACTIVE = 'active'
INACTIVE = 'inactive'
SIMULATION_GRADE = 'SIMULATION_GRADE'
REVENUE_GRADE = 'REVENUE_GRADE'
PROPOSAL_GRADE = 'PROPOSAL_GRADE'

HOLIDAY = 'Holiday'
WEEKDAY = 'Weekday'
WEEKEND = 'Weekend'


class TariffClient(SvcClient):

    def __init__(self):
        super().__init__()
        self.tariff_url = settings['stem-tariff']['url']

    @authentication_required
    def get_utilities(self, name=None, per_page=None,
                      page=None, sort_by=None, order_by=ASC, status=None):
        """
        Returns list of utilities in Stem database
        :param name: name search for utility matches
        :param per_page:
        :param page:
        :param sort_by: name, id
        :param order_by: asc,desc
        :param status: active, inactive
        :return:
        """
        url = "{url}/v1/tariff/utilities/".format(url=self.tariff_url)
        params = {}
        if name:
            params['name'] = name
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if sort_by:
            params['sort_by'] = sort_by
        if order_by:
            params['order_by'] = order_by
        if status:
            params['status'] = status
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_utility(self, utility_name):
        """
        Returns utility by name from stem db
        :param utility_name:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}".format(url=self.tariff_url, utility_name=utility_name)
        logger.info("invoking GET on url(%s)", url)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def delete_utility(self, utility_name, force=True):
        """
        Delete  Utility by it name
        :param force:
        :param utility_name:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}".format(url=self.tariff_url, utility_name=utility_name)
        logger.info("invoking DELETE on url(%s) ", url)
        response = requests.delete(url, headers=self.headers, params={'force': force})
        return response

    @authentication_required
    def get_tariffs_for_utility(self, utility_name, status=None, per_page=None,
                                page=None, sort_by=None, order_by=ASC):
        """
        Returns list of tariffs for a utility
        :param utility_name:
        :param status: SIMULATION_GRADE, REVENUE_GRADE, PROPOSAL_GRADE
        :param per_page:
        :param page:
        :param sort_by: name, status
        :param: order_by
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility}/tariffs".format(url=self.tariff_url, utility=utility_name)
        params = {}
        if status:
            params['status'] = status
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if sort_by:
            params['sort_by'] = sort_by
        if order_by:
            params['order_by'] = order_by
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    # TODO Cover by tests
    def get_tariff_for_utility(self, utility_name, tariff_name):
        """
        Returns tariff from specified utility
        :param utility_name:
        :param tariff_name:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/tariffs/{tariff_name}".format(url=self.tariff_url,
                                                                                      utility_name=utility_name,
                                                                                      tariff_name=tariff_name)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def get_utility_app_criteria(self, utility_name):
        """
        Returns app criteria for utility
        :param utility_name:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/app_criteria".format(url=self.tariff_url,
                                                                             utility_name=utility_name)
        logger.info("invoking GET on url(%s)", url)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def get_tariff_app_criteria(self, tariff_name):
        """
        Returns app criteria for tariff
        :param tariff_name:
        :return:
        """
        url = "{url}/v1/tariff/tariffs/{tariff_name}/app_criteria".format(url=self.tariff_url,
                                                                          tariff_name=tariff_name)
        logger.info("invoking GET on url(%s)", url)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def get_utility_calendars(self, utility_name, per_page=None, page=None, sort_by=None, order_by=ASC):
        """
        Returns utility calendars
        :param self:
        :param utility_name:
        :param per_page:
        :param page:
        :param sort_by: [name, alt_id]
        :param order_by: [asc, desc]
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/calendars" \
            .format(url=self.tariff_url, utility_name=utility_name)
        params = {}
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if sort_by:
            params['sort_by'] = sort_by
        if order_by:
            params['order_by'] = order_by
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_utility_calendar_by_name(self,
                                     utility_name,
                                     calendar_name,
                                     start_datetime=None,
                                     end_datetime=None,
                                     per_page=None,
                                     page=None,
                                     order_by=ASC):
        """
        Returns utility calendar items for specific calendar name
        :param self:
        :param utility_name:
        :param calendar_name:
        :param start_datetime:
        :param end_datetime:
        :param per_page:
        :param page:
        :param order_by:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/calendars/{calendar_name}".format(url=self.tariff_url,
                                                                                          utility_name=utility_name,
                                                                                          calendar_name=calendar_name)
        params = {}
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if start_datetime:
            params['start_datetime'] = start_datetime
        if end_datetime:
            params['end_datetime'] = end_datetime
        if order_by:
            params['order_by'] = order_by
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def create_calendar_for_utility(self, utility_name, json_data):
        """
        Create calendar for Utility
        :param utility_name:
        :param data: request body
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/calendars".format(url=self.tariff_url,
                                                                          utility_name=utility_name)

        logger.info("invoking POST on url(%s) ", url)
        response = requests.post(url, json=json_data, headers=self.headers)
        return response

    @authentication_required
    def update_calendar_for_utility(self, utility_name, json_data):
        """
        Update calendar for Utility
        :param utility_name:
        :param data: request body
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/calendars".format(url=self.tariff_url,
                                                                          utility_name=utility_name)

        logger.info("invoking PUT on url(%s) ", url)
        response = requests.put(url, json=json_data, headers=self.headers)
        return response

    @authentication_required
    def delete_calendars_for_utility(self, utility_name, calendar_name, event_type=None, force=True):
        """
        Delete calendars for Utility that not in Use by Tariffs
        :param calendar_name:
        :param force:
        :param event_type:
        :param utility_name:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/calendars/{calendar_name}".format(url=self.tariff_url,
                                                                                          utility_name=utility_name,
                                                                                          calendar_name=calendar_name)
        params = {'force': force}
        if event_type:
            params['event_type'] = event_type

        logger.info("invoking DELETE on url(%s) ", url)
        response = requests.delete(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_utility_bill_cycles(self, utility_name, per_page=None, page=None, sort_by=None, order_by=ASC):
        """
        Returns utility bill cycles
        :param utility_name:
        :param per_page:
        :param page:
        :param sort_by: name, id
        :param order_by:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/bill_cycles" \
            .format(url=self.tariff_url, utility_name=utility_name)
        params = {}
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if sort_by:
            params['sort_by'] = sort_by
        if order_by:
            params['order_by'] = order_by
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_bill_periods(self, utility_name, bill_cycle_name, per_page=None, page=None, sort_by=None, order_by=ASC):
        """
        Returns bill periods
        :param utility_name:
        :param bill_cycle_name:
        :param per_page:
        :param page:
        :param sort_by: name, id
        :param order_by:
        :return:
        """
        url = "{url}/v1/tariff/utilities/{utility_name}/bill_cycles/{bill_cycle_name}/bill_periods" \
            .format(url=self.tariff_url, utility_name=utility_name, bill_cycle_name=bill_cycle_name)
        params = {}
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if sort_by:
            params['sort_by'] = sort_by
        if order_by:
            params['order_by'] = order_by
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_tariffs(self, name=None, utility_name=None, status=None, vendor_id=None,
                    per_page=None, page=None, sort_by=None, order_by=ASC):
        """
        Returns tariffs from stem db
        :param name: name search for tariff matches
        :param utility_name:
        :param status: SIMULATION_GRADE, REVENUE_GRADE, PROPOSAL_GRADE
        :param vendor_id: GEN, STEM_DB, STEM_S3
        :param per_page:
        :param page:
        :param sort_by: name, utility_name, status
        :param order_by:
        :return:
        """
        url = "{url}/v1/tariff/tariffs".format(url=self.tariff_url)
        params = {}
        if name:
            params['name'] = name
        if utility_name:
            params['utility_name'] = utility_name
        if status:
            params['status'] = status
        if vendor_id:
            params['vendor_id'] = vendor_id
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        if sort_by:
            params['sort_by'] = sort_by
        if order_by:
            params['order_by'] = order_by
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_tariff(self, tariff_name):
        """
        Returns specified tariff
        :param tariff_name:
        :return:
        """
        url = "{url}/v1/tariff/tariffs/{tariff_name}".format(url=self.tariff_url, tariff_name=tariff_name)
        logger.info("invoking GET on url(%s)", url)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def get_tariff_revisions_by_id(self, tariff_id):
        """
        Returns revisions for tariff id (Legacy)
        :param tariff_id:
        :return:
        """
        url = "{url}/v1/tariff/{tariff_id}/revisions".format(url=self.tariff_url, tariff_id=tariff_id)
        logger.info("invoking GET on url(%s)", url)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def get_tariff_revisions(self, tariff_name):
        """
        Returns revisions for tariff
        :param tariff_name:
        :return:
        """
        url = "{url}/v1/tariff/tariffs/{tariff_name}/revisions".format(url=self.tariff_url, tariff_name=tariff_name)
        logger.info("invoking GET on url(%s)", url)
        response = requests.get(url, headers=self.headers)
        return response

    @authentication_required
    def get_tariff_daily_structure(self, tariff_id, resolution, start_date, end_date):
        """
        Returns daily tariff structure by specified resolution
        :param tariff_id:
        :param resolution: minutes, seconds
        :param start_date:
        :param end_date:
        :return:
        """
        url = "{url}/v1/tariff/{tariff_id}/structure/{resolution}".format(url=self.tariff_url,
                                                                          tariff_id=tariff_id,
                                                                          resolution=resolution)
        params = {}
        params['start_date'] = start_date
        params['end_date'] = end_date
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def get_tariff_umi(self, tariff_name, body):
        """
        Returns tariff rates per umi for given date range
        :param tariff_name:
        :param body: includes start_date, end_date, interval_len(default:900, app_criteria)
        :return:
        """
        url = "{url}/v1/tariff/tariffs/{tariff_name}/rates/umi".format(url=self.tariff_url, tariff_name=tariff_name)
        logger.info("invoking POST on url(%s) \nbody (%s)", url, pformat(body))
        response = requests.post(url, headers=self.headers, json=body)
        return response

    @authentication_required
    def get_utilities_from_provider(self, provider, name=None):
        """
        Returns utilities available from provider
        :param provider:
        :return:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/utilities".format(url=self.tariff_url, provider=provider)
        params = {}
        if name:
            params['name'] = name
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def import_utility(self, provider, provider_utility_id):
        """
        Import a utility from specified provider into stem db
        All calendars for the utility will also be imported
        :param provider:
        :return:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/utilities/{provider_utility_id}" \
            .format(url=self.tariff_url, provider=provider, provider_utility_id=provider_utility_id)
        logger.info("invoking POST on url(%s)", url)
        response = requests.post(url, headers=self.headers)
        return response

    @authentication_required
    def import_calendars_for_utility(self, provider, provider_utility_id):
        """
        Import holiday calendars from a provider into Stem database. Existing calendars will be updated.
        :param provider:
        :param provider_utility_id
        :return:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/utilities/{provider_utility_id}/calendars/Holiday" \
            .format(url=self.tariff_url, provider=provider, provider_utility_id=provider_utility_id)
        logger.info("invoking POST on url(%s)", url)
        response = requests.post(url, headers=self.headers)
        return response

    @authentication_required
    # TODO Cover by tests
    def import_billing_for_utility(self, provider, provider_utility_id):
        """
        Import billing from a provider into Stem database. Existing calendars will be updated.
        :param provider:
        :param provider_utility_id
        :return:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/utilities/{provider_utility_id}/calendars/Billing" \
            .format(url=self.tariff_url, provider=provider, provider_utility_id=provider_utility_id)
        logger.info("invoking POST on url(%s)", url)
        response = requests.post(url, headers=self.headers)
        return response

    @authentication_required
    def get_tariffs_from_provider(self, provider, provider_utility_id, name=None, provider_tariff_id=None,
                                  effective_on=None, per_page=None, page=None):
        """
        Returns tariffs from provider and utility that match criteria
        :param provider_utility_id:
        :param provider:
        :param name:
        :param provider_tariff_id
        :param effective_on:
        :param per_page:
        :param page:
        :return:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/utilities/{provider_utility_id}/tariffs" \
            .format(url=self.tariff_url, provider=provider, provider_utility_id=provider_utility_id)
        params = {}
        if name:
            params['name'] = name
        if provider_tariff_id:
            params['provider_tariff_id'] = provider_tariff_id
        if effective_on:
            params['effective_on'] = effective_on
        if per_page:
            params['per_page'] = per_page
        if page:
            params['page'] = page
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.get(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def import_tariff_all_versions(self, provider, provider_tariff_id, name=None, notes=None, job=True):
        """
        Imports all versions of tariff from provider
        :param provider:
        :param provider_tariff_id:
        :param name:
        :param notes:
        :param job:
        :return:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/tariffs/{provider_tariff_id}" \
            .format(url=self.tariff_url, provider=provider, provider_tariff_id=provider_tariff_id)
        params = {}
        if name:
            params['name'] = name
        if notes:
            params['notes'] = notes
        if job:
            params['job'] = job
        logger.info("invoking POST on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.post(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def import_tariff_versions(self, provider, provider_tariff_id, name=None, notes=None,
                               start_date=None, end_date=None, latest_version=None, job=True):
        """
        Imports specific (latest or within date range) tariff version(s)
        :param provider:
        :param provider_tariff_id:
        :param name:
        :param notes:
        :param start_date:
        :param end_date:
        :param latest_version:
        :param job:
        """
        url = "{url}/v1/tariff/import/providers/{provider}/tariffs/{provider_tariff_id}" \
            .format(url=self.tariff_url, provider=provider, provider_tariff_id=provider_tariff_id)
        params = {}
        if name:
            params['name'] = name
        if notes:
            params['notes'] = notes
        if start_date:
            params['start_date'] = start_date
        if end_date:
            params['end_date'] = end_date
        if latest_version:
            params['latest_version'] = latest_version
        if job:
            params['job'] = job
        logger.info("invoking PUT on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.put(url, headers=self.headers, params=params)
        return response

    @authentication_required
    def delete_genability_tariff(self, tariff_name, start_date=None, end_date=None, all_versions=True, force=True):
        """
        Deletes either all tariff versions or a subset(via dates)
        :param tariff_name:
        :param start_date:
        :param end_date:
        :param all_versions:
        :param force:
        :return:
        """
        url = "{url}/v1/tariff/tariffs/{tariff_name}".format(url=self.tariff_url, tariff_name=tariff_name)
        params = {}
        params['force'] = force
        if start_date and end_date:
            params['start_date'] = start_date
            params['end_date'] = end_date
        else:
            params['all_versions'] = all_versions
        logger.info("invoking DELETE on url(%s) \nparams (%s)", url, pformat(params))
        response = requests.delete(url, headers=self.headers, params=params)
        return response

    def import_tariff(self, provider=GENABILITY, provider_utility_id=False, tariff_id=False, master_only=True):
        """
        Aggregated Import tariffs method from Geneability
        :param provider: GENABILITY
        :param provider_utility_id: tariff related utility
        :param tariff_id: master tariff id
        :param master_only: if need to import only master tariff
        :return:
        """
        if provider_utility_id:
            logger.info('Import utility id "%s" from "Genability"', provider_utility_id)
            response = self.import_utility(provider=provider, provider_utility_id=provider_utility_id)
            assert response.status_code == 201, 'Differ status code in response:\n%s' % response.text
            logger.debug('Response:\n%s', json.dumps(response.json(), indent=2))

        if tariff_id:
            logger.info('Import tariff id "%s" from "Genability"', tariff_id)
            if not master_only:
                response = self.import_tariff_versions(provider=provider,
                                                       provider_tariff_id=tariff_id,
                                                       name='Tariff_id_{}'.format(tariff_id),
                                                       notes='Imported by Automation')
                logger.info('Tariff versions was not imported:\n %s\nMaster tariff will be imported', response.text)
            if master_only or response.status_code == 400:
                response = self.import_tariff_all_versions(provider=provider, provider_tariff_id=tariff_id)
                if response.status_code == 400:
                    logger.debug(response.text)
                    return
            job_id = response.json()['data']['resource_name']
            job_status = self.wait_job_completion(job_id=job_id, timeout=900, idle_time=5)
            logger.info('Tariff import job %s status: %s', job_id, job_status)

import json
import logging
from pprint import pformat

import requests

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class DeveloperAPIClient(StemClient):
    """
    Client used to interact with developer API, does not require authentication
    see: https://stemedge.atlassian.net/wiki/display/APP/Developer+Data+APIs#space-menu-link-content
    """

    def get_clients(self, api_key=None, params=None):
        """
        External URL- Returns list of clients to which user has access
        :param api_key:
        :param params:
        :return:
        """
        url = "%s/api/v1/clients/" % (self.url)
        if api_key:
            if params:
                logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
                response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, params=params,
                                        verify=False)
            else:
                logger.info("invoking url(%s)", url)
                response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False)
        else:
            logger.info("invoking url(%s)", url)
            response = requests.get(url=url, verify=False)
        logger.debug('Response code: %s Result: %s', response.status_code, json.dumps(response.json(), indent=4))
        return response

    def get_client(self, client, api_key):
        """
        External URL- Returns clients metadata to which user has access
        :param client:
        :param api_key:
        :return:
        """
        url = "%s/api/v1/clients/%s/" % (self.url, client)
        logger.info("invoking url(%s)", url)
        response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False)
        logger.debug('Response code: %s Result: %s', response.status_code, json.dumps(response.json(), indent=4))
        return response

    def get_sites(self, client, api_key=None, params=None):
        """
        External URL- Returns list of sites('LOCATIONS' in STEM speak) for this user
        :param client:
        :param api_key:
        :param params:
        :return:
        """
        url = "%s/api/v1/clients/%s/sites/" % (self.url, client)
        if api_key:
            if params:
                logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
                response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key},
                                        params=params, verify=False)
            else:
                logger.info("invoking url(%s)", url)
                response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False)
        else:
            logger.info("invoking url(%s)", url)
            response = requests.get(url=url, verify=False)
        logger.debug('Response code: %s Result: %s', response.status_code, json.dumps(response.json(), indent=4))
        return response

    def put_sites(self, client, api_key):
        """
        External URL- Invokes HTTP PUT on API endpt
        :param client:
        :param api_key:
        :return:
        """
        url = "%s/api/v1/clients/%s/sites/" % (self.url, client)
        logger.info("invoking url(%s)", url)
        response = requests.put(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False)
        return response

    def get_site(self, api_key, site_id):
        """
        External URL- Returns metadata for a specific site('LOCATION' in STEM speak) for this user
        :param api_key:
        :param site_id:
        :return:
        """
        url = "%s/api/v1/sites/%s" % (self.url, site_id)
        logger.info("invoking url(%s)", url)
        response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False)
        logger.debug('Response code: %s Result: %s', response.status_code, json.dumps(response.json(), indent=4))
        return response

    def get_streams(self, api_key, site_id, params):
        """
        External URL- Returns stream for specified client
        :param api_key:
        :param site_id:
        :param params:
        :return:
        """
        url = "%s/api/v1/sites/%s/streams/" % (self.url, site_id)
        if 'stream_type' in params:
            params['stream_type'] = json.dumps(params['stream_type'])
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
        response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False, params=params)
        return response

    def get_stream(self, api_key, site_id, stream_id):
        """
        External URL- Returns stream for specified client
        :param api_key:
        :param site_id:
        :param params:
        :return:
        """
        url = "%s/api/v1/sites/%s/streams/%s/" % (self.url, site_id, stream_id)
        logger.info("invoking url(%s)", url)
        response = requests.get(url=url, headers={'Authorization': 'APIKEY %s' % api_key}, verify=False)
        return response

    def get_status(self):
        """
        Internal URL- Returns api status
        :param location_code:
        :return:
        """
        url = "%s/api/v1/status/" % (self.url)
        logger.info("invoking url(%s)", url)
        return self.get(url)

    def get_api_uid(self, location_code):
        """
        Internal URL- Performs lookup for api_uid from location_code
        :param location_code:
        :return:
        """
        url = "%s/api/v1/get_api_uid_for_location_code/" % (self.url)
        params = {'location_code': location_code}
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
        response = self.get(url=url, params=params)
        return response

    def get_clients_internal(self, params=None):
        """
        Internal URL- Returns all locations to which user has access
        :param api_key:
        :param location_code:
        :return:
        """

        url = "%s/api/v1/locations/clients/" % (self.url)
        if params is not None:
            logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
            response = self.get(url=url, params=params)
        else:
            logger.info("invoking url(%s)", url)
            response = self.get(url)
        logger.debug('Result: %s', json.dumps(response.json(), indent=4))
        return response

    def get_client_internal(self, location_code):
        """
        Internal URL- Returns client metadata
        :param location_code:
        :return:
        """
        url = "%s/api/v1/locations/clients/%s/" % (self.url, location_code)
        logger.info("invoking url(%s)", url)
        return self.get(url)

    def get_client_sites_internal(self, location_code, params=None):
        """
        Internal URL- Returns all locations under given client to which user has access
        :param location_code:
        :param params:
        :return:
        """
        url = "%s/api/v1/locations/clients/%s/sites/" % (self.url, location_code)
        if params is not None:
            logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
            response = self.get(url=url, params=params)
        else:
            logger.info("invoking url(%s)", url)
            response = self.get(url)
        return response

    def get_client_site_internal(self, location_code):
        """
        Internal URL- Returns location metadata
        :param location_code:
        :return:
        """
        url = "%s/api/v1/locations/sites/%s/" % (self.url, location_code)
        logger.info("invoking url(%s)", url)
        return self.get(url)

    def get_client_site_streams_internal(self, location_code, params):
        """
        Internal URL- Returns location stream data for location_code specified
        :param location_code:
        :return:
        """
        url = "%s/api/v1/locations/sites/%s/streams/" % (self.url, location_code)
        if 'stream_type' in params:
            params['stream_type'] = json.dumps(params['stream_type'])
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
        return self.get(url, params=params)

    def get_clients_internal_non_authenticated(self, params=None):
        """
        Internal URL- Returns all locations to which user has access
        :param api_key:
        :param location_code:
        :return:
        """

        url = "%s/api/v1/locations/clients/" % (self.url)
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(params))
        response = self.get(url=url)
        return response

import json
import logging

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class PowerConfigClient(StemClient):

    def deploy_emulator_to_host(self, location_code, emulator_name, batterysoc=None, setpoint=None):
        """
        Deploys specified emulator to specified location code
        :param location_code:
        :param emulator_name:
        :param batterysoc:
        :param setpoint:
        :return:
        """
        url = "%s/%s/%s/" % (self.url, "powerconfig/deploy_emulator_to_host", location_code)
        params = {
            'use_api_contract': True
        }
        data = {
            "emulator_hostname": emulator_name
        }

        if batterysoc is not None:
            data['batterysoc'] = batterysoc
        if setpoint is not None:
            data['setpoint'] = setpoint
        logger.info("invoking url(%s) with data (%s)", url, json.dumps(data, indent=2))
        response = self.post(url, params=params, data=data)
        logger.info("Response: %s", json.dumps(response.json(), indent=2))
        return response

    def update_powerstore_configuration(self, powerstore_hostname, sample_data_monitor_id, ground_hog_day_mode=False,
                                        sample_start_date=None, pause_when_no_new_data=False,
                                        sample_end_date=None, real_time_speed=False,
                                        replay_from_db=True):
        """
        Instructs the emulator('powerstore_hostname') to use sample data from monitor('sample_data_monitor_id')
        :param powerstore_hostname: target emulator for meter data
        :param sample_data_monitor_id: source for meter data
        :param ground_hog_day_mode: uses meter day from current datetime and replays for 24hrs
        :param sample_start_date: start of range for meter data
        :param sample_end_date: end of range for meter data
        :param pause_when_no_new_data:
        :param real_time_speed:
        :param replay_from_db:
        :return:
        """
        url = "%s/%s/" % (self.url, "powerconfig/update_powerstore_configuration")
        data = {
            'ground_hog_day_mode': ground_hog_day_mode,
            'pause_when_no_new_data': pause_when_no_new_data,
            'sample_data_monitor_id': sample_data_monitor_id,
            'real_time_speed': real_time_speed,
            'powerstore_hostname': powerstore_hostname,
            'replay_from_db': replay_from_db
        }
        if sample_start_date is not None:
            data['sample_start_date'] = sample_start_date
        if sample_end_date is not None:
            data['sample_end_date'] = sample_end_date
        logger.info("invoking url(%s) with data (%s)", url, json.dumps(data, indent=2))
        response = self.post(url, data=data)
        logger.info("Response: %s", json.dumps(response.json(), indent=2))
        return response

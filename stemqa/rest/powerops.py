import logging

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class PowerOpsClient(StemClient):

    def get_site_list(self, client_code, timestamp):
        url = "%s/%s/%s/" % (self.url, "powerops/get_site_list", client_code)
        data = {"timestamp": timestamp}

        logger.info("invoking url(%s)", url)
        response = self.get(url, params=data)
        return response

    def get_site_rollup_html_to_recache(self, location_code, timestamp):
        url = "%s/%s/%s/" % (self.url, "powerops/siterollup", location_code)
        data = {"timestamp": timestamp}

        logger.info("invoking url(%s)", url)
        response = self.get(url, params=data)
        return response

    def get_fleet_data(self):
        url = "%s/%s/" % (self.url, "powerops/fleet_data")
        logger.info("invoking url(%s)", url)
        response = self.get(url)
        return response

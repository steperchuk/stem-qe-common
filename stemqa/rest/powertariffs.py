import json
import logging
from pprint import pformat

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)


class TariffClient(StemClient):
    """
    Client for tariff related operations
    """

    def find_vendor_utilities_by_name(self, search_name, vendor_code='GEN'):
        """
        Returns list of utilities, by default we return ones found in Genability('GEN')
        :param search_name:
        :param vendor_code:
        :return:
        """
        url = "%s/%s" % (self.url, "tariff_mask_document/find_vendor_utilities_by_name/?")
        data = {
            'name': search_name,
            'vendor_code': vendor_code
        }

        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.get(url, params=data)
        return response

    def create_tariff_with_vendor_mapping(self, utility_id, name, description, display_name,
                                          tariff_id_vendor, version_id_vendor, applicability_keys=None,
                                          tariff_id=''):
        """
        Creates tariff in our system from genability vendor
        :param utility_id: vendor utility id, i.e. Genability PG&E id is 734
        :param name:
        :param description:
        :param display_name:
        :param tariff_id_vendor: vendor tariff id, i.e. Genability E19S tariff is ID 81959
        :param version_id_vendor: version of tariff
        :param applicability_keys: any keys required to specify rate levels
        :param tariff_id: our tariff id
        :return:
        """
        url = "%s/%s" % (self.url, "tariff_mask_document/create_tariff_with_vendor_mapping/")
        data = {
            "utility_id": utility_id,
            "tariff_id_vendor": tariff_id_vendor,
            "tariff_id": tariff_id,
            "version_id_vendor": version_id_vendor,
            "name": name,
            "description": description,
            "display_name": display_name,
        }
        if applicability_keys:
            data["applicabilitykeys"] = json.dumps(applicability_keys)
        logger.info("invoking url(%s) \ndata (%s)", url, pformat(data))
        response = self.post(url, data=data)
        return response

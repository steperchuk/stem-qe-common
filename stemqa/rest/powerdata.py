import json
import logging
from pprint import pformat

from stemqa.rest.stembase import StemClient

logger = logging.getLogger(__name__)
STATUS_TYPE_PENDING = 'pending'
STATUS_TYPE_PROCESSED = 'processed'
COST_EXPLORER = 'COST_EXPLORER'


class PowerDataClient(StemClient):
    """
    Client which interacts with powerdata api used for retrieving third party interval data
    """

    def get_vendors(self):
        """
        Returns list of third party vendors
        :return:
        """
        url = "%s/%s/" % (self.url, "powersales/dataconnect/vendors")
        logger.info("invoking url(%s)", url)
        response = self.get(url)
        return response

    def get_vendor(self, vendor_code):
        """
        Returns metadata for third party vendor
        :param vendor_code:
        :return:
        """
        url = "%s/%s/%s" % (self.url, "powersales/dataconnect/vendors", vendor_code)
        logger.info("invoking url(%s)", url)
        response = self.get(url)
        return response

    def get_vendor_sites(self, vendor_code, **kwargs):
        """
        Returns list of available (not yet connected) sites from third party vendor
        which have been authorized for data import
        :param vendor_code:
        :param kwargs:
        :return:
        """
        params = {}
        if 'page' in kwargs:
            params['page'] = kwargs['page']
        if 'per_page' in kwargs:
            params['per_page'] = kwargs['per_page']
        if 'uid' in kwargs:
            params['uid'] = kwargs['uid']
        if 'email' in kwargs:
            params['email'] = kwargs['email']
        if 'said' in kwargs:
            params['said'] = kwargs['said']
        if 'address' in kwargs:
            params['address'] = kwargs['address']
        if 'status' in kwargs:
            params['status'] = kwargs['status']
        url = "%s/%s/%s/%s" % (self.url, "powersales/dataconnect/vendors", vendor_code, "sites")
        logger.info("invoking url(%s) \nparams (%s)", url, pformat(params))
        if params:
            response = self.get(url, params=params)
        else:
            response = self.get(url)
        return response

    def get_stem_locations(self, **kwargs):
        """
        Returns search results for internal STEM site search by address
        :param kwargs:
        :return:
        """
        url = "%s/%s" % (self.url, "powersales/dataconnect/search_locations")
        params = {}
        if 'vendor_code' in kwargs:
            params['vendor_code'] = kwargs['vendor_code']
        if 'filter_by' in kwargs:
            params['filter_by'] = kwargs['filter_by']
        if 'data_quality' in kwargs:
            params['data_quality'] = kwargs['data_quality']
        if 'page' in kwargs:
            params['page'] = kwargs['page']
        if 'per_page' in kwargs:
            params['per_page'] = kwargs['per_page']
        logger.info("invoking url(%s) \nparams (%s)", url, pformat(params))
        response = self.get(url, params=params)
        return response

    def create_streams(self, vendor_code, location_code_site, vendor_site_id,
                       data_quality='REVENUE', req_start_datetime=None, req_end_datetime=None):
        """
        Creates new entry establishing connection between UtilityAPI account and STEM account
        :param vendor_code:
        :param location_code_site:  STEM location code for site
        :param vendor_site_id:  UtilityAPI- meter.uid or vendor.vendor_site_id
        :param data_quality: REVENUE
        :param req_end_datetime: None for ongoing pull, specify date for historical pull only
        :param req_start_datetime:
        :return:
        """
        url = "%s/%s" % (self.url, "powersales/dataconnect/streams")
        data = {}
        data["location_code_site"] = location_code_site
        data["vendor_code"] = vendor_code
        data["vendor_site_id"] = vendor_site_id
        data["data_quality"] = data_quality
        data["req_end_datetime"] = req_end_datetime
        data["req_start_datetime"] = req_start_datetime
        logger.info("invoking POST on url(%s) data(%s)", url, data)
        headers = {'content-type': 'application/json'}
        response = self.post(url, data=json.dumps(data), headers=headers)
        return response

    def fetch_stream(self, stream_id):
        """
        Requests external data pull for specified stream
        :param stream_id:
        :return:
        """
        url = "%s/%s/%s/%s" % (self.url, "powersales/dataconnect/streams", stream_id, "fetch")
        logger.info("invoking POST on url(%s)", url)
        response = self.post(url)
        return response

    def get_stream_data(self, stream_id, start_datetime=None, end_datetime=None):
        """
        Returns data for specified stream from STEM cloud
        :param stream_id:
        :param start_datetime:
        :param end_datetime:
        :return:
        """
        url = "%s/%s/%s/%s" % (self.url, "powersales/dataconnect/streams", stream_id, "data")
        data = {}
        if start_datetime and end_datetime:
            data['start_datetime'] = start_datetime
            data['end_datetime'] = end_datetime
        logger.info("invoking GET on url(%s) with \n%s", url, pformat(data))
        response = self.get(url)
        return response

    def get_stream(self, stream_id):
        """
        Returns metatdata about specific connected site/stream
        :param stream_id:
        :return:
        """
        url = "%s/%s/%s" % (self.url, "powersales/dataconnect/streams", stream_id)
        logger.info("invoking GET on url(%s)", url)
        response = self.get(url)
        return response

    def get_streams(self, **kwargs):
        """
        Returns metatdata about all connected stream which satisfy filter(kwargs)
        :param stream_id:
        :return:
        """
        url = "%s/%s" % (self.url, "powersales/dataconnect/streams")
        params = {}
        if 'vendor_code' in kwargs:
            params['vendor_code'] = kwargs['vendor_code']
        if 'location_name' in kwargs:
            params['location_name'] = kwargs['location_name']
        if 'address' in kwargs:
            params['address'] = kwargs['address']
        if 'status' in kwargs:
            params['status'] = kwargs['status']
        if 'page' in kwargs:
            params['page'] = kwargs['page']
        if 'per_page' in kwargs:
            params['per_page'] = kwargs['per_page']
        logger.info("invoking GET on url(%s) \nparams (%s)", url, pformat(params))
        response = self.get(url, params=params)
        return response

    def delete_stream(self, stream_id):
        """
        Deletes connected site/stream
        :param stream_id:
        :return:
        """
        url = "%s/%s/%s/" % (self.url, "powersales/dataconnect/streams", stream_id)
        params = {}
        params['force'] = 'true'
        logger.info("invoking DELETE on url(%s) params(%s)", url, params)
        response = self.delete(url, params=params)
        return response

    def delete_s3_stream_data(self, status_type, location_code_site):
        """
        Deletes data from s3 datastore
        :param status_type:
        :param location_code_site:
        :return:
        """
        url = "%s/%s/%s/%s/" % (self.url, "powersales/dataconnect/imports", status_type, location_code_site)
        params = {}
        params['force'] = 'true'
        logger.info("invoking DELETE on url(%s) params(%s)", url, params)
        response = self.delete(url, params=params)
        return response

    def pull_data_by_location_code(self, location_code, app_location_code, start_date_time, end_date_time,
                                   resolution, separate_data_by_day, return_dataframe, start_times, format_for_charts,
                                   module):
        """
        Pulls existing 15min interval data for given location_code
        :param location_code:
        :param app_location_code:
        :param start_date_time:
        :param end_date_time:
        :param resolution:
        :param separate_data_by_day:
        :param return_dataframe:
        :param start_times:
        :param format_for_charts:
        :param module:
        :return:
        """
        url = "%s/%s/?" % (self.url, "powerdata/pull_data_by_location_code")
        params = {}
        params['location_code'] = location_code
        params['app_location_code'] = app_location_code
        params['start_date_time'] = start_date_time
        params['end_date_time'] = end_date_time
        params['resolution'] = resolution
        params['separate_data_by_day'] = separate_data_by_day
        params['return_dataframe'] = return_dataframe
        params['start_times'] = start_times
        params['format_for_charts'] = format_for_charts
        params['module'] = module
        logger.info("invoking GET on url(%s) params(%s)", url, params)
        response = self.get(url, params=params)
        return response
